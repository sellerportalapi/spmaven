import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.TableLike;
import org.jooq.impl.DSL;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.api.v1.usercontainer.dtos.UserDto;

public class Main {

	public final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	public final static String JDBC_DB_URL = "jdbc:mysql://localhost:3306/sellerportal";

	// Database Connection Properties
	public final static String JDBC_USER = "root";
	public final static String JDBC_PASS = "root";

	public static void main(String[] args) {

		try {
			String json = new ObjectMapper().findAndRegisterModules().writeValueAsString(new UserDto());
			System.out.println(json);
			Class.forName(JDBC_DRIVER);

			Result<Record> result = null;
			try (Connection connection = DriverManager.getConnection(JDBC_DB_URL, JDBC_USER, JDBC_PASS)) {

				PreparedStatement ps = connection.prepareStatement("SELECT * FROM vertadmin" + " WHERE userName = ?");
				ps.setString(1, "' OR '1' = '1");
				System.out.println(ps.executeQuery());
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					System.out.println(rs.getString(2));
				}
				try (DSLContext create = DSL.using(connection, SQLDialect.MYSQL)) {
//					result = create.select().from("vertadmin").where("userName = '' OR '1'='1'").fetch();
				}

			}

//			for (Record record : result) {
//				System.out.println(record.getValue(1));
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
