package com.test.api.v1.usercontainer.service.impls;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.test.api.v1.usercontainer.assemblers.UserAssembler;
import com.test.api.v1.usercontainer.dao.daos.UserDao;
import com.test.api.v1.usercontainer.dao.factories.UserDaoFactory;
import com.test.api.v1.usercontainer.dtos.UserDto;
import com.test.api.v1.usercontainer.entities.User;
import com.test.api.v1.usercontainer.resources.ServiceLayerArguments;
import com.test.api.v1.usercontainer.service.services.UserService;

public class UserServiceImpl implements UserService {

	private UserDao userDao;

	public UserServiceImpl() {
		userDao = UserDaoFactory.getDao();
	}

	@Override
	public Response add(UserDto dto, ServiceLayerArguments arguments) {

		ContainerRequestContext context = arguments.getContainerRequestContext();

		User user = UserAssembler.userDtoToEntity(dto);
		int userId = userDao.create(user);

		URI uri = context.getUriInfo().getAbsolutePathBuilder().path(String.valueOf(userId)).build();
		return Response.created(uri).build();
	}

	@Override
	public Response update(Integer key, UserDto dto, ServiceLayerArguments arguments) {
		User user = UserAssembler.userDtoToEntity(dto);

		if (userDao.update(key, user)) {
			return Response.ok().build();
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	@Override
	public Response remove(Integer key, ServiceLayerArguments arguments) {

		if (userDao.delete(key)) {
			return Response.noContent().build();
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	@Override
	public Response get(Integer key, ServiceLayerArguments arguments) {
		User user = userDao.retrieve(key);

		if (user != null) {
			UserDto dto = UserAssembler.userEntityToDto(user);
			return Response.ok(dto).build();
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	@Override
	public Response getAll(ServiceLayerArguments arguments) {
		List<User> users = userDao.retrieveAll();

		if (users != null) {
			List<UserDto> userDtos = new ArrayList<>();
			for (User user : users)
				userDtos.add(UserAssembler.userEntityToDto(user));

			return Response.ok(userDtos).build();
		} else {
			return Response.noContent().build();
		}
	}

	@Override
	public UserDto findByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}
}
