package com.test.api.v1.usercontainer.assemblers;

import com.test.api.v1.usercontainer.dtos.UserDto;
import com.test.api.v1.usercontainer.entities.User;

public class UserAssembler {

	public static User userDtoToEntity(UserDto userDto) {
		User user = new User();
		user.setId(userDto.getUserId());
		user.setUsername(userDto.getUsername());
		user.setFullname(userDto.getName());
		user.setEmail(userDto.getEmail());
		user.setSalary(userDto.getSalary());
		user.setBirthdate(new java.sql.Date(userDto.getBirthdate().getTime()));

		return user;
	}

	public static UserDto userEntityToDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setUserId(user.getId());
		userDto.setUsername(user.getUsername());
		userDto.setName(user.getFullname());
		userDto.setEmail(user.getEmail());
		userDto.setSalary(user.getSalary());
		userDto.setBirthdate(new java.util.Date(user.getBirthdate().getTime()));

		return userDto;
	}
}
