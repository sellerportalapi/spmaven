package com.test.api.v1.usercontainer.resources;
import javax.ws.rs.container.ContainerRequestContext;

public class ServiceLayerArguments {

	private ContainerRequestContext containerRequestContext;

	public ContainerRequestContext getContainerRequestContext() {
		return containerRequestContext;
	}

	public void setContainerRequestContext(ContainerRequestContext containerRequestContext) {
		this.containerRequestContext = containerRequestContext;
	}

	public static class Builder {

		private ServiceLayerArguments arguments;

		public Builder() {
			arguments = new ServiceLayerArguments();
		}

		public ServiceLayerArguments containerRequestContext(ContainerRequestContext containerRequestContext) {
			arguments.setContainerRequestContext(containerRequestContext);
			return arguments;
		}
	}
}
