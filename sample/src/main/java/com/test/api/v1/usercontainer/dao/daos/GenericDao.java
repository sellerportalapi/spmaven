package com.test.api.v1.usercontainer.dao.daos;

import java.util.List;

public interface GenericDao<E, K> {

	K create(E entity);

	boolean update(K key, E entity);

	boolean delete(K key);

	E retrieve(K key);

	List<E> retrieveAll();

}
