package com.test.api.v1.usercontainer.dao.factories;

import com.test.api.v1.usercontainer.dao.daos.UserDao;
import com.test.api.v1.usercontainer.dao.impls.UserDaoImpl;

public class UserDaoFactory {

	private static final UserDao dao = new UserDaoImpl();

	public static UserDao getDao() {
		return dao;
	}
}
