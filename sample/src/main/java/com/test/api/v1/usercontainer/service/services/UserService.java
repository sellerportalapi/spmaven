package com.test.api.v1.usercontainer.service.services;

import com.test.api.v1.usercontainer.dtos.UserDto;

public interface UserService extends GenericService<UserDto, Integer> {

	UserDto findByUsername(String username);
}
