package com.test.api.v1.usercontainer.service.factories;

import com.test.api.v1.usercontainer.service.impls.UserServiceImpl;
import com.test.api.v1.usercontainer.service.services.UserService;

public class UserServiceFactory {

	private static final UserService service = new UserServiceImpl();

	public static UserService getService() {
		return service;
	}
}
