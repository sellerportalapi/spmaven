package com.test.api.v1.usercontainer.dao.impls;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.test.api.v1.usercontainer.dao.daos.UserDao;
import com.test.api.v1.usercontainer.entities.User;

@SuppressWarnings("deprecation")
public class UserDaoImpl implements UserDao {

	private AtomicInteger id = new AtomicInteger(3);

	private static List<User> userMemPool = new ArrayList<>();
	static {
		User user1 = new User(1, "karthick", "Guru Karthick", "gchellamuthu@projectverte.com", 10_00_000L,
				new Date(1996, 5, 31));
		userMemPool.add(user1);

		User user2 = new User(2, "sameer", "Sameer", "ssameer@projectverte.com", 15_00_000L, new Date(1996, 11, 11));
		userMemPool.add(user2);
	}

	@Override
	public Integer create(User entity) {
		entity.setId(id.getAndIncrement());
		userMemPool.add(entity);

		return entity.getId();
	}

	@Override
	public boolean update(Integer key, User entity) {
		for (User user : userMemPool) {
			if (user.getId() == key) {
				entity.setId(key);
				userMemPool.set(key.intValue(), entity);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean delete(Integer key) {
		for (User user : userMemPool) {
			if (user.getId() == key) {
				userMemPool.remove(key.intValue());
				return true;
			}
		}
		return false;
	}

	@Override
	public User retrieve(Integer key) {
		for (User user : userMemPool) {
			if (user.getId() == key) {
				return user;
			}
		}
		return null;
	}

	@Override
	public List<User> retrieveAll() {
		return userMemPool;
	}

	@Override
	public User findByUsername(String username) {
		for (User user : userMemPool) {
			if (user.getUsername().equals(username)) {
				return user;
			}
		}
		return null;
	}
}
