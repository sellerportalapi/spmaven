package com.test.api.v1.usercontainer;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

import com.test.api.v1.usercontainer.resources.UserApi;

@ApplicationPath("test")
public class SampleApp extends ResourceConfig {

	public SampleApp() {
		System.out.println("Registering the user resources");
		register(UserApi.class);
	}
}
