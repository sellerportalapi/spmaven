package com.test.api.v1.usercontainer.resources;

import java.lang.reflect.InvocationTargetException;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.test.api.v1.usercontainer.dtos.UserDto;
import com.test.api.v1.usercontainer.service.factories.UserServiceFactory;
import com.test.api.v1.usercontainer.service.services.UserService;

@Path("users")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserApi {

	private UserService service;

	public UserApi() {
		service = UserServiceFactory.getService();
	}

	@POST
	public Response createUser(UserDto userDto, @Context ContainerRequestContext context) {
		ServiceLayerArguments arguments = new ServiceLayerArguments.Builder().containerRequestContext(context);

		return service.add(userDto, arguments);
	}

	@Path("/{id}")
	@PUT
	public Response updateUser(UserDto userDto, @PathParam("id") int id) {
		ServiceLayerArguments arguments = new ServiceLayerArguments();

		return service.update(id, userDto, arguments);
	}

	@Path("/{id}")
	@DELETE
	public Response deleteUser(@PathParam("id") int id) {
		ServiceLayerArguments arguments = new ServiceLayerArguments();

		return service.remove(id, arguments);
	}

	@Path("/{id}")
	@GET
	public Response get(@PathParam("id") int id) {
		ServiceLayerArguments arguments = new ServiceLayerArguments();

		return service.get(id, arguments);
	}

	@GET
	public Response getAll() {
		ServiceLayerArguments arguments = new ServiceLayerArguments();

		return service.getAll(arguments);
	}

	@Path("getall")
	@GET
	public Response getAll1() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		long start, end;
//		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
//		Validator validator = factory.getValidator();
//		
		start = System.currentTimeMillis();
//
//		Set<ConstraintViolation<UserDto>> violations = validator.validate(userDto);
//
//		for (ConstraintViolation<UserDto> val : violations) {
//			System.out.println("" + val.getMessage());
//			System.out.println("" + val.getMessageTemplate());
//			System.out.println("" + val.getConstraintDescriptor());
//			System.out.println("" + val.getExecutableParameters());
//			System.out.println("" + val.getExecutableReturnValue());
//			System.out.println("" + val.getInvalidValue());
//			System.out.println("" + val.getLeafBean());
//			System.out.println("" + val.getPropertyPath());
//			System.out.println("" + val.getRootBean());
//			System.out.println("" + val.getRootBeanClass());
//			System.out.println("" + val.hashCode());
//			System.out.println("");
//		}
//		System.out.println(violations.size());

//		Service service = ServiceFactory.getService();
//
//		((ServiceImpl) service).test123();
//
//		for (Annotation annotation : UserDto.class.getAnnotations()) {
//			Class<? extends Annotation> type = annotation.annotationType();
//			System.out.println("Values of " + type.getName());
//
//			for (Method method : type.getDeclaredMethods()) {
//				Object value = method.invoke(annotation, (Object[]) null);
//				System.out.println(" " + method.getName() + ": " + value);
//			}
//		}

		end = System.currentTimeMillis();
		System.out.println(end - start);
//		factory.close();

		ServiceLayerArguments arguments = new ServiceLayerArguments.Builder().containerRequestContext(null);

		return UserServiceFactory.getService().getAll(arguments);
//		return Response.ok(userDto).build();
	}

}
