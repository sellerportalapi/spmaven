package com.test.api.v1.usercontainer.dao.daos;

import com.test.api.v1.usercontainer.entities.User;

public interface UserDao extends GenericDao<User, Integer> {

	User findByUsername(String username);
}
