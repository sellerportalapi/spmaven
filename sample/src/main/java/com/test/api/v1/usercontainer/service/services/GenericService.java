package com.test.api.v1.usercontainer.service.services;

import javax.ws.rs.core.Response;

import com.test.api.v1.usercontainer.resources.ServiceLayerArguments;

public interface GenericService<E, K> {

	Response add(E dto, ServiceLayerArguments arguments);

	Response update(K key, E dto, ServiceLayerArguments arguments);

	Response remove(K key, ServiceLayerArguments arguments);

	Response get(K key, ServiceLayerArguments arguments);

	Response getAll(ServiceLayerArguments arguments);

}
