
INSERT INTO `vertadmin` VALUES (1,'admin','oNed0pBaaW1ppiMVOcC+l9J0nqCxipnwqDTCEpnBs/44OfQ8GTd7XKZR+WrcA+LMhXuTfg==',
'Administrator','vertadmin@projectverte.com','9999888776','VERTADMIN','ACTIVE',1,0,1,now(),now(),'vert',now(),'vert',now());

INSERT INTO vertrecentpassword (userId, password1, password2, password3)
SELECT userId, password , password, password
FROM vertadmin ORDER BY userId ASC;

INSERT INTO vertlogindetails (userId, failedLoginCount, lastLoginAttemptAt, lastLoginAt, lastLoginFailedAt, accountLockedAt)
SELECT userId, 0, NULL, NULL, NULL, NULL
FROM vertadmin ORDER BY userId ASC;

UPDATE vertlogindetails T1 JOIN vertadmin T2 ON T1.userId = T2.userId
SET lastLoginAttemptAt = T2.tokenIssuedAt, lastLoginAt = T2.tokenIssuedAt
WHERE T2.tokenIssuedAt <> 0;

/*
DELETE FROM vertlogindetails WHERE userId = 1;
DELETE FROM vertrecentpassword WHERE userId = 1;
DELETE FROM vertadmin WHERE userId = 1;
ALTER TABLE vertadmin AUTO_INCREMENT = 1;
*/