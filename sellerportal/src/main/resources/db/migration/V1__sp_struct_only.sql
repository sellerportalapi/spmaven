-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: sellerportal
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apicontrol`
--

DROP TABLE IF EXISTS `apicontrol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apicontrol` (
  `apiId` int(10) unsigned NOT NULL,
  `featureId` int(10) unsigned NOT NULL,
  `subfeatureId` int(10) unsigned DEFAULT NULL,
  `reqFeatureVal` tinyint(3) unsigned NOT NULL,
  `reqSubfeatureVal` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`apiId`,`featureId`),
  UNIQUE KEY `apiId` (`apiId`,`featureId`,`subfeatureId`),
  KEY `featureId` (`featureId`),
  KEY `subfeatureId` (`subfeatureId`),
  CONSTRAINT `apicontrol_ibfk_1` FOREIGN KEY (`apiId`) REFERENCES `apilist` (`apiId`),
  CONSTRAINT `apicontrol_ibfk_2` FOREIGN KEY (`featureId`) REFERENCES `features` (`id`),
  CONSTRAINT `apicontrol_ibfk_3` FOREIGN KEY (`subfeatureId`) REFERENCES `subfeatures` (`subfeatureId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `apilist`
--

DROP TABLE IF EXISTS `apilist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apilist` (
  `apiId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `methodName` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `className` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`apiId`),
  UNIQUE KEY `apiId` (`apiId`),
  UNIQUE KEY `methodName` (`methodName`,`className`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asn`
--

DROP TABLE IF EXISTS `asn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asn` (
  `asnId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asnNumber` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `sellerId` int(10) unsigned NOT NULL,
  `sellerName` varchar(64) DEFAULT NULL,
  `sellerEmail` varchar(80) DEFAULT NULL,
  `sellerPhone` varchar(16) DEFAULT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `asnStatus` varchar(16) NOT NULL DEFAULT 'Incomplete',
  `asnType` varchar(16) NOT NULL,
  `shipmentType` varchar(16) NOT NULL,
  `deliveryFacility` varchar(64) DEFAULT NULL,
  `referenceNumber` varchar(64) DEFAULT NULL,
  `bolNumber` varchar(64) DEFAULT NULL,
  `carrier` varchar(128) DEFAULT NULL,
  `trackingNumber` varchar(64) DEFAULT NULL,
  `shipmentDate` timestamp NULL DEFAULT NULL,
  `estimatedArrival` timestamp NULL DEFAULT NULL,
  `shipmentFrom` varchar(1024) DEFAULT NULL,
  `cogi` float unsigned DEFAULT NULL,
  `additionalNotes` varchar(1024) DEFAULT NULL,
  `asnDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `modifiedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`asnId`),
  UNIQUE KEY `asnNumber` (`asnNumber`),
  KEY `sellerId` (`sellerId`,`storeId`),
  CONSTRAINT `asn_ibfk_1` FOREIGN KEY (`sellerId`, `storeId`) REFERENCES `storeusermapper` (`sellerId`, `storeId`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asncaselevel`
--

DROP TABLE IF EXISTS `asncaselevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asncaselevel` (
  `caseId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asnId` int(10) unsigned NOT NULL,
  `palletId` int(10) unsigned DEFAULT NULL,
  `caseLpn` varchar(64) NOT NULL,
  `caseLength` float unsigned DEFAULT NULL,
  `caseWidth` float unsigned DEFAULT NULL,
  `caseHeight` float unsigned DEFAULT NULL,
  `caseDimensionUOM` enum('INCH') DEFAULT 'INCH',
  `caseWeight` float unsigned DEFAULT NULL,
  `caseWeightUOM` enum('POUND') DEFAULT 'POUND',
  `specialTreatmentNeeded` varchar(3) DEFAULT NULL,
  `isMaterialHazardous` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`caseId`),
  UNIQUE KEY `asnId` (`asnId`,`caseLpn`),
  KEY `palletId` (`palletId`),
  CONSTRAINT `asncaselevel_ibfk_1` FOREIGN KEY (`asnId`) REFERENCES `asn` (`asnId`),
  CONSTRAINT `asncaselevel_ibfk_2` FOREIGN KEY (`palletId`) REFERENCES `asnpalletlevel` (`palletId`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asnpalletlevel`
--

DROP TABLE IF EXISTS `asnpalletlevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asnpalletlevel` (
  `palletId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asnId` int(10) unsigned NOT NULL,
  `palletLpn` varchar(64) NOT NULL,
  `palletLength` float unsigned DEFAULT NULL,
  `palletWidth` float unsigned DEFAULT NULL,
  `palletHeight` float unsigned DEFAULT NULL,
  `palletDimensionUOM` enum('INCH') DEFAULT 'INCH',
  `palletWeight` float unsigned DEFAULT NULL,
  `palletWeightUOM` enum('POUND') DEFAULT 'POUND',
  PRIMARY KEY (`palletId`),
  UNIQUE KEY `asnId` (`asnId`,`palletLpn`),
  CONSTRAINT `asnpalletlevel_ibfk_1` FOREIGN KEY (`asnId`) REFERENCES `asn` (`asnId`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asnskulevel`
--

DROP TABLE IF EXISTS `asnskulevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asnskulevel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asnId` int(10) unsigned NOT NULL,
  `palletId` int(10) unsigned DEFAULT NULL,
  `caseId` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `sku` varchar(64) DEFAULT NULL,
  `product_name` varchar(64) DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `product_length` float unsigned DEFAULT NULL,
  `product_width` float unsigned DEFAULT NULL,
  `product_height` float unsigned DEFAULT NULL,
  `dimensions_UOM` enum('INCH') DEFAULT 'INCH',
  `weight` float unsigned DEFAULT NULL,
  `weight_UOM` enum('POUND') DEFAULT 'POUND',
  `status` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `caseId` (`caseId`,`product_id`),
  KEY `asnId` (`asnId`),
  KEY `palletId` (`palletId`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `asnskulevel_ibfk_1` FOREIGN KEY (`asnId`) REFERENCES `asn` (`asnId`),
  CONSTRAINT `asnskulevel_ibfk_2` FOREIGN KEY (`palletId`) REFERENCES `asnpalletlevel` (`palletId`),
  CONSTRAINT `asnskulevel_ibfk_3` FOREIGN KEY (`caseId`) REFERENCES `asncaselevel` (`caseId`),
  CONSTRAINT `asnskulevel_ibfk_4` FOREIGN KEY (`product_id`) REFERENCES `productmapper` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assignednames`
--

DROP TABLE IF EXISTS `assignednames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignednames` (
  `assignId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sellerId` int(10) unsigned NOT NULL,
  `roles_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`assignId`),
  UNIQUE KEY `sellerId` (`sellerId`,`roles_id`),
  KEY `roles_id` (`roles_id`),
  CONSTRAINT `assignednames_ibfk_1` FOREIGN KEY (`sellerId`) REFERENCES `users` (`sellerId`),
  CONSTRAINT `assignednames_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`roles_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `cityName` varchar(30) CHARACTER SET latin1 NOT NULL,
  `stateCode` char(2) CHARACTER SET latin1 NOT NULL,
  `zipCode` varchar(5) CHARACTER SET latin1 NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `countyName` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`zipCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contactvert`
--

DROP TABLE IF EXISTS `contactvert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactvert` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `companyName` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(80) NOT NULL,
  `phoneNumber` varchar(16) NOT NULL,
  `revenue` varchar(32) DEFAULT NULL,
  `maximumProductSku` int(10) unsigned DEFAULT NULL,
  `sellingMode` varchar(256) DEFAULT NULL,
  `modeOfContact` varchar(8) DEFAULT NULL,
  `preference` varchar(64) DEFAULT NULL,
  `timeOfContact` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `countryName` varchar(30) CHARACTER SET latin1 NOT NULL,
  `countryCode` varchar(4) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`countryCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `orderId` varchar(32) NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `customerId` int(10) unsigned NOT NULL,
  `customerName` varchar(64) NOT NULL,
  `email` varchar(80) NOT NULL,
  `phoneNumber` varchar(16) DEFAULT NULL,
  `address1` varchar(80) DEFAULT NULL,
  `address2` varchar(80) DEFAULT NULL,
  `address3` varchar(80) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `zipCode` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`orderId`,`storeId`),
  UNIQUE KEY `orderId` (`orderId`),
  CONSTRAINT `customers_ibfk_2` FOREIGN KEY (`orderId`, `storeId`) REFERENCES `orders` (`orderId`, `storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `features`
--

DROP TABLE IF EXISTS `features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feature` varchar(20) NOT NULL,
  `sub_feature_id` int(10) unsigned DEFAULT NULL,
  `none` int(10) unsigned DEFAULT NULL,
  `view` int(10) unsigned DEFAULT NULL,
  `view_edit` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fieldmaster`
--

DROP TABLE IF EXISTS `fieldmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldmaster` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `title` varchar(32) NOT NULL,
  `datatype` varchar(32) NOT NULL,
  `length` smallint(6) DEFAULT NULL,
  `default_selected` tinyint(1) NOT NULL,
  `ismandotry` tinyint(1) NOT NULL,
  `issearchable` tinyint(1) NOT NULL DEFAULT '0',
  `has_possible_values` tinyint(1) NOT NULL,
  `possible_values` json DEFAULT NULL,
  PRIMARY KEY (`field_id`),
  UNIQUE KEY `field_id` (`field_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fulfillmentcategory`
--

DROP TABLE IF EXISTS `fulfillmentcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fulfillmentcategory` (
  `categoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(64) NOT NULL,
  PRIMARY KEY (`categoryId`),
  UNIQUE KEY `categoryId` (`categoryId`),
  UNIQUE KEY `categoryName` (`categoryName`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `integration`
--

DROP TABLE IF EXISTS `integration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration` (
  `sellerId` varchar(124) CHARACTER SET latin1 NOT NULL,
  `sellerName` varchar(124) CHARACTER SET latin1 DEFAULT NULL,
  `storeName` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `storeId` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `source` varchar(124) CHARACTER SET latin1 DEFAULT NULL,
  `sourceFlag` varchar(124) CHARACTER SET latin1 DEFAULT NULL,
  `orderPrefix` varchar(124) CHARACTER SET latin1 NOT NULL,
  `host` varchar(124) CHARACTER SET latin1 DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `hostUserName` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `hostPassword` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `caRefreshtoken` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `caAuthorization` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `sftpHost` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `sftpUserName` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `sftpPassword` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `sftpFolderPath` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `sftpKeyPath` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `apiKey` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `orderPickFrequency` int(11) DEFAULT NULL,
  `imagePath` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `flag1` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `flag2` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `additionalParameter1` varchar(124) CHARACTER SET latin1 DEFAULT NULL,
  `additionalParameter2` varchar(124) CHARACTER SET latin1 DEFAULT NULL,
  `additionalParameter3` varchar(124) CHARACTER SET latin1 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`orderPrefix`),
  UNIQUE KEY `orderPrefix` (`orderPrefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orderlist`
--

DROP TABLE IF EXISTS `orderlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderlist` (
  `orderId` varchar(32) NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `price` float unsigned NOT NULL,
  PRIMARY KEY (`orderId`,`storeId`,`product_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `orderlist_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `productmapper` (`product_id`),
  CONSTRAINT `orderlist_ibfk_3` FOREIGN KEY (`orderId`, `storeId`) REFERENCES `orders` (`orderId`, `storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orderreturn`
--

DROP TABLE IF EXISTS `orderreturn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderreturn` (
  `orderReturnId` varchar(32) NOT NULL,
  `orderId` varchar(32) NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `returnedQuantity` int(10) unsigned NOT NULL,
  `returnStatus` varchar(32) NOT NULL,
  `refundAmount` float unsigned NOT NULL,
  `returnRequestedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reasonForReturn` varchar(1024) NOT NULL,
  `additionalComments` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`orderReturnId`,`orderId`,`storeId`),
  KEY `orderId` (`orderId`,`storeId`,`product_id`),
  CONSTRAINT `orderreturn_ibfk_1` FOREIGN KEY (`orderId`, `storeId`, `product_id`) REFERENCES `orderlist` (`orderId`, `storeId`, `product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderId` varchar(32) NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `orderStatus` enum('Processing','Partially Shipped','Shipped','Delivered','Cancelled') DEFAULT 'Processing',
  `source` varchar(64) NOT NULL,
  `sourceCode` varchar(16) NOT NULL,
  `orderSubtotal` float unsigned NOT NULL,
  `orderTax` float unsigned NOT NULL,
  `orderShippingcharges` float unsigned NOT NULL,
  `orderTotal` float unsigned NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastStatusUpdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`orderId`,`storeId`),
  UNIQUE KEY `orderId` (`orderId`,`storeId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `stores` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productcategory`
--

DROP TABLE IF EXISTS `productcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productcategory` (
  `categoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(64) NOT NULL,
  `categoryLevel` enum('PARENT','CHILD','SUB') NOT NULL,
  `parentId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`categoryId`),
  UNIQUE KEY `categoryId` (`categoryId`),
  UNIQUE KEY `categoryName` (`categoryName`,`parentId`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productinventory`
--

DROP TABLE IF EXISTS `productinventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productinventory` (
  `product_id` int(10) unsigned NOT NULL,
  `availableToPromise` int(10) unsigned NOT NULL,
  `fulfillableStock` int(10) unsigned NOT NULL,
  `damagedStock` int(10) unsigned NOT NULL,
  `reservedStock` int(10) unsigned NOT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_id` (`product_id`),
  CONSTRAINT `productinventory_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `productmapper` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productmanageprice`
--

DROP TABLE IF EXISTS `productmanageprice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productmanageprice` (
  `product_id` int(10) unsigned NOT NULL,
  `price` float unsigned NOT NULL,
  `scheduled_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`,`scheduled_at`),
  UNIQUE KEY `product_id` (`product_id`,`scheduled_at`),
  CONSTRAINT `productmanageprice_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `productmapper` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productmapper`
--

DROP TABLE IF EXISTS `productmapper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productmapper` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_id` (`product_id`),
  UNIQUE KEY `sku` (`sku`,`storeId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `productmapper_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `stores` (`storeId`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productvaluemaster`
--

DROP TABLE IF EXISTS `productvaluemaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productvaluemaster` (
  `product_id` int(10) unsigned NOT NULL,
  `field_id` int(10) unsigned NOT NULL,
  `value` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`product_id`,`field_id`),
  KEY `field_id` (`field_id`),
  CONSTRAINT `productvaluemaster_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `productmapper` (`product_id`),
  CONSTRAINT `productvaluemaster_ibfk_2` FOREIGN KEY (`field_id`) REFERENCES `fieldmaster` (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `roles_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `storeId` int(10) unsigned NOT NULL,
  `description` varchar(512) NOT NULL,
  PRIMARY KEY (`roles_id`),
  UNIQUE KEY `roles_id` (`roles_id`),
  UNIQUE KEY `name` (`name`,`storeId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `stores` (`storeId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `selectedfields`
--

DROP TABLE IF EXISTS `selectedfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selectedfields` (
  `sellerId` int(10) unsigned NOT NULL,
  `field_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sellerId`,`field_id`),
  KEY `field_id` (`field_id`),
  CONSTRAINT `selectedfields_ibfk_1` FOREIGN KEY (`sellerId`) REFERENCES `users` (`sellerId`),
  CONSTRAINT `selectedfields_ibfk_2` FOREIGN KEY (`field_id`) REFERENCES `fieldmaster` (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `selectedsubvalues`
--

DROP TABLE IF EXISTS `selectedsubvalues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selectedsubvalues` (
  `sub_values_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `values_id` int(10) unsigned NOT NULL,
  `feature` varchar(20) NOT NULL,
  `selected` int(10) unsigned NOT NULL,
  `none` int(10) unsigned NOT NULL,
  `view` int(10) unsigned NOT NULL,
  `view_edit` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sub_values_id`),
  KEY `values_id` (`values_id`),
  CONSTRAINT `selectedsubvalues_ibfk_1` FOREIGN KEY (`values_id`) REFERENCES `selectedvalues` (`values_id`)
) ENGINE=InnoDB AUTO_INCREMENT=280 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `selectedvalues`
--

DROP TABLE IF EXISTS `selectedvalues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selectedvalues` (
  `values_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roles_id` int(10) unsigned NOT NULL,
  `feature` varchar(20) NOT NULL,
  `selected` int(10) unsigned NOT NULL,
  `none` int(10) unsigned NOT NULL,
  `view` int(10) unsigned NOT NULL,
  `view_edit` int(10) unsigned NOT NULL,
  PRIMARY KEY (`values_id`),
  KEY `roles_id` (`roles_id`),
  CONSTRAINT `selectedvalues_ibfk_1` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`roles_id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sellingmode`
--

DROP TABLE IF EXISTS `sellingmode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sellingmode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `storeId` int(10) unsigned NOT NULL,
  `currentSellingMode` varchar(64) NOT NULL,
  `sellerSource` varchar(64) NOT NULL,
  `orderPrefix` varchar(64) NOT NULL,
  `source` varchar(64) NOT NULL,
  `hostURL` varchar(512) NOT NULL,
  `port` int(11) NOT NULL,
  `hostUserName` varchar(64) NOT NULL,
  `hostPassword` varchar(192) NOT NULL,
  `SFTPHostURL` varchar(512) NOT NULL,
  `SFTPUserName` varchar(64) NOT NULL,
  `SFTPPassword` varchar(64) NOT NULL,
  `SFTPFolderPath` varchar(512) NOT NULL,
  `SFTPKeyPath` varchar(512) NOT NULL,
  `APIKey` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `storeId` (`storeId`,`currentSellingMode`),
  CONSTRAINT `sellingmode_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `stores` (`storeId`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `states` (
  `stateName` varchar(30) CHARACTER SET latin1 NOT NULL,
  `stateCode` varchar(2) CHARACTER SET latin1 NOT NULL,
  `countryCode` varchar(4) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`stateCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores` (
  `storeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sellerCode` varchar(32) NOT NULL,
  `storeName` varchar(64) NOT NULL,
  `email` varchar(80) NOT NULL,
  `phoneNumber` varchar(16) NOT NULL,
  `allowedUsersCount` int(10) unsigned NOT NULL DEFAULT '5',
  `storeStatus` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'INACTIVE',
  `isActivated` tinyint(1) NOT NULL DEFAULT '0',
  `activeFrom` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activeTo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `maximumProductSku` int(10) unsigned NOT NULL,
  `preference` varchar(32) NOT NULL,
  `fulfillment` varchar(64) NOT NULL,
  `imagePath` varchar(1024) NOT NULL,
  `revenue` varchar(64) DEFAULT NULL,
  `modeOfContact` varchar(8) DEFAULT NULL,
  `timeOfContact` varchar(64) DEFAULT NULL,
  `orderPickFrequency` varchar(64) DEFAULT NULL,
  `additionalParameter1` varchar(256) DEFAULT NULL,
  `additionalParameter2` varchar(256) DEFAULT NULL,
  `additionalParameter3` varchar(256) DEFAULT NULL,
  `allDataEntered` tinyint(1) DEFAULT '0',
  `address` varchar(80) NOT NULL,
  `city` varchar(32) NOT NULL,
  `state` varchar(32) NOT NULL,
  `country` varchar(32) NOT NULL,
  `zipCode` varchar(5) NOT NULL,
  `createdBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `modifiedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`storeId`),
  UNIQUE KEY `storeName` (`storeName`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phoneNumber` (`phoneNumber`),
  UNIQUE KEY `sellerCode` (`sellerCode`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storeusermapper`
--

DROP TABLE IF EXISTS `storeusermapper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storeusermapper` (
  `sellerId` int(10) unsigned NOT NULL,
  `storeId` int(10) unsigned NOT NULL,
  UNIQUE KEY `sellerId_2` (`sellerId`,`storeId`),
  KEY `sellerId` (`sellerId`),
  KEY `storeId` (`storeId`),
  CONSTRAINT `storeusermapper_ibfk_1` FOREIGN KEY (`sellerId`) REFERENCES `users` (`sellerId`),
  CONSTRAINT `storeusermapper_ibfk_2` FOREIGN KEY (`storeId`) REFERENCES `stores` (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subfeatures`
--

DROP TABLE IF EXISTS `subfeatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subfeatures` (
  `subfeatureId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(10) unsigned DEFAULT NULL,
  `feature` varchar(20) NOT NULL,
  `none` int(10) unsigned NOT NULL,
  `view` int(10) unsigned NOT NULL,
  `view_edit` int(10) unsigned NOT NULL,
  UNIQUE KEY `feature` (`feature`),
  UNIQUE KEY `subfeatureId` (`subfeatureId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userlogindetails`
--

DROP TABLE IF EXISTS `userlogindetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userlogindetails` (
  `sellerId` int(10) unsigned NOT NULL,
  `failedLoginCount` int(10) unsigned NOT NULL DEFAULT '0',
  `lastLoginAttemptAt` timestamp NULL DEFAULT NULL,
  `lastLoginAt` timestamp NULL DEFAULT NULL,
  `lastLoginFailedAt` timestamp NULL DEFAULT NULL,
  `accountLockedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sellerId`),
  UNIQUE KEY `sellerId` (`sellerId`),
  CONSTRAINT `userlogindetails_ibfk_1` FOREIGN KEY (`sellerId`) REFERENCES `users` (`sellerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpasswordjti`
--

DROP TABLE IF EXISTS `userpasswordjti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpasswordjti` (
  `sellerId` int(10) unsigned NOT NULL,
  `jti` varchar(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`sellerId`),
  UNIQUE KEY `sellerId` (`sellerId`),
  UNIQUE KEY `jti` (`jti`),
  CONSTRAINT `userpasswordjti_ibfk_1` FOREIGN KEY (`sellerId`) REFERENCES `users` (`sellerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userrecentpassword`
--

DROP TABLE IF EXISTS `userrecentpassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrecentpassword` (
  `sellerId` int(10) unsigned NOT NULL,
  `password1` varchar(192) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password2` varchar(192) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password3` varchar(192) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`sellerId`),
  UNIQUE KEY `sellerId` (`sellerId`),
  CONSTRAINT `userrecentpassword_ibfk_1` FOREIGN KEY (`sellerId`) REFERENCES `users` (`sellerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `sellerId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userName` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(192) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(80) NOT NULL,
  `phoneNumber` varchar(16) DEFAULT NULL,
  `userType` varchar(32) NOT NULL,
  `sellerStatus` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'INACTIVE',
  `isActivated` tinyint(1) NOT NULL DEFAULT '0',
  `firstlogin` tinyint(1) NOT NULL DEFAULT '1',
  `isCreatedByVert` tinyint(1) NOT NULL DEFAULT '0',
  `isFirstUser` tinyint(1) NOT NULL DEFAULT '0',
  `tokenIssuedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createdBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `modifiedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sellerId`),
  UNIQUE KEY `userName` (`userName`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phoneNumber` (`phoneNumber`),
  KEY `userType` (`userType`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`userType`) REFERENCES `usertype` (`userType`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usertype`
--

DROP TABLE IF EXISTS `usertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usertype` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `userType` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userType` (`userType`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertadmin`
--

DROP TABLE IF EXISTS `vertadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertadmin` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userName` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(192) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(80) NOT NULL,
  `phoneNumber` varchar(16) DEFAULT NULL,
  `userType` enum('VERTADMIN') NOT NULL DEFAULT 'VERTADMIN',
  `vertuserStatus` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'INACTIVE',
  `isActivated` tinyint(1) NOT NULL DEFAULT '0',
  `firstlogin` tinyint(1) NOT NULL DEFAULT '1',
  `isFirstUser` tinyint(1) NOT NULL DEFAULT '0',
  `tokenIssuedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastAccessTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createdBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `modifiedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userId` (`userId`),
  UNIQUE KEY `userName` (`userName`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertapicontrol`
--

DROP TABLE IF EXISTS `vertapicontrol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertapicontrol` (
  `apiId` int(10) unsigned NOT NULL,
  `featureId` int(10) unsigned NOT NULL,
  `reqFeatureVal` int(10) unsigned NOT NULL,
  PRIMARY KEY (`apiId`,`featureId`),
  KEY `featureId` (`featureId`),
  CONSTRAINT `vertapicontrol_ibfk_1` FOREIGN KEY (`apiId`) REFERENCES `vertapilist` (`apiId`),
  CONSTRAINT `vertapicontrol_ibfk_2` FOREIGN KEY (`featureId`) REFERENCES `vertfeatures` (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertapilist`
--

DROP TABLE IF EXISTS `vertapilist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertapilist` (
  `apiId` int(10) unsigned NOT NULL,
  `resourceName` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `methodName` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `verb` enum('GET','POST','PUT','DELETE','PATCH','OPTIONS','HEAD') NOT NULL,
  PRIMARY KEY (`apiId`),
  UNIQUE KEY `apiId` (`apiId`),
  UNIQUE KEY `resourceName` (`resourceName`,`methodName`,`verb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertassignedroles`
--

DROP TABLE IF EXISTS `vertassignedroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertassignedroles` (
  `userId` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `userId` (`userId`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `vertassignedroles_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `vertadmin` (`userId`),
  CONSTRAINT `vertassignedroles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `vertroles` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertfeatures`
--

DROP TABLE IF EXISTS `vertfeatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertfeatures` (
  `feature_id` int(10) unsigned NOT NULL,
  `feature` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `none` int(10) unsigned NOT NULL,
  `view` int(10) unsigned NOT NULL,
  `view_edit` int(10) unsigned NOT NULL,
  PRIMARY KEY (`feature_id`),
  UNIQUE KEY `feature_id` (`feature_id`),
  UNIQUE KEY `feature` (`feature`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `vertfeatures_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `vertfeatures` (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertlogindetails`
--

DROP TABLE IF EXISTS `vertlogindetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertlogindetails` (
  `userId` int(10) unsigned NOT NULL,
  `failedLoginCount` int(10) unsigned NOT NULL DEFAULT '0',
  `lastLoginAttemptAt` timestamp NULL DEFAULT NULL,
  `lastLoginAt` timestamp NULL DEFAULT NULL,
  `lastLoginFailedAt` timestamp NULL DEFAULT NULL,
  `accountLockedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userId` (`userId`),
  CONSTRAINT `vertlogindetails_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `vertadmin` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertpasswordjti`
--

DROP TABLE IF EXISTS `vertpasswordjti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertpasswordjti` (
  `userId` int(10) unsigned NOT NULL,
  `jti` varchar(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userId` (`userId`),
  UNIQUE KEY `jti` (`jti`),
  CONSTRAINT `vertpasswordjti_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `vertadmin` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertrecentpassword`
--

DROP TABLE IF EXISTS `vertrecentpassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertrecentpassword` (
  `userId` int(10) unsigned NOT NULL,
  `password1` varchar(192) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password2` varchar(192) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password3` varchar(192) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userId` (`userId`),
  CONSTRAINT `vertrecentpassword_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `vertadmin` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertroles`
--

DROP TABLE IF EXISTS `vertroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertroles` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(64) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `createdBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `modifiedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_id` (`role_id`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vertselectedfeatures`
--

DROP TABLE IF EXISTS `vertselectedfeatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertselectedfeatures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `feature_id` int(10) unsigned NOT NULL,
  `selected_value` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `role_id` (`role_id`,`feature_id`),
  KEY `feature_id` (`feature_id`),
  CONSTRAINT `vertselectedfeatures_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `vertroles` (`role_id`),
  CONSTRAINT `vertselectedfeatures_ibfk_2` FOREIGN KEY (`feature_id`) REFERENCES `vertfeatures` (`feature_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-22 19:12:50
