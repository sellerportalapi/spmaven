package com.vertadmin;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.general.enums.PswdJwtType;
import com.vossa.api.general.policies.PasswordPolicy;
import com.vossa.api.others.DBConnection;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.encryption.AES;
import com.vossa.api.others.filehandlers.ConfigFileHandler;
import com.vossa.api.others.token.JWT;
import com.vossa.api.others.token.JWTHandler;
import com.vossa.api.others.token.UserClaims;
import com.vossa.api.server.DataSourceFactory;
import com.vossa.api.v1.vertadmin.pojos.VertAdmin;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;

public class AccountService {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;

		return connection;
	}

	protected Connection getDBConnectionPool() throws Exception {
		return DataSourceFactory.getInstance().getConnection();
	}

	@ApiMethod(apiname = "vertadminlogin", method = "POST")
	public ResponseClass vertAdminLogin(String username, String password, String origin) {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnection()) {
			long curr_time = System.currentTimeMillis();
			ps = connection.prepareStatement("SELECT * FROM " + Table.vertadmin + " WHERE  userName = ?");
			ps.setString(1, username);
			rs = ps.executeQuery();

			if (!rs.next()) {
				ps.close();
				rs.close();
				return new ResponseClass(401, "fail", "Invalid Username/Password",
						"Please check the Username/Password");
			}
			int userid = rs.getInt("userId");
			String plainPassword = AES.decrypt(rs.getString("password"));
			String vertuserStatus = rs.getString("vertuserStatus");

			if (vertuserStatus.equals("INACTIVE"))
				return new ResponseClass(403, "fail", "Your account is currently inactive",
						"Please contact VERT admin");

			if (isAccountLocked(userid, curr_time)) {
				return new ResponseClass(403, "fail", "Your account is currently locked for some time",
						"Please wait for some time, your account will be automatically unlocked.");
			}

			if (plainPassword.equals(password)) {
				loginSucceed(userid, curr_time);
			} else {
				loginFailed(userid, curr_time);
				return new ResponseClass(401, "fail", "Invalid Username/Password",
						"Please check the Username/Password");
			}

			UserClaims userClaims = new UserClaims(rs.getInt("userId"), username, rs.getString("userType"));
			ps.close();
			rs.close();
			userClaims.setPermission(new RoleServices().getVertPermissionMap(userid));

			JWT jwt = getJWTCreator(userClaims, origin);
			String token = JWTHandler.buildJWT(jwt);

			Timestamp issueTime = new Timestamp(jwt.getIat());
			setTokenIssuedAt(issueTime, username);

			return new ResponseClass(200, "success", "Login successful", token, "VERTADMIN");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later...");
		}
	}

	public ResponseClass vertAdminLogout(String _username) {
		PreparedStatement ps;
		try (Connection connection = getDBConnection()) {
			ps = connection
					.prepareStatement("UPDATE " + Table.vertadmin + " SET tokenIssuedAt = 0 " + " WHERE userName = ?");
			ps.setString(1, _username);
			ps.executeUpdate();
			ps.close();

			return new ResponseClass(200, "success", "Successfully logged-out");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	public Map<String, Integer> getVertPermissions(int userid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT isFirstUser FROM " + Table.vertadmin + " WHERE userId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				throw new Exception("Invalid userId");
			}
			boolean isFirstUser = rs.getBoolean("isFirstUser");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT feature_id, feature FROM " + Table.vertfeatures);
			rs1 = ps1.executeQuery();

			// deriving the metadata for the vert features
			Map<String, Integer> permissions = new HashMap<>();
			Map<Integer, String> featureMapper = new HashMap<>();
			while (rs1.next()) {
				featureMapper.put(rs1.getInt("feature_id"), rs1.getString("feature"));
				if (isFirstUser == false)
					permissions.put(rs1.getString("feature"), 1); // default selected_value will be 1 ('none')
				else
					permissions.put(rs1.getString("feature"), 3); // for the first user maximum permission
			}
			ps1.close();
			rs1.close();

			PreparedStatement ps2;
			ResultSet rs2;
			ps2 = connection.prepareStatement("SELECT T1.role_id, T1.feature_id, T1.selected_value FROM "
					+ Table.vertselectedfeatures + " T1 JOIN " + Table.vertassignedroles
					+ " T2 ON T1.role_id = T2.role_id" + " WHERE userId = ?");
			ps2.setInt(1, userid);
			rs2 = ps2.executeQuery();

			// deriving the maximum permission for every features based on various roles
			while (rs2.next()) {
				int feautre_id = rs2.getInt("feature_id");
				int curr_selected_value = rs2.getInt("selected_value");

				String feature = featureMapper.get(feautre_id);
				int prev_selected_value = permissions.get(feature);

				if (prev_selected_value < curr_selected_value)
					permissions.put(feature, curr_selected_value);
			}
			ps2.close();
			rs2.close();
			return permissions;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "resetpassword", method = "PUT")
	public ResponseClass resetPassword(String username, String currentPassword, String newPassword) {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT userId, password FROM " + Table.vertadmin + " WHERE userName = ?");
			ps.setString(1, username);
			rs = ps.executeQuery();
			rs.next();

			int userid = rs.getInt("userId");
			String plainPassword = AES.decrypt(rs.getString("password"));
			ps.close();
			rs.close();

			if (!plainPassword.equals(currentPassword))
				return new ResponseClass(400, "fail", "Wrong password",
						"Password entered doesn't match with existing password");

			PasswordPolicy.validatePassword(newPassword);
			checkRecentPassword(newPassword, userid);

			String cipherPassword = AES.encrypt(newPassword);

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("UPDATE " + Table.vertadmin
					+ " SET password = ?, modifiedBy = ?, modifiedOn = ? " + " WHERE userName = ?");
			ps1.setString(1, cipherPassword);
			ps1.setString(2, username);
			ps1.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			ps1.setString(4, username);
			ps1.execute();
			ps1.close();

			updateRecentPassword(newPassword, userid);
			System.out.println("Password reset");

			return new ResponseClass(201, "success", "Password has been successfully reset");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	@ApiMethod(apiname = "forgotpassword", method = "POST")
	public ResponseClass forgotPassword(String email, String origin) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT userId, userName FROM " + Table.vertadmin + " WHERE email = ?");
			ps.setString(1, email);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				return new ResponseClass(200, "success", "process initiated--negative");
			}
			int userid = rs.getInt("userId");
			String username = rs.getString("userName");
			ps.close();
			rs.close();

			VertAdmin admin = new VertAdmin();
			admin.setUserId(userid);
			admin.setUserName(username);
			admin.setEmail(email);

			new VertAdminService().createNewPasswordLink(admin, origin, PswdJwtType.JWT_FP);

			return new ResponseClass(200, "success", "process initiated");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "createpassword", method = "POST")
	public ResponseClass createPassword(String newPassword, int userid, String jti, String JWT_TYP) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT * FROM " + Table.vertpasswordjti + " WHERE userId = ? and jti = ?");
			ps.setInt(1, userid);
			ps.setString(2, jti);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				return new ResponseClass(200, "failure", "Invalid Link");
			}
			ps.close();
			rs.close();

			PasswordPolicy.validatePassword(newPassword);
			checkRecentPassword(newPassword, userid);

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("DELETE FROM " + Table.vertpasswordjti + " WHERE userId = ? and jti = ?");
			ps1.setInt(1, userid);
			ps1.setString(2, jti);
			ps1.execute();
			ps1.close();

			String cipherPassword = AES.encrypt(newPassword);

			PreparedStatement ps2;
			ps2 = connection.prepareStatement("UPDATE " + Table.vertadmin + " SET password = ?" + " WHERE userId = ?");
			ps2.setString(1, cipherPassword);
			ps2.setInt(2, userid);
			ps2.executeUpdate();
			ps2.close();

			updateRecentPassword(newPassword, userid);
			if (JWT_TYP.equals(PswdJwtType.JWT_CP.toString()))
				updateVertStatus(userid);

			return new ResponseClass(200, "success", "password updated");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void checkRecentPassword(String plainPassword, int userId) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT password1, password2, password3 FROM " + Table.vertrecentpassword + " WHERE userId = ?");
			ps.setInt(1, userId);
			rs = ps.executeQuery();
			rs.next();
			String password1 = AES.decrypt(rs.getString("password1"));
			String password2 = AES.decrypt(rs.getString("password2"));
			String password3 = AES.decrypt(rs.getString("password3"));
			ps.close();
			rs.close();

			if (plainPassword.equals(password1) || plainPassword.equals(password2) || plainPassword.equals(password3))
				throw new Exception("New password cannot be same as last 3 passwords");

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void updateRecentPassword(String plainPassword, int userid) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT password1, password2, password3 FROM " + Table.vertrecentpassword + " WHERE userId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();

			String password1 = rs.getString("password1");
			String password2 = rs.getString("password2");
			String password3 = rs.getString("password3");
			ps.close();
			rs.close();

			password3 = password2;
			password2 = password1;
			password1 = AES.encrypt(plainPassword);

			ps = connection.prepareStatement("UPDATE " + Table.vertrecentpassword
					+ " SET password1 = ?, password2 = ?, password3 = ? WHERE userId = ?");
			ps.setString(1, password1);
			ps.setString(2, password2);
			ps.setString(3, password3);
			ps.setInt(4, userid);
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "checktokenexpiry", method = "GET")
	public ResponseClass checkTokenExpiry(int userid, String jti) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT * FROM " + Table.vertpasswordjti + " WHERE userId = ? and jti = ?");
			ps.setInt(1, userid);
			ps.setString(2, jti);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				return new ResponseClass(200, "failure", "Invalid Link");
			}
			ps.close();
			rs.close();
			return new ResponseClass(200, "success", "valid token");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "resendpasswordlink", method = "GET")
	public ResponseClass resendPasswordLink(String token, String origin) throws Exception {
		try (Connection connection = getDBConnectionPool()) {

			String username = null;
			String jti = null;
			Integer userid = null;

			try {
				JWTHandler.parseJWTPassword(token);
			} catch (ExpiredJwtException e) {
				Claims claims = e.getClaims();
				jti = claims.getId();
				username = claims.getSubject();
				userid = new VertAdminService().getUseridByName(username);
			} catch (Exception e) {
				new ResponseClass(401, "fail", "Invalid Token");
			}

			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT * FROM " + Table.vertpasswordjti + " WHERE userId = ? and jti = ?");
			ps.setInt(1, userid);
			ps.setString(2, jti);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				return new ResponseClass(200, "failure", "Invalid Link");
			}

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT email FROM " + Table.vertadmin + " WHERE userId = ?");
			ps1.setInt(1, userid);
			rs1 = ps1.executeQuery();
			rs1.next();
			String email = rs1.getString("email");
			ps1.close();
			rs1.close();

			VertAdmin admin = new VertAdmin();
			admin.setUserId(userid);
			admin.setUserName(username);
			admin.setEmail(email);

			new VertAdminService().createNewPasswordLink(admin, origin, PswdJwtType.JWT_CP);

			return new ResponseClass(200, "success", "Valid token. Mail sent.");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void loginFailed(int userid, long curr_time) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.vertlogindetails + " WHERE userId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();

			int failedLoginCount = rs.getInt("failedLoginCount");
			if (failedLoginCount < 3) {
				long lastLoginFailedAt = 0;
				if (rs.getTimestamp("lastLoginFailedAt") != null)
					lastLoginFailedAt = rs.getTimestamp("lastLoginFailedAt").getTime();

				if (failedLoginCount != 0 && curr_time - lastLoginFailedAt > PasswordPolicy.LOCKOUT_RESET_TIME) {
					failedLoginCount = 0;
				}
				failedLoginCount++;

				PreparedStatement ps1;
				if (failedLoginCount == 3) {
					ps1 = connection.prepareStatement("UPDATE " + Table.vertlogindetails
							+ " SET failedLoginCount = ?, lastLoginAttemptAt = ?, lastLoginFailedAt = ?, accountLockedAt = ?"
							+ " WHERE userId = ?");
					ps1.setInt(1, failedLoginCount);
					ps1.setTimestamp(2, new Timestamp(curr_time));
					ps1.setTimestamp(3, new Timestamp(curr_time));
					ps1.setTimestamp(4, new Timestamp(curr_time));
					ps1.setInt(5, userid);
					ps1.executeUpdate();
					ps1.close();
				} else {
					ps1 = connection.prepareStatement("UPDATE " + Table.vertlogindetails
							+ " SET failedLoginCount = ?, lastLoginAttemptAt = ?, lastLoginFailedAt = ?"
							+ " WHERE userId = ?");
					ps1.setInt(1, failedLoginCount);
					ps1.setTimestamp(2, new Timestamp(curr_time));
					ps1.setTimestamp(3, new Timestamp(curr_time));
					ps1.setInt(4, userid);
					ps1.executeUpdate();
					ps1.close();
				}
				ps.close();
				rs.close();
			} else {
				ps.close();
				rs.close();
				throw new Exception("Server error. failedLoginCount is already 3");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void loginSucceed(int userid, long curr_time) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ps = connection.prepareStatement("UPDATE " + Table.vertlogindetails
					+ " SET failedLoginCount = ?, lastLoginAttemptAt = ?, lastLoginAt = ?" + " WHERE userId = ?");
			ps.setInt(1, 0);
			ps.setTimestamp(2, new Timestamp(curr_time));
			ps.setTimestamp(3, new Timestamp(curr_time));
			ps.setInt(4, userid);
			ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ResponseClass updateVertStatus(int userid) {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			String vertuserStatus = "ACTIVE";

			ps = connection.prepareStatement(
					"UPDATE " + Table.vertadmin + " SET vertuserStatus = ?, isActivated = ?" + " WHERE userId = ?");
			ps.setString(1, vertuserStatus);
			ps.setInt(2, 1);
			ps.setInt(3, userid);
			ps.executeUpdate();
			ps.close();

			return new ResponseClass(201, "success", "Vert_User_Status has been updated successfully");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	public boolean isAccountLocked(int userid, long curr_time) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.vertlogindetails + " WHERE userId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();

			int failedLoginCount = rs.getInt("failedLoginCount");
			if (failedLoginCount == 3) {
				long accountLockedAt = 0;
				if (rs.getTimestamp("accountLockedAt") != null) {
					accountLockedAt = rs.getTimestamp("accountLockedAt").getTime();
					ps.close();
					rs.close();
				}
				if (curr_time - accountLockedAt > PasswordPolicy.ACCOUNT_LOCKOUT_TIME) {
					PreparedStatement ps1;
					ps1 = connection.prepareStatement(
							"UPDATE " + Table.vertlogindetails + " SET failedLoginCount = ?" + " WHERE userId = ?");
					ps1.setInt(1, 0);
					ps1.setInt(2, userid);
					ps1.executeUpdate();
					ps1.close();

					return false; // Account lock time is over, so reset the failedLoginCount.
				} else {
					return true; // Account is still locked.
				}
			} else
				return false; // failedLoginCount<3, So Account will not be locked.
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void setTokenIssuedAt(Timestamp time, String _username) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ps = connection.prepareStatement("UPDATE " + Table.vertadmin + " SET tokenIssuedAt = ?, lastAccessTime = ? "
					+ " WHERE userName = ?");
			ps.setTimestamp(1, time);
			ps.setTimestamp(2, time);
			ps.setString(3, _username);
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public boolean checkTokenIssuedAt(Timestamp time, String _username) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT tokenIssuedAt FROM " + Table.vertadmin + " WHERE  userName = ?");
			ps.setString(1, _username);
			rs = ps.executeQuery();

			if (!rs.next()) {
				ps.close();
				rs.close();
				return false;
			}
			if (rs.getTimestamp("tokenIssuedAt").equals(time)) {
				ps.close();
				rs.close();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return false;
	}

	protected JWT getJWTCreator(UserClaims userClaims, String origin) throws IOException {
		ConfigFileHandler config = new ConfigFileHandler();
		String issConfig = config.get("iss");
		String audConfig = config.get("aud");

		URL issurl = new URL(issConfig);
		URL audurl;

		if (origin == null)
			audurl = new URL(audConfig);
		else
			audurl = new URL(origin);

		String jti = UUID.randomUUID().toString();
		String iss = issurl.getAuthority();
		String aud = audurl.getAuthority();
		String sub = userClaims.getUserName();

		JWT jwt = new JWT(jti, iss, aud, sub, userClaims);
		return jwt;
	}
}
