package com.vertadmin;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.general.enums.PswdJwtType;
import com.vossa.api.others.DBConnection;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.encryption.AES;
import com.vossa.api.others.mailservices.VertMailService;
import com.vossa.api.others.token.JWT;
import com.vossa.api.others.token.JWTHandler;
import com.vossa.api.server.DataSourceFactory;
import com.vossa.api.v1.vertadmin.pojos.VertAdmin;

public class VertAdminService {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;

		return connection;
	}

	protected Connection getDBConnectionPool() throws Exception {
		return DataSourceFactory.getInstance().getConnection();
	}

	@ApiMethod(apiname = "getalladmins", method = "GET")
	public List<VertAdmin> getAllAdmins() throws Exception {

		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.vertadmin + " WHERE isFirstUser = 0");
			rs = ps.executeQuery();

			List<VertAdmin> vertuserList = new ArrayList<>();
			while (rs.next()) {
				int userid = rs.getInt("userId");

				VertAdmin admin = new VertAdmin();
				admin.setUserId(userid);
				admin.setUserName(rs.getString("userName"));
				admin.setName(rs.getString("name"));
				admin.setEmail(rs.getString("email"));
				admin.setPhoneNumber(rs.getString("phoneNumber"));
				admin.setUserType(rs.getString("userType"));
				admin.setVertuserStatus(rs.getString("vertUserStatus"));
				admin.setFirstlogin(rs.getBoolean("firstlogin"));
				admin.setIsActivated(rs.getBoolean("isActivated"));
				admin.setIsFirstUser(rs.getBoolean("isFirstUser"));
				admin.setVertRoles(getAssignedRoles(userid));

				vertuserList.add(admin);
			}
			ps.close();
			rs.close();
			return vertuserList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "createvertadmin", method = "POST")
	public ResponseClass createVertAdmin(VertAdmin admin, String createdBy, String origin) {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);

			boolean validUniqueData;
			validUniqueData = checkFieldVert("userName", admin.getUserName());
			if (validUniqueData == false) {
				return new ResponseClass(400, "fail", "UserName already exists", null);
			}

			validUniqueData = checkFieldVert("email", admin.getEmail());
			if (validUniqueData == false) {
				return new ResponseClass(400, "fail", "Email already exists", null);
			}

			validUniqueData = checkFieldVert("phoneNumber", admin.getPhoneNumber());
			if (validUniqueData == false) {
				return new ResponseClass(400, "fail", "PhoneNumber already exists", null);
			}

			String plainPassword = randomAlphanumeric(16);
			String cipherPassword = AES.encrypt(plainPassword);

			PreparedStatement ps;
			ps = connection.prepareStatement(
					"INSERT INTO " + Table.vertadmin + " (userName, password, name, email, phoneNumber,"
							+ " createdBy, modifiedBy)" + " values ( ?, ?, ?, ?, ?, ?, ?)");

			ps.setString(1, admin.getUserName());
			ps.setString(2, cipherPassword);
			ps.setString(3, admin.getName());
			ps.setString(4, admin.getEmail());
			ps.setString(5, admin.getPhoneNumber());
			ps.setString(6, createdBy);
			ps.setString(7, createdBy);
			ps.execute();
			ps.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT userId FROM " + Table.vertadmin + " WHERE userName = ?");
			ps1.setString(1, admin.getUserName());
			rs1 = ps1.executeQuery();
			rs1.next();
			int userid = rs1.getInt("userId");
			ps1.close();
			rs1.close();

			vertAssignRoles(userid, admin.getVertRoles(), connection);

			PreparedStatement ps2;
			ps2 = connection.prepareStatement("INSERT INTO " + Table.vertrecentpassword + " VALUES (?, ?, ?, ?)");
			ps2.setInt(1, userid);
			ps2.setString(2, cipherPassword);
			ps2.setString(3, cipherPassword);
			ps2.setString(4, cipherPassword);
			ps2.execute();
			ps2.close();

			PreparedStatement ps3;
			ps3 = connection.prepareStatement("INSERT INTO " + Table.vertlogindetails + " VALUES (?, ?, ?, ?, ?, ?)");
			ps3.setInt(1, userid);
			ps3.setInt(2, 0);
			ps3.setTimestamp(3, null);
			ps3.setTimestamp(4, null);
			ps3.setTimestamp(5, null);
			ps3.setTimestamp(6, null);
			ps3.execute();
			ps3.close();
			System.out.println("New VERTADMIN Created");

			connection.commit();

			admin.setUserId(userid);
			createNewPasswordLink(admin, origin, PswdJwtType.JWT_CP); // when successfully created send mail.

			return new ResponseClass(201, "success", "New VERTADMIN successfully created");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "VERTADMIN not successfully created");
		}
	}

	@ApiMethod(apiname = "updatevertadmin", method = "POST")
	public ResponseClass updateVertAdmin(VertAdmin admin, String modifiedBy, String userName) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);

			if (admin.getUserId() == null)
				throw new Exception("userId cannot be null");

			boolean validUniqueData;
			validUniqueData = checkFieldVertOnUpdate(admin.getUserId(), "phoneNumber", admin.getPhoneNumber());
			if (validUniqueData == false) {
				return new ResponseClass(400, "fail", "PhoneNumber already exists", null);
			}

			PreparedStatement ps;
			ps = connection.prepareStatement("UPDATE " + Table.vertadmin
					+ " SET name = ?, phoneNumber = ?, modifiedBy = ?" + " WHERE userId = ?");
			ps.setString(1, admin.getName());
			ps.setString(2, admin.getPhoneNumber());
			ps.setString(3, modifiedBy);
			ps.setInt(4, admin.getUserId());
			ps.executeUpdate();
			ps.close();

			vertAssignRoles(admin.getUserId(), admin.getVertRoles(), connection);

			connection.commit();
			return new ResponseClass(200, "success", "VertAdmin successfully updated");

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ResponseClass updateVertuserStatus(int userid, String vertuserStatus, String modifiedBy, String origin) {
		try (Connection connection = getDBConnection()) {
			if (!vertuserStatus.equals("ACTIVE") && !vertuserStatus.equals("INACTIVE"))
				return new ResponseClass(400, "fail", "Invalid JSON input", "Invalid vertUserStatus");

			if (vertuserStatus.equals("ACTIVE"))
				checkUserActivation(userid, origin);

			PreparedStatement ps;
			ps = connection.prepareStatement(
					"UPDATE " + Table.vertadmin + " SET vertuserStatus = ?, modifiedBy = ?" + " WHERE userId = ?");
			ps.setString(1, vertuserStatus);
			ps.setString(2, modifiedBy);
			ps.setInt(3, userid);
			ps.executeUpdate();
			ps.close();

			return new ResponseClass(201, "success", "Vert_User_Status has been updated successfully");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	@ApiMethod(apiname = "checkfieldvert", method = "POST")
	public boolean checkFieldVert(String key, String value) throws Exception {
		int count = 0;
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT COUNT(*) as cnt FROM " + Table.vertadmin + " WHERE " + key + " = ?");
			ps.setString(1, value);
			rs = ps.executeQuery();
			rs.next();
			count = rs.getInt("cnt");
			ps.close();
			rs.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return (count == 0) ? true : false;
	}

	public boolean checkFieldVertOnUpdate(int userid, String key, String value) throws Exception {
		int count = 0;
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT COUNT(*) as cnt FROM " + Table.vertadmin + " WHERE userId <> ? AND " + key + " = ?");
			ps.setInt(1, userid);
			ps.setString(2, value);
			rs = ps.executeQuery();
			rs.next();
			count = rs.getInt("cnt");
			ps.close();
			rs.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return (count == 0) ? true : false;
	}

	public List<Integer> getAssignedRoles(int userid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT role_id FROM " + Table.vertassignedroles + " WHERE userId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();

			List<Integer> vertRoles = new ArrayList<>();
			while (rs.next()) {
				vertRoles.add(rs.getInt("role_id"));
			}
			ps.close();
			rs.close();
			return vertRoles;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void vertAssignRoles(int userid, List<Integer> vertRoles, Connection connection) throws Exception {
		try {
			if (vertRoles == null || vertRoles.isEmpty()) {
				throw new Exception("No roles assigned to the vertuser.");
			}

			PreparedStatement ps;
			ps = connection.prepareStatement("DELETE FROM " + Table.vertassignedroles + " WHERE userId = ?");
			ps.setInt(1, userid);
			ps.executeUpdate();
			ps.close();

			for (Integer role_id : vertRoles) {
				PreparedStatement ps1;
				ps1 = connection.prepareStatement(
						"INSERT INTO " + Table.vertassignedroles + " (userId, role_id)" + " VALUES(?, ?)");
				ps1.setInt(1, userid);
				ps1.setInt(2, role_id);
				ps1.execute();
				ps1.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void checkUserActivation(int userid, String origin) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.vertadmin + " WHERE userId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();

			if (rs.getBoolean("isActivated") == false) {
				PreparedStatement ps1;
				ps1 = connection
						.prepareStatement("UPDATE " + Table.vertadmin + " SET isActivated = ?" + " WHERE userId = ?");
				ps1.setBoolean(1, true);
				ps1.setInt(2, userid);
				ps1.executeUpdate();
				ps1.close();
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		}
	}

	public void createNewPasswordLink(VertAdmin admin, String origin, PswdJwtType jwtType) throws Exception {
		if (!jwtType.equals(PswdJwtType.JWT_CP) && !jwtType.equals(PswdJwtType.JWT_FP))
			throw new Exception("Invalid JWT_TYPE");

		try (Connection connection = getDBConnection()) {
			String jti = UUID.randomUUID().toString();

			PreparedStatement ps;
			ps = connection.prepareStatement("DELETE FROM " + Table.vertpasswordjti + " WHERE userId = ?");
			ps.setInt(1, admin.getUserId());
			ps.execute();
			ps.close();

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("INSERT INTO " + Table.vertpasswordjti + " VALUES (?, ?)");
			ps1.setInt(1, admin.getUserId());
			ps1.setString(2, jti);
			ps1.execute();
			ps1.close();

			JWT jwt = new JWT();
			jwt.setJti(jti);
			jwt.setSub(admin.getUserName());
			String token = JWTHandler.buildJWTPassword(jwt, PswdJwtType.JWT_CP);

			if (jwtType.equals(PswdJwtType.JWT_CP)) {
				String passwordUrl = origin + "/createpassword?cp_token=" + token + "&vert=true";
				String receiverEmail = admin.getEmail();
				String mailSubject = "Account has been Activated";

				VertMailService mailService = new VertMailService();
				mailService.sendmailActivation(receiverEmail, mailSubject, admin.getUserName(), passwordUrl);

			} else if (jwtType.equals(PswdJwtType.JWT_FP)) {
				String passwordResetUrl = origin + "/resetpassword?rp_token=" + token + "&vert=true";
				String receiverEmail = admin.getEmail();
				String mailSubject = "Reset Password";

				VertMailService mailService = new VertMailService();
				mailService.sendmailForgotPassword(receiverEmail, mailSubject, passwordResetUrl);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int getUseridByName(String username) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT userId FROM " + Table.vertadmin + " WHERE  userName = ?");
			ps.setString(1, username);
			rs = ps.executeQuery();

			if (rs.next()) {
				int userid = rs.getInt("userId");
				ps.close();
				rs.close();
				return userid;
			} else {
				ps.close();
				rs.close();
				throw new Exception("UserName not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String getUsernameById(int userid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT userName FROM " + Table.vertadmin + " WHERE  userId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();

			if (rs.next()) {
				String username = rs.getString("userName");
				ps.close();
				rs.close();
				return username;
			} else {
				ps.close();
				rs.close();
				throw new Exception("UserId not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String randomAlphanumeric(int length) {
		final String alphanumericText = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom random = new SecureRandom();

		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(alphanumericText.charAt(random.nextInt(alphanumericText.length())));
		return sb.toString();
	}
}
