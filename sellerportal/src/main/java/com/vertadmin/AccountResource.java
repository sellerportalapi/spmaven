package com.vertadmin;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Authenticate;
import com.vossa.api.general.annotations.filterannotations.CreatePassword;
import com.vossa.api.general.annotations.filterannotations.Insecured;
import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.JWT;
import com.vossa.api.others.token.UserClaims;

@Path("")
public class AccountResource {

	private AccountService service = new AccountService();

	@POST
	@Authenticate(UserType.VERTADMIN)
	@Path("vertadminlogin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass vertAdminLogin(String data, @Context HttpHeaders httpHeaders) {
		try {
			JSONObject json = new JSONObject(data);
			String username = json.getString("userName");
			String password = json.getString("password");
			String origin = httpHeaders.getHeaderString("origin");
			return service.vertAdminLogin(username, password, origin);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getvertpermissions")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getVertPermissions(@Context ContainerRequestContext context) throws Exception {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int userid = userClaims.getUserId();

			return service.getVertPermissions(userid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@PUT
	@Secured(UserType.VERTADMIN)
	@Path("resetpassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass resetPassword(String data, @Context ContainerRequestContext context) {
		String currentPassword, newPassword;
		JSONObject json;
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String username = userClaims.getUserName();

			json = new JSONObject(data);
			currentPassword = json.getString("currentPassword");
			newPassword = json.getString("newPassword");
			return service.resetPassword(username, currentPassword, newPassword);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Insecured
	@Path("forgotpassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass forgotPassword(Map<String, String> data, @Context HttpHeaders httpHeaders) throws Exception {
		try {
			String origin = httpHeaders.getHeaderString("origin");
			String email = data.get("email");
			return service.forgotPassword(email, origin);
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@CreatePassword
	@Path("createpassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass createPassword(Map<String, String> data, @Context ContainerRequestContext context)
			throws Exception {
		try {
			JWT jwt = (JWT) context.getProperty("jwt");
			int userId = new VertAdminService().getUseridByName(jwt.getSub());
			String jti = jwt.getJti();

			String newPassword = data.get("password");
			return service.createPassword(newPassword, userId, jti, jwt.getTyp());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@CreatePassword
	@Path("checktokenexpiry")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass checkTokenExpiry(@Context ContainerRequestContext context) throws Exception {
		try {
			JWT jwt = (JWT) context.getProperty("jwt");
			int userId = new VertAdminService().getUseridByName(jwt.getSub());
			String jti = jwt.getJti();

			return service.checkTokenExpiry(userId, jti);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Insecured
	@Path("resendpasswordlink")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass resendPasswordLink(Map<String, String> data, @Context HttpHeaders httpHeaders)
			throws Exception {
		try {
			String origin = httpHeaders.getHeaderString("origin");
			String token = data.get("token");
			System.out.println(token);
			return service.resendPasswordLink(token, origin);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
}
