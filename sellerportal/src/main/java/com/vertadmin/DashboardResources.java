package com.vertadmin;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.vossa.api.others.ResponseClass;

@Path("")
public class DashboardResources {

	private DashboardServices service;

	public DashboardResources() {
		service = new DashboardServices();
	}

	@GET
	// @Secured
	@Path("getdashboarddetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getDashboardDetails() {
		try {
			return service.getDashboardDetails();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
}
