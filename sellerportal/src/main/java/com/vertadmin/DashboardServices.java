package com.vertadmin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import com.vossa.api.general.constants.Table;

public class DashboardServices extends OtherServices {

	public Map<String, Object> getDashboardDetails() throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnection()) {
			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.stores);
			rs = ps.executeQuery();
			rs.next();

			Map<String, Object> map = new HashMap<>();
			map.put("totalStores", rs.getInt("cnt"));

			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.users);
			rs = ps.executeQuery();
			rs.next();
			int userCount = rs.getInt("cnt");
			map.put("totalSellerAccounts", userCount);

			map.put("totalBuyerAccounts", 0);

			map.put("totalProductsInW", 0);

			return map;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}