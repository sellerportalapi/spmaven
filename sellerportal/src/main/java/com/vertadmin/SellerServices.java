package com.vertadmin;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.integration.IntegrationClient;
import com.stores.StoreServices;
import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.general.enums.PswdJwtType;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.DBConnection;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.SellerCodeConversion;
import com.vossa.api.others.encryption.AES;
import com.vossa.api.server.DataSourceFactory;
import com.vossa.api.v1.common.pojos.Address;
import com.vossa.api.v1.common.pojos.AwaitingSeller;
import com.vossa.api.v1.seller.pojos.Seller;
import com.vossa.api.v1.seller.pojos.SellingMode;
import com.vossa.api.v1.seller.pojos.Store;
import com.vossa.api.v1.seller.pojos.User;

public class SellerServices {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;

		return connection;
	}

	protected Connection getDBConnectionPool() throws Exception {
		return DataSourceFactory.getInstance().getConnection();
	}

	@ApiMethod(apiname = "getallsellers", method = "GET")
	public List<Seller> getAllSellers() throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.stores);
			rs = ps.executeQuery();
			List<Seller> sellerList = new ArrayList<Seller>();
			while (rs.next()) {
				int storeId = rs.getInt("storeId");
				String storeName = rs.getString("storeName");
				String sellerCode = SellerCodeConversion.convert(storeId);

				Seller seller = new Seller();

				Store stores = new Store();
				stores.setStoreId(storeId);
				stores.setSellerCode(sellerCode);
				stores.setStoreName(storeName);
				stores.setEmail(rs.getString("email"));
				stores.setPhoneNumber(rs.getString("phoneNumber"));
				stores.setAllowedUsersCount(rs.getInt("allowedUsersCount"));
				stores.setStoreStatus(rs.getString("storeStatus"));
				stores.setIsActivated(rs.getBoolean("isActivated"));
				stores.setActiveFrom(rs.getTimestamp("activeFrom").getTime());
				stores.setActiveTo(rs.getTimestamp("activeTo").getTime());
				stores.setMaximumProductSku(rs.getInt("maximumProductSku"));
				stores.setPreference(rs.getString("preference"));
				stores.setFulfillment(rs.getString("fulfillment"));
				stores.setImagePath(rs.getString("imagePath"));
				stores.setRevenue(rs.getString("revenue"));
				stores.setModeOfContact(rs.getString("modeOfContact"));
				stores.setTimeOfContact(rs.getString("timeOfContact"));
				stores.setOrderPickFrequency(rs.getString("orderPickFrequency"));
				stores.setAdditionalParameter1(rs.getString("additionalParameter1"));
				stores.setAdditionalParameter2(rs.getString("additionalParameter2"));
				stores.setAdditionalParameter3(rs.getString("additionalParameter3"));
				stores.setAllDataEntered(rs.getBoolean("allDataEntered"));
				stores.setAddress(rs.getString("address"));
				stores.setCity(rs.getString("city"));
				stores.setState(rs.getString("state"));
				stores.setCountry(rs.getString("country"));
				stores.setZipCode(rs.getString("zipCode"));

				PreparedStatement ps1 = connection.prepareStatement("SELECT * FROM " + Table.users + " T1 JOIN "
						+ Table.storeusermapper + " T2 ON T1.sellerId = T2.sellerId WHERE T2.storeId = ?");
				ps1.setInt(1, storeId);
				ResultSet rs1 = ps1.executeQuery();

				int activeUsersCount = 0;
				List<User> usersList = new ArrayList<>();
				while (rs1.next()) {
					int sellerId = rs1.getInt("sellerId");

					if (rs1.getString("sellerStatus").equals("ACTIVE"))
						activeUsersCount++;

					User users = new User();
					users.setSellerId(sellerId);
					users.setUserName(rs1.getString("userName"));
					users.setStoreName(storeName);
					users.setStoreId(storeId);
					users.setSellerCode(sellerCode);
					users.setName(rs1.getString("name"));
					users.setEmail(rs1.getString("email"));
					users.setPhoneNumber(rs1.getString("phoneNumber"));
					users.setUserType(rs1.getString("userType"));
					users.setSellerStatus(rs1.getString("sellerStatus"));
					users.setIsActivated(rs1.getBoolean("isActivated"));
					users.setFirstlogin(rs1.getBoolean("firstlogin"));
					users.setIsCreatedByVert(rs1.getBoolean("isCreatedByVert"));
					users.setFirstlogin(rs1.getBoolean("isFirstUser"));
					users.setNotAnUser(false);

					usersList.add(users);
				}
				ps1.close();
				rs1.close();

				stores.setActiveUsersCount(activeUsersCount);

				PreparedStatement ps2 = connection
						.prepareStatement("SELECT * FROM " + Table.sellingmode + " WHERE storeId = ?");
				ps2.setInt(1, storeId);
				ResultSet rs2 = ps2.executeQuery();

				List<SellingMode> sellingModeList = new ArrayList<>();
				while (rs2.next()) {
					SellingMode mode = new SellingMode();
					mode.setId(rs2.getInt("id"));
					mode.setStoreId(storeId);
					mode.setCurrentSellingMode(rs2.getString("currentSellingMode"));
					mode.setSellerSource(rs2.getString("sellerSource"));
					mode.setOrderPrefix(rs2.getString("orderPrefix"));
					mode.setSource(rs2.getString("source"));
					mode.setHostURL(rs2.getString("hostURL"));
					mode.setPort(rs2.getInt("port"));
					mode.setHostUserName(rs2.getString("hostUserName"));
					mode.setHostPassword(rs2.getString("hostPassword"));
					mode.setSFTPHostURL(rs2.getString("SFTPHostURL"));
					mode.setSFTPUserName(rs2.getString("SFTPUserName"));
					mode.setSFTPPassword(rs2.getString("SFTPPassword"));
					mode.setSFTPFolderPath(rs2.getString("SFTPFolderPath"));
					mode.setSFTPKeyPath(rs2.getString("SFTPKeyPath"));
					mode.setAPIKey(rs2.getString("APIKey"));

					sellingModeList.add(mode);
				}
				ps2.close();
				rs2.close();
				seller.setStore(stores);
				seller.setUsers(usersList);
				seller.setSellingMode(sellingModeList);
				sellerList.add(seller);
			}
			ps.close();
			rs.close();
			return sellerList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getawaitingsellers", method = "GET")
	public List<Seller> getAwaitingSellers() throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.contactvert);
			rs = ps.executeQuery();

			List<AwaitingSeller> awaitingSellerList = new ArrayList<>();
			while (rs.next()) {
				int id = rs.getInt("id");

				AwaitingSeller awaitingSeller = new AwaitingSeller();
				awaitingSeller.setId(id);
				awaitingSeller.setCompanyName(rs.getString("companyName"));
				awaitingSeller.setName(rs.getString("name"));
				awaitingSeller.setEmail(rs.getString("email"));
				awaitingSeller.setPhoneNumber(rs.getString("phoneNumber"));
				awaitingSeller.setRevenue(rs.getString("revenue"));
				awaitingSeller.setMaximumProductSku(rs.getInt("maximumProductSku"));
				awaitingSeller.setSellingMode(rs.getString("sellingMode"));
				awaitingSeller.setModeOfContact(rs.getString("modeOfContact"));
				awaitingSeller.setTimeOfContact(rs.getString("timeOfContact"));
				awaitingSeller.setPreference(rs.getString("preference"));

				awaitingSellerList.add(awaitingSeller);
			}
			ps.close();
			rs.close();

			List<Seller> sellerList = new ArrayList<>();
			for (AwaitingSeller awaitingSeller : awaitingSellerList) {
				Seller seller = new Seller();

				Store store = new Store();
				store.setStoreName(awaitingSeller.getCompanyName());
				store.setEmail(awaitingSeller.getEmail());
				store.setPhoneNumber(awaitingSeller.getPhoneNumber());
				store.setRevenue(awaitingSeller.getRevenue());
				store.setMaximumProductSku(awaitingSeller.getMaximumProductSku());
				store.setModeOfContact(awaitingSeller.getModeOfContact());
				store.setTimeOfContact(awaitingSeller.getTimeOfContact());

				store.setAwaitingSellerId(awaitingSeller.getId());
				store.setNotASeller(true);

				List<User> UsersList = new ArrayList<>();

				List<SellingMode> SellingModeList = new ArrayList<>();
				SellingMode mode = new SellingMode();
				mode.setCurrentSellingMode(awaitingSeller.getSellingMode());
				SellingModeList.add(mode);

				seller.setStore(store);
				seller.setUsers(UsersList);
				seller.setSellingMode(SellingModeList);
				sellerList.add(seller);
			}
			return sellerList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getactivesellers", method = "GET")
	public List<Seller> getActiveSellers() throws Exception {
		try (Connection connection = getDBConnection()) {
			List<Seller> allSellerList = getAllSellers();
			List<Seller> activeSellerList = new ArrayList<>();
			for (Seller seller : allSellerList) {
				if (seller.getStore().getStoreStatus().equals("ACTIVE")) {
					activeSellerList.add(seller);
				}
			}
			return activeSellerList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getinactivesellers", method = "GET")
	public List<Seller> getInActiveSellers() throws Exception {
		try (Connection connection = getDBConnection()) {
			List<Seller> allSellerList = getAllSellers();
			List<Seller> activeSellerList = new ArrayList<>();
			for (Seller seller : allSellerList) {
				if (seller.getStore().getStoreStatus().equals("INACTIVE")) {
					activeSellerList.add(seller);
				}
			}
			return activeSellerList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "createseller", method = "POST")
	public ResponseClass createSeller(Seller seller, String createdBy) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);

			Store store = seller.getStore();
			List<User> userList = seller.getUsers();
			List<SellingMode> sellingModeList = seller.getSellingMode();

			if (store == null || userList == null || sellingModeList == null) {
				return new ResponseClass(400, "fail", "Invalid JSON input",
						"JSON key store, users, or sellingMode shouldn't be null");
			}
			if (userList.size() < 1 || sellingModeList.size() < 1) {
				return new ResponseClass(400, "fail", "Invalid JSON input",
						"Atleast 1 user and 1 selling mode should be given.");
			}

			int firstUserCount = 0;
			boolean valid = false;
			for (User user : userList) {
				if (user.getIsFirstUser() != null && user.getIsFirstUser() == true) {
					firstUserCount++;
					if (user.getUserType().equals(UserType.SUPERUSER.toString()))
						valid = true;
				}
			}
			if (firstUserCount != 1) {
				return new ResponseClass(400, "fail", "Invalid JSON input", "Should be only one FirstUser");
			}
			if (valid == false) {
				return new ResponseClass(400, "fail", "Invalid JSON input", "FirstUser should be a SUPERUSER");
			}

			int storeid = createStore(store, createdBy, connection);

			for (User user : userList) {
				String sellerStatus;
				boolean isFirstuser;
				if (user.getIsFirstUser() != null && user.getIsFirstUser() == true) {
					sellerStatus = "ACTIVE";
					isFirstuser = true;
				} else {
					sellerStatus = "INACTIVE";
					isFirstuser = false;
				}

				createUser(user, storeid, sellerStatus, isFirstuser, createdBy, connection);
			}

			for (SellingMode mode : sellingModeList) {
				createSellingMode(mode, storeid, connection);
			}

			Address address = getAddress(store.getZipCode());
			address.setAddress(store.getAddress());
			store.setStoreAddress(address);
			store.setSellerCode(SellerCodeConversion.convert(storeid));

			IntegrationClient integration = new IntegrationClient();
			integration.sendSellerMaster(seller);

			connection.commit();
			return new ResponseClass(201, "success", "New Seller successfully created or registered");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int createStore(Store store, String createdBy, Connection connection) throws Exception {
		try {
			boolean validUniqueData;
			validUniqueData = checkFieldStore("storeName", store.getStoreName());
			if (validUniqueData == false) {
				throw new Exception("StoreName already exists");
			}

			validUniqueData = checkFieldStore("email", store.getEmail());
			if (validUniqueData == false) {
				throw new Exception("Store Email already exists");
			}

			validUniqueData = checkFieldStore("phoneNumber", store.getPhoneNumber());
			if (validUniqueData == false) {
				throw new Exception("Store PhoneNumber already exists");
			}

			PreparedStatement ps;
			ps = connection.prepareStatement("INSERT INTO " + Table.stores
					+ " ( storeName, email, phoneNumber, allowedUsersCount, activeFrom, activeTo,"
					+ " maximumProductSku, preference, fulfillment, imagePath, revenue, modeOfContact, timeOfContact,"
					+ " orderPickFrequency, additionalParameter1, additionalParameter2, additionalParameter3,"
					+ " allDataEntered, address, city, state, country, zipCode, createdBy, modifiedBy)"
					+ " values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			// storeStatus default INACTIVE
			int i = 0;
			ps.setString(++i, store.getStoreName());
			ps.setString(++i, store.getEmail());
			ps.setString(++i, store.getPhoneNumber());
			ps.setInt(++i, store.getAllowedUsersCount());
			ps.setTimestamp(++i, new Timestamp(store.getActiveFrom()));
			ps.setTimestamp(++i, new Timestamp(store.getActiveTo()));
			ps.setInt(++i, store.getMaximumProductSku());
			ps.setString(++i, store.getPreference());
			ps.setString(++i, store.getFulfillment());
			ps.setString(++i, store.getImagePath());
			ps.setString(++i, store.getRevenue());
			ps.setString(++i, store.getModeOfContact());
			ps.setString(++i, store.getTimeOfContact());
			ps.setString(++i, store.getOrderPickFrequency());
			ps.setString(++i, store.getAdditionalParameter1());
			ps.setString(++i, store.getAdditionalParameter2());
			ps.setString(++i, store.getAdditionalParameter3());
			ps.setBoolean(++i, true); // allDataEntered default true, because all mandatory fields should be given.
			ps.setString(++i, store.getAddress());
			ps.setString(++i, store.getCity());
			ps.setString(++i, store.getState());
			ps.setString(++i, store.getCountry());
			ps.setString(++i, store.getZipCode());
			ps.setString(++i, createdBy);
			ps.setString(++i, createdBy);
			ps.execute();
			ps.close();
			System.out.println("New Store Created");

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT storeId FROM " + Table.stores + " WHERE storeName = ?");
			ps1.setString(1, store.getStoreName());
			rs1 = ps1.executeQuery();
			rs1.next();
			int storeid = rs1.getInt("storeId");
			ps1.close();
			rs1.close();

			if (store.getNotASeller() != null && store.getNotASeller() == true) {
				if (store.getAwaitingSellerId() != null) {
					deleteFromContactvert(store.getAwaitingSellerId(), connection);
				} else
					throw new Exception("AwaitingSellerId should be given");
			}

			return storeid;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int createUser(User user, int storeid, String sellerStatus, boolean isFirstUser, String createdBy,
			Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			boolean validUniqueData;
			validUniqueData = checkFieldUser("userName", user.getUserName());
			if (validUniqueData == false) {
				throw new Exception("UserName already exists");
			}

			validUniqueData = checkFieldUser("email", user.getEmail());
			if (validUniqueData == false) {
				throw new Exception("User Email already exists");
			}

			validUniqueData = checkFieldUser("phoneNumber", user.getPhoneNumber());
			if (validUniqueData == false) {
				throw new Exception("User PhoneNumber already exists");
			}

			String cipherPassword = AES.encrypt(randomAlphanumeric(12));

			ps = connection.prepareStatement("INSERT INTO " + Table.users
					+ " (userName, password, name, email, phoneNumber, userType, sellerStatus,"
					+ " isActivated, isCreatedByVert, isFirstUser, createdBy, modifiedBy)"
					+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			int i = 0;
			ps.setString(++i, user.getUserName());
			ps.setString(++i, cipherPassword);
			ps.setString(++i, user.getName());
			ps.setString(++i, user.getEmail());
			ps.setString(++i, user.getPhoneNumber());
			ps.setString(++i, user.getUserType());
			ps.setString(++i, sellerStatus);
			if (sellerStatus.equals("ACTIVE"))
				ps.setBoolean(++i, true);
			else
				ps.setBoolean(++i, false);

			ps.setBoolean(++i, true); // isCreatedByVert is false
			ps.setBoolean(++i, isFirstUser);
			ps.setString(++i, createdBy);
			ps.setString(++i, createdBy);
			ps.execute();
			ps.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT sellerId FROM " + Table.users + " WHERE userName = ?");
			ps1.setString(1, user.getUserName());
			rs1 = ps1.executeQuery();
			rs1.next();
			int sellerid = rs1.getInt("sellerId");
			ps1.close();
			rs1.close();

			PreparedStatement ps2;
			ps2 = connection.prepareStatement("INSERT INTO " + Table.storeusermapper + " VALUES (?, ?)");
			ps2.setInt(1, sellerid);
			ps2.setInt(2, storeid);
			ps2.execute();
			ps2.close();

			PreparedStatement ps3;
			ps3 = connection.prepareStatement("INSERT INTO " + Table.userrecentpassword + " VALUES (?, ?, ?, ?)");
			ps3.setInt(1, sellerid);
			ps3.setString(2, cipherPassword);
			ps3.setString(3, cipherPassword);
			ps3.setString(4, cipherPassword);
			ps3.execute();
			ps3.close();

			PreparedStatement ps4;
			ps4 = connection.prepareStatement("INSERT INTO " + Table.userlogindetails + " VALUES (?, ?, ?, ?, ?, ?)");
			ps4.setInt(1, sellerid);
			ps4.setInt(2, 0);
			ps4.setTimestamp(3, null);
			ps4.setTimestamp(4, null);
			ps4.setTimestamp(5, null);
			ps4.setTimestamp(6, null);
			ps4.execute();
			ps4.close();
			System.out.println("New " + user.getUserType() + " Created");

			return sellerid;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int createSellingMode(SellingMode mode, int storeid, Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			ps = connection.prepareStatement("INSERT INTO " + Table.sellingmode
					+ " (storeId, currentSellingMode, sellerSource, orderPrefix, source, hostURL, port, hostUserName, hostPassword,"
					+ " SFTPHostURL, SFTPUserName, SFTPPassword, SFTPFolderPath, SFTPKeyPath, APIKey)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			ps.setInt(1, storeid);
			ps.setString(2, mode.getCurrentSellingMode());
			ps.setString(3, mode.getSellerSource());
			ps.setString(4, mode.getOrderPrefix());
			ps.setString(5, mode.getSource());
			ps.setString(6, mode.getHostURL());
			ps.setInt(7, mode.getPort());
			ps.setString(8, mode.getHostUserName());
			ps.setString(9, mode.getHostPassword());
			ps.setString(10, mode.getSFTPHostURL());
			ps.setString(11, mode.getSFTPUserName());
			ps.setString(12, mode.getSFTPPassword());
			ps.setString(13, mode.getSFTPFolderPath());
			ps.setString(14, mode.getSFTPKeyPath());
			ps.setString(15, mode.getAPIKey());

			ps.execute();
			ps.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT LAST_INSERT_ID() AS id FROM " + Table.stores);
			rs1 = ps1.executeQuery();
			rs1.next();
			int id = rs1.getInt("id");
			ps1.close();
			rs1.close();

			return id;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "updateseller", method = "PUT")
	public ResponseClass updateSeller(Seller seller, String modifiedBy) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);

			Store store = seller.getStore();
			List<User> userList = seller.getUsers();
			List<SellingMode> sellingModeList = seller.getSellingMode();

			int storeid = updateStore(store, modifiedBy, connection);

			for (User user : userList) {
				if (user.getSellerId() == null)
					createUser(user, storeid, "INACTIVE", false, modifiedBy, connection);
				else
					updateUser(user, modifiedBy);
			}

			for (SellingMode mode : sellingModeList) {
				if (mode.getId() == null)
					createSellingMode(mode, storeid, connection);
				else
					updateSellingMode(mode, storeid, connection);
			}

			connection.commit();
			return new ResponseClass(200, "success", "Seller successfully updated");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int updateStore(Store store, String modifiedBy, Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			ResultSet rs;
			int storeid = store.getStoreId();

			boolean validUniqueData;
			validUniqueData = checkFieldStoreOnUpdate(storeid, "email", store.getEmail());
			if (validUniqueData == false) {
				throw new Exception("Store Email already exists");
			}

			validUniqueData = checkFieldStoreOnUpdate(storeid, "phoneNumber", store.getPhoneNumber());
			if (validUniqueData == false) {
				throw new Exception("Store PhoneNumber already exists");
			}

			int allowedUsersCount = store.getAllowedUsersCount();
			// validation for decreasing allowedUsersCount
			// (since what if we have more active users in that store)
			ps = connection.prepareStatement("SELECT COUNT(*) AS activeUsersCount FROM " + Table.users + " T1 JOIN "
					+ Table.storeusermapper + " T2 ON T1.sellerId = T2.sellerId JOIN " + Table.stores
					+ " T3 ON T2.storeId = T3.storeId" + " WHERE T2.storeId = ? AND T1.sellerStatus = 'ACTIVE'");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			rs.next();
			int activeUsersCount = rs.getInt("activeUsersCount");
			ps.close();
			rs.close();

			if (allowedUsersCount < activeUsersCount) {
				throw new Exception("Already " + activeUsersCount + " Active users." + " Please deactivate "
						+ (activeUsersCount - allowedUsersCount) + " more users.");
			}

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("UPDATE " + Table.stores
					+ " SET email = ?, phoneNumber = ?, allowedUsersCount = ?, activeFrom = ?, activeTo = ?,"
					+ " maximumProductSku = ?, preference = ?, fulfillment = ?, imagePath = ?, revenue = ?, modeOfContact = ?, timeOfContact = ?,"
					+ " orderPickFrequency = ?, additionalParameter1 = ?, additionalParameter2 = ?, additionalParameter3 = ?,"
					+ " address = ?, city = ?, state = ?, country = ?, zipCode = ?, modifiedBy = ?"
					+ " WHERE storeId = ?");

			int i = 0;
			ps1.setString(++i, store.getEmail());
			ps1.setString(++i, store.getPhoneNumber());
			ps1.setInt(++i, store.getAllowedUsersCount());
			ps1.setTimestamp(++i, new Timestamp(store.getActiveFrom()));
			ps1.setTimestamp(++i, new Timestamp(store.getActiveTo()));
			ps1.setInt(++i, store.getMaximumProductSku());
			ps1.setString(++i, store.getPreference());
			ps1.setString(++i, store.getFulfillment());
			ps1.setString(++i, store.getImagePath());
			ps1.setString(++i, store.getRevenue());
			ps1.setString(++i, store.getModeOfContact());
			ps1.setString(++i, store.getTimeOfContact());
			ps1.setString(++i, store.getOrderPickFrequency());
			ps1.setString(++i, store.getAdditionalParameter1());
			ps1.setString(++i, store.getAdditionalParameter2());
			ps1.setString(++i, store.getAdditionalParameter3());
			ps1.setString(++i, store.getAddress());
			ps1.setString(++i, store.getCity());
			ps1.setString(++i, store.getState());
			ps1.setString(++i, store.getCountry());
			ps1.setString(++i, store.getZipCode());
			ps1.setString(++i, modifiedBy);
			ps1.setInt(++i, storeid);
			ps1.executeUpdate();
			ps1.close();

			return storeid;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int updateUser(User user, String modifiedBy) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;

			int userid = user.getSellerId();

			boolean validUniqueData;
			validUniqueData = checkFieldUserOnUpdate(userid, "email", user.getEmail());
			if (validUniqueData == false) {
				throw new Exception("User Email already exists");
			}

			validUniqueData = checkFieldUserOnUpdate(userid, "phoneNumber", user.getPhoneNumber());
			if (validUniqueData == false) {
				throw new Exception("User PhoneNumber already exists");
			}

			ps = connection.prepareStatement(
					"UPDATE " + Table.users + " SET name = ?, email = ?, phoneNumber = ?" + " WHERE sellerId = ?");
			ps.setString(1, user.getName());
			ps.setString(2, user.getEmail());

			if (user.getPhoneNumber() != null && user.getPhoneNumber().isEmpty())
				ps.setString(3, null);
			else
				ps.setString(3, user.getPhoneNumber());
			ps.setInt(4, userid);
			ps.executeUpdate();
			ps.close();

			return userid;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int updateSellingMode(SellingMode mode, int storeid, Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			int id = mode.getId();

			ps = connection.prepareStatement("UPDATE " + Table.sellingmode
					+ " SET currentSellingMode = ?, sellerSource = ?, orderPrefix = ?, source = ?,"
					+ " hostURL = ?, port = ?, hostUserName = ?, hostPassword = ?,"
					+ " SFTPHostURL = ?, SFTPUserName = ?, SFTPPassword = ?, SFTPFolderPath = ?, SFTPKeyPath = ?, APIKey = ?"
					+ " WHERE id = ? AND storeId = ?");

			int i = 0;
			ps.setString(++i, mode.getCurrentSellingMode());
			ps.setString(++i, mode.getSellerSource());
			ps.setString(++i, mode.getOrderPrefix());
			ps.setString(++i, mode.getSource());
			ps.setString(++i, mode.getHostURL());
			ps.setInt(++i, mode.getPort());
			ps.setString(++i, mode.getHostUserName());
			ps.setString(++i, mode.getHostPassword());
			ps.setString(++i, mode.getSFTPHostURL());
			ps.setString(++i, mode.getSFTPUserName());
			ps.setString(++i, mode.getSFTPPassword());
			ps.setString(++i, mode.getSFTPFolderPath());
			ps.setString(++i, mode.getSFTPKeyPath());
			ps.setString(++i, mode.getAPIKey());
			ps.setInt(++i, id);
			ps.setInt(++i, storeid);

			ps.executeUpdate();
			ps.close();

			return id;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "updatestorestatus", method = "PUT")
	public ResponseClass updateStoreStatus(int storeid, String storeStatus, String modifiedBy, String origin) {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;

			if (!storeStatus.equals("ACTIVE") && !storeStatus.equals("INACTIVE"))
				return new ResponseClass(400, "fail", "Invalid JSON input", "Invalid storeStatus");

			ps = connection.prepareStatement("SELECT COUNT(*) AS activeUsersCount FROM " + Table.users + " T1 JOIN "
					+ Table.storeusermapper + " T2 ON T1.sellerId = T2.sellerId JOIN " + Table.stores
					+ " T3 ON T2.storeId = T3.storeId" + " WHERE T2.storeId = ? AND T1.sellerStatus = 'ACTIVE'");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			rs.next();
			int activeUsersCount = rs.getInt("activeUsersCount");
			ps.close();
			rs.close();

			if (storeStatus.equals("ACTIVE")) {
				if (activeUsersCount < 1) {
					String errmsg = "Atleast 1 user should be active to activate a store.";
					return new ResponseClass(409, "fail", errmsg, "Invalid user activation");
				}

				checkStoreActivation(storeid, origin);
			}

			PreparedStatement ps1;
			ps1 = connection.prepareStatement(
					"UPDATE " + Table.stores + " SET storeStatus = ?, modifiedBy = ?" + " WHERE storeId = ?");
			ps1.setString(1, storeStatus);
			ps1.setString(2, modifiedBy);
			ps1.setInt(3, storeid);

			ps1.execute();
			ps1.close();

			return new ResponseClass(201, "success", "STORE_STATUS has been updated successfully");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	@ApiMethod(apiname = "updateuserstatus", method = "PUT")
	public ResponseClass updateUserStatus(int userid, String sellerStatus, String modifiedBy, String origin) {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			if (!sellerStatus.equals("ACTIVE") && !sellerStatus.equals("INACTIVE"))
				return new ResponseClass(400, "fail", "Invalid JSON input", "Invalid sellerStatus");

			ps = connection.prepareStatement("SELECT storeId FROM " + Table.storeusermapper + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();
			int storeid = rs.getInt("storeId");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection
					.prepareStatement("SELECT COUNT(*) AS activeUsersCount, T3.allowedUsersCount, T3.storeStatus FROM "
							+ Table.users + " T1 JOIN " + Table.storeusermapper + " T2 ON T1.sellerId = T2.sellerId"
							+ " JOIN " + Table.stores + " T3 ON T2.storeId = T3.storeId"
							+ " WHERE T2.storeId = ? AND T1.sellerStatus = 'ACTIVE'");
			ps1.setInt(1, storeid);
			rs1 = ps1.executeQuery();
			rs1.next();
			int activeUsersCount = rs1.getInt("activeUsersCount");
			int allowedUsersCount = rs1.getInt("allowedUsersCount");
			String storeStatus = rs1.getString("storeStatus");
			ps1.close();
			rs1.close();

			if (!storeStatus.equals("ACTIVE")) {
				String errmsg = "Store is INACTIVE.";
				return new ResponseClass(409, "fail", errmsg, "Invalid user activation");
			}

			PreparedStatement ps2;
			ResultSet rs2;
			ps2 = connection.prepareStatement("SELECT * FROM " + Table.users + " WHERE sellerId = ?");
			ps2.setInt(1, userid);
			rs2 = ps2.executeQuery();
			rs2.next();
			String userType = rs2.getString("userType");
			ps2.close();
			rs2.close();

			if (sellerStatus.equals("ACTIVE") && userType.equals(UserType.STANDARDUSER.toString())) {
				PreparedStatement ps3;
				ResultSet rs3;
				ps3 = connection.prepareStatement(
						"SELECT COUNT(*) AS assignedRolesCount FROM " + Table.assignednames + " WHERE sellerId = ?");
				ps3.setInt(1, userid);
				rs3 = ps3.executeQuery();
				rs3.next();
				int assignedRolesCount = rs3.getInt("assignedRolesCount");
				ps3.close();
				rs3.close();

				if (assignedRolesCount == 0) {
					String errmsg = "No roles asigned for this STANDARDUSER";
					return new ResponseClass(409, "fail", errmsg, "Invalid user activation");
				}
			}

			if (sellerStatus.equals("ACTIVE")) {
				if (activeUsersCount + 1 > allowedUsersCount) {
					String errmsg = "License limit exceeded.";
					return new ResponseClass(409, "fail", errmsg, "Invalid user activation");
				}

				checkUserActivation(userid, origin);
			} else if (sellerStatus.equals("INACTIVE")) {
				if (activeUsersCount - 1 < 1) {
					String errmsg = "Atleast one user should be active in a store.";
					return new ResponseClass(409, "fail", errmsg, "Invalid user deactivation");
				}
			}

			PreparedStatement ps4;
			ps4 = connection.prepareStatement(
					"UPDATE " + Table.users + " SET sellerStatus = ?, modifiedBy = ?" + " WHERE sellerId = ?");
			ps4.setString(1, sellerStatus);
			ps4.setString(2, modifiedBy);
			ps4.setInt(3, userid);
			ps4.executeUpdate();
			ps4.close();

			return new ResponseClass(201, "success", "USER_STATUS has been updated successfully");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	@ApiMethod(apiname = "checkfieldstore", method = "POST")
	public boolean checkFieldStore(String key, String value) throws Exception {
		int count = 0;
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT COUNT(*) as cnt FROM " + Table.stores + " WHERE " + key + " = ?");
			ps.setString(1, value);
			rs = ps.executeQuery();
			rs.next();
			count = rs.getInt("cnt");

			rs.close();
			ps.close();
			return (count == 0) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public boolean checkFieldStoreOnUpdate(int storeid, String key, String value) throws Exception {
		int count = 0;
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT COUNT(*) as cnt FROM " + Table.stores + " WHERE storeId <> ? AND " + key + " = ?");
			ps.setInt(1, storeid);
			ps.setString(2, value);
			rs = ps.executeQuery();
			rs.next();
			count = rs.getInt("cnt");

			rs.close();
			ps.close();
			return (count == 0) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "checkfielduser", method = "POST")
	public boolean checkFieldUser(String key, String value) throws Exception {
		int count = 0;
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT COUNT(*) as cnt FROM " + Table.users + " WHERE " + key + " = ?");
			ps.setString(1, value);
			rs = ps.executeQuery();
			rs.next();
			count += rs.getInt("cnt");

			rs.close();
			ps.close();
			return (count == 0) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public boolean checkFieldUserOnUpdate(int userid, String key, String value) throws Exception {
		int count = 0;
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT COUNT(*) as cnt FROM " + Table.users + " WHERE sellerId <> ? AND " + key + " = ?");
			ps.setInt(1, userid);
			ps.setString(2, value);
			rs = ps.executeQuery();
			rs.next();
			count += rs.getInt("cnt");

			rs.close();
			ps.close();
			return (count == 0) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void checkStoreActivation(int storeid, String origin) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.stores + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			rs.next();

			if (rs.getBoolean("isActivated") == false) {
				PreparedStatement ps1;
				ps1 = connection
						.prepareStatement("UPDATE " + Table.stores + " SET isActivated = ?" + " WHERE storeId = ?");
				ps1.setBoolean(1, true);
				ps1.setInt(2, storeid);
				ps1.executeUpdate();
				ps1.close();

				PreparedStatement ps2;
				ResultSet rs2;

				ps2 = connection.prepareStatement("SELECT T1.* FROM " + Table.users + " T1 JOIN "
						+ Table.storeusermapper + " T3 ON T1.sellerId = T3.sellerId JOIN " + Table.stores
						+ " T2 ON T2.storeId = T3.storeId" + " WHERE T2.storeId = ? AND T1.isFirstUser = 1");
				ps2.setInt(1, storeid);
				rs2 = ps2.executeQuery();
				rs2.next();

				int userid = rs2.getInt("sellerId");
				String username = rs2.getString("userName");
				String email = rs2.getString("email");

				ps2.close();
				rs2.close();

				User user = new User();
				user.setSellerId(userid);
				user.setUserName(username);
				user.setEmail(email);

				new StoreServices().createNewPasswordLink(user, origin, PswdJwtType.JWT_CP);
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		}
	}

	public void checkUserActivation(int userid, String origin) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.users + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();

			if (rs.getBoolean("isActivated") == false) {
				PreparedStatement ps1;
				ps1 = connection
						.prepareStatement("UPDATE " + Table.users + " SET isActivated = ?" + " WHERE sellerId = ?");
				ps1.setBoolean(1, true);
				ps1.setInt(2, userid);
				ps1.executeUpdate();
				ps1.close();

				String username = rs.getString("userName");
				String email = rs.getString("email");

				User user = new User();
				user.setSellerId(userid);
				user.setUserName(username);
				user.setEmail(email);

				new StoreServices().createNewPasswordLink(user, origin, PswdJwtType.JWT_CP);
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		}
	}

	public void deleteFromContactvert(int id, Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			ps = connection.prepareStatement("DELETE FROM " + Table.contactvert + " WHERE id = ?");
			ps.setInt(1, id);
			ps.execute();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getaddress", method = "POST")
	public Address getAddress(String zipcode) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			Address address = new Address();

			ps = connection.prepareStatement("SELECT cityName, stateCode FROM cities " + " WHERE zipCode LIKE ?");
			ps.setString(1, zipcode);
			rs = ps.executeQuery();
			if (!rs.next()) {
				address.setValidZip(false);
				ps.close();
				rs.close();
				return address;
			}

			address.setZipCode(zipcode);
			address.setCityName(rs.getString("cityName"));
			address.setStateCode(rs.getString("stateCode"));
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT stateName, countryCode FROM states " + " WHERE stateCode = ?");
			ps1.setString(1, address.getStateCode());
			rs1 = ps1.executeQuery();

			if (!rs1.next()) {
				ps1.close();
				rs1.close();
				return null;
			}
			address.setStateName(rs1.getString("stateName"));
			address.setCountryCode(rs1.getString("countryCode"));
			ps1.close();
			rs1.close();

			PreparedStatement ps2;
			ResultSet rs2;
			ps2 = connection.prepareStatement("SELECT countryName FROM countries " + " WHERE countryCode = ?");
			ps2.setString(1, address.getCountryCode());
			rs2 = ps2.executeQuery();

			if (!rs2.next()) {
				ps2.close();
				rs2.close();
				return null;
			}

			address.setCountryName(rs2.getString("countryName"));
			address.setValidZip(true);
			ps2.close();
			rs2.close();

			return address;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String randomAlphanumeric(int length) {
		final String alphanumericText = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom random = new SecureRandom();

		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(alphanumericText.charAt(random.nextInt(alphanumericText.length())));
		return sb.toString();
	}
}