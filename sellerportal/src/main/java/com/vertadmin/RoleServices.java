package com.vertadmin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vossa.api.general.constants.Table;
import com.vossa.api.others.DBConnection;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.server.DataSourceFactory;
import com.vossa.api.v1.vertadmin.pojos.VertFeature;
import com.vossa.api.v1.vertadmin.pojos.VertRole;

public class RoleServices {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;

		return connection;
	}

	protected Connection getDBConnectionPool() throws Exception {
		return DataSourceFactory.getInstance().getConnection();
	}

	public List<VertFeature> getMetadata() throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.vertfeatures + " WHERE parent_id IS NULL");
			rs = ps.executeQuery();

			List<VertFeature> featureMeta = new ArrayList<>();
			while (rs.next()) {
				VertFeature feature = new VertFeature();
				feature.setFeature_id(rs.getInt("feature_id"));
				feature.setFeature(rs.getString("feature"));
				feature.setTitle(rs.getString("title"));
				if (rs.getInt("parent_id") == 0)
					feature.setParent_id(null); // if parent_id is 0 means its a main feature.
				else
					feature.setParent_id(rs.getInt("parent_id"));
				feature.setNone(rs.getInt("none"));
				feature.setView(rs.getInt("view"));
				feature.setView_edit(rs.getInt("view_edit"));
				feature.setSelected_value(1); // Default selected_value will be 1

				List<VertFeature> sub_features = getSubfeaturesRecursive(feature.getFeature_id());
				if (!sub_features.isEmpty())
					feature.setSub_features(sub_features);

				featureMeta.add(feature);
			}
			ps.close();
			rs.close();
			return featureMeta;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	protected List<VertFeature> getSubfeaturesRecursive(int feature_id) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.vertfeatures + " WHERE parent_id = ?");
			ps.setInt(1, feature_id);
			rs = ps.executeQuery();

			List<VertFeature> featureList = new ArrayList<>();
			while (rs.next()) {
				VertFeature feature = new VertFeature();
				feature.setFeature_id(rs.getInt("feature_id"));
				feature.setFeature(rs.getString("feature"));
				feature.setTitle(rs.getString("title"));
				feature.setParent_id(rs.getInt("parent_id"));
				feature.setNone(rs.getInt("none"));
				feature.setView(rs.getInt("view"));
				feature.setView_edit(rs.getInt("view_edit"));
				feature.setSelected_value(1); // Default selected_value will be 1

				List<VertFeature> sub_features = getSubfeaturesRecursive(feature.getFeature_id());
				if (!sub_features.isEmpty())
					feature.setSub_features(sub_features);

				featureList.add(feature);
			}
			ps.close();
			rs.close();
			return featureList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<VertRole> getAllRoles() throws Exception {
		try (Connection connection = getDBConnectionPool()) {
//			List<VertFeature> featureMeta = getMetadata();

			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.vertroles);
			rs = ps.executeQuery();

			List<VertRole> roles = new ArrayList<>();
			while (rs.next()) {
				VertRole role = new VertRole();
				role.setRole_id(rs.getInt("role_id"));
				role.setRole_name(rs.getString("role_name"));
				role.setDescription(rs.getString("description"));

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement("SELECT * FROM " + Table.vertselectedfeatures + " WHERE role_id = ?");
				ps1.setInt(1, role.getRole_id());
				rs1 = ps1.executeQuery();

				Map<Integer, Integer> map = new HashMap<>();
				while (rs1.next()) {
					map.put(rs1.getInt("feature_id"), rs1.getInt("selected_value"));
				}
				ps1.close();
				rs1.close();

				List<VertFeature> features = getMetadata();
				setSelected_valueRecursive(features, map); // set the selected_value to all features in the list

				role.setFeatures(features);
				roles.add(role);
			}
			ps.close();
			rs.close();
			return roles;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	protected void setSelected_valueRecursive(List<VertFeature> features, Map<Integer, Integer> map) throws Exception {
		if (features == null || features.isEmpty())
			return;

		for (VertFeature feature : features) {
			if (map.get(feature.getFeature_id()) != null)
				feature.setSelected_value(map.get(feature.getFeature_id()));
			setSelected_valueRecursive(feature.getSub_features(), map);
		}
	}

	public ResponseClass createRole(VertRole role, String createdBy) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);

			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.vertroles + " WHERE role_name = ?");
			ps.setString(1, role.getRole_name());
			rs = ps.executeQuery();
			rs.next();
			int cnt = rs.getInt("cnt");
			ps.close();
			rs.close();
			if (cnt > 0) {
				return new ResponseClass(400, "fail", "Rolename '" + role.getRole_name() + "' already exists.", null);
			}

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("INSERT INTO " + Table.vertroles
					+ " (role_name, description, createdBy, modifiedBy)" + " VALUES(?, ?, ?, ?)");
			ps1.setString(1, role.getRole_name());
			ps1.setString(2, role.getDescription());
			ps1.setString(3, createdBy);
			ps1.setString(4, createdBy);
			ps1.execute();
			ps1.close();

			PreparedStatement ps2;
			ResultSet rs2;
			ps2 = connection.prepareStatement("SELECT role_id FROM " + Table.vertroles + " WHERE role_name = ?");
			ps2.setString(1, role.getRole_name());
			rs2 = ps2.executeQuery();
			rs2.next();
			int role_id = rs2.getInt("role_id");
			ps2.close();
			rs2.close();

			// parent_value for first iteration will be 3, since its the largest permission
			insertFeaturesRecursive(role_id, role.getFeatures(), 3, connection);
			connection.commit();
			return new ResponseClass(201, "success", "New Role '" + role.getRole_name() + "' successfully created.");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ResponseClass updateRole(VertRole role, String modifiedBy) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);

			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.vertroles + " WHERE role_id = ?");
			ps.setInt(1, role.getRole_id());
			rs = ps.executeQuery();
			rs.next();
			int cnt = rs.getInt("cnt");
			ps.close();
			rs.close();
			if (cnt == 0) {
				return new ResponseClass(400, "fail", "Invalid role_id '" + role.getRole_id() + "'.", null);
			}

			PreparedStatement ps1;
			ResultSet rs2;
			ps1 = connection.prepareStatement(
					"SELECT COUNT(*) AS cnt FROM " + Table.vertroles + " WHERE role_name = ? AND role_id <> ?");
			ps1.setString(1, role.getRole_name());
			ps1.setInt(2, role.getRole_id());
			rs2 = ps1.executeQuery();
			rs2.next();
			cnt = rs2.getInt("cnt");
			ps1.close();
			rs2.close();
			if (cnt > 0) {
				return new ResponseClass(400, "fail", "Rolename '" + role.getRole_name() + "' already exists.", null);
			}

			PreparedStatement ps2;
			ps2 = connection.prepareStatement("UPDATE " + Table.vertroles
					+ " SET role_name = ?, description = ?, modifiedBy = ?, modifiedOn = ?" + " WHERE role_id = ?");
			ps2.setString(1, role.getRole_name());
			ps2.setString(2, role.getDescription());
			ps2.setString(3, modifiedBy);
			ps2.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			ps2.setInt(5, role.getRole_id());
			ps2.executeUpdate();
			ps2.close();

			// parent_value for first iteration will be 3, since its the largest permission
			insertFeaturesRecursive(role.getRole_id(), role.getFeatures(), 3, connection);
			connection.commit();
			return new ResponseClass(201, "success", "Role '" + role.getRole_name() + "' successfully updated.");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	protected void insertFeaturesRecursive(int role_id, List<VertFeature> features, int parent_value,
			Connection connection) throws Exception {
		if (features == null || features.isEmpty())
			return;

		for (VertFeature feature : features) {
			int selected_value = feature.getSelected_value();

			if (selected_value < 1 || selected_value > 3)
				throw new Exception("Invalid selected_value in feature " + feature.getFeature());

			if (selected_value > parent_value)
				throw new Exception("Invalid permission hierarchy for the feature_id " + feature.getFeature_id());

			System.out.println("r:" + role_id + " f:" + feature.getFeature_id() + " s" + selected_value);
			if (selected_value == 1) {
				// selected_value = 1 means 'none', so removing the feature in that role.
				// In getMetadata(), anyway its going to return selected_value as 1 default
				PreparedStatement ps;
				ps = connection.prepareStatement(
						"DELETE FROM " + Table.vertselectedfeatures + " WHERE role_id = ? AND feature_id = ?");
				ps.setInt(1, role_id);
				ps.setInt(2, feature.getFeature_id());
				ps.execute();
				ps.close();
			} else if (selected_value == 2 || selected_value == 3) {
				PreparedStatement ps;
				ps = connection.prepareStatement(
						"INSERT INTO " + Table.vertselectedfeatures + " (role_id, feature_id, selected_value)"
								+ " VALUES(?, ?, ?)" + " ON DUPLICATE KEY UPDATE selected_value = ?");
				ps.setInt(1, role_id);
				ps.setInt(2, feature.getFeature_id());
				ps.setInt(3, selected_value);
				ps.setInt(4, selected_value);
				ps.execute();
				ps.close();
			}

			insertFeaturesRecursive(role_id, feature.getSub_features(), selected_value, connection);
		}
	}

	public Map<String, Object> getVertPermissionMap(int userid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT isFirstUser FROM " + Table.vertadmin + " WHERE userId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			if (!rs.next())
				throw new Exception("Invalid userId");

			boolean isFirstUser = rs.getBoolean("isFirstUser");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT feature_id FROM " + Table.vertfeatures);
			rs1 = ps1.executeQuery();

			// deriving the metadata for the vert features
			List<Integer> feautre_idArray = new ArrayList<>();
			List<Integer> selected_valueArray = new ArrayList<>();
			while (rs1.next()) {
				feautre_idArray.add(rs1.getInt("feature_id"));
				if (isFirstUser == false)
					selected_valueArray.add(1); // default selected_value will be 1 ('none')
				else
					selected_valueArray.add(3); // for the first user maximum permission
			}
			ps1.close();
			rs1.close();

			PreparedStatement ps2;
			ResultSet rs2;
			ps2 = connection.prepareStatement("SELECT T1.role_id, T1.feature_id, T1.selected_value FROM "
					+ Table.vertselectedfeatures + " T1 JOIN " + Table.vertassignedroles
					+ " T2 ON T1.role_id = T2.role_id" + " WHERE userId = ?");
			ps2.setInt(1, userid);
			rs2 = ps2.executeQuery();

			// deriving the maximum permission for every features based on various roles
			while (rs2.next()) {
				int feautre_id = rs2.getInt("feature_id");
				int curr_selected_value = rs2.getInt("selected_value");

				int indexOf_currFeature = feautre_idArray.indexOf(feautre_id);
				int prev_selected_value = selected_valueArray.get(indexOf_currFeature);

				if (prev_selected_value < curr_selected_value)
					selected_valueArray.set(indexOf_currFeature, curr_selected_value);
			}
			ps2.close();
			rs2.close();

			Map<String, Object> permission = new HashMap<>();
			permission.put("feature_id", feautre_idArray);
			permission.put("selected_value", selected_valueArray);
			return permission;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean checkVertPermission(Map<String, Object> permission, String resourceName, String methodName,
			String verb) throws Exception {
		try (Connection connection = getDBConnection()) {
			resourceName = ""; // currently we are not using resource level api's

			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT apiId FROM " + Table.vertapilist
					+ " WHERE resourceName = ? AND methodName = ? AND verb = ?");
			ps.setString(1, resourceName);
			ps.setString(2, methodName);
			ps.setString(3, verb);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				return true;
				// throw new Exception("api is not added in the 'apilist' table.");
			}

			int apiId = rs.getInt("apiId");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement(
					"SELECT featureId, reqFeatureVal FROM " + Table.vertapicontrol + " WHERE apiId = ?");
			ps1.setInt(1, apiId);
			rs1 = ps1.executeQuery();
			if (!rs1.next()) {
				ps1.close();
				rs1.close();
				return true; // its a unsecured api, not in 'apicontrol' table
			}

			int featureId = rs1.getInt("featureId");
			int reqFeatureVal = rs1.getInt("reqFeatureVal");
			ps1.close();
			rs1.close();

			List<Integer> feautre_idArray = (List<Integer>) permission.get("feature_id");
			List<Integer> selected_valueArray = (List<Integer>) permission.get("selected_value");

			int indexOf_currFeature = feautre_idArray.indexOf(featureId);
			int actualFeatureval = selected_valueArray.get(indexOf_currFeature);

			if (actualFeatureval >= reqFeatureVal)
				return true;
			else
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
