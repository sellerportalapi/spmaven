package com.vertadmin;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.vossa.api.general.annotations.filterannotations.Insecured;
import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;

@Path("")
public class OtherResources {

	private OtherServices service;

	public OtherResources() {
		service = new OtherServices();
	}

	@POST
	@Insecured
	@Path("get")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void get(List<Map<String, Object>> map) {
		System.out.println("MAP");
		System.out.println(map);
	}

	// Not used
	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getallstores")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllStores() {
		try {
			return service.getAllStores();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getalluserstatus")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllUserStatus() {
		try {
			return service.getAllUserStatus();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getalluserdetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllUserDetails() {
		try {
			return service.getAllUserDetails();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getallcontactdetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllContactDetails() {
		try {
			return service.getAllContactDetails();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
}
