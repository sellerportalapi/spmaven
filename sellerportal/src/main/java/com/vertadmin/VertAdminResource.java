package com.vertadmin;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.UserClaims;
import com.vossa.api.v1.vertadmin.pojos.VertAdmin;

@Path("")
public class VertAdminResource {

	private VertAdminService service = new VertAdminService();

	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getalladmins")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllAdmins() {
		try {
			return service.getAllAdmins();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured(UserType.VERTADMIN)
	@Path("createvertadmin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass createVertAdmin(VertAdmin admin, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String createdBy = userClaims.getUserName();
			String origin = context.getHeaderString("origin");
			return service.createVertAdmin(admin, createdBy, origin);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "VERT_ADMIN not successfully created or registered");
		}
	}

	@PUT
	@Secured(UserType.VERTADMIN)
	@Path("updatevertadmin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateVertAdmin(VertAdmin admin, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String modifiedBy = userClaims.getUserName();
			String userName = userClaims.getUserName();

			return service.updateVertAdmin(admin, modifiedBy, userName);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@PUT
	@Secured(UserType.VERTADMIN)
	@Path("updatevertuserstatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateVertuserStatus(String data, @Context ContainerRequestContext context) {
		try {
			String origin = context.getHeaderString("origin");

			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String modifiedBy = userClaims.getUserName();

			JSONObject js = new JSONObject(data);
			String username = js.getString("userName");
			String vertUserStatus = js.getString("vertUserStatus");
			int userid = service.getUseridByName(username);

			return service.updateVertuserStatus(userid, vertUserStatus, modifiedBy, origin);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured(UserType.VERTADMIN)
	@Path("checkfieldvert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Boolean> checkFieldVert(String data) {
		Map<String, Boolean> map = new HashMap<>();
		boolean valid = false;
		try {
			JSONObject json = new JSONObject(data);
			if (!json.isNull("userName")) {
				valid = service.checkFieldVert("userName", json.getString("userName"));
			} else if (!json.isNull("email")) {
				valid = service.checkFieldVert("email", json.getString("email"));
			} else if (!json.isNull("phoneNumber")) {
				valid = service.checkFieldVert("phoneNumber", json.getString("phoneNumber"));
			}
			map.put("validData", valid);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("validData", false);
		}
		return map;
	}
}
