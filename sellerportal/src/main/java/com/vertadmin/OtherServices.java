package com.vertadmin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.stores.StoreServices;
import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.others.DBConnection;
import com.vossa.api.server.DataSourceFactory;
import com.vossa.api.v1.seller.pojos.SellingMode;
import com.vossa.api.v1.seller.pojos.Store;
import com.vossa.api.v1.seller.pojos.User;

public class OtherServices {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;

		return connection;
	}

	protected Connection getDBConnectionPool() throws Exception {
		return DataSourceFactory.getInstance().getConnection();
	}

	@ApiMethod(apiname = "getallstores", method = "GET")
	public List<Map<String, String>> getAllStores() throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT storeName FROM " + Table.stores);
			rs = ps.executeQuery();

			List<Map<String, String>> stores = new ArrayList<Map<String, String>>();
			Map<String, String> map;
			while (rs.next()) {
				map = new HashMap<>();
				map.put("storeNames", rs.getString("storeName"));
				stores.add(map);
			}
			ps.close();
			rs.close();
			
			return stores;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
//
//	public ResponseClass createUserFull(UserFull userFull, String createdBy) throws Exception {
//
//		try (Connection connection = getDBConnection()) {
//			connection.setAutoCommit(false);
//
//			Stores store = new StoreServices().getStoreObject(userFull);
//			int storeid = createStore(store, createdBy, connection);
//
//			Users user = new StoreServices().getUserObject(userFull);
//			user.setUserType(UserType.SUPERUSER.toString());
//			user.setSellerStatus("INACTIVE");
//			@SuppressWarnings("unused")
//			int sellerid = createUser(user, storeid, createdBy, connection);
//
//			List<SellingMode> modes = userFull.getSellingMode();
//			updateSellingModeList(modes, storeid, connection);
//
//			connection.commit();
//			return new ResponseClass(201, "success", "New SUPER_USER successfully created or registered");
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		}
//	}
//
//	public ResponseClass editUser(UserFull userFull, String modifiedBy) {
//		PreparedStatement ps;
//		ResultSet rs;
//		try (Connection connection = getDBConnection()) {
//
//			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.users + " WHERE userName = ?");
//			ps.setString(1, userFull.getUserName());
//			rs = ps.executeQuery();
//			rs.next();
//
//			int cnt = rs.getInt("cnt");
//			if (cnt == 0) {
//				// Setting the default password for Users coming from contact-us page.
//				userFull.setPassword("hello1234");
//				createUserFull(userFull, modifiedBy);
//				int storeid = new StoreServices()
//						.findStoreidByUserid(new StoreServices().getUseridByName(userFull.getUserName()));
//				PreparedStatement ps1 = connection
//						.prepareStatement("UPDATE " + Table.stores + " SET allDataEntered = ?" + " WHERE storeId = ?");
//				ps1.setBoolean(1, true);
//				ps1.setInt(2, storeid);
//				ps1.executeUpdate();
//				deleteFromContactvert(userFull.getStoreId());
//
//				return new ResponseClass(201, "success", "User Details updated");
//			} else {
//				return updateUserFull(userFull, modifiedBy);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ResponseClass(500, "error", e.getLocalizedMessage(),
//					"Oops, something went wrong. Internal server error. Try again later...");
//		}
//	}
//
//	public ResponseClass updateUserFull(UserFull userFull, String modifiedBy) {
//		try (Connection connection = getDBConnection()) {
//			connection.setAutoCommit(false);
//
//			StoreServices service = new StoreServices();
//
//			Users user = service.getUserObject(userFull);
//			Stores store = service.getStoreObject(userFull);
//			List<SellingMode> modes = service.getSellingModeObject(userFull);
//
//			updateStore(store, modifiedBy, connection);
//			updateUser(user, modifiedBy, connection);
//			int storeid = service.getStoreidByName(store.getStoreName());
//			updateSellingModeList(modes, storeid, connection);
//
//			connection.commit();
//			return new ResponseClass(201, "success", "User Details updated");
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ResponseClass(500, "error", e.getLocalizedMessage(),
//					"Oops, something went wrong. Internal server error. Try again later...");
//		}
//	}

	public void updateStore(Store store, String modifiedBy, Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			ps = connection.prepareStatement("UPDATE " + Table.stores
					+ " SET sellerCode = ?, revenue = ?, maximumProductSku = ?, modeOfContact = ?, preference = ?, timeOfContact = ?,  "
					+ " fulfillment = ?, imagePath = ?, orderPickFrequency = ?, additionalParameter1 = ?, additionalParameter2 = ?, additionalParameter3 = ?, allDataEntered = ?,"
					+ " address = ?, city = ?, state = ?, country = ?, zipCode = ?, modifiedBy = ?"
					+ " WHERE storeName = ?");

			ps.setString(1, store.getSellerCode());
			ps.setString(2, store.getRevenue());
			ps.setInt(3, store.getMaximumProductSku());
			ps.setString(4, store.getModeOfContact());
			ps.setString(5, store.getPreference());
			ps.setString(6, store.getTimeOfContact());
			ps.setString(7, store.getFulfillment());
			ps.setString(8, store.getImagePath());
			ps.setString(9, store.getOrderPickFrequency());
			ps.setString(10, store.getAdditionalParameter1());
			ps.setString(11, store.getAdditionalParameter2());
			ps.setString(12, store.getAdditionalParameter3());
			ps.setBoolean(13, true);
			ps.setString(14, store.getAddress());
			ps.setString(15, store.getCity());
			ps.setString(16, store.getState());
			ps.setString(17, store.getCountry());
			ps.setString(18, store.getZipCode());
			ps.setString(19, modifiedBy);
			ps.setString(20, store.getStoreName());

			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void updateUser(User user, String modifiedBy, Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			ps = connection.prepareStatement("UPDATE " + Table.users
					+ " SET name = ?, email = ?, phoneNumber = ?, userType = ?, sellerStatus = ?, "
					+ " activeFrom = ?, activeTo = ?, modifiedBy = ?" + " WHERE userName = ?");

			ps.setString(1, user.getName());
			ps.setString(2, user.getEmail());
			ps.setString(3, user.getPhoneNumber());
			ps.setString(4, user.getUserType());
			ps.setString(5, user.getSellerStatus());
//			ps.setTimestamp(6, new Timestamp(user.getActiveFrom()));
//			ps.setTimestamp(7, new Timestamp(user.getActiveTo()));
			ps.setString(8, modifiedBy);
			ps.setString(9, user.getUserName());

			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void updateSellingModeList(List<SellingMode> modes, int storeid, Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.sellingmode + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			List<String> currentSellingModes = new ArrayList<>();
			while (rs.next())
				currentSellingModes.add(rs.getString("currentSellingMode"));
			ps.close();
			rs.close();
			
			if (modes != null) {
				for (SellingMode mode : modes) {
					if (currentSellingModes.contains(mode.getCurrentSellingMode()))
						updateSellingMode(mode, storeid, connection);
//					else
//						createSellingMode(mode, storeid, connection);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void updateSellingMode(SellingMode mode, int storeid, Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			ps = connection.prepareStatement("UPDATE " + Table.sellingmode
					+ " SET sellerSource = ?, orderPrefix = ?, source = ?, hostURL = ?, port = ?, hostUserName = ?, hostPassword = ?, "
					+ " SFTPHostURL = ?, SFTPUserName = ?, SFTPPassword = ?, SFTPFolderPath = ?, SFTPKeyPath = ?, APIKey = ?"
					+ " WHERE currentSellingMode = ? AND storeId = ?");

			ps.setString(1, mode.getSellerSource());
			ps.setString(2, mode.getOrderPrefix());
			ps.setString(3, mode.getSource());
			ps.setString(4, mode.getHostURL());
			ps.setInt(5, mode.getPort());
			ps.setString(6, mode.getHostUserName());
			ps.setString(7, mode.getHostPassword());
			ps.setString(8, mode.getSFTPHostURL());
			ps.setString(9, mode.getSFTPUserName());
			ps.setString(10, mode.getSFTPPassword());
			ps.setString(11, mode.getSFTPFolderPath());
			ps.setString(12, mode.getSFTPKeyPath());
			ps.setString(13, mode.getAPIKey());
			ps.setString(14, mode.getCurrentSellingMode());
			ps.setInt(15, storeid);

			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getalluserdetails", method = "GET")
	public List<Map<String, Object>> getAllUserDetails() throws Exception {
		try {
			List<Map<String, Object>> alluserslist = new ArrayList<>();
			alluserslist.addAll(getAllAcceptedUsers());
			alluserslist.addAll(getAllContactDetails());
			return alluserslist;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Map<String, Object>> getAllAcceptedUsers() throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.users + " WHERE isCreatedByVert = ?");
			ps.setBoolean(1, true);
			rs = ps.executeQuery();

			List<Map<String, Object>> userlist = new ArrayList<>();
			while (rs.next()) {
				Map<String, Object> user = new HashMap<>();
				user.put("sellerId", rs.getInt("sellerId"));
				user.put("userName", rs.getString("userName"));
				user.put("name", rs.getString("name"));
				user.put("email", rs.getString("email"));
				user.put("phoneNumber", rs.getString("phoneNumber"));
				user.put("userType", rs.getString("userType"));
				user.put("sellerStatus", rs.getString("sellerStatus"));
				user.put("isCreatedByVert", rs.getBoolean("isCreatedByVert"));

				int sellerid = rs.getInt("sellerId");
				int storeid = new StoreServices().findStoreidByUserid(sellerid);
				PreparedStatement ps1 = connection
						.prepareStatement("SELECT * FROM " + Table.stores + " WHERE storeId = ?");
				ps1.setInt(1, storeid);
				ResultSet rs1 = ps1.executeQuery();

				if (!rs1.next())
					throw new Exception("Invalid StoreId");

				user.put("storeId", rs1.getInt("storeId"));
				user.put("sellerCode", rs1.getString("sellerCode"));
				user.put("storeName", rs1.getString("storeName"));
				user.put("revenue", rs1.getString("revenue"));
				user.put("maximumProductSku", rs1.getInt("maximumProductSku"));
				user.put("modeOfContact", rs1.getString("modeOfContact"));
				user.put("preference", rs1.getString("preference"));
				user.put("timeOfContact", rs1.getString("timeOfContact"));
				user.put("fulfillment", rs1.getString("fulfillment"));
				user.put("imagePath", rs1.getString("imagePath"));
				user.put("activeFrom", rs1.getTimestamp("activeFrom").getTime());
				user.put("activeTo", rs1.getTimestamp("activeTo").getTime());
				user.put("orderPickFrequency", rs1.getString("orderPickFrequency"));
				user.put("additionalParameter1", rs1.getString("additionalParameter1"));
				user.put("additionalParameter2", rs1.getString("additionalParameter2"));
				user.put("additionalParameter3", rs1.getString("additionalParameter3"));
				user.put("allDataEntered", rs1.getBoolean("allDataEntered"));
				user.put("address", rs1.getString("address"));
				user.put("city", rs1.getString("city"));
				user.put("state", rs1.getString("state"));
				user.put("country", rs1.getString("country"));
				user.put("zipCode", rs1.getString("zipCode"));

				ps1.close();
				rs1.close();

				PreparedStatement ps2 = connection
						.prepareStatement("SELECT * FROM " + Table.sellingmode + " WHERE storeId = ?");
				ps2.setInt(1, storeid);
				ResultSet rs2 = ps2.executeQuery();

				List<Map<String, Object>> modelist = new ArrayList<>();
				while (rs2.next()) {
					Map<String, Object> mode = new HashMap<>();
					mode.put("currentSellingMode", rs2.getString("currentSellingMode"));
					mode.put("sellerSource", rs2.getString("sellerSource"));
					mode.put("orderPrefix", rs2.getString("orderPrefix"));
					mode.put("source", rs2.getString("source"));
					mode.put("hostURL", rs2.getString("hostURL"));
					mode.put("port", rs2.getInt("port"));
					mode.put("hostUserName", rs2.getString("hostUserName"));
					mode.put("hostPassword", rs2.getString("hostPassword"));
					mode.put("SFTPHostURL", rs2.getString("SFTPHostURL"));
					mode.put("SFTPUserName", rs2.getString("SFTPUserName"));
					mode.put("SFTPPassword", rs2.getString("SFTPPassword"));
					mode.put("SFTPFolderPath", rs2.getString("SFTPFolderPath"));
					mode.put("SFTPKeyPath", rs2.getString("SFTPKeyPath"));
					mode.put("APIKey", rs2.getString("APIKey"));

					modelist.add(mode);
				}
				ps2.close();
				rs2.close();

				PreparedStatement ps3 = connection.prepareStatement(
						"SELECT COUNT(*) AS cnt FROM " + Table.users + " T1 JOIN " + Table.storeusermapper
								+ " T2 ON T1.sellerId = T2.sellerId WHERE T2.storeId = ? AND T1.sellerStatus = ?");
				ps3.setInt(1, storeid);
				ps3.setString(2, "ACTIVE");
				ResultSet rs3 = ps3.executeQuery();
				rs3.next();
				int activeUsersCount = rs3.getInt("cnt");
				user.put("activeUsersCount", activeUsersCount);

				ps3.close();
				rs3.close();

				user.put("sellingMode", modelist);
				user.put("notAnUser", false);
				userlist.add(user);
			}
			ps.close();
			rs.close();
			return userlist;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getallcontactdetails", method = "GET")
	public List<Map<String, Object>> getAllContactDetails() throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.contactvert);
			rs = ps.executeQuery();

			List<Map<String, Object>> userlist = new ArrayList<>();
			while (rs.next()) {
				Map<String, Object> user = new HashMap<>();
				user.put("sellerId", null);
				user.put("userName", null);
				user.put("password", "1234");
				user.put("name", rs.getString("name"));
				user.put("email", rs.getString("email"));
				user.put("phoneNumber", rs.getString("phoneNumber"));
				user.put("userType", null);
				user.put("sellerStatus", null);
				user.put("activeFrom", null);
				user.put("activeTo", null);

				user.put("storeId", rs.getInt("id"));
				user.put("sellerCode", null);
				user.put("storeName", rs.getString("companyName"));
				user.put("revenue", rs.getString("revenue"));
				user.put("maximumProductSku", rs.getInt("maximumProductSku"));
				user.put("modeOfContact", rs.getString("modeOfContact"));
				user.put("preference", rs.getString("preference"));
				user.put("timeOfContact", rs.getString("timeOfContact"));
				user.put("fulfillment", null);
				user.put("imagePath", null);
				user.put("orderPickFrequency", null);
				user.put("additionalParameter1", null);
				user.put("additionalParameter2", null);
				user.put("additionalParameter3", null);
				user.put("address", null);
				user.put("city", null);
				user.put("state", null);
				user.put("country", null);
				user.put("zipCode", null);

				List<Map<String, Object>> modelist = new ArrayList<>();
				String modes = rs.getString("sellingMode");

				if (modes != null) {
					String[] sellingMode = null;
					sellingMode = modes.split(",");

					for (String m : sellingMode) {
						Map<String, Object> mode = new HashMap<>();
						mode.put("currentSellingMode", m.trim());
						mode.put("sellerSource", null);
						mode.put("orderPrefix", null);
						mode.put("source", null);
						mode.put("hostURL", null);
						mode.put("port", null);
						mode.put("hostUserName", null);
						mode.put("hostPassword", null);
						mode.put("SFTPHostURL", null);
						mode.put("SFTPUserName", null);
						mode.put("SFTPPassword", null);
						mode.put("SFTPFolderPath", null);
						mode.put("SFTPKeyPath", null);
						mode.put("APIKey", null);

						modelist.add(mode);
					}
				}
				user.put("sellingMode", modelist);
				user.put("notAnUser", true);
				user.put("allDataEntered", false);
				userlist.add(user);
			}
			ps.close();
			rs.close();
			return userlist;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getalluserstatus", method = "GET")
	public List<Map<String, Object>> getAllUserStatus() throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT userName, name, email, phoneNumber, userType, sellerStatus FROM " + Table.users);
			rs = ps.executeQuery();

			List<Map<String, Object>> allUserStatus = new ArrayList<>();
			while (rs.next()) {
				Map<String, Object> user = new HashMap<>();
				user.put("name", rs.getString("name"));
				user.put("userName", rs.getString("userName"));
				user.put("email", rs.getString("email"));
				user.put("phoneNumber", rs.getString("phoneNumber"));
				user.put("userType", rs.getString("userType"));
				user.put("sellerStatus", rs.getString("sellerStatus"));
				allUserStatus.add(user);
			}
			ps.close();
			rs.close();
			return allUserStatus;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
