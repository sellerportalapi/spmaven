package com.vertadmin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.vossa.api.general.constants.Table;
import com.vossa.api.general.enums.CategoryLevel;
import com.vossa.api.others.DBConnection;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.server.DataSourceFactory;
import com.vossa.api.v1.vertadmin.pojos.ProductCategory;

public class ProductCategoryService {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;

		return connection;
	}

	protected Connection getDBConnectionPool() throws Exception {
		return DataSourceFactory.getInstance().getConnection();
	}

	public List<ProductCategory> getProductCategory() throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.productcategory
					+ " WHERE parentId = ? AND categoryLevel = ?" + " ORDER BY categoryName ASC");
			ps.setInt(1, 0);
			ps.setString(2, CategoryLevel.PARENT.toString());
			rs = ps.executeQuery();

			List<ProductCategory> parentCategoryList = new ArrayList<>();
			while (rs.next()) {
				ProductCategory parentCategory = new ProductCategory();
				parentCategory.setCategoryId(rs.getInt("categoryId"));
				parentCategory.setCategoryName(rs.getString("categoryName"));
				parentCategory.setCategoryLevel(CategoryLevel.PARENT.toString());
				parentCategory.setParentId(null); // in DB for parentCategory's parentId, we are storing 0

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement("SELECT * FROM " + Table.productcategory
						+ " WHERE parentId = ? AND categoryLevel= ?" + " ORDER BY categoryName ASC");
				ps1.setInt(1, parentCategory.getCategoryId());
				ps1.setString(2, CategoryLevel.CHILD.toString());
				rs1 = ps1.executeQuery();

				List<ProductCategory> childCategoryList = new ArrayList<>();
				while (rs1.next()) {
					ProductCategory childCategory = new ProductCategory();
					childCategory.setCategoryId(rs1.getInt("categoryId"));
					childCategory.setCategoryName(rs1.getString("categoryName"));
					childCategory.setCategoryLevel(CategoryLevel.CHILD.toString());
					childCategory.setParentId(parentCategory.getCategoryId());

					PreparedStatement ps2;
					ResultSet rs2;
					ps2 = connection.prepareStatement("SELECT * FROM " + Table.productcategory
							+ " WHERE parentId = ? AND categoryLevel = ?" + " ORDER BY categoryName ASC");
					ps2.setInt(1, childCategory.getCategoryId());
					ps2.setString(2, CategoryLevel.SUB.toString());
					rs2 = ps2.executeQuery();

					List<ProductCategory> subCategoryList = new ArrayList<>();
					while (rs2.next()) {
						ProductCategory subCategory = new ProductCategory();
						subCategory.setCategoryId(rs2.getInt("categoryId"));
						subCategory.setCategoryName(rs2.getString("categoryName"));
						subCategory.setCategoryLevel(CategoryLevel.SUB.toString());
						subCategory.setParentId(childCategory.getCategoryId());

						subCategoryList.add(subCategory);
					}
					ps2.close();
					rs2.close();

					if (!subCategoryList.isEmpty())
						childCategory.setCategoryList(subCategoryList);
					childCategoryList.add(childCategory);
				}
				ps1.close();
				rs1.close();

				if (!childCategoryList.isEmpty())
					parentCategory.setCategoryList(childCategoryList);
				parentCategoryList.add(parentCategory);
			}
			ps.close();
			rs.close();
			return parentCategoryList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ResponseClass updateProductCatagory(List<ProductCategory> parentCategoryList) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);

			if (parentCategoryList != null) {
				for (ProductCategory parentCategory : parentCategoryList) {
					parentCategory.setCategoryLevel(CategoryLevel.PARENT.toString());
					parentCategory.setParentId(0);
					int parentCategoryId = addOrEditCategory(parentCategory, connection);

					boolean allChildDeleted = true;
					if (parentCategory.getCategoryList() != null) {
						for (ProductCategory childCategory : parentCategory.getCategoryList()) {
							childCategory.setCategoryLevel(CategoryLevel.CHILD.toString());
							childCategory.setParentId(parentCategoryId);
							int childCategoryId = addOrEditCategory(childCategory, connection);

							boolean allSubDeleted = true;
							if (childCategory.getCategoryList() != null) {
								for (ProductCategory subCategory : childCategory.getCategoryList()) {
									subCategory.setCategoryLevel(CategoryLevel.SUB.toString());
									subCategory.setParentId(childCategoryId);
									addOrEditCategory(subCategory, connection);

									if (subCategory.getCategoryId() != null && subCategory.getToBeDeleted() != null
											&& subCategory.getToBeDeleted() == true) {
										deleteProductCategoryById(subCategory.getCategoryId(), connection);
									} else
										allSubDeleted = false;
								}
							}
							if (childCategory.getCategoryId() != null && childCategory.getToBeDeleted() != null
									&& childCategory.getToBeDeleted() == true) {
								if (allSubDeleted == true)
									deleteProductCategoryById(childCategory.getCategoryId(), connection);
								else
									throw new Exception("Cannot delete the Child " + childCategory.getCategoryName());
							} else
								allChildDeleted = false;
						}
					}
					if (parentCategory.getCategoryId() != null && parentCategory.getToBeDeleted() != null
							&& parentCategory.getToBeDeleted() == true) {
						if (allChildDeleted == true)
							deleteProductCategoryById(parentCategory.getCategoryId(), connection);
						else
							throw new Exception("Cannot delete the Parent " + parentCategory.getCategoryName());
					}
				}
			}
			connection.commit();
			return new ResponseClass(200, "success", "Product Category successfully updated.");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	protected int addOrEditCategory(ProductCategory category, Connection connection) throws Exception {
		try {
			if (category.getToBeDeleted() != null && category.getToBeDeleted() == true)
				return -1;

			if (category.getCategoryId() == null) {
				if (category.getToBeDeleted() == null || category.getToBeDeleted() == false) {

					PreparedStatement ps;
					ps = connection.prepareStatement("INSERT INTO " + Table.productcategory
							+ " (categoryName, categoryLevel, parentId)" + " VALUES(?, ?, ?)");
					ps.setString(1, category.getCategoryName());
					ps.setString(2, category.getCategoryLevel());
					ps.setInt(3, category.getParentId());
					ps.execute();
					ps.close();
				}
			} else {
				if (category.getToBeDeleted() == null || category.getToBeDeleted() == false) {

					PreparedStatement ps;
					ps = connection.prepareStatement(
							"UPDATE " + Table.productcategory + " SET categoryName = ?" + " WHERE categoryId = ?");
					ps.setString(1, category.getCategoryName());
					ps.setInt(2, category.getCategoryId());
					ps.execute();
					ps.close();
				}
			}

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT categoryId FROM " + Table.productcategory
					+ " WHERE categoryName = ? AND categoryLevel = ? AND parentId = ?");
			ps1.setString(1, category.getCategoryName());
			ps1.setString(2, category.getCategoryLevel());
			ps1.setInt(3, category.getParentId());
			rs1 = ps1.executeQuery();

			if (!rs1.next()) {
				ps1.close();
				rs1.close();
				throw new Exception(
						"Product Category not found (categoryName=" + category.getCategoryName() + ", categoryLevel="
								+ category.getCategoryLevel() + ", parentId=" + category.getParentId() + ").");
			}

			int categoryId = rs1.getInt("categoryId");
			ps1.close();
			rs1.close();

			return categoryId;
		} catch (Exception e) {
			throw e;
		}
	}

	protected void deleteProductCategoryById(int categoryId, Connection connection) throws Exception {
		try {
			PreparedStatement ps;
			ps = connection.prepareStatement("DELETE FROM " + Table.productcategory + " WHERE categoryId = ?");
			ps.setInt(1, categoryId);
			ps.execute();
			ps.close();
		} catch (Exception e) {
			throw e;
		}
	}

	public List<ProductCategory> getFulfillmentCategory() throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT * FROM " + Table.fulfillmentcategory + " ORDER BY categoryName ASC");
			rs = ps.executeQuery();

			List<ProductCategory> fulfillmentCategoryList = new ArrayList<>();
			while (rs.next()) {
				ProductCategory category = new ProductCategory();
				category.setCategoryId(rs.getInt("categoryId"));
				category.setCategoryName(rs.getString("categoryName"));
				fulfillmentCategoryList.add(category);
			}
			ps.close();
			rs.close();
			return fulfillmentCategoryList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ResponseClass updateFulfillmentCatagory(List<ProductCategory> fulfillmentCategoryList) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);
			if (fulfillmentCategoryList != null) {
				for (ProductCategory fulfillmentCategory : fulfillmentCategoryList) {
					addOrEditFulfillmentCategory(fulfillmentCategory, connection);
				}
			}
			connection.commit();
			return new ResponseClass(201, "success", "Fulfillment Category successfully inserted/updated.");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	protected void addOrEditFulfillmentCategory(ProductCategory fulfillmentCategory, Connection connection)
			throws Exception {
		try {
			if (fulfillmentCategory.getToBeDeleted() != null && fulfillmentCategory.getToBeDeleted() == true) {
				if (fulfillmentCategory.getCategoryId() != null) {
					PreparedStatement ps;
					ps = connection
							.prepareStatement("DELETE FROM " + Table.fulfillmentcategory + " WHERE categoryId = ?");
					ps.setInt(1, fulfillmentCategory.getCategoryId());
					ps.execute();
					ps.close();
				}
			} else if (fulfillmentCategory.getCategoryId() == null) {
				PreparedStatement ps;
				ps = connection.prepareStatement(
						"INSERT INTO " + Table.fulfillmentcategory + " (categoryName)" + " VALUE (?)");
				ps.setString(1, fulfillmentCategory.getCategoryName());
				ps.execute();
				ps.close();
			} else {
				PreparedStatement ps;
				ps = connection.prepareStatement(
						"UPDATE " + Table.fulfillmentcategory + " SET categoryName = ?" + " WHERE categoryId = ?");
				ps.setString(1, fulfillmentCategory.getCategoryName());
				ps.setInt(2, fulfillmentCategory.getCategoryId());
				ps.executeUpdate();
				ps.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
