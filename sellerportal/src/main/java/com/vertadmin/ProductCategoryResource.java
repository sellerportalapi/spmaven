package com.vertadmin;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.v1.vertadmin.pojos.ProductCategory;

@Path("")
public class ProductCategoryResource {

	private ProductCategoryService service = new ProductCategoryService();

	@GET
	@Secured({ UserType.VERTADMIN })
	@Path("getproductcategory")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getProductCategory() {
		try {
			return service.getProductCategory();
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.VERTADMIN })
	@Path("updateproductcategory")
	@Produces(MediaType.APPLICATION_JSON)
	public Object updateProductCatagory(List<ProductCategory> parentCategoryList) {
		try {
			return service.updateProductCatagory(parentCategoryList);
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured({ UserType.VERTADMIN })
	@Path("getfulfillmentcategory")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getFulfillmentCategory() {
		try {
			return service.getFulfillmentCategory();
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.VERTADMIN })
	@Path("updatefulfillmentcategory")
	@Produces(MediaType.APPLICATION_JSON)
	public Object updateFulfillmentCatagory(List<ProductCategory> fulfillmentCategoryList) {
		try {
			return service.updateFulfillmentCatagory(fulfillmentCategoryList);
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
}
