package com.vertadmin;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.stores.StoreServices;
import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.UserClaims;
import com.vossa.api.v1.common.pojos.Address;
import com.vossa.api.v1.seller.pojos.Seller;

@Path("")
public class SellerResources {

	private SellerServices service = new SellerServices();

	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getallsellers")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getaAllSellers() {
		try {
			return service.getAllSellers();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getawaitingsellers")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAwaitingSellers() {
		try {
			return service.getAwaitingSellers();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getactivesellers")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getActiveSellers() {
		try {
			return service.getActiveSellers();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
	
	@GET
	@Secured(UserType.VERTADMIN)
	@Path("getinactivesellers")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getInActiveSellers() {
		try {
			return service.getInActiveSellers();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
	
	
	@POST
	@Secured(UserType.VERTADMIN)
	@Path("createseller")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass createSeller(Seller seller, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String createdBy = userClaims.getUserName();

			return service.createSeller(seller, createdBy);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@PUT
	@Secured(UserType.VERTADMIN)
	@Path("updateseller")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateSeller(Seller seller, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String modifiedBy = userClaims.getUserName();

			return service.updateSeller(seller, modifiedBy);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@PUT
	@Secured(UserType.VERTADMIN)
	@Path("updatestorestatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateStoreStatus(String data, @Context ContainerRequestContext context) {
		String storeName, storeStatus;
		JSONObject js;
		try {
			String origin = context.getHeaderString("origin");

			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String modifiedBy = userClaims.getUserName();

			js = new JSONObject(data);
			storeName = js.getString("storeName");
			storeStatus = js.getString("storeStatus");

			int storeid = new StoreServices().getStoreidByName(storeName);
			return service.updateStoreStatus(storeid, storeStatus, modifiedBy, origin);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@PUT
	@Secured(UserType.VERTADMIN)
	@Path("updateuserstatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateUserStatus(String data, @Context ContainerRequestContext context) {
		String username, sellerStatus;
		JSONObject js;
		try {
			String origin = context.getHeaderString("origin");

			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String modifiedBy = userClaims.getUserName();

			js = new JSONObject(data);
			username = js.getString("userName");
			sellerStatus = js.getString("sellerStatus");
			int userid = new StoreServices().getUseridByName(username);

			return service.updateUserStatus(userid, sellerStatus, modifiedBy, origin);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured(UserType.VERTADMIN)
	@Path("checkfieldstore")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Boolean> checkFieldStore(String data) {
		Map<String, Boolean> map = new HashMap<>();
		boolean valid = false;
		try {
			JSONObject json = new JSONObject(data);

			if (!json.isNull("sellerCode")) {
				valid = service.checkFieldStore("sellerCode", json.getString("sellerCode"));
			} else if (!json.isNull("storeName")) {
				valid = service.checkFieldStore("storeName", json.getString("storeName"));
			} else if (!json.isNull("email")) {
				valid = service.checkFieldStore("email", json.getString("email"));
			} else if (!json.isNull("phoneNumber")) {
				valid = service.checkFieldStore("phoneNumber", json.getString("phoneNumber"));
			}

			map.put("validData", valid);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("validData", false);
		}
		return map;
	}

	@POST
	@Secured(UserType.VERTADMIN)
	@Path("checkfielduser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Boolean> checkFieldUser(String data) {
		Map<String, Boolean> map = new HashMap<>();
		boolean valid = false;
		try {
			JSONObject json = new JSONObject(data);

			if (!json.isNull("userName")) {
				valid = service.checkFieldUser("userName", json.getString("userName"));
			} else if (!json.isNull("email")) {
				valid = service.checkFieldUser("email", json.getString("email"));
			} else if (!json.isNull("phoneNumber")) {
				valid = service.checkFieldUser("phoneNumber", json.getString("phoneNumber"));
			}

			map.put("validData", valid);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("validData", false);
		}
		return map;
	}

	@POST
	@Secured(UserType.VERTADMIN)
	@Path("getaddress")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Address getAddress(String data) {
		Address address = new Address();
		JSONObject js;
		try {
			js = new JSONObject(data);
			if (js.has("zipCode") && !js.isNull("zipCode"))
				address = service.getAddress(js.getString("zipCode"));
			else
				throw new Exception("Zipcode should not be null");
		} catch (Exception e) {
			e.printStackTrace();
			address.setValidZip(false);
		}
		return address;
	}
}