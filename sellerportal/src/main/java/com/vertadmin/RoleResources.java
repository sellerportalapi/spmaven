package com.vertadmin;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.UserClaims;
import com.vossa.api.v1.vertadmin.pojos.VertRole;

@Path("")
public class RoleResources {

	private RoleServices service = new RoleServices();

	@GET
	@Secured({ UserType.VERTADMIN })
	@Path("getmetadata")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getMetadata() {
		try {
			return service.getMetadata();
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured({ UserType.VERTADMIN })
	@Path("getallroles")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllRoles() {
		try {
			return service.getAllRoles();
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.VERTADMIN })
	@Path("createrole")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass createRole(VertRole role, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String createdBy = userClaims.getUserName();

			return service.createRole(role, createdBy);
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@PUT
	@Secured({ UserType.VERTADMIN })
	@Path("updaterole")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateRole(VertRole role, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String modifiedBy = userClaims.getUserName();

			return service.updateRole(role, modifiedBy);
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
}
