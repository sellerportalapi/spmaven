package com.vossa.api.v1.seller.pojos;

import java.util.List;
import java.util.Map;

public class ASNPalletLevel {

	private Integer asnId;
	private Integer palletId;
	private String palletLpn;
	private Float palletLength;
	private Float palletWidth;
	private Float palletHeight;
	private String palletDimensionUOM;
	private Float palletWeight;
	private String palletWeightUOM;

	private List<Map<String, Object>> caseLpnLabels;

	public Integer getAsnId() {
		return asnId;
	}

	public void setAsnId(Integer asnId) {
		this.asnId = asnId;
	}

	public Integer getPalletId() {
		return palletId;
	}

	public void setPalletId(Integer palletId) {
		this.palletId = palletId;
	}

	public String getPalletLpn() {
		return palletLpn;
	}

	public void setPalletLpn(String palletLpn) {
		this.palletLpn = palletLpn;
	}

	public Float getPalletLength() {
		return palletLength;
	}

	public void setPalletLength(Float palletLength) {
		this.palletLength = palletLength;
	}

	public Float getPalletWidth() {
		return palletWidth;
	}

	public void setPalletWidth(Float palletWidth) {
		this.palletWidth = palletWidth;
	}

	public Float getPalletHeight() {
		return palletHeight;
	}

	public void setPalletHeight(Float palletHeight) {
		this.palletHeight = palletHeight;
	}

	public String getPalletDimensionUOM() {
		return palletDimensionUOM;
	}

	public void setPalletDimensionUOM(String palletDimensionUOM) {
		this.palletDimensionUOM = palletDimensionUOM;
	}

	public Float getPalletWeight() {
		return palletWeight;
	}

	public void setPalletWeight(Float palletWeight) {
		this.palletWeight = palletWeight;
	}

	public String getPalletWeightUOM() {
		return palletWeightUOM;
	}

	public void setPalletWeightUOM(String palletWeightUOM) {
		this.palletWeightUOM = palletWeightUOM;
	}

	public List<Map<String, Object>> getCaseLpnLabels() {
		return caseLpnLabels;
	}

	public void setCaseLpnLabels(List<Map<String, Object>> caseLpnLabels) {
		this.caseLpnLabels = caseLpnLabels;
	}
}
