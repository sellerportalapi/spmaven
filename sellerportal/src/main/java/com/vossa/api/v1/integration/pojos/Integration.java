package com.vossa.api.v1.integration.pojos;

public class Integration {

	private String sellerId;
	private String sellerName;
	private String storeName;
	private String storeId;
	private String source;
	private String sourceFlag;
	private String orderPrefix;
	private String host;
	private int port;
	private String hostUserName;
	private String hostPassword;
	private String sftpHost;
	private String sftpUserName;
	private String sftpPassword;
	private String sftpFolderPath;
	private String sftpKeyPath;
	private String apiKey;
	private int orderPickFrequency;
	private String imagePath;
	private String caRefreshtoken;
	private String caAuthorization;
	private String additionalParameter1;
	private String additionalParameter2;
	private String additionalParameter3;
	private String flag1;
	private String flag2;
	private int priority;

	public String getSourceFlag() {
		return sourceFlag;
	}

	public void setSourceFlag(String sourceFlag) {
		this.sourceFlag = sourceFlag;
	}

	public void setSellerName(String _sellername) {
		sellerName = _sellername;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setApiKey(String _apiKey) {
		apiKey = _apiKey;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setSftpFolderPath(String _sftpFolderPath) {
		sftpFolderPath = _sftpFolderPath;
	}

	public String getSftpFolderPath() {
		return sftpFolderPath;
	}

	public void setadditionalParameter1(String _additionalParameter1) {
		additionalParameter1 = _additionalParameter1;
	}

	public String getadditionalParameter1() {
		return additionalParameter1;
	}

	public void setadditionalParameter2(String _additionalParameter2) {
		additionalParameter2 = _additionalParameter2;
	}

	public String getadditionalParameter2() {
		return additionalParameter2;
	}

	public void setadditionalParameter3(String _additionalParameter3) {
		additionalParameter3 = _additionalParameter3;
	}

	public String getadditionalParameter3() {
		return additionalParameter3;
	}

	public void setImagePath(String _imagePath) {
		imagePath = _imagePath;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setsftpKeyPath(String _sftpKeyPath) {
		sftpKeyPath = _sftpKeyPath;
	}

	public String getSftpKeyPath() {
		return sftpKeyPath;
	}

	public void setSellerId(String _sellerId) {
		sellerId = _sellerId;
	}

	public String getSellerId() {
		return sellerId;
	}

	public void setOrderPrefix(String _orderPrefix) {
		orderPrefix = _orderPrefix;
	}

	public String getOrderPrefix() {
		return orderPrefix;
	}

	public void setSource(String _source) {
		source = _source;
	}

	public String getSource() {
		return source;
	}

	public void setHost(String _host) {
		host = _host;
	}

	public String getHost() {
		return host;
	}

	public void setHostUserName(String _hostUserName) {
		hostUserName = _hostUserName;
	}

	public String getHostUserName() {
		return hostUserName;
	}

	public void setHostPassword(String _hostPassword) {
		hostPassword = _hostPassword;
	}

	public String getHostPassword() {
		return hostPassword;
	}

	public void setSftpHost(String _sftpHost) {
		sftpHost = _sftpHost;
	}

	public String getSftpHost() {
		return sftpHost;
	}

	public void setSftpUserName(String _sftpUserName) {
		sftpUserName = _sftpUserName;
	}

	public String getSftpUserName() {
		return sftpUserName;
	}

	public String getCaRefreshtoken() {
		return caRefreshtoken;
	}

	public void setCaRefreshtoken(String caRefreshtoken) {
		this.caRefreshtoken = caRefreshtoken;
	}

	public String getCaAuthorization() {
		return caAuthorization;
	}

	public void setCaAuthorization(String caAuthorization) {
		this.caAuthorization = caAuthorization;
	}

	public void setSftpPassword(String _sftpPassword) {
		sftpPassword = _sftpPassword;
	}

	public String getSftpPassword() {
		return sftpPassword;
	}

	public String getFlag1() {
		return flag1;
	}

	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}

	public String getFlag2() {
		return flag2;
	}

	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}

	public String getAdditionalParameter1() {
		return additionalParameter1;
	}

	public void setAdditionalParameter1(String additionalParameter1) {
		this.additionalParameter1 = additionalParameter1;
	}

	public String getAdditionalParameter2() {
		return additionalParameter2;
	}

	public void setAdditionalParameter2(String additionalParameter2) {
		this.additionalParameter2 = additionalParameter2;
	}

	public String getAdditionalParameter3() {
		return additionalParameter3;
	}

	public void setAdditionalParameter3(String additionalParameter3) {
		this.additionalParameter3 = additionalParameter3;
	}

	public void setSftpKeyPath(String sftpKeyPath) {
		this.sftpKeyPath = sftpKeyPath;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setOrderPickFrequency(int orderPickFrequency) {
		this.orderPickFrequency = orderPickFrequency;
	}

	public int getPort() {
		return port;
	}

	public int getOrderPickFrequency() {
		return orderPickFrequency;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}