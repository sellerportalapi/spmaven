package com.vossa.api.v1.vertadmin.pojos;

import java.util.List;

public class ProductCategory {

	private Integer categoryId;
	private String categoryName;
	private String categoryLevel;
	private Integer parentId;
	private List<ProductCategory> categoryList;

	private Boolean toBeDeleted;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryLevel() {
		return categoryLevel;
	}

	public void setCategoryLevel(String categoryLevel) {
		this.categoryLevel = categoryLevel;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public List<ProductCategory> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<ProductCategory> categoryList) {
		this.categoryList = categoryList;
	}

	public Boolean getToBeDeleted() {
		return toBeDeleted;
	}

	public void setToBeDeleted(Boolean toBeDeleted) {
		this.toBeDeleted = toBeDeleted;
	}
}
