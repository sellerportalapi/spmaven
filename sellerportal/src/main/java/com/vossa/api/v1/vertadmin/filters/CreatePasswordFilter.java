package com.vossa.api.v1.vertadmin.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Priority;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.vossa.api.general.annotations.filterannotations.CreatePassword;
import com.vossa.api.general.constants.CorsHeaders;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.JWT;
import com.vossa.api.others.token.JWTHandler;

import io.jsonwebtoken.ExpiredJwtException;

@Provider
@CreatePassword
@Priority(Priorities.AUTHORIZATION)
public class CreatePasswordFilter implements ContainerRequestFilter, ContainerResponseFilter {

	private static final String TOKEN_HEADER_KEY = "token";
//	private static final String TOKEN_HEADER_KEY = "Authorization";
//	private static final String TOKEN_SCHEME_KEY = "Verte";

	private List<String> allowedOrigins = new ArrayList<>();
	private boolean allowCredentials = true;
	private String exposedHeaders = "content-type,accept,authorization,token";

	@Context
	private ResourceInfo resourceInfo;

	public CreatePasswordFilter() {
		allowedOrigins.add("*");
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		System.out.println("CreatePasswordFilter-Request-" + requestContext.getUriInfo().getPath());

		String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
		try {
			if (origin != null) {
				checkOrigin(origin, requestContext);
			}
		} catch (ForbiddenException e) {
			e.printStackTrace();
			requestContext.abortWith(Response.status(403).entity(e.getMessage()).build());
			return;
		}

		String authToken = requestContext.getHeaderString(TOKEN_HEADER_KEY);
		if (authToken == null) {
			Response response = Response.status(Response.Status.UNAUTHORIZED)
					.entity("There is no valid Authorization header").build();
			requestContext.abortWith(response);
			return;
		}
//		else if (authToken.startsWith(TOKEN_SCHEME_KEY) == false) {
//			Response response = Response.status(Response.Status.UNAUTHORIZED).entity("There is no valid token Scheme")
//					.build();
//			requestContext.abortWith(response);
//			return;
//		}

		String token = authToken;
//		String token = authToken.replaceFirst(TOKEN_SCHEME_KEY, "").trim();
		if (token.chars().filter(ch -> ch == '.').count() != 2) {
			Response response = Response.status(Response.Status.UNAUTHORIZED)
					.entity("Invalid token format or empty token").build();
			requestContext.abortWith(response);
			return;
		}

		try {
			JWT jwt = JWTHandler.parseJWTPassword(token);
			requestContext.setProperty("jwt", jwt);
			return;
		} catch (ExpiredJwtException e) {
			System.out.println(e);
			Map<String, String> responseEntity = new HashMap<>();
			responseEntity.put("status", "TOKEN_EXPIRED");
			Response response = Response.status(Response.Status.UNAUTHORIZED).entity(responseEntity).build();
			requestContext.abortWith(response);
			return;
		} catch (Exception e) {
			System.out.println(e);
			Response response = Response.status(Response.Status.UNAUTHORIZED).entity(e.toString()).build();
			requestContext.abortWith(response);
			return;
		}
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		int status = 200;
		if (responseContext.hasEntity() && responseContext.getEntity() instanceof ResponseClass) {
			ResponseClass responseClass = (ResponseClass) responseContext.getEntity();
			status = responseClass.getCode();
		}
		System.out.println("CreatePasswordFilter-Response-" + requestContext.getUriInfo().getPath() + "-code" + status);

		String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
		if (origin == null || requestContext.getProperty("cors.failure") != null) {
			return;
		}

		MultivaluedMap<String, Object> headers = responseContext.getHeaders();
		headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
		headers.putSingle(CorsHeaders.VARY, CorsHeaders.ORIGIN);

		if (allowCredentials)
			headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, allowCredentials);

		if (exposedHeaders != null)
			headers.putSingle(CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, exposedHeaders);
	}

	private void checkOrigin(String origin, ContainerRequestContext requestContext) throws ForbiddenException {
		if (!allowedOrigins.contains("*") && !allowedOrigins.contains(origin)) {
			requestContext.setProperty("cors.failure", true);
			throw new ForbiddenException("Request-Origin is not allowed to access this api.");
		}
	}
}
