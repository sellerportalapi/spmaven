package com.vossa.api.v1.seller.pojos;

public class SellingMode {

	private Integer id;
	private Integer storeId;
	private String storeName;
	private String sellerCode;
	private String currentSellingMode;
	private String sellerSource;
	private String orderPrefix;
	private String source;
	private String hostURL;
	private Integer port;
	private String hostUserName;
	private String hostPassword;
	private String SFTPHostURL;
	private String SFTPUserName;
	private String SFTPPassword;
	private String SFTPFolderPath;
	private String SFTPKeyPath;
	private String APIKey;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}

	public String getCurrentSellingMode() {
		return currentSellingMode;
	}

	public void setCurrentSellingMode(String currentSellingMode) {
		this.currentSellingMode = currentSellingMode;
	}

	public String getSellerSource() {
		return sellerSource;
	}

	public void setSellerSource(String sellerSource) {
		this.sellerSource = sellerSource;
	}

	public String getOrderPrefix() {
		return orderPrefix;
	}

	public void setOrderPrefix(String orderPrefix) {
		this.orderPrefix = orderPrefix;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getHostURL() {
		return hostURL;
	}

	public void setHostURL(String hostURL) {
		this.hostURL = hostURL;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getHostUserName() {
		return hostUserName;
	}

	public void setHostUserName(String hostUserName) {
		this.hostUserName = hostUserName;
	}

	public String getHostPassword() {
		return hostPassword;
	}

	public void setHostPassword(String hostPassword) {
		this.hostPassword = hostPassword;
	}

	public String getSFTPHostURL() {
		return SFTPHostURL;
	}

	public void setSFTPHostURL(String sFTPHostURL) {
		SFTPHostURL = sFTPHostURL;
	}

	public String getSFTPUserName() {
		return SFTPUserName;
	}

	public void setSFTPUserName(String sFTPUserName) {
		SFTPUserName = sFTPUserName;
	}

	public String getSFTPPassword() {
		return SFTPPassword;
	}

	public void setSFTPPassword(String sFTPPassword) {
		SFTPPassword = sFTPPassword;
	}

	public String getSFTPFolderPath() {
		return SFTPFolderPath;
	}

	public void setSFTPFolderPath(String sFTPFolderPath) {
		SFTPFolderPath = sFTPFolderPath;
	}

	public String getSFTPKeyPath() {
		return SFTPKeyPath;
	}

	public void setSFTPKeyPath(String sFTPKeyPath) {
		SFTPKeyPath = sFTPKeyPath;
	}

	public String getAPIKey() {
		return APIKey;
	}

	public void setAPIKey(String aPIKey) {
		APIKey = aPIKey;
	}
}
