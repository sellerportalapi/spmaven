package com.vossa.api.v1.seller.pojos;

public class ASNCaseLevel {

	private Integer asnId;
	private Integer palletId;
	private String palletLpn;
	private Integer caseId;
	private String caseLpn;
	private Float caseLength;
	private Float caseWidth;
	private Float caseHeight;
	private String caseDimensionUOM;
	private Float caseWeight;
	private String caseWeightUOM;
	private String specialTreatmentNeeded;
	private String isMaterialHazardous;
	private String sku;
	private Integer product_id;
	private Integer quantity;
	private String product_name;

	public Integer getAsnId() {
		return asnId;
	}

	public void setAsnId(Integer asnId) {
		this.asnId = asnId;
	}

	public Integer getPalletId() {
		return palletId;
	}

	public void setPalletId(Integer palletId) {
		this.palletId = palletId;
	}

	public String getPalletLpn() {
		return palletLpn;
	}

	public void setPalletLpn(String palletLpn) {
		this.palletLpn = palletLpn;
	}

	public Integer getCaseId() {
		return caseId;
	}

	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}

	public String getCaseLpn() {
		return caseLpn;
	}

	public void setCaseLpn(String caseLpn) {
		this.caseLpn = caseLpn;
	}

	public Float getCaseLength() {
		return caseLength;
	}

	public void setCaseLength(Float caseLength) {
		this.caseLength = caseLength;
	}

	public Float getCaseWidth() {
		return caseWidth;
	}

	public void setCaseWidth(Float caseWidth) {
		this.caseWidth = caseWidth;
	}

	public Float getCaseHeight() {
		return caseHeight;
	}

	public void setCaseHeight(Float caseHeight) {
		this.caseHeight = caseHeight;
	}

	public String getCaseDimensionUOM() {
		return caseDimensionUOM;
	}

	public void setCaseDimensionUOM(String caseDimensionUOM) {
		this.caseDimensionUOM = caseDimensionUOM;
	}

	public Float getCaseWeight() {
		return caseWeight;
	}

	public void setCaseWeight(Float caseWeight) {
		this.caseWeight = caseWeight;
	}

	public String getCaseWeightUOM() {
		return caseWeightUOM;
	}

	public void setCaseWeightUOM(String caseWeightUOM) {
		this.caseWeightUOM = caseWeightUOM;
	}

	public String getSpecialTreatmentNeeded() {
		return specialTreatmentNeeded;
	}

	public void setSpecialTreatmentNeeded(String specialTreatmentNeeded) {
		this.specialTreatmentNeeded = specialTreatmentNeeded;
	}

	public String getIsMaterialHazardous() {
		return isMaterialHazardous;
	}

	public void setIsMaterialHazardous(String isMaterialHazardous) {
		this.isMaterialHazardous = isMaterialHazardous;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
}
