package com.vossa.api.v1.seller.pojos;

import com.vossa.api.v1.common.pojos.Address;

public class Store {

	private Integer awaitingSellerId;
	private Boolean notASeller;

	private Integer storeId;
	private String sellerCode;
	private String storeName;
	private String email;
	private String phoneNumber;
	private Integer allowedUsersCount;
	private Integer activeUsersCount;
	private String storeStatus;
	private Boolean isActivated;
	private Long activeFrom;
	private Long activeTo;
	private Integer maximumProductSku;
	private String preference;
	private String fulfillment;
	private String imagePath;
	private String revenue;
	private String modeOfContact;
	private String timeOfContact;
	private String orderPickFrequency;
	private String additionalParameter1;
	private String additionalParameter2;
	private String additionalParameter3;
	private Boolean allDataEntered;
	private Address storeAddress;
	private String address;
	private String city;
	private String state;
	private String country;
	private String zipCode;

	public Integer getAwaitingSellerId() {
		return awaitingSellerId;
	}

	public void setAwaitingSellerId(Integer awaitingSellerId) {
		this.awaitingSellerId = awaitingSellerId;
	}

	public Boolean getNotASeller() {
		return notASeller;
	}

	public void setNotASeller(Boolean notASeller) {
		this.notASeller = notASeller;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getAllowedUsersCount() {
		return allowedUsersCount;
	}

	public void setAllowedUsersCount(Integer allowedUsersCount) {
		this.allowedUsersCount = allowedUsersCount;
	}

	public Integer getActiveUsersCount() {
		return activeUsersCount;
	}

	public void setActiveUsersCount(Integer activeUsersCount) {
		this.activeUsersCount = activeUsersCount;
	}

	public String getStoreStatus() {
		return storeStatus;
	}

	public void setStoreStatus(String storeStatus) {
		this.storeStatus = storeStatus;
	}

	public Boolean getIsActivated() {
		return isActivated;
	}

	public void setIsActivated(Boolean isActivated) {
		this.isActivated = isActivated;
	}

	public Long getActiveFrom() {
		return activeFrom;
	}

	public void setActiveFrom(Long activeFrom) {
		this.activeFrom = activeFrom;
	}

	public Long getActiveTo() {
		return activeTo;
	}

	public void setActiveTo(Long activeTo) {
		this.activeTo = activeTo;
	}

	public Integer getMaximumProductSku() {
		return maximumProductSku;
	}

	public void setMaximumProductSku(Integer maximumProductSku) {
		this.maximumProductSku = maximumProductSku;
	}

	public String getPreference() {
		return preference;
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}

	public String getFulfillment() {
		return fulfillment;
	}

	public void setFulfillment(String fulfillment) {
		this.fulfillment = fulfillment;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getModeOfContact() {
		return modeOfContact;
	}

	public void setModeOfContact(String modeOfContact) {
		this.modeOfContact = modeOfContact;
	}

	public String getTimeOfContact() {
		return timeOfContact;
	}

	public void setTimeOfContact(String timeOfContact) {
		this.timeOfContact = timeOfContact;
	}

	public String getOrderPickFrequency() {
		return orderPickFrequency;
	}

	public void setOrderPickFrequency(String orderPickFrequency) {
		this.orderPickFrequency = orderPickFrequency;
	}

	public String getAdditionalParameter1() {
		return additionalParameter1;
	}

	public void setAdditionalParameter1(String additionalParameter1) {
		this.additionalParameter1 = additionalParameter1;
	}

	public String getAdditionalParameter2() {
		return additionalParameter2;
	}

	public void setAdditionalParameter2(String additionalParameter2) {
		this.additionalParameter2 = additionalParameter2;
	}

	public String getAdditionalParameter3() {
		return additionalParameter3;
	}

	public void setAdditionalParameter3(String additionalParameter3) {
		this.additionalParameter3 = additionalParameter3;
	}

	public Boolean getAllDataEntered() {
		return allDataEntered;
	}

	public void setAllDataEntered(Boolean allDataEntered) {
		this.allDataEntered = allDataEntered;
	}

	public Address getStoreAddress() {
		return storeAddress;
	}

	public void setStoreAddress(Address storeAddress) {
		this.storeAddress = storeAddress;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}
