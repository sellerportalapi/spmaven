package com.vossa.api.v1.seller.pojos;

import java.util.List;

public class ASN {

	private Integer asnId;
	private String asnNumber;
	private Integer sellerId;
	private String sellerName;
	private String sellerEmail;
	private String sellerPhone;
	private Integer storeId;
	private String sellerCode;
	private String asnStatus;
	private String asnType;
	private String shipmentType;
	private String deliveryFacility;
	private String referenceNumber;
	private String bolNumber;
	private String carrier;
	private String trackingNumber;
	private Long shipmentDate;
	private Long estimatedArrival;
	private String shipmentFrom;
	private Float cogi;
	private String additionalNotes;
	private Long asnDate;

	private List<ASNPalletLevel> pallets;
	private List<ASNCaseLevel> cases;
	private List<ASNSkuLevel> skus;

	private int list_id;

	public Integer getAsnId() {
		return asnId;
	}

	public void setAsnId(Integer asnId) {
		this.asnId = asnId;
	}

	public String getAsnNumber() {
		return asnNumber;
	}

	public void setAsnNumber(String asnNumber) {
		this.asnNumber = asnNumber;
	}

	public Integer getSellerId() {
		return sellerId;
	}

	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getAsnStatus() {
		return asnStatus;
	}

	public void setAsnStatus(String asnStatus) {
		this.asnStatus = asnStatus;
	}

	public String getAsnType() {
		return asnType;
	}

	public void setAsnType(String asnType) {
		this.asnType = asnType;
	}

	public String getShipmentType() {
		return shipmentType;
	}

	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}

	public String getDeliveryFacility() {
		return deliveryFacility;
	}

	public void setDeliveryFacility(String deliveryFacility) {
		this.deliveryFacility = deliveryFacility;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getBolNumber() {
		return bolNumber;
	}

	public void setBolNumber(String bolNumber) {
		this.bolNumber = bolNumber;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public Long getShipmentDate() {
		return shipmentDate;
	}

	public void setShipmentDate(Long shipmentDate) {
		this.shipmentDate = shipmentDate;
	}

	public Long getEstimatedArrival() {
		return estimatedArrival;
	}

	public void setEstimatedArrival(Long estimatedArrival) {
		this.estimatedArrival = estimatedArrival;
	}

	public String getShipmentFrom() {
		return shipmentFrom;
	}

	public void setShipmentFrom(String shipmentFrom) {
		this.shipmentFrom = shipmentFrom;
	}

	public Float getCogi() {
		return cogi;
	}

	public void setCogi(Float cogi) {
		this.cogi = cogi;
	}

	public String getAdditionalNotes() {
		return additionalNotes;
	}

	public void setAdditionalNotes(String additionalNotes) {
		this.additionalNotes = additionalNotes;
	}

	public Long getAsnDate() {
		return asnDate;
	}

	public void setAsnDate(Long asnDate) {
		this.asnDate = asnDate;
	}

	public List<ASNPalletLevel> getPallets() {
		return pallets;
	}

	public void setPallets(List<ASNPalletLevel> pallets) {
		this.pallets = pallets;
	}

	public List<ASNCaseLevel> getCases() {
		return cases;
	}

	public void setCases(List<ASNCaseLevel> cases) {
		this.cases = cases;
	}

	public List<ASNSkuLevel> getSkus() {
		return skus;
	}

	public void setSkus(List<ASNSkuLevel> skus) {
		this.skus = skus;
	}

	public int getList_id() {
		return list_id;
	}

	public void setList_id(int list_id) {
		this.list_id = list_id;
	}

	public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getSellerEmail() {
		return sellerEmail;
	}

	public void setSellerEmail(String sellerEmail) {
		this.sellerEmail = sellerEmail;
	}

	public String getSellerPhone() {
		return sellerPhone;
	}

	public void setSellerPhone(String sellerPhone) {
		this.sellerPhone = sellerPhone;
	}
}
