package com.vossa.api.v1.seller.pojos;

public class Product {

	private String sku;
	private String parent_sku_id;
	private Integer upc;
	private Integer mpn;
	private String product_name;
	private String description;
	private String long_description;
	private Float weight;
	private Float price_on_ecommerce;
	private String main_image;
	private Boolean child_product;
	private String display_position;
	private Boolean multipack;
	private Integer multipack_Quantity;
	private String multipack_items_name;
	private Float product_length;
	private Float product_width;
	private Float product_height;
	private String product_color;
	private String product_size;
	private String volume_UOM;
	private String weight_UOM;
	private String dimensions_UOM;
	private Float standard_price;
	private Float retail_price;
	private String barcode_number;
	private String shipping_exceptions;
	private String package_type;
	private Float shipping_weight;
	private Float package_length;
	private Float package_width;
	private Float package_height;
	private String package_weight_UOM;
	private Boolean handling_type;
	private Boolean fragile;
	private Boolean country_of_manufacture;
	private String sub_category;
	private Boolean taxable;
	private String tax_category;
	private String brand;
	private Boolean serial_number_tracked;
	private Boolean batch_number_tracked;
	private String manufacturer;
	private String features;
	private String search_term_suggestions;
	private Boolean customization_available;
	private Boolean gift_message_available;
	private Boolean Safety_wrap_required;
	private String side_image_1;
	private String side_image_2;
	private String top_image_1;
	private String top_image_2;
	private String front_image_1;
	private String front_image_2;
	private String rear_image_1;
	private String rear_image_2;
	private String specification_image;
	private String size_chart_image;
	private String additional_attribute_1;
	private String additional_attribute_2;
	private String additional_attribute_3;
	private String additional_attribute_4;
	private String additional_attribute_5;
	private String additional_attribute_6;
	private String additional_attribute_7;
	private String additional_attribute_8;
	private String additional_attribute_9;
	private String additional_attribute_10;
	private Long created_at;
	private Long updated_at;
	private String product_type;
	private String storage;
	private String category_level;
	private String parent_category;
	private String child_category;
	private Boolean online_availability;
	private String material;
	private String design;
	private String sleeve;
	private String radial_dimension;
	private Long new_from_date;
	private Long new_to_date;
	private String asin;
	private String vas_description;
	private String product_category;
	private String package_dimension_UOM;

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getParent_sku_id() {
		return parent_sku_id;
	}

	public void setParent_sku_id(String parent_sku_id) {
		this.parent_sku_id = parent_sku_id;
	}

	public Integer getUpc() {
		return upc;
	}

	public void setUpc(Integer upc) {
		this.upc = upc;
	}

	public Integer getMpn() {
		return mpn;
	}

	public void setMpn(Integer mpn) {
		this.mpn = mpn;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLong_description() {
		return long_description;
	}

	public void setLong_description(String long_description) {
		this.long_description = long_description;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Float getPrice_on_ecommerce() {
		return price_on_ecommerce;
	}

	public void setPrice_on_ecommerce(Float price_on_ecommerce) {
		this.price_on_ecommerce = price_on_ecommerce;
	}

	public String getMain_image() {
		return main_image;
	}

	public void setMain_image(String main_image) {
		this.main_image = main_image;
	}

	public Boolean getChild_product() {
		return child_product;
	}

	public void setChild_product(Boolean child_product) {
		this.child_product = child_product;
	}

	public String getDisplay_position() {
		return display_position;
	}

	public void setDisplay_position(String display_position) {
		this.display_position = display_position;
	}

	public Boolean getMultipack() {
		return multipack;
	}

	public void setMultipack(Boolean multipack) {
		this.multipack = multipack;
	}

	public Integer getMultipack_Quantity() {
		return multipack_Quantity;
	}

	public void setMultipack_Quantity(Integer multipack_Quantity) {
		this.multipack_Quantity = multipack_Quantity;
	}

	public String getMultipack_items_name() {
		return multipack_items_name;
	}

	public void setMultipack_items_name(String multipack_items_name) {
		this.multipack_items_name = multipack_items_name;
	}

	public Float getProduct_length() {
		return product_length;
	}

	public void setProduct_length(Float product_length) {
		this.product_length = product_length;
	}

	public Float getProduct_width() {
		return product_width;
	}

	public void setProduct_width(Float product_width) {
		this.product_width = product_width;
	}

	public Float getProduct_height() {
		return product_height;
	}

	public void setProduct_height(Float product_height) {
		this.product_height = product_height;
	}

	public String getProduct_color() {
		return product_color;
	}

	public void setProduct_color(String product_color) {
		this.product_color = product_color;
	}

	public String getProduct_size() {
		return product_size;
	}

	public void setProduct_size(String product_size) {
		this.product_size = product_size;
	}

	public String getVolume_UOM() {
		return volume_UOM;
	}

	public void setVolume_UOM(String volume_UOM) {
		this.volume_UOM = volume_UOM;
	}

	public String getWeight_UOM() {
		return weight_UOM;
	}

	public void setWeight_UOM(String weight_UOM) {
		this.weight_UOM = weight_UOM;
	}

	public String getDimensions_UOM() {
		return dimensions_UOM;
	}

	public void setDimensions_UOM(String dimensions_UOM) {
		this.dimensions_UOM = dimensions_UOM;
	}

	public Float getStandard_price() {
		return standard_price;
	}

	public void setStandard_price(Float standard_price) {
		this.standard_price = standard_price;
	}

	public Float getRetail_price() {
		return retail_price;
	}

	public void setRetail_price(Float retail_price) {
		this.retail_price = retail_price;
	}

	public String getBarcode_number() {
		return barcode_number;
	}

	public void setBarcode_number(String barcode_number) {
		this.barcode_number = barcode_number;
	}

	public String getShipping_exceptions() {
		return shipping_exceptions;
	}

	public void setShipping_exceptions(String shipping_exceptions) {
		this.shipping_exceptions = shipping_exceptions;
	}

	public String getPackage_type() {
		return package_type;
	}

	public void setPackage_type(String package_type) {
		this.package_type = package_type;
	}

	public Float getShipping_weight() {
		return shipping_weight;
	}

	public void setShipping_weight(Float shipping_weight) {
		this.shipping_weight = shipping_weight;
	}

	public Float getPackage_length() {
		return package_length;
	}

	public void setPackage_length(Float package_length) {
		this.package_length = package_length;
	}

	public Float getPackage_width() {
		return package_width;
	}

	public void setPackage_width(Float package_width) {
		this.package_width = package_width;
	}

	public Float getPackage_height() {
		return package_height;
	}

	public void setPackage_height(Float package_height) {
		this.package_height = package_height;
	}

	public String getPackage_weight_UOM() {
		return package_weight_UOM;
	}

	public void setPackage_weight_UOM(String package_weight_UOM) {
		this.package_weight_UOM = package_weight_UOM;
	}

	public Boolean getHandling_type() {
		return handling_type;
	}

	public void setHandling_type(Boolean handling_type) {
		this.handling_type = handling_type;
	}

	public Boolean getFragile() {
		return fragile;
	}

	public void setFragile(Boolean fragile) {
		this.fragile = fragile;
	}

	public Boolean getCountry_of_manufacture() {
		return country_of_manufacture;
	}

	public void setCountry_of_manufacture(Boolean country_of_manufacture) {
		this.country_of_manufacture = country_of_manufacture;
	}

	public String getSub_category() {
		return sub_category;
	}

	public void setSub_category(String sub_category) {
		this.sub_category = sub_category;
	}

	public Boolean getTaxable() {
		return taxable;
	}

	public void setTaxable(Boolean taxable) {
		this.taxable = taxable;
	}

	public String getTax_category() {
		return tax_category;
	}

	public void setTax_category(String tax_category) {
		this.tax_category = tax_category;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Boolean getSerial_number_tracked() {
		return serial_number_tracked;
	}

	public void setSerial_number_tracked(Boolean serial_number_tracked) {
		this.serial_number_tracked = serial_number_tracked;
	}

	public Boolean getBatch_number_tracked() {
		return batch_number_tracked;
	}

	public void setBatch_number_tracked(Boolean batch_number_tracked) {
		this.batch_number_tracked = batch_number_tracked;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getSearch_term_suggestions() {
		return search_term_suggestions;
	}

	public void setSearch_term_suggestions(String search_term_suggestions) {
		this.search_term_suggestions = search_term_suggestions;
	}

	public Boolean getCustomization_available() {
		return customization_available;
	}

	public void setCustomization_available(Boolean customization_available) {
		this.customization_available = customization_available;
	}

	public Boolean getGift_message_available() {
		return gift_message_available;
	}

	public void setGift_message_available(Boolean gift_message_available) {
		this.gift_message_available = gift_message_available;
	}

	public Boolean getSafety_wrap_required() {
		return Safety_wrap_required;
	}

	public void setSafety_wrap_required(Boolean safety_wrap_required) {
		Safety_wrap_required = safety_wrap_required;
	}

	public String getSide_image_1() {
		return side_image_1;
	}

	public void setSide_image_1(String side_image_1) {
		this.side_image_1 = side_image_1;
	}

	public String getSide_image_2() {
		return side_image_2;
	}

	public void setSide_image_2(String side_image_2) {
		this.side_image_2 = side_image_2;
	}

	public String getTop_image_1() {
		return top_image_1;
	}

	public void setTop_image_1(String top_image_1) {
		this.top_image_1 = top_image_1;
	}

	public String getTop_image_2() {
		return top_image_2;
	}

	public void setTop_image_2(String top_image_2) {
		this.top_image_2 = top_image_2;
	}

	public String getFront_image_1() {
		return front_image_1;
	}

	public void setFront_image_1(String front_image_1) {
		this.front_image_1 = front_image_1;
	}

	public String getFront_image_2() {
		return front_image_2;
	}

	public void setFront_image_2(String front_image_2) {
		this.front_image_2 = front_image_2;
	}

	public String getRear_image_1() {
		return rear_image_1;
	}

	public void setRear_image_1(String rear_image_1) {
		this.rear_image_1 = rear_image_1;
	}

	public String getRear_image_2() {
		return rear_image_2;
	}

	public void setRear_image_2(String rear_image_2) {
		this.rear_image_2 = rear_image_2;
	}

	public String getSpecification_image() {
		return specification_image;
	}

	public void setSpecification_image(String specification_image) {
		this.specification_image = specification_image;
	}

	public String getSize_chart_image() {
		return size_chart_image;
	}

	public void setSize_chart_image(String size_chart_image) {
		this.size_chart_image = size_chart_image;
	}

	public String getAdditional_attribute_1() {
		return additional_attribute_1;
	}

	public void setAdditional_attribute_1(String additional_attribute_1) {
		this.additional_attribute_1 = additional_attribute_1;
	}

	public String getAdditional_attribute_2() {
		return additional_attribute_2;
	}

	public void setAdditional_attribute_2(String additional_attribute_2) {
		this.additional_attribute_2 = additional_attribute_2;
	}

	public String getAdditional_attribute_3() {
		return additional_attribute_3;
	}

	public void setAdditional_attribute_3(String additional_attribute_3) {
		this.additional_attribute_3 = additional_attribute_3;
	}

	public String getAdditional_attribute_4() {
		return additional_attribute_4;
	}

	public void setAdditional_attribute_4(String additional_attribute_4) {
		this.additional_attribute_4 = additional_attribute_4;
	}

	public String getAdditional_attribute_5() {
		return additional_attribute_5;
	}

	public void setAdditional_attribute_5(String additional_attribute_5) {
		this.additional_attribute_5 = additional_attribute_5;
	}

	public String getAdditional_attribute_6() {
		return additional_attribute_6;
	}

	public void setAdditional_attribute_6(String additional_attribute_6) {
		this.additional_attribute_6 = additional_attribute_6;
	}

	public String getAdditional_attribute_7() {
		return additional_attribute_7;
	}

	public void setAdditional_attribute_7(String additional_attribute_7) {
		this.additional_attribute_7 = additional_attribute_7;
	}

	public String getAdditional_attribute_8() {
		return additional_attribute_8;
	}

	public void setAdditional_attribute_8(String additional_attribute_8) {
		this.additional_attribute_8 = additional_attribute_8;
	}

	public String getAdditional_attribute_9() {
		return additional_attribute_9;
	}

	public void setAdditional_attribute_9(String additional_attribute_9) {
		this.additional_attribute_9 = additional_attribute_9;
	}

	public String getAdditional_attribute_10() {
		return additional_attribute_10;
	}

	public void setAdditional_attribute_10(String additional_attribute_10) {
		this.additional_attribute_10 = additional_attribute_10;
	}

	public Long getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Long created_at) {
		this.created_at = created_at;
	}

	public Long getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Long updated_at) {
		this.updated_at = updated_at;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}

	public String getStorage() {
		return storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getCategory_level() {
		return category_level;
	}

	public void setCategory_level(String category_level) {
		this.category_level = category_level;
	}

	public String getParent_category() {
		return parent_category;
	}

	public void setParent_category(String parent_category) {
		this.parent_category = parent_category;
	}

	public String getChild_category() {
		return child_category;
	}

	public void setChild_category(String child_category) {
		this.child_category = child_category;
	}

	public Boolean getOnline_availability() {
		return online_availability;
	}

	public void setOnline_availability(Boolean online_availability) {
		this.online_availability = online_availability;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getSleeve() {
		return sleeve;
	}

	public void setSleeve(String sleeve) {
		this.sleeve = sleeve;
	}

	public String getRadial_dimension() {
		return radial_dimension;
	}

	public void setRadial_dimension(String radial_dimension) {
		this.radial_dimension = radial_dimension;
	}

	public Long getNew_from_date() {
		return new_from_date;
	}

	public void setNew_from_date(Long new_from_date) {
		this.new_from_date = new_from_date;
	}

	public Long getNew_to_date() {
		return new_to_date;
	}

	public void setNew_to_date(Long new_to_date) {
		this.new_to_date = new_to_date;
	}

	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	public String getVas_description() {
		return vas_description;
	}

	public void setVas_description(String vas_description) {
		this.vas_description = vas_description;
	}

	public String getProduct_category() {
		return product_category;
	}

	public void setProduct_category(String product_category) {
		this.product_category = product_category;
	}

	public String getPackage_dimension_UOM() {
		return package_dimension_UOM;
	}

	public void setPackage_dimension_UOM(String package_dimension_UOM) {
		this.package_dimension_UOM = package_dimension_UOM;
	}
}
