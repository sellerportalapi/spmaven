package com.vossa.api.v1.seller.pojos;

import java.util.List;

public class Order {

	private String orderId;
	private Integer storeId;
	private String orderStatus;
	private String source;
	private String sourceCode;
	private Float orderSubtotal;
	private Float orderTax;
	private Float orderShippingCharges;
	private Float orderTotal;
	private Long createdOn;
	private Long lastStatusUpdate;

	private OrderCustomer customer;
	private List<OrderList> orderList;

	private int list_id;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public Float getOrderSubtotal() {
		return orderSubtotal;
	}

	public void setOrderSubtotal(Float orderSubtotal) {
		this.orderSubtotal = orderSubtotal;
	}

	public Float getOrderTax() {
		return orderTax;
	}

	public void setOrderTax(Float orderTax) {
		this.orderTax = orderTax;
	}

	public Float getOrderShippingCharges() {
		return orderShippingCharges;
	}

	public void setOrderShippingCharges(Float orderShippingCharges) {
		this.orderShippingCharges = orderShippingCharges;
	}

	public Float getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(Float orderTotal) {
		this.orderTotal = orderTotal;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}

	public long getLastStatusUpdate() {
		return lastStatusUpdate;
	}

	public void setLastStatusUpdate(Long lastStatusUpdate) {
		this.lastStatusUpdate = lastStatusUpdate;
	}

	public OrderCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(OrderCustomer customer) {
		this.customer = customer;
	}

	public List<OrderList> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<OrderList> orderList) {
		this.orderList = orderList;
	}

	public int getList_id() {
		return list_id;
	}

	public void setList_id(int list_id) {
		this.list_id = list_id;
	}
}
