package com.vossa.api.v1.seller.pojos;

public class UserLogin {

	private Integer sellerId;
	private Integer failedLoginCount;
	private Long lastLoginAttemptAt;
	private Long lastLoginAt;
	private Long lastLoginFailedAt;
	private Long accountLockedAt;

	public Integer getSellerId() {
		return sellerId;
	}

	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}

	public Integer getFailedLoginCount() {
		return failedLoginCount;
	}

	public void setFailedLoginCount(Integer failedLoginCount) {
		this.failedLoginCount = failedLoginCount;
	}

	public Long getLastLoginAttemptAt() {
		return lastLoginAttemptAt;
	}

	public void setLastLoginAttemptAt(Long lastLoginAttemptAt) {
		this.lastLoginAttemptAt = lastLoginAttemptAt;
	}

	public Long getLastLoginAt() {
		return lastLoginAt;
	}

	public void setLastLoginAt(Long lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}

	public Long getLastLoginFailedAt() {
		return lastLoginFailedAt;
	}

	public void setLastLoginFailedAt(Long lastLoginFailedAt) {
		this.lastLoginFailedAt = lastLoginFailedAt;
	}

	public Long getAccountLockedAt() {
		return accountLockedAt;
	}

	public void setAccountLockedAt(Long accountLockedAt) {
		this.accountLockedAt = accountLockedAt;
	}
}
