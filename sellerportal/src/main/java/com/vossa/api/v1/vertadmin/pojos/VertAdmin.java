package com.vossa.api.v1.vertadmin.pojos;

import java.util.List;

public class VertAdmin {

	private Integer userId;
	private String userName;
	private String password;
	private String name;
	private String email;
	private String phoneNumber;
	private String userType;
	private String vertuserStatus;
	private Boolean isActivated;
	private Boolean firstlogin;
	private Boolean isFirstUser;

	private List<Integer> vertRoles;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getVertuserStatus() {
		return vertuserStatus;
	}

	public void setVertuserStatus(String vertuserStatus) {
		this.vertuserStatus = vertuserStatus;
	}

	public Boolean getIsActivated() {
		return isActivated;
	}

	public void setIsActivated(Boolean isActivated) {
		this.isActivated = isActivated;
	}

	public Boolean getFirstlogin() {
		return firstlogin;
	}

	public void setFirstlogin(Boolean firstlogin) {
		this.firstlogin = firstlogin;
	}

	public Boolean getIsFirstUser() {
		return isFirstUser;
	}

	public void setIsFirstUser(Boolean isFirstUser) {
		this.isFirstUser = isFirstUser;
	}

	public List<Integer> getVertRoles() {
		return vertRoles;
	}

	public void setVertRoles(List<Integer> vertRoles) {
		this.vertRoles = vertRoles;
	}
}