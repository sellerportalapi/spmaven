package com.vossa.api.v1.seller.pojos;

import java.util.List;

public class Seller {

	private Store store;
	private List<User> users;
	private List<SellingMode> sellingMode;

	public Store getStore() {
		return store;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<SellingMode> getSellingMode() {
		return sellingMode;
	}

	public void setSellingMode(List<SellingMode> sellingMode) {
		this.sellingMode = sellingMode;
	}

	public void setStore(Store store) {
		this.store = store;
	}
}