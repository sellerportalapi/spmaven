package com.vossa.api.v1.vertadmin.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.vossa.api.general.annotations.filterannotations.Insecured;
import com.vossa.api.general.constants.CorsHeaders;
import com.vossa.api.others.ResponseClass;

@Provider
@Insecured
public class InsecuredFilter implements ContainerRequestFilter, ContainerResponseFilter {

	private List<String> allowedOrigins = new ArrayList<>();
	private boolean allowCredentials = true;
//	private String allowedMethods = "GET,POST,PUT,DELETE";
//	private String allowedHeaders = "content-type,accept,authorization,token";
	private String exposedHeaders = "content-type,accept,authorization,token";

	public InsecuredFilter() {
		allowedOrigins.add("*");
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		System.out.println("InsecuredFilter-Request-" + requestContext.getUriInfo().getPath());

		String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
		try {
			if (origin != null) {
				checkOrigin(origin, requestContext);
			}
		} catch (ForbiddenException e) {
			e.printStackTrace();
			requestContext.abortWith(Response.status(403).entity(e.getMessage()).build());
		}
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		int status = 200;
		if(responseContext.hasEntity() && responseContext.getEntity() instanceof ResponseClass) {
			ResponseClass responseClass = (ResponseClass) responseContext.getEntity();
			status = responseClass.getCode();
		}
		System.out.println("InsecuredFilter-Response-" + requestContext.getUriInfo().getPath() + "-code" + status);

		String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
		if (origin == null || requestContext.getProperty("cors.failure") != null) {
			return;
		}

		MultivaluedMap<String, Object> headers = responseContext.getHeaders();
		headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
		headers.putSingle(CorsHeaders.VARY, CorsHeaders.ORIGIN);

		if (allowCredentials)
			headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, allowCredentials);
//		headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_METHODS, allowedMethods);

//		headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS, allowedHeaders);
		if (exposedHeaders != null)
			headers.putSingle(CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, exposedHeaders);
	}

	private void checkOrigin(String origin, ContainerRequestContext requestContext) throws ForbiddenException {
		if (!allowedOrigins.contains("*") && !allowedOrigins.contains(origin)) {
			requestContext.setProperty("cors.failure", true);
			throw new ForbiddenException("Request-Origin is not allowed to access this api.");
		}
	}
}
