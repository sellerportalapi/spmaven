package com.vossa.api.v1.vertadmin.pojos;

import java.util.List;

public class VertFeature {

	private Integer feature_id;
	private String feature;
	private String title;
	private Integer parent_id;
	private Integer none;
	private Integer view;
	private Integer view_edit;
	private Integer selected_value;

	private List<VertFeature> sub_features;

	public Integer getFeature_id() {
		return feature_id;
	}

	public void setFeature_id(Integer feature_id) {
		this.feature_id = feature_id;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getParent_id() {
		return parent_id;
	}

	public void setParent_id(Integer parent_id) {
		this.parent_id = parent_id;
	}

	public Integer getNone() {
		return none;
	}

	public void setNone(Integer none) {
		this.none = none;
	}

	public Integer getView() {
		return view;
	}

	public void setView(Integer view) {
		this.view = view;
	}

	public Integer getView_edit() {
		return view_edit;
	}

	public void setView_edit(Integer view_edit) {
		this.view_edit = view_edit;
	}

	public Integer getSelected_value() {
		return selected_value;
	}

	public void setSelected_value(Integer selected_value) {
		this.selected_value = selected_value;
	}

	public List<VertFeature> getSub_features() {
		return sub_features;
	}

	public void setSub_features(List<VertFeature> sub_features) {
		this.sub_features = sub_features;
	}
}
