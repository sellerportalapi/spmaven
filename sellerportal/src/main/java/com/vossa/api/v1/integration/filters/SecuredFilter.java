package com.vossa.api.v1.integration.filters;

import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Priority;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.stores.RoleServices;
import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.constants.CorsHeaders;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.JWT;
import com.vossa.api.others.token.JWTHandler;
import com.vossa.api.others.token.UserClaims;

@Provider
@Secured
@Priority(Priorities.AUTHORIZATION)
public class SecuredFilter implements ContainerRequestFilter, ContainerResponseFilter {

	private static final String TOKEN_HEADER_KEY = "token";
//	private static final String TOKEN_HEADER_KEY = "Authorization";
//	private static final String TOKEN_SCHEME_KEY = "Verte";

	private List<String> allowedOrigins = new ArrayList<>();
	private boolean allowCredentials = true;
	private String exposedHeaders = "content-type,accept,authorization,token";

	@Context
	private ResourceInfo resourceInfo;

	public SecuredFilter() {
		allowedOrigins.add("*");
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		System.out.println("SecuredFilter-Request-" + requestContext.getUriInfo().getPath());

		String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
		try {
			if (origin != null) {
				checkOrigin(origin, requestContext);
			}
		} catch (ForbiddenException e) {
			e.printStackTrace();
			requestContext.abortWith(Response.status(403).entity(e.getMessage()).build());
			return;
		}

		String authToken = requestContext.getHeaderString(TOKEN_HEADER_KEY);
		if (authToken == null) {
			Response response = Response.status(Response.Status.UNAUTHORIZED)
					.entity("There is no valid Authorization header").build();
			requestContext.abortWith(response);
			return;
		}
//		else if (authToken.startsWith(TOKEN_SCHEME_KEY) == false) {
//			Response response = Response.status(Response.Status.UNAUTHORIZED).entity("There is no valid token Scheme")
//					.build();
//			requestContext.abortWith(response);
//			return;
//		}

		String token = authToken;
//		String token = authToken.replaceFirst(TOKEN_SCHEME_KEY, "").trim();
		if (token.chars().filter(ch -> ch == '.').count() != 2) {
			Response response = Response.status(Response.Status.UNAUTHORIZED)
					.entity("Invalid token format or empty token").build();
			requestContext.abortWith(response);
			return;
		}

		try {
			JWT jwt = JWTHandler.parseJWT(token);
			UserClaims user = jwt.getUser();

			if (!user.getUserType().equals(UserType.SUPERUSER.toString())
					&& !user.getUserType().equals(UserType.STANDARDUSER.toString())) {
				String errmsg = "You are trying to get access with an invalid token.";
				Response response = Response.status(Response.Status.FORBIDDEN).entity(errmsg).build();
				requestContext.abortWith(response);
				return;
			}

			Method resourceMethod = resourceInfo.getResourceMethod();
			List<UserType> allowedUserTypeList = extractUserTypes(resourceMethod);
			if (!allowedUserTypeList.isEmpty())
				checkPermissions(allowedUserTypeList, user.getUserType());

			if (user.getUserType().equals(UserType.STANDARDUSER.toString())) {
				String path = requestContext.getUriInfo().getPath();
				Map<String, Object> permission = user.getPermission();
				if (permission == null) {
					Response response = Response.status(Response.Status.UNAUTHORIZED)
							.entity("Invalid token. No permission found for the standard user.").build();
					requestContext.abortWith(response);
					return;
				}

				boolean valid = new RoleServices().checkStdUserPermission(permission, path, null);
				if (valid == false) {
					Response response = Response.status(Response.Status.FORBIDDEN)
							.entity("You are not allowed to access this api.").build();
					requestContext.abortWith(response);
					return;
				}
			}

			requestContext.setProperty("user", user);

//			MultivaluedMap<String, String> headers = requestContext.getHeaders();
//			headers.add("userName", user.getUserName());
			return;
		} catch (ForbiddenException e) {
			System.out.println(e);
			requestContext.abortWith(e.getResponse());
			return;
		} catch (Exception e) {
			System.out.println(e);
			Response response = Response.status(Response.Status.UNAUTHORIZED).entity(e.toString()).build();
			requestContext.abortWith(response);
			return;
		}
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		int status = 200;
		if(responseContext.hasEntity() && responseContext.getEntity() instanceof ResponseClass) {
			ResponseClass responseClass = (ResponseClass) responseContext.getEntity();
			status = responseClass.getCode();
		}
		System.out.println("SecuredFilter-Response-" + requestContext.getUriInfo().getPath() + "-code" + status);

		String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
		if (origin == null || requestContext.getProperty("cors.failure") != null) {
			return;
		}

		MultivaluedMap<String, Object> headers = responseContext.getHeaders();
		headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
		headers.putSingle(CorsHeaders.VARY, CorsHeaders.ORIGIN);

		if (allowCredentials)
			headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, allowCredentials);
//		headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_METHODS, allowedMethods);

//		headers.putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS, allowedHeaders);
		if (exposedHeaders != null)
			headers.putSingle(CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, exposedHeaders);
	}

	// Extract the UserType from the annotated element
	private List<UserType> extractUserTypes(AnnotatedElement annotatedElement) {
		if (annotatedElement == null) {
			return new ArrayList<UserType>();
		} else {
			Secured secured = annotatedElement.getAnnotation(Secured.class);

			if (secured == null) {
				return new ArrayList<UserType>();
			} else {
				UserType[] allowedUserTypes = secured.value();
				return Arrays.asList(allowedUserTypes);
			}
		}
	}

	// Check the user has permission to make the api request
	private void checkPermissions(List<UserType> allowedUserTypes, String userType) throws Exception {
		for (UserType type : allowedUserTypes) {
			if (userType.equals(type.toString())) {
				return;
			}
		}
		Response response = Response.status(Response.Status.FORBIDDEN)
				.entity("FORBIDDEN. ACCESS DENIED to use this api method.").build();
		throw new ForbiddenException(response);
	}

	private void checkOrigin(String origin, ContainerRequestContext requestContext) throws ForbiddenException {
		if (!allowedOrigins.contains("*") && !allowedOrigins.contains(origin)) {
			requestContext.setProperty("cors.failure", true);
			throw new ForbiddenException("Request-Origin is not allowed to access this api.");
		}
	}
}
