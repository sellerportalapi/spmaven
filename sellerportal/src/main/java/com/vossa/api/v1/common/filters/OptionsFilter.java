package com.vossa.api.v1.common.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.vossa.api.general.constants.CorsHeaders;

@Provider
public class OptionsFilter implements ContainerRequestFilter, ContainerResponseFilter {

	private static final String OPTIONS_METHOD = "OPTIONS";

	private List<String> allowedOrigins = new ArrayList<>();
	private boolean allowCredentials = true;
	private String allowedMethods = "GET,POST";
	private String allowedHeaders = "content-type,accept";
	private int corsMaxAge = 600; // Max-Age will be 10 minutes (10*60s=600s).

	public OptionsFilter() {
		allowedOrigins.add("*");
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		if (!requestContext.getMethod().equalsIgnoreCase(OPTIONS_METHOD))
			return;

		String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
		try {
			if (origin != null) {
				checkOrigin(origin, requestContext);
				handlePreflight(origin, requestContext);
				return;
			}
		} catch (ForbiddenException e) {
			e.printStackTrace();
			requestContext.abortWith(Response.status(403).entity(e.getMessage()).build());
		}
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {

	}

	private void checkOrigin(String origin, ContainerRequestContext requestContext) throws ForbiddenException {
		if (!allowedOrigins.contains("*") && !allowedOrigins.contains(origin)) {
			requestContext.setProperty("cors.failure", true);
			throw new ForbiddenException("Request-Origin is not allowed to access this api.");
		}
	}

	private void handlePreflight(String origin, ContainerRequestContext requestContext) throws IOException {

		Response.ResponseBuilder builder = Response.ok();
		builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
		builder.header(CorsHeaders.VARY, CorsHeaders.ORIGIN);

		if (allowCredentials)
			builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");

		String requestMethod = requestContext.getHeaderString(CorsHeaders.ACCESS_CONTROL_REQUEST_METHOD);
		if (requestMethod != null) {
			if (allowedMethods.contains(requestMethod)) {
				builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_METHODS, allowedMethods);
			} else {
				throw new ForbiddenException("Requested Method (" + requestMethod + ") is not accessible.");
			}
		}

		String requestHeaders = requestContext.getHeaderString(CorsHeaders.ACCESS_CONTROL_REQUEST_HEADERS);
		if (requestHeaders != null) {
			List<String> allowedHeadersList = Arrays.asList(allowedHeaders.toLowerCase().split(","));
			List<String> requestHeadersList = Arrays.asList(requestHeaders.toLowerCase().split(","));
			allowedHeadersList.replaceAll(String::trim);
			requestHeadersList.replaceAll(String::trim);

			if (allowedHeadersList.containsAll(requestHeadersList)) {
				builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS, requestHeaders);
			} else {
				throw new ForbiddenException("Requested Headers (" + requestHeaders + ") is not accessible.");
			}
		}

		if (corsMaxAge > -1) {
			builder.header(CorsHeaders.ACCESS_CONTROL_MAX_AGE, corsMaxAge);
		}

		requestContext.abortWith(builder.build());
	}
}
