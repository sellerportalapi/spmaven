package com.vossa.api.v1.vertadmin.pojos;

import java.util.List;

public class VertRole {

	private Integer role_id;
	private String role_name;
	private String description;
	private List<VertFeature> features;

	public Integer getRole_id() {
		return role_id;
	}

	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<VertFeature> getFeatures() {
		return features;
	}

	public void setFeatures(List<VertFeature> features) {
		this.features = features;
	}
}
