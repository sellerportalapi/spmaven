package com.vossa.api.v1.seller.pojos;

public class OrderReturn {

	private String orderReturnId;
	private String orderId;
	private Integer storeId;
	private String customerName;
	private Integer productId;
	private String sku;
	private String product_name;
	private String product_category;
	private String sub_category;
	private Integer orderedQuantity;
	private Integer returnedQuantity;
	private String returnStatus;
	private Float refundAmount;
	private Long purchasedOn;
	private Long returnRequestedOn;
	private String reasonForReturn;
	private String additionalComments;

	private int list_id;

	public String getOrderReturnId() {
		return orderReturnId;
	}

	public void setOrderReturnId(String orderReturnId) {
		this.orderReturnId = orderReturnId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getProduct_category() {
		return product_category;
	}

	public void setProduct_category(String product_category) {
		this.product_category = product_category;
	}

	public String getSub_category() {
		return sub_category;
	}

	public void setSub_category(String sub_category) {
		this.sub_category = sub_category;
	}

	public Integer getOrderedQuantity() {
		return orderedQuantity;
	}

	public void setOrderedQuantity(Integer orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}

	public Integer getReturnedQuantity() {
		return returnedQuantity;
	}

	public void setReturnedQuantity(Integer returnedQuantity) {
		this.returnedQuantity = returnedQuantity;
	}

	public String getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(String returnStatus) {
		this.returnStatus = returnStatus;
	}

	public Float getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Float refundAmount) {
		this.refundAmount = refundAmount;
	}

	public long getPurchasedOn() {
		return purchasedOn;
	}

	public void setPurchasedOn(Long purchasedOn) {
		this.purchasedOn = purchasedOn;
	}

	public long getReturnRequestedOn() {
		return returnRequestedOn;
	}

	public void setReturnRequestedOn(Long returnRequestedOn) {
		this.returnRequestedOn = returnRequestedOn;
	}

	public String getReasonForReturn() {
		return reasonForReturn;
	}

	public void setReasonForReturn(String reasonForReturn) {
		this.reasonForReturn = reasonForReturn;
	}

	public String getAdditionalComments() {
		return additionalComments;
	}

	public void setAdditionalComments(String additionalComments) {
		this.additionalComments = additionalComments;
	}

	public int getList_id() {
		return list_id;
	}

	public void setList_id(int list_id) {
		this.list_id = list_id;
	}
}
