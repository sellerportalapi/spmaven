package com.vossa.api.v1.common.pojos;

public class AwaitingSeller {

	private int id;
	private String companyName;
	private String name;
	private String email;
	private String phoneNumber;
	private String revenue;
	private Integer maximumProductSku;
	private String sellingMode;
	private String modeOfContact;
	private String preference;
	private String timeOfContact;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public Integer getMaximumProductSku() {
		return maximumProductSku;
	}

	public void setMaximumProductSku(Integer maximumProductSku) {
		this.maximumProductSku = maximumProductSku;
	}

	public String getSellingMode() {
		return sellingMode;
	}

	public void setSellingMode(String sellingMode) {
		this.sellingMode = sellingMode;
	}

	public String getModeOfContact() {
		return modeOfContact;
	}

	public void setModeOfContact(String modeOfContact) {
		this.modeOfContact = modeOfContact;
	}

	public String getPreference() {
		return preference;
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}

	public String getTimeOfContact() {
		return timeOfContact;
	}

	public void setTimeOfContact(String timeOfContact) {
		this.timeOfContact = timeOfContact;
	}
}
