package com.vossa.api.v1.seller.pojos;

public class User {

	private Integer sellerId;
	private String userName;
	private String password;
	private String storeName;
	private Integer storeId;
	private String sellerCode;
	private String name;
	private String email;
	private String phoneNumber;
	private String userType;
	private String sellerStatus;
	private Boolean isActivated;
	private Boolean firstlogin;
	private Boolean isCreatedByVert;
	private Boolean isFirstUser;
	private Boolean notAnUser;

	public Integer getSellerId() {
		return sellerId;
	}

	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getSellerStatus() {
		return sellerStatus;
	}

	public void setSellerStatus(String sellerStatus) {
		this.sellerStatus = sellerStatus;
	}

	public Boolean getIsActivated() {
		return isActivated;
	}

	public void setIsActivated(Boolean isActivated) {
		this.isActivated = isActivated;
	}

	public Boolean getFirstlogin() {
		return firstlogin;
	}

	public void setFirstlogin(Boolean firstlogin) {
		this.firstlogin = firstlogin;
	}

	public Boolean getIsCreatedByVert() {
		return isCreatedByVert;
	}

	public void setIsCreatedByVert(Boolean isCreatedByVert) {
		this.isCreatedByVert = isCreatedByVert;
	}

	public Boolean getIsFirstUser() {
		return isFirstUser;
	}

	public void setIsFirstUser(Boolean isFirstUser) {
		this.isFirstUser = isFirstUser;
	}

	public Boolean getNotAnUser() {
		return notAnUser;
	}

	public void setNotAnUser(Boolean notAnUser) {
		this.notAnUser = notAnUser;
	}
}
