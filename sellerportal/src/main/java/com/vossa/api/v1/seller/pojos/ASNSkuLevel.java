package com.vossa.api.v1.seller.pojos;

public class ASNSkuLevel {

	private Integer asnId;
	private Integer palletId;
	private Float product_length;
	private Float product_width;
	private Float product_height;
	private String dimensions_UOM;
	private Float weight;
	private String weight_UOM;
	private String sku;
	private Integer product_id;
	private Integer quantity;
	private String product_name;
	private String status;

	public Integer getAsnId() {
		return asnId;
	}

	public void setAsnId(Integer asnId) {
		this.asnId = asnId;
	}

	public Integer getPalletId() {
		return palletId;
	}

	public void setPalletId(Integer palletId) {
		this.palletId = palletId;
	}

	public Float getProduct_length() {
		return product_length;
	}

	public void setProduct_length(Float product_length) {
		this.product_length = product_length;
	}

	public Float getProduct_width() {
		return product_width;
	}

	public void setProduct_width(Float product_width) {
		this.product_width = product_width;
	}

	public Float getProduct_height() {
		return product_height;
	}

	public void setProduct_height(Float product_height) {
		this.product_height = product_height;
	}

	public String getDimensions_UOM() {
		return dimensions_UOM;
	}

	public void setDimensions_UOM(String dimensions_UOM) {
		this.dimensions_UOM = dimensions_UOM;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public String getWeight_UOM() {
		return weight_UOM;
	}

	public void setWeight_UOM(String weight_UOM) {
		this.weight_UOM = weight_UOM;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
