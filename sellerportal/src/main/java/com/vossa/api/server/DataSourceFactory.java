package com.vossa.api.server;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import com.vossa.api.others.filehandlers.ConfigFileHandler;

public abstract class DataSourceFactory {

	private static volatile DataSource instance = null;

	public static DataSource getInstance() {
		if (instance == null) {
			synchronized (DataSourceFactory.class) {
				if (instance == null)
					instance = DataSourceFactory.initDataSource();
			}
		}
		return instance;
	}

	/**
	 * Initializing the DataSource instance using Apache Tomcat JDBC Database
	 * Connection Pooling(DBCP) method. By using tomcat-jdbc.jar. When we go by this
	 * method we need the tomcat server to run the application.
	 */
	private static DataSource initDataSource() {
		try {
			ConfigFileHandler config = new ConfigFileHandler();
			String connectionUrl = config.get("mysqlurl") + "/" + config.get("databasename") + "?useSSL=false";

			PoolProperties poolProperties = new PoolProperties();
			poolProperties.setDriverClassName(config.get("mysqldriver"));
			poolProperties.setUrl(connectionUrl);
			poolProperties.setName(config.get("connectionpoolname"));
			poolProperties.setUsername(config.get("dbusername"));
			poolProperties.setPassword(config.get("dbpassword"));

			poolProperties.setInitialSize(Integer.parseInt(config.get("initialsize")));
			poolProperties.setMaxActive(Integer.parseInt(config.get("maxactive")));
			poolProperties.setMaxIdle(Integer.parseInt(config.get("maxidle")));
			poolProperties.setMinIdle(Integer.parseInt(config.get("minidle")));

			poolProperties.setMaxWait(Integer.parseInt(config.get("maxwait")));
			poolProperties.setValidationInterval(Integer.parseInt(config.get("validationInterval")));
			poolProperties
					.setTimeBetweenEvictionRunsMillis(Integer.parseInt(config.get("timeBetweenEvictionRunsMillis")));
			poolProperties.setRemoveAbandonedTimeout(Integer.parseInt(config.get("removeAbandonedTimeout")));

			poolProperties.setValidationQuery("SELECT 1");
			poolProperties.setTestOnBorrow(true);
			poolProperties.setRemoveAbandoned(true);

			DataSource dataSource = new DataSource(poolProperties);
			poolProperties = null;

			// Dummy Connection for checking whether connection is available.
			dataSource.getConnection().close();
			System.out.println("GOT DataSource (DBCP), Connection Object tested, working fine.");

			return dataSource;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return instance;
	}
}
