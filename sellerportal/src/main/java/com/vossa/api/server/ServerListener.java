package com.vossa.api.server;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.tomcat.jdbc.pool.DataSource;

import com.mysql.jdbc.AbandonedConnectionCleanupThread;

public class ServerListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		// On Application Startup (ie. Tomcat Startup).

		System.out.println();
		System.out.println("\n-------------------BEGIN-------------------");
		System.out.println("Application Startup (Tomcat Server running...)");

		System.out.println("Doing DBCP, Initalizing DataSource (It provides the Connection Object).");
		DataSourceFactory.getInstance();

		System.out.println("Everything working fine, Enjoy your day...\n");
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// On Application Shutdown (ie. Tomcat Shutdown).

		System.out.println("\nApplication Shutdown (Tomcat Server shuting down...)");

		System.out.println("Closing DataSource connection (DBCP), if it is initialized and open.");
		DataSource dataSource = DataSourceFactory.getInstance();
		if (dataSource != null)
			dataSource.close();

		System.out.println("Unloading SQL drivers from DBCP."); // Actually unloading all drivers used
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		while (drivers.hasMoreElements()) {
			Driver driver = drivers.nextElement();
			if (driver.getClass().getClassLoader().equals(getClass().getClassLoader())) {
				try {
					DriverManager.deregisterDriver(driver);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("Shuting down AbandonedConnectionCleanupThread.");
		AbandonedConnectionCleanupThread.checkedShutdown();

		System.out.println("Thank you for using Sellerportal API, Come back soon...");
		System.out.println("--------------------END--------------------\n");
	}
}
