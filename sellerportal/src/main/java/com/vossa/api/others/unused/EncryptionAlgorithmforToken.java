package com.vossa.api.others.unused;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionAlgorithmforToken {

	private static final String MESSAGEDIGEST_ALGORITHM = "SHA-256";
	private static final String SECRET_KEY_SPEC_ALGORITHM = "AES";
	
	//Different encryption algorithms for header, signature and payload
	private static final String HEADER_ENCRYPTION_ALGORITHM = "AES/CBC/PKCS5Padding";
	private static final String PAYLOAD_ENCRYPTION_ALGORITHM = "AES/CTR/PKCS5Padding";
	private static final String SIGNATURE_ENCRYPTION_ALGORITHM = "AES/CBC/PKCS5Padding";
	private static final int IV_LENGTH = 16;		//Always 16
	private static final int KEY_LENGTH = 32;		//Multiples of 16
	
	//Different keys for header, signature and payload
	private static final String HEADER_KEY = "hello_world_mother_base";
	private static final String PAYLOAD_KEY = "hello_world_mother_base";
	private static final String SIGNATURE_KEY = "hello_world_mother_base";
	
	protected String encryptHeader(String plaintext)
	{	
		byte plaintextBytes[] = plaintext.getBytes();
		
		//Generate a random iv
		byte iv[] = new byte[IV_LENGTH];
		SecureRandom random = new SecureRandom();
		random.nextBytes(iv);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

		try
		{
			//Initializing MessageDigest
			MessageDigest digest = MessageDigest.getInstance(MESSAGEDIGEST_ALGORITHM);
			digest.update(HEADER_KEY.getBytes());	
			
			//Creating a hashed key digest based on header key
			byte keyBytes[] = new byte[KEY_LENGTH];
			System.arraycopy( digest.digest(), 0, keyBytes, 0, KEY_LENGTH);
			SecretKeySpec secretKey = new SecretKeySpec(keyBytes, SECRET_KEY_SPEC_ALGORITHM);
						
			//Initializing the Cipher instance
			Cipher cipher = Cipher.getInstance(HEADER_ENCRYPTION_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
			
			//Encrypting the header plaintext
			byte ciphertextBytes[] = cipher.doFinal(plaintextBytes);
			int ciphertextLength = ciphertextBytes.length;
			
			//Postpend the iv with ciphertext
			byte finaltextBytes[] = new byte[ciphertextLength + IV_LENGTH];
			System.arraycopy( ciphertextBytes, 0, finaltextBytes, 0, ciphertextLength);
			System.arraycopy( iv, 0, finaltextBytes, ciphertextLength, IV_LENGTH);

			String finaltext = Base64.getEncoder().encodeToString(finaltextBytes);
			return finaltext;
		}
		catch(Exception e)
		{
			System.out.println(e);
			return null;
		}
	}
	
	protected String encryptPayload(String plaintext)
	{
		byte plaintextBytes[] = plaintext.getBytes();
		
		//Generate a random iv
		byte iv[] = new byte[IV_LENGTH];
		SecureRandom random = new SecureRandom();
		random.nextBytes(iv);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
		
		try
		{
			//Initializing MessageDigest
			MessageDigest digest = MessageDigest.getInstance(MESSAGEDIGEST_ALGORITHM);
			digest.update(PAYLOAD_KEY.getBytes());
			
			//Creating a hashed key digest based on payload key
			byte keyBytes[] = new byte[KEY_LENGTH];
			System.arraycopy( digest.digest(), 0, keyBytes, 0, KEY_LENGTH);
			SecretKeySpec secretKey = new SecretKeySpec(keyBytes, SECRET_KEY_SPEC_ALGORITHM);
						
			//Initializing the Cipher instance
			Cipher cipher = Cipher.getInstance(PAYLOAD_ENCRYPTION_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
			
			//Encrypting the payload plaintext
			byte ciphertextBytes[] = cipher.doFinal(plaintextBytes);
			int ciphertextLength = ciphertextBytes.length;
			
			//Prepend the iv with ciphertext
			byte finaltextBytes[] = new byte[IV_LENGTH + ciphertextLength];			
			System.arraycopy( iv, 0, finaltextBytes, 0, IV_LENGTH);
			System.arraycopy( ciphertextBytes, 0, finaltextBytes, IV_LENGTH, ciphertextLength);
			
			String finaltext = Base64.getEncoder().encodeToString(finaltextBytes);
			return finaltext;
		}
		catch(Exception e)
		{
			System.out.println(e);
			return null;
		}
	}
	
	protected String encryptSignature(String plaintext)
	{
		byte plaintextBytes[] = plaintext.getBytes();
		
		//Generate a random iv
		byte iv[] = new byte[IV_LENGTH];
		SecureRandom random = new SecureRandom();
		random.nextBytes(iv);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
		
		try
		{		
			//Initializing MessageDigest
			MessageDigest digest = MessageDigest.getInstance(MESSAGEDIGEST_ALGORITHM);
			digest.update(SIGNATURE_KEY.getBytes());
			
			//Creating a hashed key digest based on signature key
			byte keyBytes[] = new byte[KEY_LENGTH];
			System.arraycopy( digest.digest(), 0, keyBytes, 0, KEY_LENGTH);
			SecretKeySpec secretKey = new SecretKeySpec(keyBytes, SECRET_KEY_SPEC_ALGORITHM);
			
			//Initializing the Cipher instance
			Cipher cipher = Cipher.getInstance(SIGNATURE_ENCRYPTION_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
			
			//Encrypting the signature plaintext
			byte ciphertextBytes[] = cipher.doFinal(plaintextBytes);
			int ciphertextLength = ciphertextBytes.length;
			
			//Postpend the iv with ciphertext
			byte finaltextBytes[] = new byte[ciphertextLength + IV_LENGTH];
			System.arraycopy( ciphertextBytes, 0, finaltextBytes, 0, ciphertextLength);
			System.arraycopy( iv, 0, finaltextBytes, ciphertextLength, IV_LENGTH);
			
			String finaltext = Base64.getEncoder().encodeToString(finaltextBytes);
			return finaltext;
		}
		catch(Exception e)
		{
			System.out.println(e);
			return null;
		}
	}
	
	
	protected String decryptHeader(String finaltext)
	{
		byte finaltextBytes[] = Base64.getDecoder().decode(finaltext);
		
		// Extracting the ciphertext
		int ciphertextLength = finaltextBytes.length - IV_LENGTH;
		byte ciphertextBytes[] = new byte[ciphertextLength];
		System.arraycopy(finaltextBytes, 0, ciphertextBytes, 0, ciphertextLength);
		
		// Extracting the iv
		byte iv[] = new byte[IV_LENGTH];
		System.arraycopy(finaltextBytes, ciphertextLength, iv, 0, IV_LENGTH);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
		
		try
		{
			//Initializing MessageDigest		
			MessageDigest digest = MessageDigest.getInstance(MESSAGEDIGEST_ALGORITHM);
			digest.update(HEADER_KEY.getBytes());
			
			//Creating a hashed key digest based on header key
			byte keyBytes[] = new byte[KEY_LENGTH];
			System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);
			SecretKeySpec secretKey = new SecretKeySpec(keyBytes, SECRET_KEY_SPEC_ALGORITHM);

			//Initializing the Cipher instance
			Cipher cipher = Cipher.getInstance(HEADER_ENCRYPTION_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
		
			//Decrypting the header ciphertext
			byte plaintextBytes[] = cipher.doFinal(ciphertextBytes);
			
			String plaintext = new String(plaintextBytes);
			return plaintext;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}		
	}

	protected String decryptPayload(String finaltext)
	{
		byte finaltextBytes[] = Base64.getDecoder().decode(finaltext);
		
		// Extracting the iv
		byte iv[] = new byte[IV_LENGTH];
		System.arraycopy(finaltextBytes, 0, iv, 0, IV_LENGTH);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
				
		// Extracting the ciphertext
		int ciphertextLength = finaltextBytes.length - IV_LENGTH;
		byte ciphertextBytes[] = new byte[ciphertextLength];
		System.arraycopy(finaltextBytes, IV_LENGTH, ciphertextBytes, 0, ciphertextLength);
			
		try
		{
			//Initializing MessageDigest		
			MessageDigest digest = MessageDigest.getInstance(MESSAGEDIGEST_ALGORITHM);
			digest.update(PAYLOAD_KEY.getBytes());
			
			//Creating a hashed key digest based on payload key
			byte keyBytes[] = new byte[KEY_LENGTH];
			System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);
			SecretKeySpec secretKey = new SecretKeySpec(keyBytes, SECRET_KEY_SPEC_ALGORITHM);

			//Initializing the Cipher instance
			Cipher cipher = Cipher.getInstance(PAYLOAD_ENCRYPTION_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
		
			//Decrypting the payload ciphertext
			byte plaintextBytes[] = cipher.doFinal(ciphertextBytes);
			
			String plaintext = new String(plaintextBytes);
			return plaintext;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	protected String decryptSignature(String finaltext)
	{
		byte finaltextBytes[] = Base64.getDecoder().decode(finaltext);
		
		// Extracting the ciphertext
		int ciphertextLength = finaltextBytes.length - IV_LENGTH;
		byte ciphertextBytes[] = new byte[ciphertextLength];
		System.arraycopy(finaltextBytes, 0, ciphertextBytes, 0, ciphertextLength);
		
		// Extracting the iv
		byte iv[] = new byte[IV_LENGTH];
		System.arraycopy(finaltextBytes, ciphertextLength, iv, 0, IV_LENGTH);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
		
		try
		{
			//Initializing MessageDigest		
			MessageDigest digest = MessageDigest.getInstance(MESSAGEDIGEST_ALGORITHM);
			digest.update(SIGNATURE_KEY.getBytes());
			
			//Creating a hashed key digest based on signature key
			byte keyBytes[] = new byte[KEY_LENGTH];
			System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);
			SecretKeySpec secretKey = new SecretKeySpec(keyBytes, SECRET_KEY_SPEC_ALGORITHM);

			//Initializing the Cipher instance
			Cipher cipher = Cipher.getInstance(SIGNATURE_ENCRYPTION_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
		
			//Decrypting the signature ciphertext
			byte plaintextBytes[] = cipher.doFinal(ciphertextBytes);
			
			String plaintext = new String(plaintextBytes);
			return plaintext;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}