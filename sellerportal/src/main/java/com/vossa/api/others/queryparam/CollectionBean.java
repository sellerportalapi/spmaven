package com.vossa.api.others.queryparam;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

public class CollectionBean {

	@QueryParam("search")
	public SearchParam searchParam;

	@QueryParam("filters")
	public FilterParam filterParam;

	@QueryParam("sort")
	public SortParam sortingParam;

	@QueryParam("offset")
	@DefaultValue("1")
	public int offset;

	@QueryParam("limit")
	@DefaultValue("15")
	public int limit;
}
