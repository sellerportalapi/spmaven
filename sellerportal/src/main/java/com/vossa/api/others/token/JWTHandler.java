package com.vossa.api.others.token;

import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vossa.api.general.enums.PswdJwtType;
import com.vossa.api.others.filehandlers.ConfigFileHandler;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTHandler {

	// Token expiration interval time
	private static final long EXPIRY_INTERVAL = (120 * 60 * 1000); // (120 minutes)( mm * ss * ms )
	private static final long EXPIRY_INTERVAL_CREATE_PSWD = (12 * 60 * 60 * 1000); // (12 hours)( hh * mm * ss * ms )
	private static final long EXPIRY_INTERVAL_FORGOT_PSWD = (24 * 60 * 60 * 1000); // (24 hours)( hh * mm * ss * ms )

	// Token Signing Algorithm
	private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;

	private static Map<String, Object> buildHeader(JWT jwt) {
		Map<String, Object> header = new HashMap<>();
		if (jwt.getTyp() == null)
			jwt.setTyp("JWT");
		header.put(Header.TYPE, jwt.getTyp());
		return header;
	}

	private static Claims buildClaims(JWT jwt) throws IllegalArgumentException {

		Claims claims = Jwts.claims();
		claims.setId(jwt.getJti());
		claims.setIssuer(jwt.getIss());
		claims.setAudience(jwt.getAud());
		claims.setSubject(jwt.getSub());

		UserClaims user = jwt.getUser();
		if (user != null) {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.NON_NULL);
			Map<String, Object> userMap = mapper.convertValue(user, new TypeReference<Map<String, Object>>() {
			});
			claims.put("user", userMap);
		}

		return claims;
	}

	public static String buildJWT(JWT jwt) throws Exception {

		// issued time and expire time is set to the time when we are building a token.
		jwt.setIat(System.currentTimeMillis());
		jwt.setExp(jwt.getIat() + EXPIRY_INTERVAL);
		jwt.setTyp("JWT_AUTH");

		try {
			String token = Jwts.builder().setHeader(buildHeader(jwt)).setClaims(buildClaims(jwt))
					.setIssuedAt(new Date(jwt.getIat())).setExpiration(new Date(jwt.getExp()))
					.signWith(SIGNATURE_ALGORITHM, "hello").compact();
			return token;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static String buildJWTPassword(JWT jwt, PswdJwtType jwtType) throws Exception {

		final long EXPIRY_INTERVAL;
		if (jwtType.equals(PswdJwtType.JWT_CP))
			EXPIRY_INTERVAL = EXPIRY_INTERVAL_CREATE_PSWD;
		else if (jwtType.equals(PswdJwtType.JWT_FP))
			EXPIRY_INTERVAL = EXPIRY_INTERVAL_FORGOT_PSWD;
		else
			throw new Exception("Invalid JWT_TYPE");

		// issued time and expire time is set to the time when we are building a token.
		jwt.setIat(System.currentTimeMillis());
		jwt.setExp(jwt.getIat() + EXPIRY_INTERVAL);
		jwt.setTyp(jwtType.toString());

		try {
			String token = Jwts.builder().setHeader(buildHeader(jwt)).setClaims(buildClaims(jwt))
					.setIssuedAt(new Date(jwt.getIat())).setExpiration(new Date(jwt.getExp()))
					.signWith(SIGNATURE_ALGORITHM, "hello").compact();
			return token;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	private static JWT parseHeader(JWT jwt, Header header) throws Exception {
		jwt.setTyp(header.getType());

		return jwt;
	}

	private static JWT parseClaims(JWT jwt, Claims claims) throws Exception {
		jwt.setJti(claims.getId());
		jwt.setIss(claims.getIssuer());
		jwt.setAud(claims.getAudience());
		jwt.setSub(claims.getSubject());
		jwt.setIat(claims.getIssuedAt().getTime());
		jwt.setExp(claims.getExpiration().getTime());

		if (claims.containsKey("user")) {
			ObjectMapper mapper = new ObjectMapper();
			UserClaims user = mapper.convertValue(claims.get("user"), UserClaims.class);
			jwt.setUser(user);
		}

		return jwt;
	}

	public static JWT parseJWT(String token) throws Exception {
		ConfigFileHandler config = new ConfigFileHandler();
		String issConfig = config.get("iss");
		URL issurl = new URL(issConfig);
		String iss = issurl.getAuthority();

		try {
			Claims claims = Jwts.parser().requireIssuer(iss).setSigningKey("hello").parseClaimsJws(token).getBody();

			JWT jwt = new JWT();
			jwt = parseClaims(jwt, claims);
			return jwt;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public static JWT parseJWTPassword(String token) throws Exception {
		try {
			Jwt jwtObj = Jwts.parser().setSigningKey("hello").parse(token);

			JWT jwt = new JWT();
			jwt = parseHeader(jwt, jwtObj.getHeader());
			jwt = parseClaims(jwt, (Claims) jwtObj.getBody());
			return jwt;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static void main(String args[]) {

		try {
			long low = System.currentTimeMillis();

			UserClaims userClaims = new UserClaims(11, "guru", "VERTADMIN", 11, "Apple");
			JWT jwt = new JWT("3105", "Verte", "Vossa", "guru", userClaims);

			System.out.println(jwt);
			String token = JWTHandler.buildJWTPassword(jwt, PswdJwtType.JWT_CP);
			System.out.println(token);
			System.out.println("len " + token.length());

			jwt = JWTHandler.parseJWTPassword(token);
			System.out.println(jwt);

			long end = System.currentTimeMillis();
			System.out.println(end - low);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
