package com.vossa.api.others.queryparam;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

import com.vossa.api.general.enums.SortCondition;

@Provider
public class CollectionParamConverterProvider implements ParamConverterProvider {

	@Override
	public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
		if (rawType.getName().equals(SearchParam.class.getName())) {
			return new ParamConverter<T>() {

				@Override
				public T fromString(String value) {
					List<String> searchList = Arrays.asList(value.split(","));
					return rawType.cast(new SearchParam(searchList));
				}

				@Override
				public String toString(T value) {
					if (value != null)
						return value.toString();
					return null;
				}
			};
		} else if (rawType.getName().equals(FilterParam.class.getName())) {
			return new ParamConverter<T>() {

				@Override
				public T fromString(String value) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public String toString(T value) {
					if (value != null)
						return value.toString();
					return null;
				}
			};
		} else if (rawType.getName().equals(SortParam.class.getName())) {
			return new ParamConverter<T>() {

				@Override
				public T fromString(String value) {
					List<String> sortList = Arrays.asList(value.split(","));

					List<Sorter> sorters = new ArrayList<>();
					for (String sort : sortList) {
						String split[] = sort.split("::");

						Sorter sorter = new Sorter(split[0], SortCondition.valueOf(split[1]));
						sorters.add(sorter);
					}
					return rawType.cast(new SortParam(sorters));
				}

				@Override
				public String toString(T value) {
					if (value != null)
						return value.toString();
					return null;
				}
			};
		}
		return null;
	}

}
