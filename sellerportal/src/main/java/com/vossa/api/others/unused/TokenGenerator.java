package com.vossa.api.others.unused;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import org.json.JSONObject;

import com.stores.RoleServices;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.v1.seller.pojos.User;

import io.jsonwebtoken.*;

@SuppressWarnings("unused")
public class TokenGenerator {

	// Headers
	private String typ;
	private String kid;

	// Reserved Claims
	private String jti;
	private String iss;
	private String aud;
	private String sub;
	private long iat;
	private long exp;

	private User user;
	private String userName;
	
	
	// Token expiration interval time
	private static final long EXPIRY_INTERVAL = (120 * 60 * 1000); // (120 minutes)( minutes * seconds * milliseconds )
	private static final long EXPIRY_INTERVAL_RESET = (1440 * 60 * 1000);//(24 hours)
	
	// Token Signing Algorithm
	private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;
	private String randomKeyText; // This will be generated while calling kid generator

	// Key generation
	private static final String SECRET_KEY_FACTORY_ALGORITHM = "PBKDF2WithHmacSHA1";
	private static final String SECRET_KEY_SPEC_ALGORITHM = "AES";
	private static final int ITERATION_COUNT = 3105;
	private static final int KEY_LENGTH = 256;

	public TokenGenerator() {

	}

	public void setDefaultClaims() // Mostly not used
	{
		typ = Header.JWT_TYPE + "-" + "Verte";

		jti = UUID.randomUUID().toString(); // OR random userid given to the projectusers
		sub = "username"; // username of the projectuser
		iss = "http://localhost:8080"; // server-side url
		aud = "http://localhost:4200"; // client-side url
	}

	public void setClaims(String id, String subject, String issuer, String audience) {
		typ = Header.JWT_TYPE + "-" + "Verte";

		jti = id;
		sub = subject;
		iss = issuer;
		aud = audience;
	}

	public void setUserClaims(String id, String subject, String issuer, String audience, User userObject) {
		typ = Header.JWT_TYPE + "-" + "Verte";

		jti = id;
		sub = subject;
		iss = issuer;
		aud = audience;

		user = userObject;
	}
	public void setUserResetClaims(String id, String subject, String issuer, String audience, String userName) {
		typ = Header.JWT_TYPE + "-" + "Verte";

		jti = id;
		sub = subject;
		iss = issuer;
		aud = audience;
		this.userName=userName;
	}
	private Map<String, Object> buildHeader() {
		// kid = generateRandomKeyId();
		kid = "NO KEY ID";

		Map<String, Object> header = new HashMap<>();
		header.put(Header.TYPE, typ);
		header.put(JwsHeader.KEY_ID, kid);

		return header;
	}

	private Map<String, Object> buildClaim() {
		Map<String, Object> claim = new HashMap<>();
		claim.put(Claims.ID, jti);
		claim.put(Claims.ISSUER, iss);
		claim.put(Claims.AUDIENCE, aud);
		claim.put(Claims.SUBJECT, sub);
		claim.put("user", user);

		try {
			if (user.getUserType().equals(UserType.STANDARDUSER.toString())) {
				Map<String, Object> permission = new RoleServices().getPermissionMap(user.getSellerId());
				claim.put("permission", permission);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return claim;
	}
	private Map<String, Object> buildResetClaim() {
		Map<String, Object> claim = new HashMap<>();
		//claim.put(Claims.ID, jti);
		//claim.put(Claims.ISSUER, iss);
		//claim.put(Claims.AUDIENCE, aud);
		//claim.put(Claims.SUBJECT, sub);
		claim.put("userName", userName);

		
		return claim;
	}
	public String buildResetToken() {
		iat = System.currentTimeMillis();
		exp = iat + EXPIRY_INTERVAL_RESET;
		try {
			String token = Jwts.builder().setClaims(buildResetClaim()).setHeader(buildHeader()).setIssuedAt(new Date(iat))
					.setExpiration(new Date(exp))
					.signWith(SIGNATURE_ALGORITHM, "ME@vert-2018$butnotOnlyME") // Using fixed key to sign the token.
					.compact();
			return token;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	public String buildToken() {
		// issued time and expire time is created when we are building a token. Not when
		// creating a TokenGenerator instance
		iat = System.currentTimeMillis();
		exp = iat + EXPIRY_INTERVAL;

		try {
			String token = Jwts.builder().setClaims(buildClaim()).setHeader(buildHeader()).setIssuedAt(new Date(iat))
					.setExpiration(new Date(exp))
					// .compressWith(CompressionCodecs.DEFLATE)
					// .signWith( SIGNATURE_ALGORITHM, generateKey(randomKeyText)) //For now, not
					// using the Random key. Because it cause session timeout (Invalid signature
					// exception).
					.signWith(SIGNATURE_ALGORITHM, "ME@vert-2018$butnotOnlyME") // Using fixed key to sign the token.
					.compact();

			// String cipherToken = encryptToken(token);
			// return cipherToken;
			return token;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public Claims parseToken(String cipherToken) throws Exception {
		// String token = decryptToken(cipherToken);
		String token = cipherToken;

		String headerBase64 = token.split(Pattern.quote("."))[0];
		String header = new String(Base64.getDecoder().decode(headerBase64));
		try {
			JSONObject jsonHeader = new JSONObject(header);
			kid = jsonHeader.getString("kid");
		} catch (Exception e) {
			throw new SignatureException(e.getMessage());
		}

		// EncryptionAlgorithmforKeyId decryption = new EncryptionAlgorithmforKeyId();
		// String randomKeyText = decryption.decrypt(kid);
		// decryption = null;

		Claims claims = Jwts.parser()
				// .requireIssuedAt(new Date(isssuedAt))
				// .requireExpiration(new Date(isssuedAt + expiryInterval))
				// .requireId(id)
				// .requireAudience(audience)
				// .setSigningKey(generateKey(randomKeyText)) //For now, not using the Random
				// key. Because it cause session timeout (Invalid signature exception).
				.setSigningKey("ME@vert-2018$butnotOnlyME") // Used fixed key to sign the token.
				.parseClaimsJws(token).getBody();

		jti = claims.getId();
		sub = claims.getSubject();
		iss = claims.getIssuer();
		aud = claims.getSubject();
		iat = claims.getIssuedAt().getTime();
		exp = claims.getExpiration().getTime();

		return claims;
	}

	// private String encryptToken(String token)
	// {
	// EncryptionAlgorithmforToken encryption = new EncryptionAlgorithmforToken();
	//
	//// String fields[] = token.split("\\.");
	// String fields[] = token.split(Pattern.quote("."));
	//
	// String header = encryption.encryptHeader(fields[0]);
	// String payload = encryption.encryptPayload(fields[1]);
	// String signature = encryption.encryptSignature(fields[2]);
	//
	// encryption = null;
	// String cipherToken = header + "." + payload + "." + signature;
	//
	// return cipherToken;
	// }
	//
	// private String decryptToken(String cipherToken)
	// {
	// EncryptionAlgorithmforToken decryption = new EncryptionAlgorithmforToken();
	//
	//// String fields[] = token.split("\\.");
	// String fields[] = cipherToken.split(Pattern.quote("."));
	//
	// String header = decryption.decryptHeader(fields[0]);
	// String payload = decryption.decryptPayload(fields[1]);
	// String signature = decryption.decryptSignature(fields[2]);
	//
	// decryption = null;
	// String token = header + "." + payload + "." + signature;
	//
	// return token;
	// }
	//
	// private String generateRandomKeyId()
	// {
	// SecureRandom random = new SecureRandom();
	// int len = random.nextInt(16) + 16;
	//
	//// byte randomBytes[] = new byte[len];
	//// random.nextBytes(randomBytes);
	//// randomKeyText = new String(randomBytes);
	//
	// //Problem in generating random unicode bytes, the key is not correct. So
	// switched to AlphaNumeric String random key
	// randomKeyText = generateRandomAlphanumeric(len);
	//
	// EncryptionAlgorithmforKeyId encryption = new EncryptionAlgorithmforKeyId();
	// String keyId = encryption.encrypt(randomKeyText);
	// encryption = null;
	//
	// return keyId;
	// }
	//
	// private String generateRandomAlphanumeric(int length)
	// {
	// SecureRandom random = new SecureRandom();
	// return random.ints( 48, 122)
	// .mapToObj(i -> (char) i)
	// .limit(length)
	// .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
	// .toString();
	// }
	//
	// private Key generateKey(String randomKeyText)
	// {
	// try
	// {
	// byte saltBytes[] = "hello_world_mother_base".getBytes();
	//
	// SecretKeyFactory factoryAlgorithm =
	// SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_ALGORITHM);
	// PBEKeySpec spec = new PBEKeySpec( randomKeyText.toCharArray(), saltBytes,
	// ITERATION_COUNT, KEY_LENGTH);
	// SecretKey secretKey = factoryAlgorithm.generateSecret(spec);
	// SecretKeySpec key = new SecretKeySpec( secretKey.getEncoded(),
	// SECRET_KEY_SPEC_ALGORITHM);
	// return key;
	// }
	// catch(Exception e)
	// {
	// System.out.println(e);
	// return null;
	// }
	// }

	public void setId(String id) {
		jti = id;
	}

	public void setIssuer(String issuer) {
		iss = issuer;
	}

	public void setAudience(String audience) {
		aud = audience;
	}

	public void setSubject(String subject) {
		sub = subject;
	}

	public long getIssueTime() {
		return iat;
	}

	public long getEXPIRY_INTERVAL() {
		return EXPIRY_INTERVAL;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public static void main(String args[]) {
		try {
			long low = System.currentTimeMillis();

			TokenGenerator tg = new TokenGenerator();
			tg.setDefaultClaims();
			while (true) {
				String token = tg.buildToken();
				System.out.println(token);
				Claims claims = tg.parseToken(token);
				User user = (User) claims.get("user");
				
				System.out.println(user);
				if (true)
					break;
			}

			long end = System.currentTimeMillis();
			System.out.println(end - low);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
