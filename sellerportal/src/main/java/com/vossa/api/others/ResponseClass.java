package com.vossa.api.others;

public class ResponseClass {

	private int code;
	private String status;
	private String message;
	private String data;
	
	private String token;
	private String userType;
	private String storeName;
	private String displayName;
	private String userName;
	
	public ResponseClass() {
		
	}
	public ResponseClass(int code, String status, String data)		//Standard success response
	{
		this.code = code;
		this.status = status;
		this.data = data;
	}
	public ResponseClass(int code, String status, String message, String data)		//Standard failure or error response
	{
		this(code, status, data);
		this.message = message;
	}
	public ResponseClass(int code, String status, String data, String token, String type)	//Success Response(with TOKEN and USER_TYPE) when VERT_ADMIN login 
	{
		this(code, status, data);
		this.token = token;
		this.userType = type;
	}
	public ResponseClass(int code, String status, String data, String token, String type, String storeName, String displayName, String userName)	//Success Response(with TOKEN, USER_TYPE, STORENAME and DISPLAYNAME) when STORE ADMIN or USER login
	{
		this(code, status, data, token, type);
		this.storeName = storeName;
		this.displayName = displayName;
		this.userName = userName;
	}
	
	public void setCode(int code)
	{
		this.code = code;
	}
	public int getCode()
	{
		return code;
	}
	
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getStatus()
	{
		return status;
	}
	
	public void setMessage(String message)
	{
		this.message = message;
	}
	public String getMessage()
	{
		return message;
	}
	
	public void setData(String data)
	{
		this.data = data;
	}
	public String getData()
	{
		return data;
	}
	
	public void setToken(String tok)
	{
		token = tok;
	}
	public String getToken()
	{
		return token;
	}
	
	public void setuserType(String type)
	{
		userType = type;
	}
	public String getUserType()
	{
		return userType;
	}
	
	public void setStoreName(String name)
	{
		storeName = name;
	}
	public String getStoreName()
	{
		return storeName;
	}
	
	public void setDisplayName(String name)
	{
		displayName = name;
	}
	public String getDisplayName()
	{
		return displayName;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}