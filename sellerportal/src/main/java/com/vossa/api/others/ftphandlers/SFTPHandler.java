package com.vossa.api.others.ftphandlers;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONObject;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SFTPHandler {

	private static final String SFTPHOST = "52.14.189.24";
	private static final int SFTPPORT = 22;
	private static final String SFTPUSER = "verte_sftp";
	private static final String PRIVATE_KEY = "verte_sftp.key";
	private static final String SFTPWORKINGDIR = "/local/SP_UAT/ITM_MAST/";

	public void putProductList(List<JSONObject> productList) throws Exception {
		JSch jSch = new JSch();
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;

		try {
			// Adding the private key from the resource folder
			jSch.addIdentity(getClassFilePath(PRIVATE_KEY));
			// Creating a new session
			session = jSch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);

			Properties properties = new Properties();
			properties.put("StrictHostKeyChecking", "no");
			session.setConfig(properties);
			session.connect();

			channel = session.openChannel("sftp");
			channel.connect();
			// shell channel connected....

			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(SFTPWORKINGDIR);
			// Changed the working directory...

			// filename will be UUID
			String filename = UUID.randomUUID().toString();
			InputStream inputStream = new ByteArrayInputStream(productList.toString().getBytes("UTF-8"));
			channelSftp.put(inputStream, filename + ".json");

			inputStream.close();
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();

		} catch (JSchException e) {
			e.printStackTrace();
			throw e;
		} catch (SftpException e) {
			e.printStackTrace();
			throw e;
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (channelSftp != null)
				channelSftp.disconnect();
			if (channel != null)
				channel.disconnect();
			if (session != null)
				session.disconnect();
		}
	}

	protected String getClassFilePath(String filename) throws URISyntaxException {
		URL url = SFTPHandler.class.getClassLoader().getResource(filename);
		Path path = Paths.get(url.toURI());
		return path.toAbsolutePath().toString();
	}
}
