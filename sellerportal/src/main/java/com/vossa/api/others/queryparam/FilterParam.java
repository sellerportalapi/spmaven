package com.vossa.api.others.queryparam;

import java.util.List;

public class FilterParam {

	private List<Filter> filters;

	public FilterParam(List<Filter> filters) {
		super();
		this.filters = filters;
	}

	public List<Filter> getFilters() {
		return filters;
	}

	public void setFilters(List<Filter> filters) {
		this.filters = filters;
	}
}
