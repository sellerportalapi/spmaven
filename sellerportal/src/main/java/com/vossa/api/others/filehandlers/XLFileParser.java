package com.vossa.api.others.filehandlers;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;
import javax.ws.rs.InternalServerErrorException;
import org.json.JSONObject;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.stores.ProductServices;

public class XLFileParser {

	private List<JSONObject> xlsToJson(InputStream file) throws Exception {
		HSSFWorkbook wb = new HSSFWorkbook(file);
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFRow row;
		HSSFCell cell;

		// Find the available field_names and store it in a LIST.
		// It was used as JSON field key later.
		ArrayList<Row> filteredRows = new ArrayList<>();
		for (Row filerow : sheet) {
			for (Cell filecell : filerow) {
				if (filecell != null && filecell.getCellTypeEnum() != CellType.BLANK) {
					// This row has at least one non-null value, So it is a valid row.
					filteredRows.add(filerow);
					break;
				}
			}
		}

		Iterator<Row> rows = sheet.rowIterator();
		row = (HSSFRow) rows.next();
		Iterator<Cell> cells = row.cellIterator();

		// Validate all column header, check all are string only.
		int field_length = 0;
		List<String> field_names = new ArrayList<String>();
		while (cells.hasNext()) {
			cell = (HSSFCell) cells.next();
			switch (cell.getCellTypeEnum()) {
			case STRING:
				field_names.add(cell.getStringCellValue());
				field_length++;
				break;
			case BLANK:
				throw new FileFormatException("XLS Format incorrect. Column Header shouldn't be empty.");
			default:
				throw new FileFormatException("XLS Format incorrect. Column Header accepts only String type.");
			}
		}

		List<JSONObject> productsList = new ArrayList<JSONObject>();
		JSONObject product = null;

		filteredRows.remove(0);
		for (Row frow : filteredRows) {
			row = (HSSFRow) frow;
			product = new JSONObject();

			for (int i = 0; i < field_length; i++) {
				cell = row.getCell(i);
				if (cell == null)
					continue;

				if (cell.getCellTypeEnum() == CellType.STRING && cell.getStringCellValue().isEmpty())
					continue;

				Object objectValue = null;
				String datatype = new ProductServices().getFieldDatatype(field_names.get(i));

				switch (datatype) {
				case "boolean":
					if (cell.getCellTypeEnum() == CellType.BOOLEAN)
						objectValue = Boolean.valueOf(cell.getBooleanCellValue());
					else if ((cell.getCellTypeEnum() == CellType.STRING))
						objectValue = String.valueOf(cell.getStringCellValue());
					else if (cell.getCellTypeEnum() == CellType.NUMERIC)
						objectValue = String.valueOf((int) cell.getNumericCellValue());
					break;
				case "int":
					if (cell.getCellTypeEnum() == CellType.NUMERIC) {
						objectValue = Integer.valueOf((int) cell.getNumericCellValue());
					} else if ((cell.getCellTypeEnum() == CellType.STRING)) {
						objectValue = String.valueOf(cell.getStringCellValue());
					}
					break;
				case "long":
					if (cell.getCellTypeEnum() == CellType.NUMERIC) {
						objectValue = Long.valueOf((long) cell.getNumericCellValue());
					} else if ((cell.getCellTypeEnum() == CellType.STRING)) {
						objectValue = String.valueOf(cell.getStringCellValue());
					}
					break;
				case "float":
					if (cell.getCellTypeEnum() == CellType.NUMERIC) {
						objectValue = Float.valueOf((float) cell.getNumericCellValue());
					} else if ((cell.getCellTypeEnum() == CellType.STRING)) {
						objectValue = String.valueOf(cell.getStringCellValue());
					}
					break;
				case "double":
					if (cell.getCellTypeEnum() == CellType.NUMERIC) {
						objectValue = Double.valueOf(cell.getNumericCellValue());
					} else if ((cell.getCellTypeEnum() == CellType.STRING)) {
						objectValue = String.valueOf(cell.getStringCellValue());
					}
					break;
				case "varchar":
					if (cell.getCellTypeEnum() == CellType.STRING)
						objectValue = String.valueOf(cell.getStringCellValue());
					else if (cell.getCellTypeEnum() == CellType.NUMERIC)
						objectValue = String.valueOf((int) cell.getNumericCellValue());
					break;
				case "timestamp":
					if (cell.getCellTypeEnum() == CellType.NUMERIC && cell.getDateCellValue() != null) {
						objectValue = Long.valueOf((long) cell.getDateCellValue().getTime());
					}
					break;
				}
				product.put(field_names.get(i), objectValue);
			}
			productsList.add(product);
		}
		wb.close();
		file.close();

		return productsList;
	}

	private List<JSONObject> xlsxToJson(InputStream file) throws Exception {
		XSSFWorkbook wb = new XSSFWorkbook(file);
		XSSFSheet sheet = wb.getSheetAt(0);
		XSSFRow row;
		XSSFCell cell;

		// Find the available field_names and store it in a LIST.
		// It was used as JSON field key later.
		ArrayList<Row> filteredRows = new ArrayList<>();
		for (Row filerow : sheet) {
			for (Cell filecell : filerow) {
				if (filecell != null && filecell.getCellTypeEnum() != CellType.BLANK) {
					// This row has at least one non-null value, So it is a valid row.
					filteredRows.add(filerow);
					break;
				}
			}
		}

		Iterator<Row> rows = sheet.rowIterator();
		row = (XSSFRow) rows.next();
		Iterator<Cell> cells = row.cellIterator();

		// Validate all column header, check all are string only.
		int field_length = 0;
		List<String> field_names = new ArrayList<String>();
		while (cells.hasNext()) {
			cell = (XSSFCell) cells.next();
			switch (cell.getCellTypeEnum()) {
			case STRING:
				field_names.add(cell.getStringCellValue());
				field_length++;
				break;
			case BLANK:
				throw new FileFormatException("XLS Format incorrect. Column Header shouldn't be empty.");
			default:
				throw new FileFormatException("XLS Format incorrect. Column Header accepts only String type.");
			}
		}

		List<JSONObject> productsList = new ArrayList<JSONObject>();
		JSONObject product = null;

		filteredRows.remove(0);
		for (Row frow : filteredRows) {
			row = (XSSFRow) frow;
			product = new JSONObject();

			for (int i = 0; i < field_length; i++) {
				cell = row.getCell(i);
				if (cell == null)
					continue;

				if (cell.getCellTypeEnum() == CellType.STRING && cell.getStringCellValue().isEmpty())
					continue;

				Object objectValue = null;
				String datatype = new ProductServices().getFieldDatatype(field_names.get(i));
				if (cell != null) {
					switch (datatype) {
					case "boolean":
						if (cell.getCellTypeEnum() == CellType.BOOLEAN)
							objectValue = Boolean.valueOf(cell.getBooleanCellValue());
						else if ((cell.getCellTypeEnum() == CellType.STRING))
							objectValue = String.valueOf(cell.getStringCellValue());
						else if (cell.getCellTypeEnum() == CellType.NUMERIC)
							objectValue = String.valueOf((int) cell.getNumericCellValue());
						break;
					case "int":
						if (cell.getCellTypeEnum() == CellType.NUMERIC) {
							objectValue = Integer.valueOf((int) cell.getNumericCellValue());
						} else if ((cell.getCellTypeEnum() == CellType.STRING)) {
							objectValue = String.valueOf(cell.getStringCellValue());
						}
						break;
					case "long":
						if (cell.getCellTypeEnum() == CellType.NUMERIC) {
							objectValue = Long.valueOf((long) cell.getNumericCellValue());
						} else if ((cell.getCellTypeEnum() == CellType.STRING)) {
							objectValue = String.valueOf(cell.getStringCellValue());
						}
						break;
					case "float":
						if (cell.getCellTypeEnum() == CellType.NUMERIC) {
							objectValue = Float.valueOf((float) cell.getNumericCellValue());
						} else if ((cell.getCellTypeEnum() == CellType.STRING)) {
							objectValue = String.valueOf(cell.getStringCellValue());
						}
						break;
					case "double":
						if (cell.getCellTypeEnum() == CellType.NUMERIC) {
							objectValue = Double.valueOf(cell.getNumericCellValue());
						} else if ((cell.getCellTypeEnum() == CellType.STRING)) {
							objectValue = String.valueOf(cell.getStringCellValue());
						}
						break;
					case "varchar":
						if (cell.getCellTypeEnum() == CellType.STRING)
							objectValue = String.valueOf(cell.getStringCellValue());
						else if (cell.getCellTypeEnum() == CellType.NUMERIC)
							objectValue = String.valueOf((int) cell.getNumericCellValue());
						break;
					case "timestamp":
						if (cell.getCellTypeEnum() == CellType.NUMERIC && cell.getDateCellValue() != null) {
							objectValue = Long.valueOf((long) cell.getDateCellValue().getTime());
						}
						break;
					}
				}
				product.put(field_names.get(i), objectValue);
			}
			productsList.add(product);
		}
		wb.close();
		file.close();

		return productsList;
	}

	private ArrayList<JSONObject> csvToJson(InputStream stream) throws Exception {
		String line = "";
		boolean initial = true;
		ArrayList<String> header = new ArrayList<String>();
		ArrayList<JSONObject> productList = new ArrayList<>();
		try {
			BufferedReader fileReader = new BufferedReader(new InputStreamReader(stream));
			while ((line = fileReader.readLine()) != null) {
				int i = 0;
				JSONObject product = new JSONObject();
				String[] tokens = line.split(",");
				if (initial) {

					for (String token : tokens) {
						if (token.startsWith("\"")) {
							token = token.substring(1);
						}
						if (token.endsWith("\"")) {
							token = token.substring(0, token.length() - 1);
						}
						header.add(token);
					}
					initial = false;
					continue;
				}

				// Get all tokens available in line
				for (String token : tokens) {
					if (token.startsWith("\"")) {
						token = token.substring(1);
					}
					if (token.endsWith("\"")) {
						token = token.substring(0, token.length() - 1);
					}
					if (token != null && !token.equalsIgnoreCase("")) {
						Object objectValue = null;
						String datatype = new ProductServices().getFieldDatatype(header.get(i));
						if (token != null) {
							switch (datatype) {
							case "boolean":
								if (token.equalsIgnoreCase("TRUE") || token.equalsIgnoreCase("FALSE")) {
									objectValue = Boolean.valueOf(token);
								} else {
									objectValue = token;
								}
								break;
							case "int":
								if (isNumeric(token)) {
									objectValue = Integer.valueOf(token);
								} else {
									objectValue = token;
								}
								break;
							case "long":
								if (isNumeric(token)) {
									objectValue = Long.valueOf(token);
								} else {
									objectValue = token;
								}
								break;
							case "float":
								if (isNumeric(token)) {
									objectValue = Float.valueOf(token);
								} else {
									objectValue = token;
								}
								break;
							case "double":
								if (isNumeric(token)) {
									objectValue = Double.valueOf(token);
								} else {
									objectValue = token;
								}
								break;
							case "varchar":
								objectValue = String.valueOf(token);
								break;
							case "timestamp":
								if (token != null) {
									objectValue = Long.valueOf(token);
								}
								break;
							}
						}
						product.put(header.get(i).toString(), objectValue);
					}
					i++;
					if (i == header.size())
						break;
				}
				if (product != null && product.length() != 0) {
					productList.add(product);
				}
			}
			fileReader.close();

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private S3ObjectInputStream getAwsInputStream(String path) throws Exception {

		final String accessKey = "AKIAIS5QTDSG5XMKIQTQ";
		final String secretKey = "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";
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_2).build();

		String bucketName = null;
		String filePath = null;

		for (Bucket bucket : s3Client.listBuckets()) {
			if (path.contains(bucket.getName())) {
				bucketName = bucket.getName();
				filePath = path.replaceFirst(s3Client.getUrl(bucket.getName(), "").toExternalForm(), "");
			}
		}
		if (bucketName == null || filePath == null)
			throw new InternalServerErrorException("Given bucket doesnot exist in AMAZON S3");

		S3Object s3object = s3Client.getObject(new GetObjectRequest(bucketName, filePath));

		try {
			S3ObjectInputStream inputStream = s3object.getObjectContent();
			if (inputStream != null)
				return inputStream;
			throw new Exception("Unable to read the AWS content.");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	// for accessing aws files
	private List<JSONObject> parseAwsfileToJSON(String path) throws Exception {

		int i = path.lastIndexOf(".");
		String fileType = path.substring(i + 1);

		try (InputStream inputStream = getAwsInputStream(path)) {

			List<JSONObject> productList = null;
			switch (fileType) {
			case "xls":
				productList = xlsToJson(inputStream);
				break;
			case "xlsx":
				productList = xlsxToJson(inputStream);
				break;
			case "csv":
				productList = csvToJson(inputStream);
				break;
			default:
				throw new FileFormatException("Invalid file format. Allowed formats .xls, .xlsx, .csv");
			}

			if (productList != null)
				return productList;
			else
				throw new InternalServerErrorException("Product list is not proper");

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	// for accessing local files
	private List<JSONObject> parseLocalFileToJSON(String path) throws Exception {

		int i = path.lastIndexOf(".");
		String fileType = path.substring(i + 1);

		try (FileInputStream inputStream = new FileInputStream(path)) {

			List<JSONObject> productList = null;
			switch (fileType) {
			case "xls":
				productList = xlsToJson(inputStream);
				break;
			case "xlsx":
				productList = xlsxToJson(inputStream);
				break;
			case "csv":
				productList = csvToJson(inputStream);
				break;
			default:
				throw new FileFormatException("Invalid file format. Allowed formats .xls, .xlsx, .csv");
			}

			if (productList != null)
				return productList;
			else
				throw new InternalServerErrorException("Product list is not proper");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<JSONObject> fileParserToJSON(String filepath) throws Exception {
		try {
			filepath = URLDecoder.decode(filepath, "UTF-8");

			List<JSONObject> productList;
			if (isCloudFilepath(filepath))
				productList = parseAwsfileToJSON(filepath);
			else
				productList = parseLocalFileToJSON(filepath);

			return productList;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw e;
		}
	}

	private static boolean isCloudFilepath(String filepath) {
		try {
			new URL(filepath);
			return true;
		} catch (MalformedURLException e) {
			return false;
		}
	}

	private static boolean isNumeric(String str) {
		try {
			Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public static void main(String args[]) {
		XLFileParser x = new XLFileParser();

		// String file = "C:\\Users\\Karthick\\Documents\\sample.xlsx";
		// String path ="C:\\Users\\sample.xlsx";
		// "https://elasticbeanstalk-us-east-2-275487973053.s3.us-east-2.amazonaws.com/sample_format/sample.xlsx";

		// String path = "C:\\Users\\Karthick\\Documents\\sample.xlsx";
		String path = "https%3A%2F%2Fr1---sn-ci5gup-cags.googlevideo.com%2Fvideoplayback%3Fpcm2cms%3Dyes%26mime%3Dvideo%2Fmp4%26pl%3D21%26itag%3D22%26%26itag%3D43%26type%3Dvideo%2Fwebm%3B+codecs%3D%22vp8.0%2C+vorbis%22%26quality%3Dmedium";

		try {
			x.fileParserToJSON(path);
			// List<JSONObject> jsonObjects = null;
			// if (path.substring(0, 1).equalsIgnoreCase("C")) {
			// jsonObjects = x.getXLbyJSONLocal(path);
			// } else {
			// jsonObjects = x.getXLbyJSONAws(path);
			// }
			// System.out.println(jsonObjects);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
