package com.vossa.api.others.queryparam;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

public class FieldQueryBean {

	@QueryParam("fields")
	public String fieldsQuery;

	/**
	 * For single field, all three patterns will be supported.
	 * For multiple fields, only pattern 1 will be supported.
	 * pattern = 1 : [{"name":"guru"}, {"name":"steve"}, {"name":"dennis"}]
	 * pattern = 2 : ["guru", "steve", "dennis"]
	 * pattern = 3 : {"name":["guru", "steve", "dennis"]}
	 */
	@QueryParam("pattern")
	@DefaultValue("1")
	public int pattern;
}
