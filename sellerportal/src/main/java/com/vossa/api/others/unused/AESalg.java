package com.vossa.api.others.unused;

import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESalg {

	private final String ALGORITHM = "AES";
    private final int ITERATIONS = 2;
    private final String SALT = "oUr-NeW-mOtHeRbAsE_WeLcOmE-hOmE";
 
    public String encrypt(String plaintext)                                           
    {
        Key key;
        Cipher c;
        
        String valueToEncrypt = "";
        String ciphertext = plaintext;
        try
        {
	        key = generateKey();
	        c = Cipher.getInstance(ALGORITHM);
	        c.init(Cipher.ENCRYPT_MODE, key);
	        
	        for (int i = 0; i < ITERATIONS; i++) 
	        {
	            valueToEncrypt = SALT + ciphertext;
	            byte[] encValue = c.doFinal(valueToEncrypt.getBytes());
	            ciphertext = Base64.getEncoder().encodeToString(encValue);
	        }
        }
        catch(Exception e)
        {
        	System.out.println(e);
        	e.printStackTrace();
        }
        return ciphertext;
    }
 
    public String decrypt(String ciphertext)
    {
        Key key;
        Cipher c;
        
        String plaintext = "";
        String valueToDecrypt = ciphertext;
        try
        {
        	key = generateKey();
        	c = Cipher.getInstance(ALGORITHM);
        	c.init(Cipher.DECRYPT_MODE, key);
        	
        	for (int i = 0; i < ITERATIONS; i++) 
            {
                byte[] decodedValue = Base64.getDecoder().decode(valueToDecrypt);
                byte[] decValue = c.doFinal(decodedValue);
                plaintext = new String(decValue).substring(SALT.length());
                valueToDecrypt = plaintext;
            }
        }
        catch(Exception e)
        {
        	System.out.println(e);
        	e.printStackTrace();
        }
        return plaintext;
    }
 
    public Key generateKey()
    {
    	Key key = null;
    	try
    	{
    		key = new SecretKeySpec("_VeRt_EnTeRpRiSe".getBytes(), ALGORITHM);
    	}
    	catch(Exception e)
    	{
    		System.out.println(e);
    		e.printStackTrace();
    	}
        return key;
    }	
}