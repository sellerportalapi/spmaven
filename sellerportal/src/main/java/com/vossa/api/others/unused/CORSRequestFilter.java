package com.vossa.api.others.unused;

import java.io.IOException;
import java.util.Map;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.stores.RoleServices;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.token.JWT;
import com.vossa.api.others.token.JWTHandler;
import com.vossa.api.others.token.UserClaims;

//@Provider
public class CORSRequestFilter implements ContainerRequestFilter {

	private static final String TOKEN_HEADER_KEY = "token";
	private static final String OPTIONS_METHOD = "OPTIONS";

	@Override

	public void filter(ContainerRequestContext requestContext) throws IOException {

		System.out.println("request-store");
		if (requestContext.getMethod().equals(OPTIONS_METHOD))
			return;

		if (requestContext.getUriInfo().getPath().contains("login"))
			return;

		if(requestContext.getUriInfo().getPath().contains("forgotpassword"))
			return;
		
		String authToken = requestContext.getHeaderString(TOKEN_HEADER_KEY);
		if(requestContext.getUriInfo().getPath().contains("createpassword") || requestContext.getUriInfo().getPath().contains("checktokenexpiry"))
		{
			String token =  requestContext.getHeaderString("token");
			
			try {
				
				TokenGenerator tokenGenerator = new TokenGenerator();
				tokenGenerator.parseToken(token);
			}catch(Exception e){
				e.printStackTrace();
				String errmsg = "You are trying to get access with an invalid token.";
				Response response = Response.status(Response.Status.UNAUTHORIZED).entity(errmsg).build();
				requestContext.abortWith(response);
			}
				return;
		}
		
		if (authToken == null) {
			Response response = Response.status(Response.Status.UNAUTHORIZED)
					.entity("There is no valid Authorization header").build();
			requestContext.abortWith(response);
			return;
		}

		String token = authToken;
		try {
//			TokenGenerator tokenGenerator = new TokenGenerator();
//			Claims claims = tokenGenerator.parseToken(token);
			
//			ObjectMapper mapper = new ObjectMapper();
//			Users user = mapper.convertValue(claims.get("user"), Users.class);
			
			JWT jwt = JWTHandler.parseJWT(token);
			UserClaims user = jwt.getUser();
			
			if (!user.getUserType().equals(UserType.SUPERUSER.toString())
					&& !user.getUserType().equals(UserType.STANDARDUSER.toString())) {
				String errmsg = "You are trying to get access with an invalid token.";
				Response response = Response.status(Response.Status.FORBIDDEN).entity(errmsg).build();
				requestContext.abortWith(response);
				return;
			}

			if (user.getUserType().equals(UserType.STANDARDUSER.toString())) {

				String path = requestContext.getUriInfo().getPath();
				Map<String, Object> permission = user.getPermission();
				if(permission==null) {
					Response response = Response.status(Response.Status.UNAUTHORIZED)
							.entity("Invalid token. No permission found for the standard user.").build();
					requestContext.abortWith(response);
					return;
				}
				
				boolean valid = new RoleServices().checkStdUserPermission(permission, path, null);
				if (valid == false) {
					Response response = Response.status(Response.Status.FORBIDDEN)
							.entity("You are not allowed to access this api.").build();
					requestContext.abortWith(response);
					return;
				}
			}

			requestContext.setProperty("user", user);

			MultivaluedMap<String, String> headers = requestContext.getHeaders();
			headers.add("userName", user.getUserName());
			return;
			
		} catch (Exception e) {
			System.out.println(e);
			Response response = Response.status(Response.Status.UNAUTHORIZED).entity(e.toString()).build();
			requestContext.abortWith(response);
			return;
		}
	}
}
