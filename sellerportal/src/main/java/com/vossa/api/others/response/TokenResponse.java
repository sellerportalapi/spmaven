package com.vossa.api.others.response;

import java.security.Timestamp;

public class TokenResponse {

	private String accessToken;
	private String tokenScheme;
	private String refreshToken;
	private Timestamp tokenIssuedTime;
	private Integer expiresInSeconds;

	public TokenResponse() {
		super();
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenScheme() {
		return tokenScheme;
	}

	public void setTokenScheme(String tokenScheme) {
		this.tokenScheme = tokenScheme;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Timestamp getTokenIssuedTime() {
		return tokenIssuedTime;
	}

	public void setTokenIssuedTime(Timestamp tokenIssuedTime) {
		this.tokenIssuedTime = tokenIssuedTime;
	}

	public Integer getExpiresInSeconds() {
		return expiresInSeconds;
	}

	public void setExpiresInSeconds(Integer expiresInSeconds) {
		this.expiresInSeconds = expiresInSeconds;
	}
}
