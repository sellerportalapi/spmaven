package com.vossa.api.others;

import java.sql.Connection;
import java.sql.DriverManager;

import com.vossa.api.others.filehandlers.ConfigFileHandler;

public class DBConnection {

	public Connection getConnection() throws Exception {
		ConfigFileHandler config = new ConfigFileHandler();
		Connection connection;

		Class.forName(config.get("mysqldriver"));

		String connectionUrl = config.get("mysqlurl") + "/" + config.get("databasename") + "?useSSL=false";
		connection = DriverManager.getConnection(connectionUrl, config.get("dbusername"), config.get("dbpassword"));
		// connection = DriverManager.getConnection("jdbc:mysql://18.191.59.121:3306/sellerportal", "sp", "sp");

		// String connectionUrl = config.get("mysqlurl") + "/" + config.get("sellerportal") + "?useSSL=false";

		config = null;
		return connection;
	}

}