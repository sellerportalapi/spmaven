package com.vossa.api.others.unused;

import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionAlgorithmforKeyId {

	private static final String ENCRYPTION_KEY = "hello_world_mother_base";
	
	//SecretKey Generation
	private static final String SECRET_KEY_FACTORY_ALGORITHM = "PBKDF2WithHmacSHA1";
	private static final String SECRET_KEY_SPEC_ALGORITHM = "AES";
	private static final int ITERATION_COUNT = 1119;
	private static final int KEY_LENGTH = 256;
	
	private static final String ENCRYPTION_ALGORITHM = "AES/CBC/PKCS5Padding";
	
	private static final int SALT1_LENGTH = 5;
	private static final int SALT2_LENGTH = 11;
	
	private static final int ORIGINALSALT_LOWERLIMIT = 4;
	private static final int ORIGINALSALT_UPPERLIMIT = 15;
	
	protected String encrypt(String plaintext) 
	{		
		byte salt1Bytes[], salt2Bytes[];
		byte ivBytes[] = null;
		byte ciphertextBytes[] = null;		
		byte plaintextBytes[] = plaintext.getBytes();
		
		SecureRandom random = new SecureRandom();
		
		// Generate random salt1
		byte randomBytes[] = new byte[SALT1_LENGTH];
	    random.nextBytes(randomBytes);
	    salt1Bytes = randomBytes;
	    
	    // Generate random salt2
	    randomBytes = new byte[SALT2_LENGTH];
	    random.nextBytes(randomBytes);
	    salt2Bytes = randomBytes;
	    		    
		try 
		{
			//Deriving the original salt
			String combinedSalt = new String(salt1Bytes) + new String(salt2Bytes);
			String originalSalt = combinedSalt.substring( ORIGINALSALT_LOWERLIMIT, ORIGINALSALT_UPPERLIMIT);
			byte originalSaltBytes[] = originalSalt.getBytes();
			
		    //Deriving the encryption key
			SecretKeyFactory factoryAlgorithm = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_ALGORITHM);
			PBEKeySpec spec = new PBEKeySpec( ENCRYPTION_KEY.toCharArray(), originalSaltBytes, ITERATION_COUNT, KEY_LENGTH);
			SecretKey secretKey = factoryAlgorithm.generateSecret(spec);
			SecretKeySpec key = new SecretKeySpec( secretKey.getEncoded(), SECRET_KEY_SPEC_ALGORITHM);
			
		    //Initializing the Cipher instance
			Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, key);
		    
			//Deriving the Initialization Vector(iv)
			AlgorithmParameters parameters = cipher.getParameters();
			ivBytes =   parameters.getParameterSpec(IvParameterSpec.class).getIV();
			
			//Encrypting the plaintext
			ciphertextBytes = cipher.doFinal(plaintextBytes);
		}
		catch(Exception e)
	    {
			e.printStackTrace();
			return null;
	    }
		
		//Finding the total length (salt + iv + ciphertext)
		int bufferLength = salt1Bytes.length + ivBytes.length + ciphertextBytes.length + salt2Bytes.length;
		
		//prepend salt1 & iv and postpend salt2 to the ciphertext
		byte[] buffer = new byte[bufferLength];
		System.arraycopy( salt1Bytes, 0, buffer, 0, salt1Bytes.length);
		System.arraycopy( ivBytes, 0, buffer, salt1Bytes.length, ivBytes.length);
		System.arraycopy( ciphertextBytes, 0, buffer, salt1Bytes.length + ivBytes.length, ciphertextBytes.length);
		System.arraycopy( salt2Bytes, 0, buffer, salt1Bytes.length + ivBytes.length + ciphertextBytes.length, salt2Bytes.length);
		
		String ciphertext = Base64.getEncoder().encodeToString(buffer);
		return ciphertext;
	}
	
	protected String decrypt(String encryptedText) 
	{
		byte salt1Bytes[], salt2Bytes[]; 
		byte ivBytes[];
		byte ciphertextBytes[];
		byte plaintextBytes[];
		
		try
		{
			Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
		
			salt1Bytes = new byte[SALT1_LENGTH];
			ivBytes = new byte[cipher.getBlockSize()];
			salt2Bytes = new byte[SALT2_LENGTH];
			
		    //strip off the salt1 and iv from encryptedText
			ByteBuffer buffer = ByteBuffer.wrap( Base64.getDecoder().decode(encryptedText) );		
			buffer.get(salt1Bytes, 0, salt1Bytes.length);						
			buffer.get(ivBytes, 0, ivBytes.length);
			
			//strip off the ciphertext from encryptedText
			int ciphertextLength = buffer.capacity() - salt1Bytes.length - ivBytes.length - salt2Bytes.length;
			ciphertextBytes = new byte[ciphertextLength];		  
			buffer.get(ciphertextBytes, 0, ciphertextLength);
			
			//strip off the salt2 from encryptedText
			buffer.get(salt2Bytes, 0, salt2Bytes.length);
			
			//Deriving the original salt
			String combinedSalt = new String(salt1Bytes) + new String(salt2Bytes);
			String originalSalt = combinedSalt.substring( ORIGINALSALT_LOWERLIMIT, ORIGINALSALT_UPPERLIMIT);
			byte originalSaltBytes[] = originalSalt.getBytes();
			
		    // Deriving the key
			SecretKeyFactory factoryAlgorithm = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_ALGORITHM);
			PBEKeySpec spec = new PBEKeySpec(ENCRYPTION_KEY.toCharArray(), originalSaltBytes, ITERATION_COUNT, KEY_LENGTH);
			SecretKey secretKey = factoryAlgorithm.generateSecret(spec);
			SecretKeySpec key = new SecretKeySpec(secretKey.getEncoded(), SECRET_KEY_SPEC_ALGORITHM);
			
			//Initializing the Cipher instance
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(ivBytes));
			
			//Decrypting the word
			plaintextBytes = cipher.doFinal(ciphertextBytes);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		
		String plaintext = new String(plaintextBytes);
		return plaintext;
	}
}
