package com.vossa.api.others;

public class SellerCodeConversion {

	public static String convert(int storeid) {
		return String.format("VS%04d", storeid);
	}
}
