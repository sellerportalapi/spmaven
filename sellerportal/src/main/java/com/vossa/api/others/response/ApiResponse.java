package com.vossa.api.others.response;

import java.util.List;

public class ApiResponse {
	private Integer code;
	private String status;
	private String httpStatus;
	private String message;
	private String userMessage;
	private String clientMessage;
	private String htmlMessage;

	private Object data;

	private TokenResponse token;
	private Metadata metadata;
	private ApiError error;
	private List<HateoasLink> links;

	public ApiResponse() {
		super();
	}

	public ApiResponse(Integer code, String status, String message) {
		super();
		this.code = code;
		this.status = status;
		this.message = message;
	}

	public ApiResponse(Integer code, String status, String message, Object data) {
		super();
		this.code = code;
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public ApiResponse code(Integer code) {
		this.code = code;
		return this;
	}

	public ApiResponse status(String status) {
		this.status = status;
		return this;
	}

	public ApiResponse httpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
		return this;
	}

	public ApiResponse message(String message) {
		this.message = message;
		return this;
	}

	public ApiResponse userMessage(String userMessage) {
		this.userMessage = userMessage;
		return this;
	}

	public ApiResponse clientMessage(String clientMessage) {
		this.clientMessage = clientMessage;
		return this;
	}

	public ApiResponse htmlMessage(String htmlMessage) {
		this.htmlMessage = htmlMessage;
		return this;
	}

	public ApiResponse data(Object data) {
		this.data = data;
		return this;
	}

	public ApiResponse token(TokenResponse token) {
		this.token = token;
		return this;
	}

	public ApiResponse metadata(Metadata metadata) {
		this.metadata = metadata;
		return this;
	}

	public ApiResponse error(ApiError error) {
		this.error = error;
		return this;
	}

	public ApiResponse links(List<HateoasLink> links) {
		this.links = links;
		return this;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUserMessage() {
		return userMessage;
	}

	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	public String getClientMessage() {
		return clientMessage;
	}

	public void setClientMessage(String clientMessage) {
		this.clientMessage = clientMessage;
	}

	public String getHtmlMessage() {
		return htmlMessage;
	}

	public void setHtmlMessage(String htmlMessage) {
		this.htmlMessage = htmlMessage;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public TokenResponse getToken() {
		return token;
	}

	public void setToken(TokenResponse token) {
		this.token = token;
	}

	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	public ApiError getError() {
		return error;
	}

	public void setError(ApiError error) {
		this.error = error;
	}

	public List<HateoasLink> getLinks() {
		return links;
	}

	public void setLinks(List<HateoasLink> links) {
		this.links = links;
	}
}
