package com.vossa.api.others.token;

public class JWT {

	// Headers
	private String typ;

	// Reserved Claims
	private String jti;
	private String iss;
	private String aud;
	private String sub;
	private long iat;
	private long exp;

	// Private Claims
	private UserClaims user;

	public JWT() {
		super();
	}

	// JWT without user details
	public JWT(String jti, String iss, String aud, String sub) {
		super();
		this.jti = jti;
		this.iss = iss;
		this.aud = aud;
		this.sub = sub;
	}

	// JWT with user details
	public JWT(String jti, String iss, String aud, String sub, UserClaims user) {
		super();
		this.jti = jti;
		this.iss = iss;
		this.aud = aud;
		this.sub = sub;
		this.user = user;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getJti() {
		return jti;
	}

	public void setJti(String jti) {
		this.jti = jti;
	}

	public String getIss() {
		return iss;
	}

	public void setIss(String iss) {
		this.iss = iss;
	}

	public String getAud() {
		return aud;
	}

	public void setAud(String aud) {
		this.aud = aud;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public long getIat() {
		return iat;
	}

	public void setIat(long iat) {
		this.iat = iat;
	}

	public long getExp() {
		return exp;
	}

	public void setExp(long exp) {
		this.exp = exp;
	}

	public UserClaims getUser() {
		return user;
	}

	public void setUser(UserClaims user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "JWT [typ=" + typ + ", jti=" + jti + ", iss=" + iss + ", aud=" + aud + ", sub=" + sub + ", iat=" + iat
				+ ", exp=" + exp + ", user=" + user + "]";
	}
}
