package com.vossa.api.others.queryparam;

import java.util.List;

public class SearchParam {

	private List<String> searchList;

	public SearchParam(List<String> searchList) {
		super();
		this.searchList = searchList;
	}

	public List<String> getSearchList() {
		return searchList;
	}

	public void setSearchList(List<String> searchList) {
		this.searchList = searchList;
	}
}
