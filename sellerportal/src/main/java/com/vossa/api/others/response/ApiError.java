package com.vossa.api.others.response;

import java.util.List;

public class ApiError {
	private Integer code;
	private String type;
	private String message;
	private String detail;
	private Boolean isAttributeError;
	private String attributeErrorType;
	private List<AttributeError> attributeErrors;

	public ApiError() {
		super();
	}

	public ApiError(Integer code, String type, String message) {
		super();
		this.code = code;
		this.type = type;
		this.message = message;
	}

	public ApiError(Integer code, String type, String message, String detail) {
		super();
		this.code = code;
		this.type = type;
		this.message = message;
		this.detail = detail;
	}

	public ApiError(Integer code, String type, String message, String detail, Boolean isAttributeError,
			String attributeErrorType, List<AttributeError> attributeErrors) {
		super();
		this.code = code;
		this.type = type;
		this.message = message;
		this.detail = detail;
		this.isAttributeError = isAttributeError;
		this.attributeErrorType = attributeErrorType;
		this.attributeErrors = attributeErrors;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Boolean getIsAttributeError() {
		return isAttributeError;
	}

	public void setIsAttributeError(Boolean isAttributeError) {
		this.isAttributeError = isAttributeError;
	}

	public String getAttributeErrorType() {
		return attributeErrorType;
	}

	public void setAttributeErrorType(String attributeErrorType) {
		this.attributeErrorType = attributeErrorType;
	}

	public List<AttributeError> getAttributeErrors() {
		return attributeErrors;
	}

	public void setAttributeErrors(List<AttributeError> attributeErrors) {
		this.attributeErrors = attributeErrors;
	}
}
