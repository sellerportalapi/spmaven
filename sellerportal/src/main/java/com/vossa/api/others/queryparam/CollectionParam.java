package com.vossa.api.others.queryparam;

import java.util.List;

public final class CollectionParam {

	public List<String> searchValues;
	public List<Filter> filterKeys;
	public List<Sorter> sortingKeys;
	public int offset;
	public int limit;

	public CollectionParam searchValues(List<String> searchValues) {
		this.searchValues = searchValues;
		return this;
	}

	public CollectionParam filterKeys(List<Filter> filterKeys) {
		this.filterKeys = filterKeys;
		return this;
	}

	public CollectionParam sortingKeys(List<Sorter> sortingKeys) {
		this.sortingKeys = sortingKeys;
		return this;
	}

	public CollectionParam offset(int offset) {
		this.offset = offset;
		return this;
	}

	public CollectionParam limit(int limit) {
		this.limit = limit;
		return this;
	}
}
