package com.vossa.api.others.response;

public class HateoasLink {
	private String href;
	private String method;
	private String rel;

	public HateoasLink() {
		super();
	}

	public HateoasLink(String href, String method, String rel) {
		super();
		this.href = href;
		this.method = method;
		this.rel = rel;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}
}
