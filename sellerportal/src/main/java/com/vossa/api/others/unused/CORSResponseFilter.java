package com.vossa.api.others.unused;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;

//@Provider
public class CORSResponseFilter implements ContainerResponseFilter {

	
	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException 
	{
		System.out.println("response-store");

		MultivaluedMap<String, Object> headers = responseContext.getHeaders();
//		headers.add("Access-Control-Allow-Origin", "http://localhost:4200");
//		headers.add("Access-Control-Allow-Origin", "http://13.232.4.131");
		headers.add("Access-Control-Allow-Origin", "*");

//		headers.add( "Access-Control-Allow-Credentials", true );
		headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");	
		
		headers.add("Access-Control-Allow-Headers", "Content-Type, Authorization, token" );
		headers.add("Access-Control-Expose-Headers", "Content-Type, Authorization, token" );
	}
}