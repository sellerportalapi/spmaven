package com.vossa.api.others.queryparam;

import java.util.List;

public class SortParam {

	private List<Sorter> sorters;

	public SortParam(List<Sorter> sorters) {
		super();
		this.sorters = sorters;
	}

	public List<Sorter> getSorters() {
		return sorters;
	}

	public void setSorters(List<Sorter> sorters) {
		this.sorters = sorters;
	}
}
