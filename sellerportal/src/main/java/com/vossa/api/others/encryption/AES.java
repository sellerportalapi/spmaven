package com.vossa.api.others.encryption;

import java.nio.ByteBuffer;
import java.security.*;
import java.util.Base64;

import javax.crypto.*;
import javax.crypto.spec.*;

public class AES {

	private static final String encryptionKey = "@_VeRt(-)eNtErPrI$e_%";
	private static final String encryptionAlgorithm_forKey = "AES";
	private static final String encryptionAlgorithm = "AES/CBC/PKCS5Padding";

	private static final String secretKeyAlgorithm = "PBKDF2WithHmacSHA1";
	private static final int iterationCount = 65556;
	private static final int keyLength = 256;
	
	private static final int saltLength = 20;
	
	public static String encrypt(String plaintext) 
	{
		byte[] saltBytes = null; 
		byte[] ivBytes = null;
		byte[] ciphertextBytes = null;

		Cipher cipher;
		
		// Generate random salt
		SecureRandom random = new SecureRandom();
	    byte randomBytes[] = new byte[saltLength];
	    random.nextBytes(randomBytes);
	    saltBytes = randomBytes;
	    		    
		try 
		{
		    // Derive the key
			SecretKeyFactory factoryAlgorithm = SecretKeyFactory.getInstance(secretKeyAlgorithm);
			PBEKeySpec spec = new PBEKeySpec( encryptionKey.toCharArray(), saltBytes, iterationCount, keyLength);
			SecretKey secretKey = factoryAlgorithm.generateSecret(spec);
			SecretKeySpec key = new SecretKeySpec(secretKey.getEncoded(), encryptionAlgorithm_forKey);
			
		     //encrypting the word
			cipher = Cipher.getInstance(encryptionAlgorithm);
			cipher.init(Cipher.ENCRYPT_MODE, key);
		    
			AlgorithmParameters params = cipher.getParameters();

			ivBytes =   params.getParameterSpec(IvParameterSpec.class).getIV();
			ciphertextBytes = cipher.doFinal(plaintext.getBytes());		     
		}
		catch(Exception e)
	    {
			e.printStackTrace();
	    }
		
		//prepend salt and vi
		int bufferLength = saltBytes.length + ivBytes.length + ciphertextBytes.length;
		byte[] buffer = new byte[bufferLength];
		System.arraycopy( saltBytes, 0, buffer, 0, saltBytes.length);
		System.arraycopy( ivBytes, 0, buffer, saltBytes.length, ivBytes.length);
		System.arraycopy( ciphertextBytes, 0, buffer, saltBytes.length + ivBytes.length, ciphertextBytes.length);
		
//		return new Base64().encodeToString(buffer);
		return Base64.getEncoder().encodeToString(buffer);
	}
	
	public static String decrypt(String encryptedText) 
	{
		byte[] saltBytes = null; 
		byte[] ivBytes = null;
		byte[] ciphertextBytes = null;
		byte[] plaintextBytes = null;

		Cipher cipher;
		
		try
		{
			cipher = Cipher.getInstance(encryptionAlgorithm);
		
		    //strip off the salt and iv
//			ByteBuffer buffer = ByteBuffer.wrap(new Base64().decode(encryptedText));
			ByteBuffer buffer = ByteBuffer.wrap( Base64.getDecoder().decode(encryptedText) );
			saltBytes = new byte[saltLength];
			buffer.get(saltBytes, 0, saltBytes.length);
			
			ivBytes = new byte[cipher.getBlockSize()];
			buffer.get(ivBytes, 0, ivBytes.length);
			
			int ciphertextLength = buffer.capacity() - saltBytes.length - ivBytes.length;
			ciphertextBytes = new byte[ciphertextLength];		  
			buffer.get(ciphertextBytes);
			
		    // Deriving the key
			SecretKeyFactory factoryAlgorithm = SecretKeyFactory.getInstance(secretKeyAlgorithm);
			PBEKeySpec spec = new PBEKeySpec(encryptionKey.toCharArray(), saltBytes, iterationCount, keyLength);
			SecretKey secretKey = factoryAlgorithm.generateSecret(spec);
			SecretKeySpec key = new SecretKeySpec(secretKey.getEncoded(), encryptionAlgorithm_forKey);
			
			//Decrypting the word
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(ivBytes));		    
			plaintextBytes = cipher.doFinal(ciphertextBytes);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}    
		return new String(plaintextBytes);
	}
	
	public static void main(String args[])
	{
		String plain = "admin";
		String cipher;
		
		cipher = AES.encrypt(plain);
		cipher = "GeJVJYePdnHK335HGuMqbGM/FiNsu9rwfs7SCvjxMfczLbrzYzFCfdhQoDrGE3b9m/rh/A==";
		plain = AES.decrypt(cipher);
		System.out.println(plain);
		System.out.println(cipher);
		System.out.println(cipher.length());
		
		System.out.println(AES.decrypt(cipher));
	}
}
