package com.vossa.api.others.filehandlers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
 
public class ConfigFileHandler {	
 
	private static Properties property = new Properties();	
	
	public ConfigFileHandler() throws IOException
	{
		final String configFileName = "spconfig.properties";
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName);
 
		if(inputStream != null)
		{
			property.load(inputStream);
		} 
		else 
		{
			throw new FileNotFoundException("property file '" + configFileName + "' not found in the classpath");
		}
		inputStream.close();
	}
	
	//Get the required property from spconfig.properties
	public String get(String field)
	{
		return property.getProperty(field);
	}
}
