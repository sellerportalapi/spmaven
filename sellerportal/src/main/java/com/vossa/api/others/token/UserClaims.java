package com.vossa.api.others.token;

import java.util.Map;

public class UserClaims {

	private Integer userId;
	private String userName;
	private String userType;
	private Integer storeId;
	private String storeName;
	private Map<String, Object> permission;

	public UserClaims() {
		super();
	}

	// For VERTADMIN jwt
	public UserClaims(Integer userId, String userName, String userType) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userType = userType;
	}

	// For STOREUSER jwt
	public UserClaims(Integer userId, String userName, String userType, Integer storeId, String storeName) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userType = userType;
		this.storeId = storeId;
		this.storeName = storeName;
	}
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Map<String, Object> getPermission() {
		return permission;
	}

	public void setPermission(Map<String, Object> permission) {
		this.permission = permission;
	}

	@Override
	public String toString() {
		return "UserClaims [userId=" + userId + ", userName=" + userName + ", userType=" + userType + ", storeId="
				+ storeId + ", storeName=" + storeName + "]";
	}
}
