package com.vossa.api.others.response;

public class AttributeError {
	private String title;
	private String attribute;
	private Object value;
	private String reason;

	public AttributeError() {
		super();
	}

	public AttributeError(String attribute, Object value) {
		super();
		this.attribute = attribute;
		this.value = value;
	}

	public AttributeError(String title, String attribute, Object value) {
		super();
		this.title = title;
		this.attribute = attribute;
		this.value = value;
	}

	public AttributeError(String title, String attribute, Object value, String reason) {
		super();
		this.title = title;
		this.attribute = attribute;
		this.value = value;
		this.reason = reason;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
