package com.vossa.api.others.mailservices;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;

import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.vossa.api.others.ResponseClass;

public class SellerMailService {
	Response response;
	int _statuscode;
	String status;
	Boolean _status;

	private static final String DVIVERT_EMAIL = "dvivert_global@projectverte.com";
	private static final String SENDGRID_API_KEY = "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";

	public ResponseClass sendmailContactUs(String receiver, String subject, String name) throws Exception {

		Mail mail = new Mail(new Email(DVIVERT_EMAIL), subject, new Email(receiver),
				new Content("text/html", "using template"));
		mail.setSubject(subject);
		mail.personalization.get(0).addSubstitution("-name-", name);
		mail.setTemplateId("bd1ba96a-7010-441f-951a-8c07f7fb3485");

		SendGrid sg = new SendGrid(SENDGRID_API_KEY);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sg.api(request);
		} catch (Exception ex) {
			throw ex;
		}
		_statuscode = response.getStatusCode();
		status = "" + _statuscode;
		status = status.substring(0, 1);
		if (status.equals("2")) {
			_status = true;
		} else
			_status = false;

		if (_status == false)
			return new ResponseClass(404, "fail", "Invalid Email id", "Please check your mail id");

		return new ResponseClass(201, "success", "Your request is accepted, we will contact you soon...");
	}

	public ResponseClass sendmailSamTeam(String receiver, String subject, Map<String, String> map) throws Exception {

		Mail mail = new Mail(new Email(DVIVERT_EMAIL), subject, new Email(receiver),
				new Content("text/html", "using template"));
		mail.setSubject(subject);
		@SuppressWarnings("rawtypes")
		Iterator it = map.entrySet().iterator();
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			Map.Entry<String, String> m = (Entry<String, String>) it.next();
			mail.personalization.get(0).addSubstitution(m.getKey(), m.getValue());
		}
		mail.setTemplateId("a33dc273-f44f-4c95-a1bc-4f1c332e5a80");

		SendGrid sg = new SendGrid(SENDGRID_API_KEY);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sg.api(request);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		_statuscode = response.getStatusCode();
		status = "" + _statuscode;
		status = status.substring(0, 1);
		if (status.equals("2")) {
			_status = true;
		} else
			_status = false;

		if (_status == false)
			return new ResponseClass(404, "fail", "Invalid Email id", "Please check your mail id");

		return new ResponseClass(201, "success", "Your request is accepted, we will contact you soon...");
	}

	public void sendmailActivation(String receiver, String subject, String username, String passwordUrl)
			throws Exception {
		try {
			Mail mail = new Mail(new Email(DVIVERT_EMAIL), subject, new Email(receiver),
					new Content("text/html", "using template"));
			mail.setSubject(subject);
			mail.personalization.get(0).addSubstitution("-userName-", username);
			mail.personalization.get(0).addSubstitution("-passwordURL-", passwordUrl);
			mail.setTemplateId("39fe9a2f-b38b-4b45-a188-7550a8ecb1a6");

			SendGrid sg = new SendGrid(SENDGRID_API_KEY);
			Request request = new Request();

			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sg.api(request);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void sendmailForgotPassword(String receiver, String subject, String passwordResetUrl) throws Exception {

		Mail mail = new Mail(new Email(DVIVERT_EMAIL), subject, new Email(receiver),
				new Content("text/html", "using template"));
		mail.setSubject(subject);
		mail.personalization.get(0).addSubstitution("-resetURL-", passwordResetUrl);
		mail.setTemplateId("7bb1bc87-ac14-4bfe-b50d-5c57727d7a33");

		SendGrid sg = new SendGrid(SENDGRID_API_KEY);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sg.api(request);
		} catch (Exception ex) {
			throw ex;
		}
	}
}
