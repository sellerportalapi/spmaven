package com.vossa.api.others.queryparam;

import com.vossa.api.general.enums.SortCondition;

public final class Sorter {

	private final String attribute;
	private SortCondition condition = SortCondition.asc; // default: ascending sort

	public Sorter(String attribute) {
		super();
		this.attribute = attribute;
	}

	public Sorter(String attribute, SortCondition condition) {
		super();
		this.attribute = attribute;
		this.condition = condition;
	}

	public String getAttribute() {
		return attribute;
	}

	public SortCondition getCondition() {
		return condition;
	}

	public void setCondition(SortCondition condition) {
		this.condition = condition;
	}
}
