package com.vossa.api.others.queryparam;

import java.util.List;

public class FieldParam {

	public List<String> fields;
	public int pattern;

	public FieldParam fields(List<String> fields) {
		this.fields = fields;
		return this;
	}

	public FieldParam pattern(int pattern) {
		this.pattern = pattern;
		return this;
	}
}
