package com.vossa.api.others.response;

import java.util.List;

public class Metadata {

	private Integer offset;
	private Integer limit;
	private Integer returnCount;
	private Integer totalCount;

	private List<HateoasLink> links;

	public Metadata() {
		super();
	}

	public Metadata(Integer offset, Integer limit) {
		super();
		this.offset = offset;
		this.limit = limit;
	}

	public Metadata(Integer offset, Integer limit, Integer returnCount, Integer totalCount) {
		super();
		this.offset = offset;
		this.limit = limit;
		this.returnCount = returnCount;
		this.totalCount = totalCount;
	}

	public Metadata(Integer offset, Integer limit, Integer returnCount, Integer totalCount, List<HateoasLink> links) {
		super();
		this.offset = offset;
		this.limit = limit;
		this.returnCount = returnCount;
		this.totalCount = totalCount;
		this.links = links;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getReturnCount() {
		return returnCount;
	}

	public void setReturnCount(Integer returnCount) {
		this.returnCount = returnCount;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<HateoasLink> getLinks() {
		return links;
	}

	public void setLinks(List<HateoasLink> links) {
		this.links = links;
	}
}
