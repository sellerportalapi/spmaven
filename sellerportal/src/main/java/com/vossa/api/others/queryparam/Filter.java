package com.vossa.api.others.queryparam;

import com.vossa.api.general.enums.FilterCondition;

public final class Filter {

	private final String attribute;
	private final Object value;
	private FilterCondition condition = FilterCondition.e; // default: equals to

	public Filter(String attribute, Object value) {
		super();
		this.attribute = attribute;
		this.value = value;
	}

	public Filter(String attribute, Object value, FilterCondition condition) {
		super();
		this.attribute = attribute;
		this.value = value;
		this.condition = condition;
	}

	public String getAttribute() {
		return attribute;
	}

	public Object getValue() {
		return value;
	}

	public FilterCondition getCondition() {
		return condition;
	}

	public void setCondition(FilterCondition condition) {
		this.condition = condition;
	}
}
