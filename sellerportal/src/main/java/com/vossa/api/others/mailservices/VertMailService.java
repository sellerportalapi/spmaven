package com.vossa.api.others.mailservices;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

public class VertMailService {

	Response response;
	int _statuscode;
	String status;
	Boolean _status;

	private static final String DVIVERT_EMAIL = "dvivert_global@projectverte.com";
	private static final String SENDGRID_API_KEY = "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";

	public void sendmailActivation(String receiver, String subject, String username, String passwordUrl)
			throws Exception {
		try {
			Mail mail = new Mail(new Email(DVIVERT_EMAIL), subject, new Email(receiver),
					new Content("text/html", "using template"));
			mail.setSubject(subject);
			mail.personalization.get(0).addSubstitution("-userName-", username);
			mail.personalization.get(0).addSubstitution("-passwordURL-", passwordUrl);
			mail.setTemplateId("ab87250c-8b95-4476-bf1b-e6d1a40de0b5");

			SendGrid sg = new SendGrid(SENDGRID_API_KEY);
			Request request = new Request();

			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sg.api(request);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void sendmailForgotPassword(String receiver, String subject, String passwordResetUrl) throws Exception {

		Mail mail = new Mail(new Email(DVIVERT_EMAIL), subject, new Email(receiver),
				new Content("text/html", "using template"));
		mail.setSubject(subject);
		mail.personalization.get(0).addSubstitution("-resetURL-", passwordResetUrl);
		mail.setTemplateId("7bb1bc87-ac14-4bfe-b50d-5c57727d7a33");

		SendGrid sg = new SendGrid(SENDGRID_API_KEY);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = sg.api(request);
		} catch (Exception ex) {
			throw ex;
		}
	}
}
