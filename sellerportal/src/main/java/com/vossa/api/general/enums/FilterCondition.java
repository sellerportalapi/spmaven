package com.vossa.api.general.enums;

public enum FilterCondition {
	in, // in the list
	nin, // not in the list
	e, // equals to
	ne, // not equals to
	lte, // less than or equals to
	gte, // greater than or equals to
	before, // before time
	after // after time
}
