package com.vossa.api.general.enums;

public enum CategoryLevel {
	PARENT,
	CHILD,
	SUB
}
