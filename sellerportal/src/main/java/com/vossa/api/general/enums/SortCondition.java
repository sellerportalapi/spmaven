package com.vossa.api.general.enums;

public enum SortCondition {
	asc, // Ascending order
	desc // Descending order
}
