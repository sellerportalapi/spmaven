package com.vossa.api.general.exceptionmappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.vossa.api.general.exceptions.ServerException;

@Provider
public class ServerExceptionMapper implements ExceptionMapper<ServerException> {

	@Override
	public Response toResponse(ServerException exception) {
		return Response.serverError().build();
	}
}
