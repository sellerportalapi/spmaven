package com.vossa.api.general.exceptions;

public class ServerException extends ApiException {

	private static final long serialVersionUID = -3153614256892755177L;

	public ServerException(String userMessage) {
		super(userMessage);
	}

	public ServerException(String userMessage, String clientMessage) {
		super(userMessage, clientMessage);
	}
}
