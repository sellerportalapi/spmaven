package com.vossa.api.general.annotations.filterannotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

import com.vossa.api.general.enums.UserType;

@NameBinding
@Retention( value = RetentionPolicy.RUNTIME )
@Target( {ElementType.TYPE, ElementType.METHOD} )
public @interface Secured { 
	
	UserType[] value() default{};
}