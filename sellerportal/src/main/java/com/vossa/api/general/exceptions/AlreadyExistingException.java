package com.vossa.api.general.exceptions;

public class AlreadyExistingException extends ClientException {

	private static final long serialVersionUID = 1L;

	private String title;
	private String attribute;
	private Object value;

	public AlreadyExistingException(String userMessage) {
		super(userMessage);
	}

	public AlreadyExistingException(String title, Object value) {
		super(title + " '" + value + "' already exists.");
		this.title = title;
		this.value = value;
	}

	public AlreadyExistingException(String title, String attribute, Object value) {
		super(title + " '" + value + "' already exists.", attribute + " '" + value + "' already exists.");
		this.title = title;
		this.attribute = attribute;
		this.value = value;
	}

	public String getTitle() {
		return title;
	}

	public String getAttribute() {
		return attribute;
	}

	public Object getValue() {
		return value;
	}
}
