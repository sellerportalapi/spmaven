package com.vossa.api.general.policies;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailPolicy {

	public static void validateEmail(String email) throws Exception {

		int numberOfAt = email.replaceAll("[^@]", "").length();

		if (numberOfAt == 1) {

			String first = (String) email.split("@")[0];
			System.out.println("localname: " + first);

			Pattern before_at = Pattern.compile("^[a-zA-Z0-9]+");
			Matcher hasbefore_at = before_at.matcher(first);
			boolean x = hasbefore_at.matches();

			String second_half = (String) email.split("@")[1];
			System.out.println("domainname: " + second_half);

			int numberOfdot = email.replaceAll("[^.]", "").length();

			if (numberOfdot == 1) {
				String second = (String) second_half.split("\\.")[0];
				System.out.println("Low_level_domain_name " + second);

				String match = "projectverte";
				Pattern before_dot = Pattern.compile(match);
				Matcher hasbefore_dot = before_dot.matcher(second);
				boolean y = hasbefore_dot.matches();

				String third = (String) second_half.split("\\.")[1];
				System.out.println("Top_level_domain_name: " + third);

				Pattern after_dot = Pattern.compile("[a-z]{2,6}$");
				Matcher hasafter_dot = after_dot.matcher(third);
				boolean z = hasafter_dot.matches();

				if (x == true && y == true && z == true) {
					System.out.println("Valid Email");
				} else {
					throw new Exception("Invalid Email");
				}
			} else {
				throw new Exception("Invalid Email");
			}
		} else {
			throw new Exception("Invalid Email");
		}
	}

	protected static void validateDomain(String domain) throws UnknownHostException {
		InetAddress.getByName(domain); // throws UnknownHostException for invalid domain
	}
}
