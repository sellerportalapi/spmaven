package com.vossa.api.general.enums;

public enum ResponseStatus {
	SUCCESS("2XX", "SUCCESSFUL"), FAILURE("4XX", "CLIENT_ERROR"), ERROR("5XX", "SERVER_ERROR");

	private final String category;
	private final String family;

	ResponseStatus(final String category, final String family) {
		this.category = category;
		this.family = family;
	}

	public String getCategory() {
		return category;
	}

	public String getFamily() {
		return family;
	}
}
