package com.vossa.api.general.exceptions;

public class ApiException extends Exception {

	private static final long serialVersionUID = 2179393166221918190L;

	private String userMessage;
	private String clientMessage;

	public ApiException() {
		super();
	}

	public ApiException(String userMessage) {
		super(userMessage);
		this.userMessage = userMessage;
	}

	public ApiException(String userMessage, String clientMessage) {
		super(userMessage);
		this.userMessage = userMessage;
		this.clientMessage = clientMessage;
	}

	public String getUserMessage() {
		return userMessage;
	}

	public String getClientMessage() {
		return clientMessage;
	}
}
