package com.vossa.api.general.enums;

public enum HttpMethod {
	GET,
	POST,
	PUT,
	DELETE,
	PATCH,
	HEAD,
	OPTIONS
}
