package com.vossa.api.general.enums;

public enum PaginationHateoasRelation {
	first,
	prev,
	self,
	next,
	last,
}
