package com.vossa.api.general.enums;

public enum UserType {
	SUPERUSER,
	STANDARDUSER,
	VERTADMIN
}
