package com.vossa.api.general.policies;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordPolicy {

	public static final long ACCOUNT_LOCKOUT_TIME = 30 * 60 * 1000;
	public static final long LOCKOUT_RESET_TIME = 15 * 60 * 1000;

	public static void validatePassword(String plainPassword) throws Exception {
		if (plainPassword.length() < 8)
			throw new Exception("Password must be 8 characters long");
		else if (plainPassword.length() > 25)
			throw new Exception("Password must be less than 25 characters");

		Pattern lowercaseLetter = Pattern.compile("[a-z]");
		Pattern uppercaseLetter = Pattern.compile("[A-Z]");
		Pattern digit = Pattern.compile("[0-9]");
		Pattern specialchar = Pattern.compile("[!@#$%^&*(){}\\[\\]_]");

		Matcher hasLowercaseLetter = lowercaseLetter.matcher(plainPassword);
		Matcher hasUppercaseLetter = uppercaseLetter.matcher(plainPassword);
		Matcher hasDigit = digit.matcher(plainPassword);
		Matcher hasSpecialchar = specialchar.matcher(plainPassword);

		int flag1 = hasLowercaseLetter.find() ? 1 : 0;
		int flag2 = hasUppercaseLetter.find() ? 1 : 0;
		int flag3 = hasDigit.find() ? 1 : 0;
		int flag4 = hasSpecialchar.find() ? 1 : 0;

		int complexity = flag1 + flag2 + flag3 + flag4;
		final int requiredComplexity = 3;
		if (complexity < requiredComplexity) {
			throw new Exception("Password should contain atleast one number, letter and a special character");
		}
	}
}
