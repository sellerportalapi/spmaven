package com.vossa.api.general.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target( {ElementType.METHOD} )
public @interface ApiMethod {
	
	String classname() default "";
	String apiname() default "";
	String method() default "";
}
