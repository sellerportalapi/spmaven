package com.vossa.api.general.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention( value = RetentionPolicy.RUNTIME )
@Target( {ElementType.FIELD} )
public @interface Attribute {

	String name();
	String title();
	String fieldname();
	String keyname();
	boolean required();
}
