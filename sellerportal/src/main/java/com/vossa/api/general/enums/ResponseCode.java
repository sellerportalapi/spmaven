package com.vossa.api.general.enums;

public enum ResponseCode {
	OK(200, "OK", ResponseStatus.SUCCESS),
	CREATED(201, "Created", ResponseStatus.SUCCESS),
	ACCEPTED(202, "Accepted", ResponseStatus.SUCCESS),
	NO_CONTENT(204, "No Content", ResponseStatus.SUCCESS),
	BAD_REQUEST(400, "Bad Request", ResponseStatus.FAILURE),
	UNAUTHORIZED(401, "Unauthorized", ResponseStatus.FAILURE),
	FORBIDDEN(403, "Forbidden", ResponseStatus.FAILURE),
	NOT_FOUND(404, "Not Found", ResponseStatus.FAILURE),
	CONFLICT(409, "Conflict", ResponseStatus.FAILURE),
	INTERNAL_SERVER_ERROR(500, "Internal Server Error", ResponseStatus.ERROR);

	private final int code;
	private final String httpStatus;
	private final ResponseStatus responseStatus;

	private ResponseCode(int code, String httpStatus, ResponseStatus responseStatus) {
		this.code = code;
		this.httpStatus = httpStatus;
		this.responseStatus = responseStatus;
	}

	public int getCode() {
		return code;
	}

	public String getHttpStatus() {
		return httpStatus;
	}

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}
}
