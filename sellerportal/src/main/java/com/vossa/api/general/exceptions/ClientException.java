package com.vossa.api.general.exceptions;

public class ClientException extends ApiException {

	private static final long serialVersionUID = -2488409567322126541L;

	public ClientException(String userMessage) {
		super(userMessage);
	}

	public ClientException(String userMessage, String clientMessage) {
		super(userMessage, clientMessage);
	}
}
