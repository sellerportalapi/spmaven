package com.vossa.api.general.exceptions;

public class InvalidDataException extends ClientException {

	private static final long serialVersionUID = -910530005960752581L;

	private String title;
	private String attribute;
	private Object value;

	public InvalidDataException(String userMessage) {
		super(userMessage);
	}

	public InvalidDataException(String title, Object value) {
		super(title + " '" + value + "' is an invalid data.");
		this.title = title;
		this.value = value;
	}

	public InvalidDataException(String title, String attribute, Object value) {
		super(title + " '" + value + "' is an invalid data.", attribute + " '" + value + "' is an invalid data.");
		this.title = title;
		this.attribute = attribute;
		this.value = value;
	}

	public String getTitle() {
		return title;
	}

	public String getAttribute() {
		return attribute;
	}

	public Object getValue() {
		return value;
	}
}
