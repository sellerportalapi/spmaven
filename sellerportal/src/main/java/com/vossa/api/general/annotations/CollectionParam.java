package com.vossa.api.general.annotations;

public @interface CollectionParam {

	boolean searchable() default false;
	boolean filterable() default false;
	boolean sortable() default false;
}
