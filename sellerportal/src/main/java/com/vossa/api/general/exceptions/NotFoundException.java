package com.vossa.api.general.exceptions;

public class NotFoundException extends ClientException {

	private static final long serialVersionUID = -910530005960752581L;

	private String title;
	private String attribute;
	private Object value;

	public NotFoundException(String userMessage) {
		super(userMessage);
	}

	public NotFoundException(String title, Object value) {
		super(title + " '" + value + "' not found.");
		this.title = title;
		this.value = value;
	}

	public NotFoundException(String title, String attribute, Object value) {
		super(title + " '" + value + "' not found.", attribute + " '" + value + "' not found.");
		this.title = title;
		this.attribute = attribute;
		this.value = value;
	}

	public String getTitle() {
		return title;
	}

	public String getAttribute() {
		return attribute;
	}

	public Object getValue() {
		return value;
	}
}
