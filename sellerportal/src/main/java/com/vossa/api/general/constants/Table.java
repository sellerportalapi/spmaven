package com.vossa.api.general.constants;

public class Table {

	public static final String vertadmin = "vertadmin";
	public static final String vertrecentpassword = "vertrecentpassword";
	public static final String vertpasswordjti = "vertpasswordjti";
	public static final String vertassignedroles = "vertassignedroles";
	public static final String vertlogindetails = "vertlogindetails";

	public static final String vertfeatures = "vertfeatures";
	public static final String vertroles = "vertroles";
	public static final String vertselectedfeatures = "vertselectedfeatures";
	public static final String vertapilist = "vertapilist";
	public static final String vertapicontrol = "vertapicontrol";

	public static final String usertype = "usertype";
	public static final String stores = "stores";
	public static final String users = "users";
	public static final String sellingmode = "sellingmode";
	public static final String userrecentpassword = "userrecentpassword";
	public static final String userlogindetails = "userlogindetails";
	public static final String userpasswordjti = "userpasswordjti";
	public static final String storeusermapper = "storeusermapper";

	public static final String fieldmaster = "fieldmaster";
	public static final String productmapper = "productmapper";
	public static final String productvaluemaster = "productvaluemaster";
	public static final String productinventory = "productinventory";
	public static final String productmanageprice = "productmanageprice";
	public static final String selectedfields = "selectedfields";
	public static final String productcategory = "productcategory";
	public static final String fulfillmentcategory = "fulfillmentcategory";

	public static final String contactvert = "contactvert";

	public static final String cities = "cities";
	public static final String states = "states";
	public static final String countries = "countries";

	public static final String asn = "asn";
	public static final String asnpalletlevel = "asnpalletlevel";
	public static final String asncaselevel = "asncaselevel";
	public static final String asnskulevel = "asnskulevel";

	public static final String features = "features";
	public static final String subfeatures = "subfeatures";
	public static final String roles = "roles";
	public static final String selectedvalues = "selectedvalues";
	public static final String selectedsubvalues = "selectedsubvalues";
	public static final String assignednames = "assignednames";

	public static final String apilist = "apilist";
	public static final String apicontrol = "apicontrol";

	public static final String orders = "orders";
	public static final String orderlist = "orderlist";
	public static final String orderreturn = "orderreturn";
	public static final String customers = "customers";

	public static final String integration = "integration";

}