package com.landingpage;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Insecured;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.v1.common.pojos.AwaitingSeller;

@Path("")
public class LandingpageResources {

	private LandingpageServices service;

	public LandingpageResources() {
		service = new LandingpageServices();
	}

	@POST
	@Insecured
	@Path("contactus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass contactUs(AwaitingSeller awaitingSeller) {
		try {
			return service.contactVertAdmin(awaitingSeller);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Insecured
	@Path("checkfield")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Boolean> checkFieldStore(String data) {
		Map<String, Boolean> map = new HashMap<>();
		boolean valid = false;
		try {
			JSONObject json = new JSONObject(data);
			if (!json.isNull("storeName")) {
				valid = service.checkStoreName(json.getString("storeName"));
			} else if (!json.isNull("email")) {
				valid = service.checkEmail(json.getString("email"));
			}
			map.put("validData", valid);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("validData", false);
		}
		return map;
	}
}
