package com.landingpage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import com.vossa.api.general.constants.Table;
import com.vossa.api.others.DBConnection;

public class TablecountServices {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;
		return connection;
	}

	public Map<String, Integer> getAllTableCount() throws Exception {

		PreparedStatement ps;
		ResultSet rs;
		Map<String, Integer> tableDetails = new HashMap<>();
		try (Connection connection = getDBConnection()) {

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.vertadmin);
			rs = ps.executeQuery();
			rs.next();
			int vertadmin = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.users);
			rs = ps.executeQuery();
			rs.next();
			int users = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.stores);
			rs = ps.executeQuery();
			rs.next();
			int stores = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.usertype);
			rs = ps.executeQuery();
			rs.next();
			int usertype = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.storeusermapper);
			rs = ps.executeQuery();
			rs.next();
			int storeusermapper = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.sellingmode);
			rs = ps.executeQuery();
			rs.next();
			int sellingmode = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.userpasswordjti);
			rs = ps.executeQuery();
			rs.next();
			int forgotpassword = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.fieldmaster);
			rs = ps.executeQuery();
			rs.next();
			int fieldmaster = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.productmapper);
			rs = ps.executeQuery();
			rs.next();
			int productmapper = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.productvaluemaster);
			rs = ps.executeQuery();
			rs.next();
			int productvaluemaster = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.productinventory);
			rs = ps.executeQuery();
			rs.next();
			int productinventory = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.productmanageprice);
			rs = ps.executeQuery();
			rs.next();
			int productmanageprice = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.selectedfields);
			rs = ps.executeQuery();
			rs.next();
			int selectedfields = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.contactvert);
			rs = ps.executeQuery();
			rs.next();
			int contactvert = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.asn);
			rs = ps.executeQuery();
			rs.next();
			int asn = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.asnpalletlevel);
			rs = ps.executeQuery();
			rs.next();
			int asnpalletlevel = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.asncaselevel);
			rs = ps.executeQuery();
			rs.next();
			int asncaselevel = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.asnskulevel);
			rs = ps.executeQuery();
			rs.next();
			int asnskulevel = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.features);
			rs = ps.executeQuery();
			rs.next();
			int features = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.subfeatures);
			rs = ps.executeQuery();
			rs.next();
			int subfeatures = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.roles);
			rs = ps.executeQuery();
			rs.next();
			int roles = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.selectedvalues);
			rs = ps.executeQuery();
			rs.next();
			int selectedvalues = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.selectedsubvalues);
			rs = ps.executeQuery();
			rs.next();
			int selectedsubvalues = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.assignednames);
			rs = ps.executeQuery();
			rs.next();
			int assignednames = rs.getInt("count");
			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.apilist);
			rs = ps.executeQuery();
			rs.next();
			int apilist = rs.getInt("count");
			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.apicontrol);
			rs = ps.executeQuery();
			rs.next();
			int apicontrol = rs.getInt("count");
			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.orders);
			rs = ps.executeQuery();
			rs.next();
			int orders = rs.getInt("count");
			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.orderlist);
			rs = ps.executeQuery();
			rs.next();
			int orderlist = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.customers);
			rs = ps.executeQuery();
			rs.next();
			int customers = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.orderreturn);
			rs = ps.executeQuery();
			rs.next();
			int orderreturn = rs.getInt("count");

			ps = connection.prepareStatement("SELECT count(*) AS count FROM " + Table.integration);
			rs = ps.executeQuery();
			rs.next();
			int integration = rs.getInt("count");

			tableDetails.put("vertadmin", vertadmin);
			tableDetails.put("usertype", usertype);
			tableDetails.put("stores", stores);
			tableDetails.put("users", users);
			tableDetails.put("sellingmode", sellingmode);
			tableDetails.put("forgotpassword", forgotpassword);
			tableDetails.put("storeusermapper", storeusermapper);
			tableDetails.put("fieldmaster", fieldmaster);
			tableDetails.put("productmapper", productmapper);
			tableDetails.put("productvaluemaster", productvaluemaster);
			tableDetails.put("productinventory", productinventory);
			tableDetails.put("productmanageprice", productmanageprice);
			tableDetails.put("selectedfields", selectedfields);
			tableDetails.put("contactvert", contactvert);
			tableDetails.put("asn", asn);
			tableDetails.put("asnpalletlevel", asnpalletlevel);
			tableDetails.put("asncaselevel", asncaselevel);
			tableDetails.put("asnskulevel", asnskulevel);
			tableDetails.put("features", features);
			tableDetails.put("subfeatures", subfeatures);
			tableDetails.put("roles", roles);
			tableDetails.put("selectedvalues", selectedvalues);
			tableDetails.put("selectedsubvalues", selectedsubvalues);
			tableDetails.put("assignednames", assignednames);
			tableDetails.put("apilist", apilist);
			tableDetails.put("apicontrol", apicontrol);
			tableDetails.put("orders", orders);
			tableDetails.put("orderlist", orderlist);
			tableDetails.put("orderreturn", orderreturn);
			tableDetails.put("customers", customers);
			tableDetails.put("integration", integration);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return tableDetails;

	}

}
