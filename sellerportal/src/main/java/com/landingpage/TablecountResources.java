package com.landingpage;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.vossa.api.general.annotations.filterannotations.Insecured;
import com.vossa.api.others.ResponseClass;

public class TablecountResources {

	private TablecountServices service;

	public TablecountResources() {
		service = new TablecountServices();
	}

	@GET
	@Insecured
	@Path("getalltablecount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllTableCount(@Context ContainerRequestContext context) {
		try {
			return service.getAllTableCount();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

}
