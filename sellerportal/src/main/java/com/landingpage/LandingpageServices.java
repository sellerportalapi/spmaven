package com.landingpage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import com.vossa.api.general.constants.Table;
import com.vossa.api.others.DBConnection;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.mailservices.SellerMailService;
import com.vossa.api.v1.common.pojos.AwaitingSeller;

public class LandingpageServices {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;
		return connection;
	}

//	protected Connection getDBConnection() throws Exception
//	{
//		return ServerInitializer.getDataSource().getConnection();
//	}

	public ResponseClass contactVertAdmin(AwaitingSeller awaitingSeller) {
		PreparedStatement ps;
		try (Connection connection = getDBConnection()) {
			ps = connection.prepareStatement("INSERT INTO " + Table.contactvert
					+ " ( companyName, name, email, phoneNumber, revenue, maximumProductSku, "
					+ "sellingMode, modeOfContact, preference, timeOfContact) "
					+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, awaitingSeller.getCompanyName());
			ps.setString(2, awaitingSeller.getName());
			ps.setString(3, awaitingSeller.getEmail());
			ps.setString(4, awaitingSeller.getPhoneNumber());
			ps.setString(5, awaitingSeller.getRevenue());
			ps.setInt(6, awaitingSeller.getMaximumProductSku());
			ps.setString(7, awaitingSeller.getSellingMode());
			ps.setString(8, awaitingSeller.getModeOfContact());
			ps.setString(9, awaitingSeller.getPreference());
			ps.setString(10, awaitingSeller.getTimeOfContact());
			ps.execute();
			ps.close();

			SellerMailService ms = new SellerMailService();
			String receiver = awaitingSeller.getEmail();
			String _name = awaitingSeller.getName();
			String subject = "Welcome to Project Verte";
			ResponseClass rc1 = ms.sendmailContactUs(receiver, subject, _name);
			receiver = "gmuthiah@projectverte.com";
			subject = "New customer(Seller) wants to register his store. Details below...";
			Map<String, String> map = new HashMap<String, String>();
			map.put("-sellerName-", awaitingSeller.getName());
			map.put("-storeName-", awaitingSeller.getCompanyName());
			map.put("-email-", awaitingSeller.getEmail());
			map.put("-phoneNumber-", awaitingSeller.getPhoneNumber());
			map.put("-revenue-", awaitingSeller.getRevenue());
			map.put("-maxSKU-", Integer.toString(awaitingSeller.getMaximumProductSku()));
			map.put("-csm-", awaitingSeller.getSellingMode());
			map.put("-pmc-", awaitingSeller.getModeOfContact());
			map.put("-ii-", awaitingSeller.getPreference());
			map.put("-ptc-", awaitingSeller.getTimeOfContact());
			ResponseClass rc = ms.sendmailSamTeam(receiver, subject, map);
			if (rc1.getCode() == 201 && rc.getCode() == 201)
				return new ResponseClass(201, "success", "Your request is accepted, we will contact you soon...");

			return new ResponseClass(404, "fail", "Invalid Email id", "Please check your mail id");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Your request is denied");
		}
	}

	public boolean checkStoreName(String _storename) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		int count = 0;
		try (Connection connection = getDBConnection()) {
			ps = connection.prepareStatement("SELECT COUNT(*) as cnt FROM " + Table.stores + " WHERE storeName = ?");
			ps.setString(1, _storename);
			rs = ps.executeQuery();
			rs.next();
			count = rs.getInt("cnt");

			rs.close();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return (count == 0) ? true : false;
	}

	public boolean checkEmail(String _email) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		int count = 0;
		try (Connection connection = getDBConnection()) {
			ps = connection.prepareStatement("SELECT COUNT(*) as cnt FROM " + Table.vertadmin + " WHERE email = ?");
			ps.setString(1, _email);
			rs = ps.executeQuery();
			rs.next();
			count = rs.getInt("cnt");

			ps = connection.prepareStatement("SELECT COUNT(*) as cnt FROM " + Table.users + " WHERE email = ?");
			ps.setString(1, _email);
			rs = ps.executeQuery();
			rs.next();
			count += rs.getInt("cnt");

			rs.close();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return (count == 0) ? true : false;
	}

	public String constructEmailText(AwaitingSeller contactus) {
		String text = "";

		text += "\nCompany name : " + contactus.getCompanyName();
		text += "\nRepresentator name : " + contactus.getName();
		text += "\nRepresentator email : " + contactus.getEmail();
		text += "\nRepresentator phonenumber : " + contactus.getPhoneNumber();
		if (contactus.getRevenue() != null)
			text += "\nProjected revenue this year : " + contactus.getRevenue();
		if (contactus.getMaximumProductSku() != 0)
			text += "\nMax product SKUs planned to maintain : " + contactus.getMaximumProductSku();
		if (contactus.getSellingMode() != null)
			text += "\nSelling mode : " + contactus.getSellingMode();
		if (contactus.getPreference() != null)
			text += "\nPreference : " + contactus.getPreference();
		if (contactus.getTimeOfContact() != null)
			text += "\nContact time : " + contactus.getTimeOfContact();
		if (contactus.getModeOfContact() != null)
			text += "\nContact method : " + contactus.getModeOfContact();

		return text;
	}
}
