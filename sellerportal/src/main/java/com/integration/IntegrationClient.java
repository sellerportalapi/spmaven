package com.integration;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.client.Invocation.Builder;

import com.vossa.api.others.ResponseClass;
import com.vossa.api.v1.seller.pojos.ASN;
import com.vossa.api.v1.seller.pojos.Seller;

public class IntegrationClient {

	private static final String X_API_KEY_VALUE = "paC8cL7rkT34VjRPARecXVM5gq7SN4M6ySDnYGfa";
	private static final String X_API_KEY_HEADER = "x-api-key";

	public void sendASN(ASN asn) throws Exception {

		Client client = ClientBuilder.newBuilder().build();
		WebTarget target = client.target("https://api.vossa.com/v1-uat/asn");
		Builder builder = target.request(MediaType.APPLICATION_JSON).header(X_API_KEY_HEADER, X_API_KEY_VALUE);

		ResponseClass res = builder.post(Entity.entity(asn, MediaType.APPLICATION_JSON), ResponseClass.class);

		if (!res.getStatus().equalsIgnoreCase("success")) {
			throw new Exception(res.getMessage());
		}
	}

	public void sendSellerMaster(Seller seller) throws Exception {

		Client client = ClientBuilder.newBuilder().build();
		WebTarget target = client.target("https://api.vossa.com/v1-uat/sellermaster");
		Builder builder = target.request(MediaType.APPLICATION_JSON).header(X_API_KEY_HEADER, X_API_KEY_VALUE);

		ResponseClass res = builder.post(Entity.entity(seller, MediaType.APPLICATION_JSON), ResponseClass.class);

		if (!res.getStatus().equalsIgnoreCase("success")) {
			throw new Exception(res.getMessage());
		}
	}
}
