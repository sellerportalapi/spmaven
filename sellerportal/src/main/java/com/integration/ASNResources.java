package com.integration;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Insecured;
import com.vossa.api.others.ResponseClass;

@Path("")
public class ASNResources {

	private ASNServices service;

	public ASNResources() {
		service = new ASNServices();
	}

	@PUT
	@Insecured
	@Path("updateasnstatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateASNStatus(String data) {
		try {
			JSONObject json = new JSONObject(data);
			String asnNumber = json.getString("asnNumber");
			String asnStatus = json.getString("asnStatus");

			return service.updateASNStatus(asnNumber, asnStatus);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
}
