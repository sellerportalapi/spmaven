package com.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.vossa.api.general.constants.Table;
import com.vossa.api.others.DBConnection;
import com.vossa.api.server.DataSourceFactory;
import com.vossa.api.v1.integration.pojos.Integration;

public class IntegrationServices {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;
		return connection;
	}

	protected Connection getDBConnectionPool() throws Exception {
		return DataSourceFactory.getInstance().getConnection();
	}
	
//	protected Connection getDBConnection() throws Exception
//	{
//		return ServerInitializer.getDataSource().getConnection();
//	}

	public Response createStore(Integration integration) {
		try (Connection connection = getDBConnection()) {

			PreparedStatement ps = connection
					.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.integration + " WHERE orderPrefix = ?");
			ps.setString(1, integration.getOrderPrefix());
			ResultSet rs = ps.executeQuery();
			rs.next();
			int cnt = rs.getInt("cnt");
			if (cnt != 0) {
				Map<String, Object> responseMap = new HashMap<>();
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "orderPrefix already exists");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
			ps.close();
			rs.close();
			
			PreparedStatement ps1 = connection.prepareStatement("INSERT INTO " + Table.integration
					+ "(sellerId, sellerName, storeName, storeId,"
					+ "orderPrefix, source, sourceFlag,host, port, hostUserName, hostPassword, caRefreshtoken,caAuthorization,sftpHost,"
					+ "sftpUserName, sftpPassword, sftpFolderPath, sftpKeyPath, apiKey, orderPickFrequency, imagePath, flag1, flag2,"
					+ "additionalParameter1, additionalParameter2, additionalParameter3, priority)"
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			ps1.setString(1, integration.getSellerId());
			ps1.setString(2, integration.getSellerName());
			ps1.setString(3, integration.getStoreName());
			ps1.setString(4, integration.getStoreId());
			ps1.setString(5, integration.getOrderPrefix());
			ps1.setString(6, integration.getSource());
			ps1.setString(7, integration.getSourceFlag());
			ps1.setString(8, integration.getHost());
			ps1.setInt(9, integration.getPort());
			ps1.setString(10, integration.getHostUserName());
			ps1.setString(11, integration.getHostPassword());
			ps1.setString(12, integration.getCaRefreshtoken());
			ps1.setString(13, integration.getCaAuthorization());
			ps1.setString(14, integration.getSftpHost());
			ps1.setString(15, integration.getSftpUserName());
			ps1.setString(16, integration.getSftpPassword());
			ps1.setString(17, integration.getSftpFolderPath());
			ps1.setString(18, integration.getSftpKeyPath());
			ps1.setString(19, integration.getApiKey());
			ps1.setInt(20, integration.getOrderPickFrequency());
			ps1.setString(21, integration.getImagePath());
			ps1.setString(22, integration.getFlag1());
			ps1.setString(23, integration.getFlag2());
			ps1.setString(24, integration.getAdditionalParameter1());
			ps1.setString(25, integration.getAdditionalParameter2());
			ps1.setString(26, integration.getAdditionalParameter3());
			ps1.setInt(27, integration.getPriority());
			ps1.execute();
			ps1.close();
			System.out.println("Record Created");

			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 201);
			responseMap.put("status", "success");
			responseMap.put("message", "Record Created");
			return Response.created(null).entity(responseMap).build();
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 500);
			responseMap.put("status", "error");
			responseMap.put("message", e.getMessage());
			return Response.serverError().entity(responseMap).build();
		}
	}

	public Response updateStore(Integration integration, String orderPrefix) {
		try (Connection connection = getDBConnection()) {

			PreparedStatement ps = connection
					.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.integration + " WHERE orderPrefix = ?");
			ps.setString(1, orderPrefix);
			ResultSet  rs = ps.executeQuery();
			rs.next();
			int cnt = rs.getInt("cnt");
			if (cnt == 0) {
				Map<String, Object> responseMap = new HashMap<>();
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "Invalid orderPrefix");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
			ps.close();
			rs.close();
			PreparedStatement ps1 = connection.prepareStatement("UPDATE " + Table.integration
					+ " SET sellerId = ?, sellerName = ?, storeName = ?, storeId = ?, source = ?, sourceFlag = ?,"
					+ " host = ?, port = ?, hostUserName = ?, hostPassword = ?, caRefreshtoken = ?, caAuthorization = ?,"
					+ " sftpHost = ?, sftpUserName = ?, sftpPassword = ?, sftpFolderPath = ?, sftpKeyPath = ?, apiKey = ?, orderPickFrequency = ?,"
					+ " imagePath = ?, flag1 = ?, flag2 = ?,additionalParameter1 = ?, additionalParameter2 = ?, additionalParameter3 = ?, priority = ?"
					+ " WHERE orderPrefix = ?");
			ps1.setString(1, integration.getSellerId());
			ps1.setString(2, integration.getSellerName());
			ps1.setString(3, integration.getStoreName());
			ps1.setString(4, integration.getStoreId());
			ps1.setString(5, integration.getSource());
			ps1.setString(6, integration.getSourceFlag());
			ps1.setString(7, integration.getHost());
			ps1.setInt(8, integration.getPort());
			ps1.setString(9, integration.getHostUserName());
			ps1.setString(10, integration.getHostPassword());
			ps1.setString(11, integration.getCaRefreshtoken());
			ps1.setString(12, integration.getCaAuthorization());
			ps1.setString(13, integration.getSftpHost());
			ps1.setString(14, integration.getSftpUserName());
			ps1.setString(15, integration.getSftpPassword());
			ps1.setString(16, integration.getSftpFolderPath());
			ps1.setString(17, integration.getSftpKeyPath());
			ps1.setString(18, integration.getApiKey());
			ps1.setInt(19, integration.getOrderPickFrequency());
			ps1.setString(20, integration.getImagePath());
			ps1.setString(21, integration.getFlag1());
			ps1.setString(22, integration.getFlag2());
			ps1.setString(23, integration.getAdditionalParameter1());
			ps1.setString(24, integration.getAdditionalParameter2());
			ps1.setString(25, integration.getAdditionalParameter3());
			ps1.setInt(26, integration.getPriority());
			ps1.setString(27, orderPrefix);
			ps1.execute();
			ps1.close();
			
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 201);
			responseMap.put("status", "success");
			responseMap.put("message", "Details Updated");
			return Response.created(null).entity(responseMap).build();
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 500);
			responseMap.put("status", "error");
			responseMap.put("message", e.getMessage());
			return Response.serverError().entity(responseMap).build();
		}
	}

	public List<String> allDetails(ResultSet rs) throws Exception {
		try {
			List<String> details = new ArrayList<String>();
			JSONObject json = null;
			while (rs.next()) {
				json = new JSONObject();
				json.put("sellerId", rs.getString("sellerId"));
				json.put("sellerName", rs.getString("sellerName"));
				json.put("storeName", rs.getString("storeName"));
				json.put("storeId", rs.getString("storeId"));
				json.put("orderPrefix", rs.getString("orderPrefix"));
				json.put("source", rs.getString("source"));
				json.put("sourceFlag", rs.getString("sourceFlag"));
				json.put("host", rs.getString("host"));
				json.put("port", rs.getInt("port"));
				json.put("hostUserName", rs.getString("hostUserName"));
				json.put("hostPassword", rs.getString("hostPassword"));
				json.put("caRefreshtoken", rs.getString("caRefreshtoken"));
				json.put("caAuthorization", rs.getString("caAuthorization"));
				json.put("sftpHost", rs.getString("sftpHost"));
				json.put("sftpUserName", rs.getString("sftpUserName"));
				json.put("sftpPassword", rs.getString("sftpPassword"));
				json.put("sftpFolderPath", rs.getString("sftpFolderPath"));
				json.put("sftpPassword", rs.getString("sftpPassword"));
				json.put("sftpFolderPath", rs.getString("sftpFolderPath"));
				json.put("sftpKeyPath", rs.getString("sftpKeyPath"));
				json.put("apiKey", rs.getString("apiKey"));
				json.put("orderPickFrequency", rs.getInt("orderPickFrequency"));
				json.put("imagePath", rs.getString("imagePath"));
				json.put("priority", rs.getInt("priority"));
				json.put("flag1", rs.getString("flag1"));
				json.put("flag2", rs.getString("flag2"));
				json.put("imagePath", rs.getString("imagePath"));
				json.put("additionalParameter1", rs.getString("additionalParameter1"));
				json.put("additionalParameter2", rs.getString("additionalParameter2"));
				json.put("additionalParameter3", rs.getString("additionalParameter3"));
				details.add(json.toString());
			}
			return details;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<String> getAll() throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps = connection.prepareStatement("SELECT * from " + Table.integration);
			try(ResultSet rs = ps.executeQuery())
			{
				return allDetails(rs);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<String> getSearchdynamic(JSONObject data) throws Exception {
		 try (Connection connection = getDBConnectionPool()) {
			StringBuffer query = new StringBuffer();

			String jsonKeys[] = JSONObject.getNames(data);
			query.append("SELECT * FROM integration");
			for (int j = 0; j < jsonKeys.length; j++) {
				if (j == 0)
					query.append(" WHERE ");
				else
					query.append(" AND ");

				query.append(jsonKeys[j] + " ='" + data.get(jsonKeys[j]) + "'");
			}

			PreparedStatement ps = connection.prepareStatement(query.toString());
			try(ResultSet rs = ps.executeQuery())
			{
				return allDetails(rs);
			}
		

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
