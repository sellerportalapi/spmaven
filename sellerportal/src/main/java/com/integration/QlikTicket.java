package com.integration;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.SellerCodeConversion;
import com.vossa.api.others.token.UserClaims;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

@Path("")
public class QlikTicket {

	static {
		// if certificate for create for an IP address you need to do this.
		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				if (hostname.equals("xx.xx.xx.xx"))
					return true;
				return false;
			}

		});
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getticket")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getTicket(@Context ContainerRequestContext context) {
		HttpsURLConnection request = null;

		InputStream inputStream = null;
		String string = null;
		String res = "";

		try {
			KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			String proxyCert = "/home/ubuntu/QlikClient.pfx";
			String proxyCertPass = "password12*";
			String rootCert = "/home/ubuntu/DVAQLKUSI01-CA.pfx";
			String rootCertPass = "password12*";

			FileInputStream instream = new FileInputStream(new File(proxyCert));
			keyStore.load(instream, proxyCertPass.toCharArray());
			instream.close();

			String Xrfkey = "0123456789abcdef";
			String targetURL = "https://DVAQLKUSI01:4243/qps/ramcotenant_proxy/ticket";

			URL url = new URL(targetURL + "?Xrfkey=" + Xrfkey);
			request = (HttpsURLConnection) url.openConnection();
			request.setRequestMethod("POST");
			request.setRequestProperty("Content-Type", "application/json");
			request.setRequestProperty("X-Qlik-Xrfkey", Xrfkey);
			request.setUseCaches(false);
			request.setDoInput(true);
			request.setDoOutput(true);

			String userId = "qlikuser";

			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();
			String sellerCode = getSellerCode(storeid);

			if (sellerCode.equals("J001") || sellerCode.equals("J002"))
//				userId = "vert/" + sellerCode;
				userId = sellerCode;

			String userDirectory = "VERT";
			String body = "{'UserDirectory':'" + userDirectory + "', 'UserId':'" + userId + "','Attributes': []}";
			byte[] bodyBytes = body.getBytes("UTF-8");

			KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
			keyManagerFactory.init(keyStore, proxyCertPass.toCharArray());

			KeyStore ksTrust = KeyStore.getInstance("JKS");
			ksTrust.load(new FileInputStream(rootCert), rootCertPass.toCharArray());
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(ksTrust);

			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(keyManagerFactory.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());

			SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
			request.setSSLSocketFactory(sslSocketFactory);
			request.setDoOutput(true);
			request.setDoInput(true);

			DataOutputStream out = new DataOutputStream(request.getOutputStream());
			out.write(bodyBytes);
			out.flush();
			out.close();

			inputStream = request.getErrorStream();
			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedreader = new BufferedReader(inputStreamReader);

				string = null;
				while ((string = bufferedreader.readLine()) != null) {
					System.out.println("Error Received " + string);
				}
				bufferedreader.close();
				inputStreamReader.close();
				inputStream.close();
			}

			inputStream = request.getInputStream();
			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedreader = new BufferedReader(inputStreamReader);

				string = null;
				while ((string = bufferedreader.readLine()) != null) {
					System.out.println("Received " + string);
					res = string;
				}
				bufferedreader.close();
				inputStreamReader.close();
				inputStream.close();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return Response.serverError().entity(ex.getMessage()).build();
		}
		return res.toString();
	}

	private static String getSellerCode(int storeid) throws Exception {
//		try (Connection connection = new DBConnection().getConnection()) {
//			PreparedStatement ps = connection
//					.prepareStatement("SELECT sellerCode FROM " + Table.stores + " WHERE storeId = ?");
//			ps.setInt(1, storeid);
//			ResultSet rs = ps.executeQuery();
//			rs.next();
//
//			String sellerCode = rs.getString("sellerCode");
//			ps.close();
//			rs.close();
//
//			return sellerCode;
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		}
		return SellerCodeConversion.convert(storeid);
	}
}
