package com.integration;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;

import com.vossa.api.general.constants.Table;
import com.vossa.api.others.DBConnection;
import com.vossa.api.v1.seller.pojos.Order;
import com.vossa.api.v1.seller.pojos.OrderCustomer;
import com.vossa.api.v1.seller.pojos.OrderList;
import com.vossa.api.v1.seller.pojos.OrderReturn;

public class OrderServices {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;
		return connection;
	}

//	protected Connection getDBConnection() throws Exception
//	{
//		return ServerInitializer.getDataSource().getConnection();
//	}

	public Response insertOrders(Order order, int storeId) {
		Map<String, Object> responseMap = new HashMap<>();
		try (Connection connection = getDBConnection()) {

			connection.setAutoCommit(false);
			connection.setSavepoint();

			PreparedStatement ps = connection
					.prepareStatement("SELECT orderId FROM " + Table.orders + " WHERE orderId = ? AND storeId = ?");
			ps.setString(1, order.getOrderId());
			ps.setInt(2, storeId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "OrderId already exists in the Store. Enter a new one.");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
			ps.close();
			rs.close();

			PreparedStatement ps1 = connection.prepareStatement("INSERT INTO " + Table.orders
					+ " (orderId, storeId, source, sourceCode, orderSubtotal, orderTax, orderShippingcharges, orderTotal, createdOn, lastStatusUpdate)"
					+ " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps1.setString(1, order.getOrderId());
			ps1.setInt(2, storeId);
			ps1.setString(3, order.getSource());
			ps1.setString(4, order.getSourceCode());
			ps1.setFloat(5, order.getOrderSubtotal());
			ps1.setFloat(6, order.getOrderTax());
			ps1.setFloat(7, order.getOrderShippingCharges());
			ps1.setFloat(8, order.getOrderTotal());
			ps1.setTimestamp(9, new Timestamp(order.getCreatedOn()));
			ps1.setTimestamp(10, new Timestamp(order.getCreatedOn()));
			ps1.execute();
			ps1.close();

			if (insertOrderList(order.getOrderList(), storeId, order.getOrderId(), connection))
				insertCustomersDetails(order.getCustomer(), storeId, order.getOrderId(), connection);
			else {
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "Record not  Created INVALID SKU");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}

			connection.commit();
			responseMap.put("code", 201);
			responseMap.put("status", "success");
			responseMap.put("message", "Order Created");
			return Response.created(null).entity(responseMap).build();
		} catch (SQLException e) {
			e.printStackTrace();
			responseMap.put("code", 400);
			responseMap.put("status", "failure");
			responseMap.put("message", "problem with JSON VALUE");
			return Response.status(Response.Status.BAD_REQUEST).entity(responseMap).build();

		} catch (Exception e1) {
			e1.printStackTrace();
			responseMap.put("code", 400);
			responseMap.put("status", "failure");
			responseMap.put("message", e1.toString());
			return Response.status(Response.Status.BAD_REQUEST).entity(responseMap).build();
		}
	}

	public Boolean insertOrderList(List<OrderList> orderlist, int storeId, String orderId, Connection connection)
			throws Exception {
		try {
			for (int i = 0; i < orderlist.size(); i++) {
				PreparedStatement ps = connection.prepareStatement(
						"SELECT product_id FROM " + Table.productmapper + " WHERE storeId = ? and sku = ? ");
				ps.setInt(1, storeId);
				ps.setString(2, orderlist.get(i).getSku());
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					PreparedStatement ps1 = connection.prepareStatement("INSERT INTO " + Table.orderlist
							+ " (orderId, storeId, product_id, quantity, price)" + " values(?, ?, ?, ?, ?)");
					ps1.setString(1, orderId);
					ps1.setInt(2, storeId);
					ps1.setInt(3, rs.getInt("product_id"));
					ps1.setInt(4, orderlist.get(i).getQuantity());
					ps1.setFloat(5, orderlist.get(i).getPrice());
					ps1.execute();
					ps1.close();
					ps.close();
				} else {
					connection.rollback();
					return false;
				}
				// throw new Exception("Invalid SKU");
				ps.close();
				rs.close();
				// ps.close();
			}
			return true;
		} catch (Exception e) {
			System.out.println("transaction failure");
			connection.rollback();
			e.printStackTrace();
			throw e;
		}
	}

	public void insertCustomersDetails(OrderCustomer customer, int storeId, String orderId, Connection connection)
			throws Exception {
		try {
			PreparedStatement ps = connection.prepareStatement("INSERT INTO " + Table.customers
					+ " (orderId, storeId, customerId, customerName, email, phoneNumber,"
					+ " address1, address2, address3, city, state, country, zipCode)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, orderId);
			ps.setInt(2, storeId);
			ps.setInt(3, customer.getCustomerId());
			ps.setString(4, customer.getCustomerName());
			ps.setString(5, customer.getEmail());
			ps.setString(6, customer.getPhoneNumber());
			ps.setString(7, customer.getAddress1());
			ps.setString(8, customer.getAddress2());
			ps.setString(9, customer.getAddress3());
			ps.setString(10, customer.getCity());
			ps.setString(11, customer.getState());
			ps.setString(12, customer.getCountry());
			ps.setString(13, customer.getZipCode());
			ps.execute();
			ps.close();
		} catch (Exception e) {
			connection.rollback();
			e.printStackTrace();
			throw e;
		}
	}

	public Response updateOrderStatus(Map<String, Object> map, int storeId) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);
			PreparedStatement ps = connection.prepareStatement("UPDATE " + Table.orders
					+ " SET orderStatus = ?, lastStatusUpdate = ?" + " WHERE orderId = ? AND storeId = ?");
			ps.setString(1, (String) map.get("orderStatus"));
			ps.setTimestamp(2, new Timestamp(((BigDecimal) map.get("lastStatusUpdate")).longValue()));
			ps.setString(3, (String) map.get("orderId"));
			ps.setInt(4, storeId);

			if (ps.executeUpdate() > 0) {
				connection.commit();
				ps.close();
				Map<String, Object> responseMap = new HashMap<>();
				responseMap.put("code", 201);
				responseMap.put("status", "success");
				responseMap.put("message", "Order Status Updated");
				return Response.created(null).entity(responseMap).build();
			} else {
				Map<String, Object> responseMap = new HashMap<>();
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "Invalid OrderId");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Response insertOrderReturn(OrderReturn orderReturn, int storeId) {
		Map<String, Object> responseMap = new HashMap<>();
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps = connection
					.prepareStatement("SELECT orderId FROM " + Table.orders + " WHERE orderId = ? AND storeId = ?");
			ps.setString(1, orderReturn.getOrderId());
			ps.setInt(2, storeId);
			ResultSet rs = ps.executeQuery();
			if (!rs.next()) {
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "OrderId doesn't exists in the Store.");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
			ps.close();
			rs.close();

			PreparedStatement ps1 = connection.prepareStatement(
					"SELECT product_id FROM " + Table.productmapper + " WHERE sku = ? AND storeId = ?");
			ps1.setString(1, orderReturn.getSku());
			ps1.setInt(2, storeId);
			ResultSet rs1 = ps1.executeQuery();
			if (!rs1.next()) {
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "SKU doesn't exists in the Store.");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
			int product_id = rs1.getInt("product_id");
			ps1.close();
			rs1.close();

			PreparedStatement ps2 = connection.prepareStatement("SELECT quantity FROM " + Table.orderlist
					+ " WHERE orderId = ? AND storeId = ? AND product_id = ?");
			ps2.setString(1, orderReturn.getOrderId());
			ps2.setInt(2, storeId);
			ps2.setInt(3, product_id);
			ResultSet rs2 = ps2.executeQuery();
			if (!rs2.next()) {
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "SKU doesn't exists in the given Order.");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}

			if (rs2.getInt("quantity") < orderReturn.getReturnedQuantity()) {
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "Returning quantity is more than the Ordered quantity.");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
			ps2.close();
			rs2.close();

			PreparedStatement ps3 = connection.prepareStatement("SELECT orderReturnId FROM " + Table.orderreturn
					+ " WHERE orderReturnId = ? AND orderId = ? AND storeId = ?");
			ps3.setString(1, orderReturn.getOrderReturnId());
			ps3.setString(2, orderReturn.getOrderId());
			ps3.setInt(3, storeId);
			ResultSet rs3 = ps3.executeQuery();
			if (rs3.next()) {
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "ReturnId + OrderId already exists in the Store. Enter a new one.");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
			ps3.close();
			rs3.close();

			PreparedStatement ps4 = connection.prepareStatement("INSERT INTO " + Table.orderreturn
					+ " (orderReturnId, orderId, storeId, product_id, returnedQuantity, returnStatus,"
					+ " refundAmount, returnRequestedOn, reasonForReturn, additionalComments)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps4.setString(1, orderReturn.getOrderReturnId());
			ps4.setString(2, orderReturn.getOrderId());
			ps4.setInt(3, storeId);
			ps4.setInt(4, product_id);
			ps4.setInt(5, orderReturn.getReturnedQuantity());
			ps4.setString(6, orderReturn.getReturnStatus());
			ps4.setFloat(7, orderReturn.getRefundAmount());
			ps4.setTimestamp(8, new Timestamp(orderReturn.getReturnRequestedOn()));
			ps4.setString(9, orderReturn.getReasonForReturn());
			ps4.setString(10, orderReturn.getAdditionalComments());
			ps4.execute();
			ps4.close();

			responseMap.put("code", 201);
			responseMap.put("status", "success");
			responseMap.put("message", "Order return Created");
			return Response.created(null).entity(responseMap).build();
		} catch (SQLException e) {
			e.printStackTrace();
			responseMap.put("code", 400);
			responseMap.put("status", "failure");
			responseMap.put("message", "problem with JSON VALUE");
			return Response.status(Response.Status.BAD_REQUEST).entity(responseMap).build();

		} catch (Exception e1) {
			e1.printStackTrace();
			responseMap.put("code", 400);
			responseMap.put("status", "failure");
			responseMap.put("message", e1.toString());
			return Response.status(Response.Status.BAD_REQUEST).entity(responseMap).build();
		}
	}

	public Response updateOrderReturnStatus(Map<String, Object> map, int storeId) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);
			PreparedStatement ps = connection.prepareStatement("UPDATE " + Table.orderreturn + " SET returnStatus = ?"
					+ " WHERE orderReturnId = ? AND orderId = ? AND storeId = ?");
			ps.setString(1, (String) map.get("returnStatus"));
			ps.setString(2, (String) map.get("orderReturnId"));
			ps.setString(3, (String) map.get("orderId"));
			ps.setInt(4, storeId);

			if (ps.executeUpdate() > 0) {
				connection.commit();
				ps.close();
				Map<String, Object> responseMap = new HashMap<>();
				responseMap.put("code", 201);
				responseMap.put("status", "success");
				responseMap.put("message", "OrderReturn Status Updated");
				return Response.created(null).entity(responseMap).build();
			} else {
				Map<String, Object> responseMap = new HashMap<>();
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "Invalid OrderId or ReturnId");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
