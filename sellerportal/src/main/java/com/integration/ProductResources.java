package com.integration;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.vossa.api.general.annotations.filterannotations.Insecured;

@Path("")
public class ProductResources {

	@PUT
	@Insecured
	@Path("updateproductinventory")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)

	public Response updateProductInventory(Map<String, Object> productInventory, @QueryParam("storeid")int storeid) {
		try {
			return new ProductServices().updateProductInventory(productInventory, storeid);
		}
		catch(Exception e) {
			e.printStackTrace();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 500);
			responseMap.put("status", "error");
			responseMap.put("message", e.getMessage());
			return Response.serverError().entity(responseMap).build();
		}
	}
}
