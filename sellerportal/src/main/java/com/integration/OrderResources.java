package com.integration;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.vossa.api.general.annotations.filterannotations.Insecured;
import com.vossa.api.v1.seller.pojos.Order;
import com.vossa.api.v1.seller.pojos.OrderReturn;

@Path("")
public class OrderResources {
	private OrderServices service;

	public OrderResources() {
		service = new OrderServices();
	}

	@POST
	@Insecured
	@Path("insertorders")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertOrders(Order order, @QueryParam("storeId") int storeId) {
		try {
			return service.insertOrders(order, storeId);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 500);
			responseMap.put("status", "error");
			responseMap.put("message", e.getMessage());
			return Response.serverError().entity(responseMap).build();
		}
	}

	@PUT
	@Insecured
	@Path("updateorderstatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateOrderStatus(Map<String, Object> map, @QueryParam("storeId") int storeId) {
		try {
			return service.updateOrderStatus(map, storeId);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 500);
			responseMap.put("status", "error");
			responseMap.put("message", e.getMessage());
			return Response.serverError().entity(responseMap).build();
		}
	}

	@POST
	@Insecured
	@Path("insertorderreturn")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertOrderReturn(OrderReturn orderReturn, @QueryParam("storeId") int storeId) {
		try {
			return service.insertOrderReturn(orderReturn, storeId);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 500);
			responseMap.put("status", "error");
			responseMap.put("message", e.getMessage());
			return Response.serverError().entity(responseMap).build();
		}

	}

	@PUT
	@Insecured
	@Path("updateorderreturnstatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateOrderReturnStatus(Map<String, Object> map, @QueryParam("storeId") int storeId) {
		try {
			return service.updateOrderReturnStatus(map, storeId);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 500);
			responseMap.put("status", "error");
			responseMap.put("message", e.getMessage());
			return Response.serverError().entity(responseMap).build();
		}
	}
}