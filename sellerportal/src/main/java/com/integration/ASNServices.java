package com.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.others.DBConnection;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.server.DataSourceFactory;

public class ASNServices {

	public ASNServices() {

	}

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;
		return connection;
	}

	protected Connection getDBConnectionPool() throws Exception {
		return DataSourceFactory.getInstance().getConnection();
	}

	@ApiMethod(apiname = "updateasnstatus", method = "PUT")
	public ResponseClass updateASNStatus(String asnNumber, String asnStatus) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps = connection
					.prepareStatement("SELECT asnId FROM " + Table.asn + " WHERE asnNumber = ? ");
			ps.setString(1, asnNumber);
			ResultSet rs = ps.executeQuery();

			if (!rs.next())
				throw new Exception("Invalid ASN Number");

			int asnId = rs.getInt("asnId");

			if (!asnStatus.equalsIgnoreCase("Approved") && !asnStatus.equalsIgnoreCase("Rejected")) {
				throw new Exception("Invalid ASN Status");
			}
			PreparedStatement ps1 = connection.prepareStatement("UPDATE " + Table.asn + " SET asnStatus = ? " + " WHERE asnId = ?");
			ps1.setString(1, asnStatus);
			ps1.setInt(2, asnId);
			ps1.executeUpdate();
			ps1.close();
			ps.close();
			rs.close();
			return new ResponseClass(200, "success", "ASN Status successfully updated");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
