package com.integration;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.Response;

import com.vossa.api.general.constants.Table;
import com.vossa.api.others.DBConnection;

public class ProductServices {

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;
		return connection;
	}
	
//	protected Connection getDBConnection() throws Exception
//	{
//		return ServerInitializer.getDataSource().getConnection();
//	}
	
	public Response updateProductInventory(Map<String, Object> productInventory, int storeid) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps = connection.prepareStatement("SELECT product_id FROM " + Table.productmapper
					+ " WHERE sku = ? AND storeId = ?");
			ps.setString(1, (String) productInventory.get("sku"));
			ps.setInt(2, storeid);
			ResultSet rs = ps.executeQuery();
			if(!rs.next()) {
				Map<String, Object> responseMap = new HashMap<>();
				responseMap.put("code", 200);
				responseMap.put("status", "failure");
				responseMap.put("message", "Invalid SKU or StoreId");
				return Response.status(Response.Status.OK).entity(responseMap).build();
			}
			int product_id =  rs.getInt("product_id");
			ps.close();
			rs.close();
			PreparedStatement ps1 = connection.prepareStatement("UPDATE " + Table.productinventory
					+ " SET availableToPromise = ?, fulfillableStock = ?, damagedStock = ?, reservedStock = ?"
					+ " WHERE product_id = ?");
			ps1.setInt(1, ((BigDecimal) productInventory.get("availableToPromise")).intValue());
			ps1.setInt(2, ((BigDecimal) productInventory.get("fulfillableStock")).intValue());
			ps1.setInt(3, ((BigDecimal) productInventory.get("damagedStock")).intValue());
			ps1.setInt(4, ((BigDecimal) productInventory.get("reservedStock")).intValue());
			ps1.setInt(5, product_id);
			ps1.executeUpdate();
			ps.close();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 201);
			responseMap.put("status", "success");
			responseMap.put("message", "Product Inventory Updated");
			return Response.created(null).entity(responseMap).build();
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
