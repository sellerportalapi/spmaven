package com.integration;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Insecured;
import com.vossa.api.v1.integration.pojos.Integration;

@Path("")
public class IntegrationResources {

	private IntegrationServices service;

	public IntegrationResources() {
		service = new IntegrationServices();
	}

	@POST
	@Insecured
	@Path("create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createStore(Integration in) {
		return service.createStore(in);
	}

	@PUT
	@Insecured
	@Path("update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateStore(Integration in, @QueryParam("orderPrefix") String orderPrefix) {
		return service.updateStore(in, orderPrefix);
	}
	
	@GET
	@Insecured
	@Path("getall")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllSellers() {
		try {
			return service.getAll().toString();
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 500);
			responseMap.put("status", "error");
			responseMap.put("message", e.getMessage());
			return Response.serverError().entity(responseMap).build();
		}
	}

	@POST
	@Insecured
	@Path("get")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSearchdynamic(String data) {
		try {

			JSONObject jsonObject = new JSONObject(data);
			return service.getSearchdynamic(jsonObject).toString();
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("code", 500);
			responseMap.put("status", "error");
			responseMap.put("message", e.getMessage());
			return Response.serverError().entity(responseMap).build();
		}
	}
}