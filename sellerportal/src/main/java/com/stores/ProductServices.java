package com.stores;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.SellerCodeConversion;
import com.vossa.api.others.filehandlers.XLFileParser;
import com.vossa.api.others.ftphandlers.SFTPHandler;

public class ProductServices extends StoreServices {

	@ApiMethod(apiname = "getallproducts", method = "GET")
	public List<Map<String, Object>> getAllProducts(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>();

			ps = connection.prepareStatement("SELECT * FROM " + Table.productmapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			while (rs.next()) {
				Map<String, Object> product = getProduct(rs.getInt("product_id"));
				productList.add(product);
			}

			ps.close();
			rs.close();

			Collections.sort(productList, new SortByTime("updated_at"));
			int i = 1;
			for (Map<String, Object> product : productList)
				product.put("list_id", i++);

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Map<String, Object>> getAllProductsEmpty(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>();

			ps = connection.prepareStatement("SELECT * FROM " + Table.productmapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			while (rs.next()) {
				Map<String, Object> product = getProductEmpty(rs.getInt("product_id"));
				productList.add(product);
			}

			ps.close();
			rs.close();

			Collections.sort(productList, new SortByTime("updated_at"));
			int i = 1;
			for (Map<String, Object> product : productList)
				product.put("list_id", i++);

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpageproducts", method = "POST")
	public List<Map<String, Object>> getPageProducts(int storeid, int startIndex, int load) throws Exception {
		try {
			List<Map<String, Object>> allProductList = getAllProducts(storeid);
			List<Map<String, Object>> productList = new ArrayList<>();

			for (Map<String, Object> product : allProductList) {
				int list_id = (int) product.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					productList.add(product);
			}
			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpageproductsdownload", method = "GET")
	public List<Map<String, Object>> getPageProductsDownload(int storeid, int startIndex, int load) throws Exception {
		try {
			List<Map<String, Object>> allProductList = getAllProductsEmpty(storeid);
			List<Map<String, Object>> productList = new ArrayList<>();

			for (Map<String, Object> product : allProductList) {
				int list_id = (int) product.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					productList.add(product);
			}
			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getselectedproductsdownload", method = "GET")
	public List<HashMap<String, Object>> getSelectedProductsDownload(HashMap<String, Object> selectedProducts,
			int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {

			@SuppressWarnings("unchecked")
			ArrayList<BigDecimal> product_ids = (ArrayList<BigDecimal>) selectedProducts.get("selectedProducts");

			List<HashMap<String, Object>> productList = new ArrayList<HashMap<String, Object>>();
			for (BigDecimal product_id : product_ids) {
				HashMap<String, Object> product = (HashMap<String, Object>) getProductEmpty(product_id.intValue());
				productList.add(product);
			}

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getproductcount", method = "GET")
	public int getProductCount(int storeid) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnectionPool()) {
			ps = connection
					.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.productmapper + " WHERE storeID = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			rs.next();
			int count = rs.getInt("cnt");

			ps.close();
			rs.close();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getsearchproducts", method = "GET")
	public List<Map<String, Object>> getSearchProducts(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT field_id FROM " + Table.fieldmaster + " WHERE issearchable = 1");
			rs = ps.executeQuery();
			String queryString = "";
			rs.next();
			queryString = "field_id = " + rs.getInt("field_id");

			while (rs.next())
				queryString = queryString + " OR " + "field_id = " + rs.getInt("field_id");

			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT product_id FROM " + Table.productmapper + " WHERE storeId = ?");
			ps1.setInt(1, storeid);
			rs1 = ps1.executeQuery();

			List<Map<String, Object>> productList = new ArrayList<>();
			while (rs1.next()) {
				int product_id = rs1.getInt("product_id");

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection.prepareStatement("SELECT DISTINCT(product_id) FROM " + Table.productvaluemaster
						+ " WHERE value LIKE '%" + searchText + "%' AND product_id = ? AND (" + queryString + ")");
				ps2.setInt(1, product_id);
				rs2 = ps2.executeQuery();

				if (rs2.next()) {
					productList.add(getProduct(product_id));
				}
				ps2.close();
				rs2.close();
			}

			ps1.close();
			rs1.close();

			Collections.sort(productList, new SortByTime("updated_at"));
			int i = 1;
			for (Map<String, Object> product : productList)
				product.put("list_id", i++);

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Map<String, Object>> getSearchProductsEmpty(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT field_id FROM " + Table.fieldmaster + " WHERE issearchable = 1");
			rs = ps.executeQuery();
			String queryString = "";
			rs.next();
			queryString = "field_id = " + rs.getInt("field_id");

			while (rs.next())
				queryString = queryString + " OR " + "field_id = " + rs.getInt("field_id");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT product_id FROM " + Table.productmapper + " WHERE storeId = ?");
			ps1.setInt(1, storeid);
			rs1 = ps1.executeQuery();

			List<Map<String, Object>> productList = new ArrayList<>();
			while (rs1.next()) {
				int product_id = rs1.getInt("product_id");

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection.prepareStatement("SELECT DISTINCT(product_id) FROM " + Table.productvaluemaster
						+ " WHERE value LIKE '%" + searchText + "%' AND product_id = ? AND (" + queryString + ")");
				ps2.setInt(1, product_id);
				rs2 = ps2.executeQuery();

				if (rs2.next()) {
					productList.add(getProductEmpty(product_id));
				}

				ps2.close();
				rs2.close();
			}
			ps1.close();
			rs1.close();

			Collections.sort(productList, new SortByTime("updated_at"));
			int i = 1;
			for (Map<String, Object> product : productList)
				product.put("list_id", i++);

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpagesearchproducts", method = "POST")
	public List<Map<String, Object>> getPageSearchProducts(String searchText, int storeid, int startIndex, int load)
			throws Exception {
		try {
			List<Map<String, Object>> allProductList = getSearchProducts(searchText, storeid);
			List<Map<String, Object>> productList = new ArrayList<>();

			for (Map<String, Object> product : allProductList) {
				int list_id = (int) product.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					productList.add(product);
			}
			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpagesearchproductsdownload", method = "POST")
	public List<Map<String, Object>> getPageSearchProductsDownload(String searchText, int storeid, int startIndex,
			int load) throws Exception {
		try {
			List<Map<String, Object>> allProductList = getSearchProductsEmpty(searchText, storeid);
			List<Map<String, Object>> productList = new ArrayList<>();

			for (Map<String, Object> product : allProductList) {
				int list_id = (int) product.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					productList.add(product);
			}
			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getsearchproductcount", method = "POST")
	public int getSearchProductCount(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT field_id FROM " + Table.fieldmaster + " WHERE issearchable = 1");
			rs = ps.executeQuery();
			String queryString = "";
			rs.next();
			queryString = "field_id = " + rs.getInt("field_id");

			while (rs.next())
				queryString = queryString + " OR " + "field_id = " + rs.getInt("field_id");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT product_id FROM " + Table.productmapper + " WHERE storeId = ?");
			ps1.setInt(1, storeid);
			rs1 = ps1.executeQuery();

			int count = 0;
			while (rs1.next()) {
				int product_id = rs1.getInt("product_id");

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection.prepareStatement("SELECT DISTINCT(product_id) FROM " + Table.productvaluemaster
						+ " WHERE value LIKE '%" + searchText + "%' AND product_id = ? AND (" + queryString + ")");
				ps2.setInt(1, product_id);
				rs2 = ps2.executeQuery();

				if (rs2.next()) {
					count++;
				}
				ps2.close();
				rs2.close();
			}
			ps1.close();
			rs1.close();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "addsingleproduct", method = "POST")
	public ResponseClass addSingleProduct(JSONObject jsonObject, int storeid) {
		try {
			addProduct(jsonObject, storeid);

			// Products are uploaded in Shared Folder given by integration through SFTP.
			try {
				List<JSONObject> productList = new ArrayList<>();
				productList.add(jsonObject);

				productList = alterProductJSONforIntegration(productList, storeid);
				SFTPHandler handler = new SFTPHandler();
				handler.putProductList(productList);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return new ResponseClass(201, "success", "Product added or updated");

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not added");
		}
	}

	@ApiMethod(apiname = "addbulkproducts", method = "POST")
	public ResponseClass addBulkProducts(String path, int storeid) throws JSONException {
		try {
			XLFileParser fileParser = new XLFileParser();
			List<JSONObject> productList = fileParser.fileParserToJSON(path);

			StringBuffer error = new StringBuffer();
			if (productList.size() > 0) {
				for (int i = 0; i < productList.size(); i++) {
					StringBuffer rowError = isValidData(productList.get(i));
					if (!rowError.toString().equalsIgnoreCase("")) {
						error.append("<label style=float:left;><b>sku id <i>" + productList.get(i).get("sku")
								+ "</i></b></label><br><p style=float:left;text-align:left;>" + rowError + "</p><br>");
					}
				}
			} else {
				error.append("Please add details for one or more products in the file and upload again.");
			}

			if (!error.toString().equalsIgnoreCase(""))
				return new ResponseClass(500, "error", error.toString(), "not added");

			for (JSONObject product : productList) {
				addProduct(product, storeid);
			}

			// Products are uploaded in Shared Folder given by integration through SFTP.
			try {
				productList = alterProductJSONforIntegration(productList, storeid);
				SFTPHandler handler = new SFTPHandler();
				handler.putProductList(productList);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String msg;
			if (productList.size() == 1) {
				msg = productList.size() + " product added to the product catalog.";
			} else {
				msg = productList.size() + " products added to the product catalog.";
			}
			return new ResponseClass(201, "success", msg);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not added");
		}
	}

	@ApiMethod(apiname = "editbulkproducts", method = "POST")
	public ResponseClass editBulkProducts(String path, int storeid) throws JSONException {
		try {
			XLFileParser fileParser = new XLFileParser();
			List<JSONObject> productList = fileParser.fileParserToJSON(path);

			for (JSONObject product : productList) {
				try {
					product = addMandatoryFields(product, storeid);
				} catch (Exception e) {
					return new ResponseClass(400, "fail", "Unknown SKU : " + product.getString("sku"), "not added");
				}
			}

			StringBuffer error = new StringBuffer();
			if (productList.size() > 0) {
				for (int i = 0; i < productList.size(); i++) {
					StringBuffer rowError = isValidData(productList.get(i));
					if (!rowError.toString().equalsIgnoreCase("")) {
						error.append("<label style=float:left;><b>sku id <i>" + productList.get(i).get("sku")
								+ "</i></b></label><br><p style=float:left;text-align:left;>" + rowError + "</p><br>");
					}
				}
			} else {
				error.append("Please add details for one or more products in the file and upload again.");
			}

			if (!error.toString().equalsIgnoreCase(""))
				return new ResponseClass(500, "error", error.toString(), "not added");

			for (JSONObject product : productList)
				addProduct(product, storeid);

			// For now, bulk product upload is not integrated with RAMCO
			// Products are uploaded in Shared Folder given by integration through SFTP.
			// try {
			// productList = alterProductJSONforIntegration(productList, storeid);
			// SFTPHandler handler = new SFTPHandler();
			// handler.putProductList(productList);
			// } catch (Exception e) {
			// e.printStackTrace();
			// }

			String msg;
			if (productList.size() == 1) {
				msg = productList.size() + " product updated to the product catalog.";
			} else {
				msg = productList.size() + " products updated to the product catalog.";
			}
			return new ResponseClass(201, "success", msg);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not added");
		}
	}

	public void addProduct(JSONObject jsonObject, int storeid) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT COUNT(*) as cnt FROM " + Table.productmapper + " WHERE sku = ? AND storeId = ?");
			ps.setString(1, jsonObject.getString("sku"));
			ps.setInt(2, storeid);
			rs = ps.executeQuery();

			rs.next();
			if (rs.getInt("cnt") == 0) {
				createProduct(jsonObject, storeid);
			} else {
				updateProduct(jsonObject, storeid);
			}

			ps.close();
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<JSONObject> alterProductJSONforIntegration(List<JSONObject> productList, int storeid) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT name FROM " + Table.fieldmaster);
			rs = ps.executeQuery();

			List<String> field_nameList = new ArrayList<>();
			while (rs.next())
				field_nameList.add(rs.getString("name"));
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT storeName FROM " + Table.stores + " WHERE storeId = ?");
			ps1.setInt(1, storeid);
			rs1 = ps1.executeQuery();
			rs1.next();
			String sellerCode = SellerCodeConversion.convert(storeid);
			String sellerName = rs1.getString("storeName");

			for (JSONObject product : productList) {
				for (String field_name : field_nameList) {
					if (!product.has(field_name))
						product.put(field_name, JSONObject.NULL);
				}

				product.put("seller_code", sellerCode);
				product.put("seller_name", sellerName);
				product.put("description_of_sub_units", JSONObject.NULL);
				product.put("sub_units_qty", JSONObject.NULL);
			}

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public JSONObject addMandatoryFields(JSONObject product, int storeid) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement(
					"SELECT product_id FROM " + Table.productmapper + " WHERE sku = ? AND storeId = ?");
			ps.setString(1, product.getString("sku"));
			ps.setInt(2, storeid);
			rs = ps.executeQuery();

			if (!rs.next()) {
				ps.close();
				rs.close();
				throw new IllegalArgumentException("Enter a valid (already existing) sku.");
			}
			int product_id = rs.getInt("product_id");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT field_id FROM " + Table.fieldmaster + " WHERE ismandotry = ?");
			ps1.setBoolean(1, true);
			rs1 = ps1.executeQuery();

			while (rs1.next()) {
				int field_id = rs1.getInt("field_id");

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection.prepareStatement(
						"SELECT value FROM " + Table.productvaluemaster + " WHERE product_id = ? AND field_id = ?");
				ps2.setInt(1, product_id);
				ps2.setInt(2, field_id);
				rs2 = ps2.executeQuery();

				while (rs2.next()) {
					String value = rs2.getString("value");
					Map<String, Object> map = getFieldValue(field_id, value);

					String fieldName = getFieldNameById(field_id);
					if (!product.has(fieldName)) {
						product.put(fieldName, map.get(fieldName));
					}
				}
				ps2.close();
				rs2.close();
			}
			ps1.close();
			rs1.close();

			return product;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public StringBuffer isValidData(JSONObject jsonObject) {
		StringBuffer isValid = new StringBuffer();
		try {
			if (jsonObject.has("sku")) {
				if (jsonObject.getString("sku") == null || jsonObject.getString("sku").equalsIgnoreCase("")) {
					isValid.append("sku id should not be empty<br>");
				}
			} else {
				isValid.append("sku id field is missing<br>");
			}

			if (jsonObject.has("product_name")) {
				if (jsonObject.getString("product_name") == null
						|| jsonObject.getString("product_name").equalsIgnoreCase("")) {
					isValid.append("product_name should not be empty<br>");
				}
			} else {
				isValid.append("product name field is missing <br>");
			}

			if (jsonObject.has("description")) {
				if (jsonObject.getString("description") == null
						|| jsonObject.getString("description").equalsIgnoreCase("")) {
					isValid.append("description should not be empty<br>");
				}
			} else {
				isValid.append("description field is missing<br>");
			}

			if (jsonObject.has("main_image")) {
				if (jsonObject.getString("main_image") == null
						|| jsonObject.getString("main_image").equalsIgnoreCase("")) {
					isValid.append("main_image should not be empty<br>");
				}
			} else {
				isValid.append("main_image field is missing<br>");
			}

			if (jsonObject.has("weight")) {
				if ((jsonObject.get("weight") instanceof Double || jsonObject.get("weight") instanceof Float
						|| jsonObject.get("weight") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("weight").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'weight', weight should not be empty or negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'weight'<br>");
				}
			} else {
				isValid.append("weight field is missing<br>");
			}

			if (jsonObject.has("price_on_ecommerce")) {
				if ((jsonObject.get("price_on_ecommerce") instanceof Double
						|| jsonObject.get("price_on_ecommerce") instanceof Float
						|| jsonObject.get("price_on_ecommerce") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("price_on_ecommerce").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'price_on_ecommerce', price_on_ecommerce should not be empty or negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'price_on_ecommerce'<br>");
				}
			} else {
				isValid.append("price_on_ecommerce field is missing<br>");
			}

			if (!((jsonObject.has("UPC") && !jsonObject.isNull("UPC"))
					|| (jsonObject.has("MPN") && !jsonObject.isNull("MPN"))))
				isValid.append("UPC and MPN field is missing,Enter either one<br>");

			if (jsonObject.has("UPC") && !jsonObject.isNull("UPC")) {
				if (!(jsonObject.get("UPC") instanceof Integer)) {
					isValid.append("Please check the data for field 'UPC'<br>");
				}
			}

			if (jsonObject.has("MPN") && !jsonObject.isNull("MPN")) {
				if (!(jsonObject.get("MPN") instanceof Integer)) {
					isValid.append("Please check the data for field 'MPN'<br>");
				}
			}

			// negative values check
			if (jsonObject.has("product_length") && !jsonObject.isNull("product_length")) {
				if ((jsonObject.get("product_length") instanceof Double
						|| jsonObject.get("product_length") instanceof Float
						|| jsonObject.get("product_length") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("product_length").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'product_length',product_length should not be negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'product_length'<br>");
				}
			}

			if (jsonObject.has("product_width") && !jsonObject.isNull("product_width")) {
				if ((jsonObject.get("product_width") instanceof Double
						|| jsonObject.get("product_width") instanceof Float
						|| jsonObject.get("product_width") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("product_width").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'product_width', product_width should not be negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'product_width'<br>");
				}
			}

			if (jsonObject.has("product_height") && !jsonObject.isNull("product_height")) {
				if ((jsonObject.get("product_height") instanceof Double
						|| jsonObject.get("product_height") instanceof Float
						|| jsonObject.get("product_height") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("product_height").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'product_height' , product_height should not be negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'product_height'<br>");
				}
			}

			if (jsonObject.has("standard_price") && !jsonObject.isNull("standard_price")) {
				if ((jsonObject.get("standard_price") instanceof Double
						|| jsonObject.get("standard_price") instanceof Float
						|| jsonObject.get("standard_price") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("standard_price").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'standard_price' , standard_price should not be negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'standard_price'<br>");
				}
			}

			if (jsonObject.has("retail_price") && !jsonObject.isNull("retail_price")) {
				if ((jsonObject.get("retail_price") instanceof Double || jsonObject.get("retail_price") instanceof Float
						|| jsonObject.get("retail_price") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("retail_price").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'retail_price' ,retail_price should not be negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'retail_price'<br>");
				}
			}

			if (jsonObject.has("shipping_weight") && !jsonObject.isNull("shipping_weight")) {
				if ((jsonObject.get("shipping_weight") instanceof Double
						|| jsonObject.get("shipping_weight") instanceof Float
						|| jsonObject.get("shipping_weight") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("shipping_weight").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'shipping_weight' ,shipping_weight should not be negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'shippig_weight'<br>");
				}
			}

			if (jsonObject.has("package_length") && !jsonObject.isNull("package_length")) {
				if ((jsonObject.get("package_length") instanceof Double
						|| jsonObject.get("package_length") instanceof Float
						|| jsonObject.get("package_length") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("package_length").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'package_length' , package_length should not be negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'package_length'<br>");
				}
			}

			if (jsonObject.has("package_width") && !jsonObject.isNull("package_width")) {
				if ((jsonObject.get("package_width") instanceof Double
						|| jsonObject.get("package_width") instanceof Float
						|| jsonObject.get("package_width") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("package_width").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'package_width' , package_width should not be negative value;");
					}
				} else {
					isValid.append("Please check the data for field 'package_width'<br>");
				}
			}

			if (jsonObject.has("package_height") && !jsonObject.isNull("package_height")) {
				if ((jsonObject.get("package_height") instanceof Double
						|| jsonObject.get("package_height") instanceof Float
						|| jsonObject.get("package_height") instanceof Integer)) {
					if (Float.valueOf(jsonObject.get("package_height").toString()) <= 0.0) {
						isValid.append(
								"Please check the data for field 'package_height' , package_height should not be negative value<br>");
					}
				} else {
					isValid.append("Please check the data for field 'package_height'<br>");
				}
			}

			if (jsonObject.has("volume_UOM") && !jsonObject.isNull("volume_UOM")) {
				jsonObject.put("volume_UOM", jsonObject.getString("volume_UOM").toUpperCase());
				if (!jsonObject.getString("volume_UOM").equalsIgnoreCase("CU IN"))
					isValid.append("'volume_UOM' should be CU IN<br>");
			}
			if (jsonObject.has("weight_UOM") && !jsonObject.isNull("weight_UOM")) {
				jsonObject.put("weight_UOM", jsonObject.getString("weight_UOM").toUpperCase());
				if (!jsonObject.getString("weight_UOM").equalsIgnoreCase("POUND"))
					isValid.append("'weight_UOM' should be POUND<br>");
			}
			if (jsonObject.has("dimensions_UOM") && !jsonObject.isNull("dimensions_UOM")) {
				jsonObject.put("dimensions_UOM", jsonObject.getString("dimensions_UOM").toUpperCase());
				if (!jsonObject.getString("dimensions_UOM").equalsIgnoreCase("INCH"))
					isValid.append("'dimensions_UOM' should be INCH<br>");
			}
			if (jsonObject.has("package_weight_UOM") && !jsonObject.isNull("package_weight_UOM")) {
				jsonObject.put("package_weight_UOM", jsonObject.getString("package_weight_UOM").toUpperCase());
				if (!jsonObject.getString("package_weight_UOM").equalsIgnoreCase("POUND"))
					isValid.append("'package_weight_UOM'should be POUND<br>");
			}
			if (jsonObject.has("package_dimension_UOM") && !jsonObject.isNull("package_dimension_UOM")) {
				jsonObject.put("package_dimension_UOM", jsonObject.getString("package_dimension_UOM").toUpperCase());
				if (!jsonObject.getString("package_dimension_UOM").equalsIgnoreCase("INCH"))
					isValid.append("'package_dimension_UOM' should be INCH<br>");
			}

			if (jsonObject.has("multipack_Quantity") && !jsonObject.isNull("multipack_Quantity")) {
				if (!(jsonObject.get("multipack_Quantity") instanceof Integer)) {
					isValid.append("Please check the data for field 'multipack_Quantity'<br>");
				}
			}

			if (jsonObject.has("asin") && !jsonObject.isNull("asin")) {
				if (jsonObject.getString("asin").length() != 10) {
					isValid.append(
							"Please check the data for field 'asin' && asin should be 10 digit identification number<br>");
				}
			}

			// for boolean values
			if (jsonObject.has("child_product") && !jsonObject.isNull("child_product")) {
				if (!(jsonObject.get("child_product") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'child_product' &&  Please enter child product field as true or false <br>");
				}
			}
			if (jsonObject.has("handling_type") && !jsonObject.isNull("handling_type")) {
				if (!(jsonObject.get("handling_type") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'handling_type' && Please enter handling_type field as true or false <br>");
				}
			}
			if (jsonObject.has("country_of_manufacture") && !jsonObject.isNull("country_of_manufacture")) {
				if (!(jsonObject.get("country_of_manufacture") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'country_of_manfacture' && Please enter country_of_manufacture field as true or false <br>");
				}
			}
			if (jsonObject.has("multipack") && !jsonObject.isNull("multipack")) {
				if (!(jsonObject.get("multipack") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'multipack' && Please enter multipack field as true or false <br>");
				}
			}
			if (jsonObject.has("fragile") && !jsonObject.isNull("fragile")) {
				if (!(jsonObject.get("fragile") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'fragile' && Please enter fragile field as true or false<br>");
				}
			}
			if (jsonObject.has("taxable") && !jsonObject.isNull("taxable")) {
				if (!(jsonObject.get("taxable") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'taxable' && Please enter taxable field as true or false<br>");
				}
			}
			if (jsonObject.has("serial_number_tracked") && !jsonObject.isNull("serial_number_tracked")) {
				if (!(jsonObject.get("serial_number_tracked") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'serial_number_tracked' && Please enter serial_number_tracked field as true or false<br>");
				}
			}
			if (jsonObject.has("batch_number_tracked") && !jsonObject.isNull("batch_number_tracked")) {
				if (!(jsonObject.get("batch_number_tracked") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'batch_number_tracked' && Please enter batch_number_tracked field as true or false<br>");
				}
			}
			if (jsonObject.has("customization_available") && !jsonObject.isNull("customization_available")) {
				if (!(jsonObject.get("customization_available") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'customization_available' && Please enter customization_available field as true or false<br>");
				}
			}
			if (jsonObject.has("gift_message_available") && !jsonObject.isNull("gift_message_available")) {
				if (!(jsonObject.get("gift_message_available") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'gift_message_availble' && Please enter gift_message_available field as true or false<br>");
				}
			}
			if (jsonObject.has("Safety_wrap_required") && !jsonObject.isNull("Safety_wrap_required")) {
				if (!(jsonObject.get("Safety_wrap_required") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'Safety_wrap_required' && Please enter Safety_wrap_required field as true or false<br>");
				}
			}
			if (jsonObject.has("online_availability") && !jsonObject.isNull("online_availability")) {
				if (!(jsonObject.get("online_availability") instanceof Boolean)) {
					isValid.append(
							"Please check the data for field 'online_availability' && Please enter online_availability field as true or false<br>");
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			isValid.append(e.toString());
		}
		return isValid;
	}

	public boolean isAlpha(String name) {
		if (name.matches("[a-zA-Z]+")) {
			return true;
		} else {
			return false;
		}
	}

	@ApiMethod(apiname = "deleteproduct", method = "POST")
	public ResponseClass deleteProduct(String sku, int storeid) {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			connection.setAutoCommit(false);

			int product_id = getProductidBySku(sku, storeid);

			ps = connection
					.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.asnskulevel + " WHERE product_id = ?");
			ps.setInt(1, product_id);
			rs = ps.executeQuery();
			rs.next();
			int cnt = rs.getInt("cnt");

			ps.close();
			rs.close();
			if (cnt > 0)
				throw new Exception("Given product SKU is in ASN list. Can't delete the product.");

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("DELETE FROM " + Table.productvaluemaster + " WHERE product_id = ?");
			ps1.setInt(1, product_id);
			ps1.execute();
			ps1.close();

			PreparedStatement ps2;
			ps2 = connection.prepareStatement("DELETE FROM " + Table.productmanageprice + " WHERE product_id = ?");
			ps2.setInt(1, product_id);
			ps2.execute();
			ps2.close();

			PreparedStatement ps3;
			ps3 = connection.prepareStatement("DELETE FROM " + Table.productinventory + " WHERE product_id = ?");
			ps3.setInt(1, product_id);
			ps3.execute();
			ps3.close();

			PreparedStatement ps4;
			ps4 = connection.prepareStatement("DELETE FROM " + Table.productmapper + " WHERE product_id = ?");
			ps4.setInt(1, product_id);
			ps4.execute();
			ps4.close();
			connection.commit();

			return new ResponseClass(200, "success", "Product deleted");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not deleted");
		}
	}

	public Map<String, Object> getProduct(int product_id) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT * FROM " + Table.productvaluemaster + " WHERE product_id = ?");
			ps.setInt(1, product_id);
			rs = ps.executeQuery();

			Map<String, Object> product = new HashMap<String, Object>();
			product.put("product_id", product_id);
			while (rs.next()) {
				int field_id = rs.getInt("field_id");
				String value = rs.getString("value");

				if (!value.equals("null")) {
					product.putAll(getFieldValue(field_id, value));
				}
			}
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT * FROM " + Table.productmanageprice
					+ " WHERE product_id = ? AND scheduled_at <= NOW() ORDER BY scheduled_at DESC LIMIT 1");
			ps1.setInt(1, product_id);
			rs1 = ps1.executeQuery();
			rs1.next();
			product.put("price_on_ecommerce", rs1.getFloat("price"));

			ps1.close();
			rs1.close();
			return product;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Map<String, Object> getProductNull(int product_id) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT * FROM " + Table.productvaluemaster + " WHERE product_id = ?");
			ps.setInt(1, product_id);
			rs = ps.executeQuery();

			List<Integer> fieldIds = getAllFieldIds();
			List<Integer> availFields = new ArrayList<>();
			Map<String, Object> product = new HashMap<String, Object>();
			product.put("product_id", product_id);
			while (rs.next()) {
				int field_id = rs.getInt("field_id");
				String value = rs.getString("value");

				if (!value.equals("null")) {
					product.putAll(getFieldValue(field_id, value));
					availFields.add(field_id);
				}
			}
			ps.close();
			rs.close();

			for (Integer i : fieldIds) {
				if (!availFields.contains(i))
					product.put(getFieldNameById(i), null);
			}

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT * FROM " + Table.productmanageprice
					+ " WHERE product_id = ? AND scheduled_at <= NOW() ORDER BY scheduled_at DESC LIMIT 1");
			ps1.setInt(1, product_id);
			rs1 = ps1.executeQuery();
			rs1.next();
			product.put("price_on_ecommerce", rs1.getFloat("price"));

			ps1.close();
			rs1.close();

			return product;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Map<String, Object> getProductEmpty(int product_id) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.productvaluemaster + " WHERE product_id = ?");
			ps.setInt(1, product_id);
			rs = ps.executeQuery();

			List<Integer> fieldIds = getAllFieldIds();
			List<Integer> availFields = new ArrayList<>();
			Map<String, Object> product = new HashMap<String, Object>();
			product.put("product_id", product_id);
			while (rs.next()) {
				int field_id = rs.getInt("field_id");
				String value = rs.getString("value");

				if (!value.equals("null")) {
					product.putAll(getFieldValue(field_id, value));
					availFields.add(field_id);
				}
			}
			ps.close();
			rs.close();

			for (Integer i : fieldIds) {
				if (!availFields.contains(i))
					product.put(getFieldNameById(i), "");
			}

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT * FROM " + Table.productmanageprice
					+ " WHERE product_id = ? AND scheduled_at <= NOW() ORDER BY scheduled_at DESC LIMIT 1");
			ps1.setInt(1, product_id);
			rs1 = ps1.executeQuery();
			rs1.next();
			product.put("price_on_ecommerce", rs1.getFloat("price"));
			ps1.close();
			rs1.close();

			return product;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Map<String, Object> getFieldValue(int field_id, String value) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT name, datatype FROM " + Table.fieldmaster + " WHERE field_id = ?");
			ps.setInt(1, field_id);
			rs = ps.executeQuery();
			rs.next();

			String name = rs.getString("name");
			String datatype = rs.getString("datatype");
			ps.close();
			rs.close();

			Object objectValue = null;
			switch (datatype) {
			case "boolean":
				objectValue = Boolean.valueOf(value);
				break;
			case "int":
				objectValue = Integer.valueOf(value);
				break;
			case "long":
				objectValue = Long.valueOf(value);
				break;
			case "float":
				objectValue = Float.valueOf(value);
				break;
			case "double":
				objectValue = Double.valueOf(value);
				break;
			case "varchar":
				objectValue = String.valueOf(value);
				break;
			case "timestamp":
				objectValue = Long.valueOf(value);
				break;
			}

			Map<String, Object> fieldname_value = new HashMap<String, Object>();
			fieldname_value.put(name, objectValue);
			return fieldname_value;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void createProduct(JSONObject jsonObject, int storeid) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);
			PreparedStatement ps;
			ps = connection
					.prepareStatement("INSERT INTO " + Table.productmapper + " (sku, storeID)" + " VALUES (?, ?)");
			ps.setString(1, jsonObject.getString("sku"));
			ps.setInt(2, storeid);
			ps.execute();
			ps.close();

			int field_id;
			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement(
					"SELECT product_id FROM " + Table.productmapper + " WHERE sku = ? AND storeId = ?");
			ps1.setString(1, jsonObject.getString("sku"));
			ps1.setInt(2, storeid);
			rs1 = ps1.executeQuery();
			rs1.next();
			int product_id = rs1.getInt("product_id");
			ps1.close();
			rs1.close();

			String jsonKeys[] = JSONObject.getNames(jsonObject);

			PreparedStatement ps2;
			ps2 = connection.prepareStatement("INSERT INTO " + Table.productvaluemaster + " VALUES (?, ?, ?)");
			ps2.setInt(1, product_id);
			for (int i = 0; i < jsonKeys.length; i++) {
				if (jsonObject.isNull(jsonKeys[i]))
					continue;
				if (new String("" + jsonObject.get(jsonKeys[i])).isEmpty())
					continue;

				field_id = getFieldidByName(jsonKeys[i]);
				ps2.setInt(2, field_id);
				ps2.setString(3, "" + jsonObject.get(jsonKeys[i]));
				ps2.execute();
			}

			long current_time_millis = System.currentTimeMillis();
			// created at
			ps2.setInt(2, getFieldidByName("created_at"));
			ps2.setString(3, "" + current_time_millis);
			ps2.execute();
			jsonObject.put("created_at", current_time_millis);

			// updated at
			ps2.setInt(2, getFieldidByName("updated_at"));
			ps2.setString(3, "" + current_time_millis);
			ps2.execute();
			ps2.close();
			jsonObject.put("updated_at", current_time_millis);

			// Inserting the product inventory
			PreparedStatement ps3;
			ps3 = connection.prepareStatement("INSERT INTO " + Table.productinventory
					+ " (product_id, availableToPromise, fulfillableStock, damagedStock, reservedStock)"
					+ " VALUES (?, 0, 0, 0, 0)");
			ps3.setInt(1, product_id);
			ps3.execute();
			ps3.close();

			// Inserting into the product manage price
			PreparedStatement ps4;
			ps4 = connection.prepareStatement("INSERT INTO " + Table.productmanageprice
					+ " (product_id, price, scheduled_at)" + " VALUES (?, ?, ?)");
			ps4.setInt(1, product_id);
			ps4.setFloat(2, (float) jsonObject.getDouble("price_on_ecommerce"));
			ps4.setTimestamp(3, new Timestamp(current_time_millis));
			ps4.execute();
			ps4.close();

			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void updateProduct(JSONObject jsonObject, int storeid) throws Exception {
		try (Connection connection = getDBConnection()) {
			int field_id;
			int product_id = getProductidBySku(jsonObject.getString("sku"), storeid);

			String jsonKeys[] = JSONObject.getNames(jsonObject);

			for (int i = 0; i < jsonKeys.length; i++) {

				field_id = getFieldidByName(jsonKeys[i]);

				if (jsonObject.isNull(jsonKeys[i]) || new String("" + jsonObject.get(jsonKeys[i])).isEmpty()) {
					PreparedStatement ps;
					ps = connection.prepareStatement(
							"DELETE FROM " + Table.productvaluemaster + " WHERE product_id = ? AND field_id =?");
					ps.setInt(1, product_id);
					ps.setInt(2, field_id);
					ps.execute();
					ps.close();
					continue;
				}

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement("SELECT COUNT(*) as cnt FROM " + Table.productvaluemaster
						+ " WHERE product_id = ? AND field_id = ?");
				ps1.setInt(1, product_id);
				ps1.setInt(2, field_id);
				rs1 = ps1.executeQuery();
				rs1.next();
				// ps.close();

				if (rs1.getInt("cnt") == 1) {
					PreparedStatement ps2;
					ps2 = connection.prepareStatement("UPDATE " + Table.productvaluemaster
							+ " SET value = ? WHERE product_id = ? AND field_id = ?");
					ps2.setString(1, "" + jsonObject.get(jsonKeys[i]));
					ps2.setInt(2, product_id);
					ps2.setInt(3, field_id);
					ps2.execute();
					ps2.close();
					// ps.close();
				} else {
					PreparedStatement ps3;
					ps3 = connection.prepareStatement("INSERT INTO " + Table.productvaluemaster + " VALUES (?, ?, ?)");
					ps3.setInt(1, product_id);
					ps3.setInt(2, field_id);
					ps3.setString(3, "" + jsonObject.get(jsonKeys[i]));
					ps3.execute();
					ps3.close();
				}
				ps1.close();
				rs1.close();
			}

			long current_time_millis = System.currentTimeMillis();
			// updated date
			PreparedStatement ps4;
			ps4 = connection.prepareStatement(
					"UPDATE " + Table.productvaluemaster + " SET value = ? WHERE product_id = ? AND field_id = ?");
			ps4.setString(1, "" + current_time_millis);
			ps4.setInt(2, product_id);
			ps4.setInt(3, getFieldidByName("updated_at"));
			ps4.execute();
			ps4.close();
			jsonObject.put("updated_at", current_time_millis);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "filterproduct", method = "POST")
	public List<Map<String, Object>> getFilterProduct(String _productName, String _brand, String _manufacturer,
			int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			int field1 = getFieldidByName("product_name");
			int field2 = getFieldidByName("brand");
			int field3 = getFieldidByName("manufacturer");
			PreparedStatement ps;
			ResultSet rs;

			StringBuffer query = new StringBuffer();

			query.append("SELECT DISTINCT product_id FROM " + Table.productvaluemaster);
			int i = 1;

			if (_productName != null && !_productName.isEmpty()) {
				if (i == 1)
					query.append(" WHERE");
				else
					query.append(" AND");
				query.append(" product_id IN (SELECT product_id FROM " + Table.productvaluemaster
						+ " WHERE value LIKE '%" + _productName + "%' AND field_id ='" + field1 + "')");
				i++;
			}

			if (_brand != null && !_brand.isEmpty()) {
				if (i == 1)
					query.append(" WHERE");
				else
					query.append(" AND");
				query.append(" product_id IN (SELECT product_id FROM " + Table.productvaluemaster
						+ " WHERE value LIKE '%" + _brand + "%' AND field_id ='" + field2 + "')");
				i++;
			}

			if (_manufacturer != null && !_manufacturer.isEmpty()) {
				if (i == 1)
					query.append(" WHERE");
				else
					query.append(" AND");
				query.append(" product_id IN (SELECT product_id FROM " + Table.productvaluemaster
						+ " WHERE value LIKE '%" + _manufacturer + "%' AND field_id ='" + field3 + "')");
				i++;
			}

			ps = connection.prepareStatement(query.toString());
			rs = ps.executeQuery();

			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			while (rs.next()) {
				int productid = rs.getInt("product_id");
				if (checkProductStoreid(productid, storeid)) {
					Map<String, Object> product = getProductEmpty(productid);
					list.add(product);
				}
			}
			ps.close();
			rs.close();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getadvancefilter", method = "POST")
	public List<Map<String, Object>> getAdvanceFilter(JSONObject data, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			StringBuffer query = new StringBuffer();

			String jsonKeys[] = JSONObject.getNames(data);

			HashMap<String, Integer> fieldMaps = new HashMap<>();
			for (int i = 0; i < jsonKeys.length; i++) {
				fieldMaps.put(jsonKeys[i], getFieldidByName(jsonKeys[i]));
			}

			query.append("SELECT DISTINCT product_id FROM " + Table.productvaluemaster);
			for (int j = 0; j < fieldMaps.size(); j++) {
				if (j == 0)
					query.append(" WHERE");
				else
					query.append(" AND");

				query.append(
						" product_id IN (SELECT product_id FROM " + Table.productvaluemaster + " WHERE value LIKE '%"
								+ data.get(jsonKeys[j]) + "%' AND field_id ='" + fieldMaps.get(jsonKeys[j]) + "')");
			}

			ps = connection.prepareStatement(query.toString());
			rs = ps.executeQuery();

			List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>();
			while (rs.next()) {
				int productid = rs.getInt("product_id");
				if (checkProductStoreid(productid, storeid)) {
					Map<String, Object> product = getProduct(productid);
					productList.add(product);
				}
			}
			ps.close();
			rs.close();

			Collections.sort(productList, new SortByTime("updated_at"));
			int i = 1;
			for (Map<String, Object> product : productList)
				product.put("list_id", i++);

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Map<String, Object>> getAdvanceFilterEmpty(JSONObject data, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			StringBuffer query = new StringBuffer();

			String jsonKeys[] = JSONObject.getNames(data);

			HashMap<String, Integer> fieldMaps = new HashMap<>();
			for (int i = 0; i < jsonKeys.length; i++) {
				fieldMaps.put(jsonKeys[i], getFieldidByName(jsonKeys[i]));
			}

			query.append("SELECT DISTINCT product_id FROM " + Table.productvaluemaster);
			for (int j = 0; j < fieldMaps.size(); j++) {
				if (j == 0)
					query.append(" WHERE");
				else
					query.append(" AND");

				query.append(
						" product_id IN (SELECT product_id FROM " + Table.productvaluemaster + " WHERE value LIKE '%"
								+ data.get(jsonKeys[j]) + "%' AND field_id ='" + fieldMaps.get(jsonKeys[j]) + "')");
			}

			ps = connection.prepareStatement(query.toString());
			rs = ps.executeQuery();

			List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>();
			while (rs.next()) {
				int productid = rs.getInt("product_id");
				if (checkProductStoreid(productid, storeid)) {
					Map<String, Object> product = getProductEmpty(productid);
					productList.add(product);
				}
			}
			ps.close();
			rs.close();

			Collections.sort(productList, new SortByTime("updated_at"));
			int i = 1;
			for (Map<String, Object> product : productList)
				product.put("list_id", i++);

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpageadvancefilter", method = "POST")
	public List<Map<String, Object>> getPageAdvanceFilter(JSONObject data, int storeid, int startIndex, int load)
			throws Exception {
		try {
			List<Map<String, Object>> allProductList = getAdvanceFilter(data, storeid);
			List<Map<String, Object>> productList = new ArrayList<>();

			for (Map<String, Object> product : allProductList) {
				int list_id = (int) product.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					productList.add(product);
			}
			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpageadvancefilterdownload", method = "POST")
	public List<Map<String, Object>> getPageAdvanceFilterDownload(JSONObject data, int storeid, int startIndex,
			int load) throws Exception {
		try {
			List<Map<String, Object>> allProductList = getAdvanceFilterEmpty(data, storeid);
			List<Map<String, Object>> productList = new ArrayList<>();

			for (Map<String, Object> product : allProductList) {
				int list_id = (int) product.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					productList.add(product);
			}
			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getadvancefiltercount", method = "GET")
	public int getAdvanceFilterCount(JSONObject data, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			StringBuffer query = new StringBuffer();

			String jsonKeys[] = JSONObject.getNames(data);

			HashMap<String, Integer> fieldMaps = new HashMap<>();
			for (int i = 0; i < jsonKeys.length; i++) {
				fieldMaps.put(jsonKeys[i], getFieldidByName(jsonKeys[i]));
			}

			query.append("SELECT DISTINCT product_id FROM " + Table.productvaluemaster);
			for (int j = 0; j < fieldMaps.size(); j++) {
				if (j == 0)
					query.append(" WHERE");
				else
					query.append(" AND");

				query.append(
						" product_id IN (SELECT product_id FROM " + Table.productvaluemaster + " WHERE value LIKE '%"
								+ data.get(jsonKeys[j]) + "%' AND field_id ='" + fieldMaps.get(jsonKeys[j]) + "')");
			}

			ps = connection.prepareStatement(query.toString());
			rs = ps.executeQuery();
			int count = 0;
			while (rs.next()) {
				int productid = rs.getInt("product_id");
				if (checkProductStoreid(productid, storeid)) {
					count++;
				}
			}
			ps.close();
			rs.close();
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getallbrands", method = "GET")
	public Map<String, Object> getAllBrandAndManufacturer(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			int field_sku = getFieldidByName("sku");
			int field_brand = getFieldidByName("brand");
			int field_manu = getFieldidByName("manufacturer");

			ps = connection.prepareStatement(
					"SELECT DISTINCT(value), product_id FROM " + Table.productvaluemaster + " WHERE field_id = ?");
			ps.setInt(1, field_sku);
			rs = ps.executeQuery();

			List<String> sku = new ArrayList<String>();
			while (rs.next()) {
				PreparedStatement ps1;
				ResultSet rs1;

				ps1 = connection.prepareStatement(
						"SELECT COUNT(*) AS cnt FROM " + Table.productmapper + " WHERE product_id = ? AND storeId = ?");
				ps1.setInt(1, rs.getInt("product_id"));
				ps1.setInt(2, storeid);
				rs1 = ps1.executeQuery();
				rs1.next();

				int cnt = rs1.getInt("cnt");
				ps1.close();
				rs1.close();
				if (cnt == 1)
					sku.add(rs.getString("value"));
			}
			ps.close();
			rs.close();

			PreparedStatement ps2;
			ResultSet rs2;
			ps2 = connection.prepareStatement(
					"SELECT DISTINCT(value), product_id FROM " + Table.productvaluemaster + " WHERE field_id = ?");
			ps2.setInt(1, field_brand);
			rs2 = ps2.executeQuery();

			List<String> brand = new ArrayList<String>();
			while (rs2.next()) {
				PreparedStatement ps3;
				ResultSet rs3;
				ps3 = connection.prepareStatement(
						"SELECT COUNT(*) AS cnt FROM " + Table.productmapper + " WHERE product_id = ? AND storeId = ?");
				ps3.setInt(1, rs2.getInt("product_id"));
				ps3.setInt(2, storeid);
				rs3 = ps3.executeQuery();
				rs3.next();

				int cnt = rs3.getInt("cnt");
				ps3.close();
				rs3.close();
				if (cnt == 1)
					brand.add(rs2.getString("value"));
			}
			ps2.close();
			rs2.close();

			PreparedStatement ps4;
			ResultSet rs4;
			ps4 = connection.prepareStatement(
					"SELECT DISTINCT(value), product_id FROM " + Table.productvaluemaster + " WHERE field_id = ?");
			ps4.setInt(1, field_manu);
			rs4 = ps4.executeQuery();

			List<String> manufacturer = new ArrayList<String>();
			while (rs4.next()) {
				PreparedStatement ps5;
				ResultSet rs5;
				ps5 = connection.prepareStatement(
						"SELECT COUNT(*) AS cnt FROM " + Table.productmapper + " WHERE product_id = ? AND storeId = ?");
				ps5.setInt(1, rs4.getInt("product_id"));
				ps5.setInt(2, storeid);
				rs5 = ps5.executeQuery();
				rs5.next();

				int cnt = rs5.getInt("cnt");
				ps5.close();
				rs5.close();
				if (cnt == 1)
					manufacturer.add(rs4.getString("value"));
			}
			ps4.close();
			rs4.close();

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("sku", sku);
			map.put("brand", brand);
			map.put("manufacturer", manufacturer);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getallfields", method = "GET")
	public List<Map<String, Object>> getAllFieldDetails() throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

			ps = connection.prepareStatement("SELECT * FROM " + Table.fieldmaster);
			rs = ps.executeQuery();

			while (rs.next()) {
				Map<String, Object> field = new HashMap<String, Object>();
				field.put("field_id", rs.getInt("field_id"));
				field.put("name", rs.getString("name"));
				field.put("title", rs.getString("title"));
				// field.put("title", rs.getString("title"));
				field.put("datatype", rs.getString("datatype"));
				// field.put("fieldtype", rs.getString("fieldtype"));
				// if (rs.getObject("min_length") != null)
				// field.put("min_length", rs.getInt("min_length"));
				// if (rs.getObject("max_length") != null)
				// field.put("max_length", rs.getInt("max_length"));
				if (rs.getObject("length") != null)
					field.put("length", rs.getInt("length"));
				// field.put("is_mandatory", rs.getBoolean("is_mandatory"));
				// field.put("is_enabled", rs.getBoolean("is_enabled"));
				field.put("ismandotry", rs.getBoolean("ismandotry"));
				field.put("has_possible_values", rs.getBoolean("has_possible_values"));

				if (rs.getString("possible_values") != null) {
					JSONObject json = new JSONObject(rs.getString("possible_values"));
					JSONArray array = json.getJSONArray("value");

					List<String> possible_values = new ArrayList<String>();
					for (int i = 0; i < array.length(); i++)
						possible_values.add(array.getString(i));

					field.put("possible_values", possible_values);
				}
				// field.put("field_category", rs.getString("field_category"));
				list.add(field);
			}
			ps.close();
			rs.close();

			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "setselectedfields", method = "POST")
	public void setSelectedFields(Map<String, Object> selectedFields, int userid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			@SuppressWarnings("unchecked")
			ArrayList<BigDecimal> fieldIds = (ArrayList<BigDecimal>) selectedFields.get("selectedFields");
			ps = connection.prepareStatement("DELETE FROM " + Table.selectedfields + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			ps.execute();
			ps.close();

			for (BigDecimal field_id : fieldIds) {
				PreparedStatement ps1;
				ps1 = connection.prepareStatement("INSERT INTO " + Table.selectedfields + " VALUES (?, ?)");
				ps1.setInt(1, userid);
				ps1.setInt(2, field_id.intValue());
				ps1.execute();
				ps1.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getsortedfields", method = "GET")
	public List<Map<String, Object>> getSortedFields(int userid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.selectedfields + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();
			int cnt = rs.getInt("cnt");
			ps.close();
			rs.close();
			if (cnt == 0)
				addDefaultFieldsToTable(userid);

			List<Integer> selectedFieldIds = getSelectedFieldIds(userid);
			List<Map<String, Object>> allFields = getAllFieldDetails();

			for (Map<String, Object> field : allFields) {
				if (selectedFieldIds.contains((Integer) field.get("field_id"))) {
					field.put("selected", true);
				} else
					field.put("selected", false);
			}

			allFields = sortFields(allFields);
			Map<String, Object> skuMap = null, mainImageMap = null, productNameMap = null;

			for (Map<String, Object> field : allFields) {
				if (field.get("name").equals("main_image")) {
					mainImageMap = field;
				}
				if (field.get("name").equals("sku")) {
					skuMap = field;
				}
				if (field.get("name").equals("product_name")) {
					productNameMap = field;
				}
			}

			if (mainImageMap != null) {
				allFields.remove(mainImageMap);
				mainImageMap.replace("selected", true);
				allFields.add(0, mainImageMap);
			}
			if (skuMap != null) {
				allFields.remove(skuMap);
				skuMap.replace("selected", true);
				allFields.add(1, skuMap);
			}
			if (productNameMap != null) {
				allFields.remove(productNameMap);
				productNameMap.replace("selected", true);
				allFields.add(2, productNameMap);
			}

			int i = 1;
			for (Map<String, Object> field : allFields) {
				field.put("orderId", i);
				i++;
			}
			return allFields;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Map<String, Object>> sortFields(List<Map<String, Object>> fields) throws Exception {
		try {
			Collections.sort(fields, new SortByAlpha("title"));
			Collections.sort(fields, new SortByBool("selected"));
			return fields;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void addDefaultFieldsToTable(int userid) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT field_id FROM " + Table.fieldmaster + " WHERE default_selected = ?");
			ps.setBoolean(1, true);
			rs = ps.executeQuery();

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("INSERT INTO " + Table.selectedfields + " VALUES (?, ?)");
			while (rs.next()) {
				ps1.setInt(1, userid);
				ps1.setInt(2, rs.getInt("field_id"));
				ps1.execute();
			}
			ps1.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getproductinventory", method = "GET")
	public List<Map<String, Object>> getProductInventory(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.productmapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			List<Map<String, Object>> inventory = new ArrayList<>();
			while (rs.next()) {
				Map<String, Object> productInventoryDetail = getProductInventoryDetails(rs.getInt("product_id"));
				if (productInventoryDetail != null)
					inventory.add(productInventoryDetail);
			}
			ps.close();
			rs.close();

			Collections.sort(inventory, new SortByTime("updatedOn"));
			int i = 1;
			for (Map<String, Object> product : inventory)
				product.put("list_id", i++);

			return inventory;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpageproductinventory", method = "POST")
	public List<Map<String, Object>> getPageProductInventory(int storeid, int startIndex, int load) throws Exception {
		try {
			List<Map<String, Object>> allProductInventoryList = getProductInventory(storeid);
			List<Map<String, Object>> productInventoryList = new ArrayList<>();

			for (Map<String, Object> product : allProductInventoryList) {
				int list_id = (int) product.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					productInventoryList.add(product);
			}
			return productInventoryList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getproductinventorycount", method = "GET")
	public int getProductInventoryCount(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.productmapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			int i = 0;
			while (rs.next()) {
				Map<String, Object> productInventoryDetail = getProductInventoryDetails(rs.getInt("product_id"));
				if (productInventoryDetail != null)
					i++;
			}
			ps.close();
			rs.close();

			return i;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Map<String, Object> getProductInventoryDetails(int product_id) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.productinventory + " WHERE product_id = ?");
			ps.setInt(1, product_id);
			rs = ps.executeQuery();

			if (rs.next()) {
				Map<String, Object> product = getProduct(rs.getInt("product_id"));
				Map<String, Object> productInventoryDetail = new HashMap<>();
				productInventoryDetail.put("sku", (String) product.get("sku"));
				productInventoryDetail.put("product_name", product.get("product_name"));
				productInventoryDetail.put("image", product.get("main_image"));
				productInventoryDetail.put("availableToPromise", rs.getInt("availableToPromise"));
				productInventoryDetail.put("fulfillableStock", rs.getInt("fulfillableStock"));
				productInventoryDetail.put("damagedStock", rs.getInt("damagedStock"));
				productInventoryDetail.put("reservedStock", rs.getInt("reservedStock"));
				productInventoryDetail.put("updatedOn", rs.getTimestamp("updatedOn").getTime());
				return productInventoryDetail;
			}
			ps.close();
			rs.close();

			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Integer> getAllFieldIds() throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			List<Integer> list = new ArrayList<Integer>();

			ps = connection.prepareStatement("SELECT * FROM " + Table.fieldmaster + " WHERE name <> ? AND name <> ?");
			ps.setString(1, "created_at");
			ps.setString(2, "updated_at");
			rs = ps.executeQuery();

			while (rs.next()) {
				list.add(rs.getInt("field_id"));
			}
			ps.close();
			rs.close();

			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Integer> getSelectedFieldIds(int userid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			List<Integer> list = new ArrayList<Integer>();
			ps = connection.prepareStatement("SELECT field_id FROM " + Table.selectedfields + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();

			while (rs.next()) {
				list.add(rs.getInt("field_id"));
			}
			ps.close();
			rs.close();

			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getmanageprice", method = "GET")
	public List<Map<String, Object>> getManagePrice(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT product_id, sku FROM " + Table.productmapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			List<Map<String, Object>> productList = new ArrayList<>();
			while (rs.next()) {
				Map<String, Object> productMeta = new HashMap<>();
				int product_id = rs.getInt("product_id");
				String sku = rs.getString("sku");

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement("SELECT price, scheduled_at FROM " + Table.productmanageprice
						+ " WHERE product_id = ? AND scheduled_at >= NOW() ORDER BY scheduled_at ASC LIMIT 1");
				ps1.setInt(1, product_id);
				rs1 = ps1.executeQuery();

				if (rs1.next()) {
					productMeta.put("new_price", rs1.getFloat("price"));
					productMeta.put("scheduled_at", rs1.getTimestamp("scheduled_at").getTime());
				} else {

					PreparedStatement ps2;
					ResultSet rs2;
					ps2 = connection.prepareStatement("SELECT price, scheduled_at FROM " + Table.productmanageprice
							+ " WHERE product_id = ? AND scheduled_at <= NOW() ORDER BY scheduled_at DESC LIMIT 1");
					ps2.setInt(1, product_id);
					rs2 = ps2.executeQuery();
					rs2.next();
					productMeta.put("new_price", rs2.getFloat("price"));
					productMeta.put("scheduled_at", rs2.getTimestamp("scheduled_at").getTime());
					ps2.close();
					rs2.close();
				}
				ps1.close();
				rs1.close();

				Map<String, Object> product = getProduct(product_id);
				productMeta.put("product_id", product_id);
				productMeta.put("image", product.get("main_image"));
				productMeta.put("sku", sku);
				productMeta.put("product_name", product.get("product_name"));
				productMeta.put("current_price", product.get("price_on_ecommerce"));
				productList.add(productMeta);
			}
			ps.close();
			rs.close();
			Collections.sort(productList, new SortByTime("scheduled_at"));
			int i = 1;
			for (Map<String, Object> product : productList)
				product.put("list_id", i++);

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpagemanageprice", method = "POST")
	public List<Map<String, Object>> getPageManagePrice(int storeid, int startIndex, int load) throws Exception {
		try {
			List<Map<String, Object>> allNewPriceProductsList = getManagePrice(storeid);
			List<Map<String, Object>> NewPriceProductList = new ArrayList<>();

			for (Map<String, Object> product : allNewPriceProductsList) {
				int list_id = (int) product.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					NewPriceProductList.add(product);
			}
			return NewPriceProductList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getsearchmanageprice", method = "GET")
	public List<Map<String, Object>> getSearchManagePrice(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT product_id, sku FROM " + Table.productmapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			int fieldsku = getFieldidByName("sku");
			int fieldname = getFieldidByName("product_name");
			List<Map<String, Object>> productList = new ArrayList<>();
			while (rs.next()) {
				int product_id = rs.getInt("product_id");
				String sku = rs.getString("sku");

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement(
						"SELECT DISTINCT(product_id) FROM " + Table.productvaluemaster + " WHERE value LIKE '%"
								+ searchText + "%' AND product_id = ? AND (field_id = ? OR field_id =?)");
				ps1.setInt(1, product_id);
				ps1.setInt(2, fieldsku);
				ps1.setInt(3, fieldname);
				rs1 = ps1.executeQuery();

				if (rs1.next()) {
					Map<String, Object> productMeta = new HashMap<>();

					PreparedStatement ps2;
					ResultSet rs2;

					ps2 = connection.prepareStatement("SELECT price, scheduled_at FROM " + Table.productmanageprice
							+ " WHERE product_id = ? AND scheduled_at >= NOW() ORDER BY scheduled_at ASC LIMIT 1");
					ps2.setInt(1, product_id);
					rs2 = ps2.executeQuery();

					if (rs2.next()) {
						productMeta.put("new_price", rs2.getFloat("price"));
						productMeta.put("scheduled_at", rs2.getTimestamp("scheduled_at").getTime());
					} else {

						PreparedStatement ps3;
						ResultSet rs3;
						ps3 = connection.prepareStatement("SELECT price, scheduled_at FROM " + Table.productmanageprice
								+ " WHERE product_id = ? AND scheduled_at <= NOW() ORDER BY scheduled_at DESC LIMIT 1");
						ps3.setInt(1, product_id);
						rs3 = ps3.executeQuery();
						rs3.next();
						productMeta.put("new_price", rs3.getFloat("price"));
						productMeta.put("scheduled_at", rs3.getTimestamp("scheduled_at").getTime());
						ps3.close();
						rs3.close();
					}
					ps2.close();
					rs2.close();

					Map<String, Object> product = getProduct(product_id);
					productMeta.put("product_id", product_id);
					productMeta.put("image", product.get("main_image"));
					productMeta.put("sku", sku);
					productMeta.put("product_name", product.get("product_name"));
					productMeta.put("current_price", product.get("price_on_ecommerce"));
					productList.add(productMeta);
				}
				ps1.close();
				rs1.close();
			}
			ps.close();
			rs.close();

			Collections.sort(productList, new SortByAlpha("product_name"));
			int i = 1;
			for (Map<String, Object> product : productList)
				product.put("list_id", i++);

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpagesearchmanageprice", method = "POST")
	public List<Map<String, Object>> getPageSearchManagePrice(String searchText, int storeid, int startIndex, int load)
			throws Exception {
		try {
			List<Map<String, Object>> allProductList = getSearchManagePrice(searchText, storeid);
			List<Map<String, Object>> productList = new ArrayList<>();

			for (Map<String, Object> product : allProductList) {
				int list_id = (int) product.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					productList.add(product);
			}
			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getsearchmanagepricecount", method = "POST")
	public int getSearchManagePriceCount(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			int fieldsku = getFieldidByName("sku");
			int fieldname = getFieldidByName("product_name");

			ps = connection
					.prepareStatement("SELECT product_id, sku FROM " + Table.productmapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			int count = 0;
			while (rs.next()) {
				int product_id = rs.getInt("product_id");

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement(
						"SELECT DISTINCT(product_id) FROM " + Table.productvaluemaster + " WHERE value LIKE '%"
								+ searchText + "%' AND product_id = ? AND (field_id = ? OR field_id =?)");
				ps1.setInt(1, product_id);
				ps1.setInt(2, fieldsku);
				ps1.setInt(3, fieldname);
				rs1 = ps1.executeQuery();

				if (rs1.next()) {
					count++;
				}
				ps1.close();
				rs1.close();
			}
			ps.close();
			rs.close();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "updatemanageprice", method = "PUT")
	public ResponseClass updateManagePrice(int storeid, List<HashMap<String, Object>> mapList) {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			connection.setAutoCommit(false);
			for (HashMap<String, Object> map : mapList) {
				int product_id = getProductidBySku((String) map.get("sku"), storeid);
				float price = ((BigDecimal) map.get("new_price")).floatValue();
				long scheduled_at = ((BigDecimal) map.get("scheduled_at")).longValue();

				if (scheduled_at < System.currentTimeMillis())
					throw new Exception("Scheduled Time should not be lesser than the current time.");

				ps = connection.prepareStatement(
						"DELETE FROM " + Table.productmanageprice + " WHERE product_id = ? AND scheduled_at > NOW()");
				ps.setInt(1, product_id);
				ps.execute();
				ps.close();

				PreparedStatement ps1;
				ps1 = connection.prepareStatement("INSERT INTO " + Table.productmanageprice
						+ " (product_id, price, scheduled_at)" + " VALUES (?, ?, ?)");
				ps1.setInt(1, product_id);
				ps1.setFloat(2, price);
				ps1.setTimestamp(3, new Timestamp(scheduled_at));
				ps1.execute();
				ps1.close();
			}
			connection.commit();
			return new ResponseClass(201, "success", "Product price updated.");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not added");
		}
	}

	public boolean checkProductStoreid(int productid, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT COUNT(*) AS cnt FROM " + Table.productmapper + " WHERE product_id = ? AND storeId = ?");
			ps.setInt(1, productid);
			ps.setInt(2, storeid);
			rs = ps.executeQuery();
			rs.next();

			int cnt = rs.getInt("cnt");
			ps.close();
			rs.close();
			if (cnt == 0)
				return false;
			else
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String getFieldNameById(int field_id) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			String name;
			ps = connection.prepareStatement("SELECT name FROM " + Table.fieldmaster + " WHERE field_id = ?");
			ps.setInt(1, field_id);
			rs = ps.executeQuery();
			if (rs.next()) {
				name = rs.getString("name");
				ps.close();
				rs.close();
			} else {
				ps.close();
				rs.close();
				throw new Exception("field name not found");
			}
			return name;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int getFieldidByName(String field_name) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			int field_id;
			ps = connection.prepareStatement("SELECT field_id FROM " + Table.fieldmaster + " WHERE name = ?");
			ps.setString(1, field_name);
			rs = ps.executeQuery();
			if (rs.next()) {
				field_id = rs.getInt("field_id");
				ps.close();
				rs.close();
			} else {
				ps.close();
				rs.close();
				throw new Exception("field name not found");
			}
			return field_id;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int getProductidBySku(String sku, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			int product_id;
			ps = connection.prepareStatement(
					"SELECT product_id FROM " + Table.productmapper + " WHERE sku = ? AND storeId = ?");
			ps.setString(1, sku);
			ps.setInt(2, storeid);
			rs = ps.executeQuery();
			if (rs.next()) {
				product_id = rs.getInt("product_id");
				ps.close();
				rs.close();
			} else {
				ps.close();
				rs.close();
				throw new Exception("Product sku not found");
			}
			return product_id;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String getFieldDatatype(String field_name) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			String datatype;
			ps = connection.prepareStatement("SELECT datatype FROM " + Table.fieldmaster + " WHERE name = ?");
			ps.setString(1, field_name);
			rs = ps.executeQuery();
			if (rs.next()) {
				datatype = rs.getString("datatype");
				ps.close();
				rs.close();
			} else {
				ps.close();
				rs.close();
				throw new Exception("field name not found");
			}
			return datatype;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}

// Will sort in ascending order of the given two strings
class SortByAlpha implements Comparator<Map<String, Object>> {

	private String fieldname;

	public SortByAlpha(String name) {
		fieldname = name;
	}

	@Override
	public int compare(Map<String, Object> o1, Map<String, Object> o2) {
		String title1 = (String) o1.get(fieldname);
		String title2 = (String) o2.get(fieldname);

		return title1.compareTo(title2);
	}

}

// Will sort (put boolean value TRUE first and FALSE last)
class SortByBool implements Comparator<Map<String, Object>> {

	private String fieldname;

	public SortByBool(String name) {
		fieldname = name;
	}

	@Override
	public int compare(Map<String, Object> o1, Map<String, Object> o2) {
		boolean b1 = (boolean) o1.get(fieldname);
		boolean b2 = (boolean) o2.get(fieldname);
		if (b1 == true && b2 == true)
			return 0;
		else if (b1 == true && b2 == false)
			return -1;
		else if (b1 == false && b2 == true)
			return 1;
		else
			return 0;
	}
}

// Will sort (comparing the time object and will sort based on max time first)
class SortByTime implements Comparator<Map<String, Object>> {

	private String fieldname;

	public SortByTime(String name) {
		fieldname = name;
	}

	@Override
	public int compare(Map<String, Object> o1, Map<String, Object> o2) {
		long t1 = (long) o1.get(fieldname);
		long t2 = (long) o2.get(fieldname);

		if (t2 > t1)
			return 1;
		else if (t2 == t1)
			return 0;
		else
			return -1;
	}
}