package com.stores;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.UserClaims;
import com.vossa.api.v1.seller.pojos.ASN;

@Path("")
public class ASNResources {

	private ASNServices service;

	public ASNResources() {
		service = new ASNServices();
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getskuwithname")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSkuWithName(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getSkuWithName(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("generateasnnumber")
	@Produces(MediaType.APPLICATION_JSON)
	public Object generateASNNumber() {
		try {
			return service.generateASNNumber();

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("checkasnnumber")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkASNNumber(String data) {
		try {
			JSONObject json = new JSONObject(data);
			String asnNumber = json.getString("asnNumber");
			Map<String, Boolean> map = new HashMap<>();
			map.put("validData", service.checkASNNumber(asnNumber));
			return map;

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("createasn")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass createASN(ASN asn, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String createdBy = userClaims.getUserName();
			int userid = userClaims.getUserId();
			int storeid = userClaims.getStoreId();

			return service.createASN(asn, userid, storeid, createdBy);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@PUT
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("editasnshipment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass editASNShipment(ASN asn, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String modifiedBy = userClaims.getUserName();
			int storeid = userClaims.getStoreId();

			return service.editASNShipment(asn, storeid, modifiedBy);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getallasn")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllASN(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();
			return service.getAllASN(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpageasn")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageASN(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageASN(storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpagesearchasn")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageSearchASN(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageSearchASN(searchText, storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getasncount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getASNCount(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			int count = service.getASNCount(storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getsearchasncount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSearchASNCount(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");

			int count = service.getSearchASNCount(searchText, storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

//	@POST
//	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
//	@Path("getfilteredasn")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Object getFilteredASN(String data, @Context ContainerRequestContext context) {
//		try {
//			UserClaims userClaims = (UserClaims) context.getProperty("user");
//			int storeid = userClaims.getStoreId();
//			
//			JSONObject json = new JSONObject(data);
//			long fromDate = json.getLong("fromDate");
//			long toDate = json.getLong("toDate");
//			return service.getFilteredASN(fromDate, toDate, storeid);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ResponseClass(500, "error", e.getLocalizedMessage(),
//					"Oops, something went wrong. Internal server error. Try again later.");
//		}
//	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("deleteasn")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object deleteASN(Map<String, String> map) {
		try {
			String asnNumber = map.get("asnNumber");
			return service.deleteASN(asnNumber);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
}
