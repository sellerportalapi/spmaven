package com.stores;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.UserClaims;

@Path("")
public class OrderResources {

	private OrderServices service;

	public OrderResources() {
		service = new OrderServices();
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getallorders")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllOrders(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getAllOrders(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpageorders")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageOrders(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageOrders(storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getordercount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getOrderCount(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			int count = service.getOrderCount(storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getallreturns")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllReturns(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getAllReturns(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpagereturns")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageReturns(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageReturns(storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getreturncount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getReturnCount(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			int count = service.getReturnCount(storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpagesearchorders")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageSearchOrders(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);

			String searchText = json.getString("searchText");
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageSearchOrders(storeid, searchText, startIndex, load);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getsearchordercount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSearchOrderCount(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");

			int count = service.getSearchOrderCount(searchText, storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpagesearchreturns")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageSearchReturn(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageSearchReturns(storeid, searchText, startIndex, load);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getsearchreturncount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSearchReturnCount(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");

			int count = service.getSearchReturnCount(searchText, storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}
}
