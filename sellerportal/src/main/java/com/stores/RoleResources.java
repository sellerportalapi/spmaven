package com.stores;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.UserClaims;

@Path("")
public class RoleResources {

	private RoleServices service;

	public RoleResources() {
		service = new RoleServices();
	}

	@GET
	@Secured({ UserType.SUPERUSER })
	@Path("getmanagemeta")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getMetaDetails() {
		try {
			return service.getMetaDetails();
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");

		}
	}

	@GET
	@Secured({ UserType.SUPERUSER })
	@Path("getroleslist")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getRolesList(@Context ContainerRequestContext context) {

		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getRoles(storeid);
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");

		}
	}

	@POST
	@Secured({ UserType.SUPERUSER })
	@Path("createrole")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass createRole(String data, @Context ContainerRequestContext context) throws Exception {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			service.createRole(json, storeid);
			return new ResponseClass(200, "success", "ROLE CREATED");
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER })
	@Path("updaterole")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateRole(String data) throws Exception {
		try {
			JSONObject json = new JSONObject(data);
			service.updateRole(json);
			return new ResponseClass(200, "success", "ROLE updated");
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER })
	@Path("storeroles")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass storeRoles(String data, @Context ContainerRequestContext context) throws Exception {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String username = userClaims.getUserName();

			JSONObject json = new JSONObject(data);
			service.storeRoles(json, username);
			return new ResponseClass(200, "success", "ROLE stored");
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER })
	@Path("getassignedroles")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAssignedRoles(String data, @Context ContainerRequestContext context) throws Exception {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String username = userClaims.getUserName();

			JSONObject json = new JSONObject(data);
			return service.getAssignedRoles(json, username);
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured({ UserType.STANDARDUSER })
	@Path("getpermissions")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPermissions(@Context ContainerRequestContext context) throws Exception {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String username = userClaims.getUserName();

			return service.getPermissions(username);
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
}
