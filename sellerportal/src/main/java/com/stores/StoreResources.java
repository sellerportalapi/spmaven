package com.stores;

import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Authenticate;
import com.vossa.api.general.annotations.filterannotations.CreatePassword;
import com.vossa.api.general.annotations.filterannotations.Insecured;
import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.JWT;
import com.vossa.api.others.token.UserClaims;
import com.vossa.api.v1.seller.pojos.User;

@Path("")
public class StoreResources {

	private StoreServices service;

	public StoreResources() {
		service = new StoreServices();
	}

	@GET
	@Insecured
	@Path("get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() {
		return Response.ok("hello").build();
	}

	@GET
	@Insecured
	@Path("systemclean")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> systemClean() {

		Map<String, Object> map = new HashMap<>();

		Runtime r = Runtime.getRuntime();
		map.put("maxsize", r.maxMemory());
		map.put("totalsize", r.totalMemory());

		long size = r.freeMemory();
		map.put("initsize", size);

		r.gc();
		System.gc();
		size = r.freeMemory();
		map.put("finalsize", size);

		return map;
	}

	@GET
	@Insecured
	@Path("getdbdetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> getDBDetails() {
		return service.getDBDetails();
	}

	@POST
	@Insecured
	@Path("create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(String data) {
		try {
			URL url = new URL("https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods");
			URI uri = url.toURI();
			System.out.println(url.getUserInfo());
			System.out.println(uri.getScheme());
			Response response = Response.created(uri).entity(uri).build();
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			Response response = Response.serverError().build();
			return response;
		}
	}

	@POST
	@Authenticate({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("storelogin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass storeLogin(String data, @Context HttpHeaders httpHeaders) {
		try {
			JSONObject json = new JSONObject(data);
			String username = json.getString("userName");
			String password = json.getString("password");
			String origin = httpHeaders.getHeaderString("origin");
			return service.storeLogin(username, password, origin);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER })
	@Path("createuser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass createUser(User user, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String createdBy = userClaims.getUserName();
			int storeid = userClaims.getStoreId();

			if (!user.getUserName().isEmpty() && !user.getName().isEmpty() && !user.getEmail().isEmpty()
					&& !user.getUserType().isEmpty()) {
				return service.createUser(user, storeid, createdBy);
			} else {
				return new ResponseClass(500, "error", "Oops, Mandatory Fiels are not entered. Try again later.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@PUT
	@Secured({ UserType.SUPERUSER })
	@Path("updateuser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateUser(User user, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String modifiedBy = userClaims.getUserName();
			return service.updateUser(user, modifiedBy);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER })
	@Path("getallstoreusers")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllStoreUsers(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getAllStoreUsers(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER })
	@Path("checkfielduser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<Object, Boolean> checkFieldUser(String data) {
		Map<Object, Boolean> map = new HashMap<>();
		boolean valid = false;
		try {
			JSONObject js = new JSONObject(data);

			if (!js.isNull("userName")) {
				valid = service.checkFieldUser("userName", js.getString("userName"));
			} else if (!js.isNull("email")) {
				valid = service.checkFieldUser("email", js.getString("email"));
			} else if (!js.isNull("phoneNumber")) {
				valid = service.checkFieldUser("phoneNumber", js.getString("phoneNumber"));
			}

			map.put("validData", valid);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("validData", false);
		}
		return map;
	}

	@PUT
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("resetpassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass resetPassword(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String username = userClaims.getUserName();
			String currentPassword, newPassword;
			JSONObject json;

			json = new JSONObject(data);
			currentPassword = json.getString("currentPassword");
			newPassword = json.getString("newPassword");
			return service.resetPassword(username, currentPassword, newPassword);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@PUT
	@Secured({ UserType.SUPERUSER })
	@Path("updateuserstatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass updateUserStatus(String data, @Context ContainerRequestContext context) {
		String username, sellerStatus;
		JSONObject js;
		try {
			String origin = context.getHeaderString("origin");

			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String modifiedBy = userClaims.getUserName();

			js = new JSONObject(data);
			username = js.getString("userName");
			sellerStatus = js.getString("sellerStatus");
			int userid = service.getUseridByName(username);

			return service.updateUserStatus(userid, sellerStatus, modifiedBy, origin);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getuserdetails")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getUserdetails(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			String username = userClaims.getUserName();
			return service.getUserDetails(username);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER })
	@Path("edituserdetails")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass editUserDetails(String data, @Context ContainerRequestContext context) {
		String modifiedBy;
		String username, name, email;
		String phonenumber = null;
		JSONObject js;
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			modifiedBy = userClaims.getUserName();
			js = new JSONObject(data);
			username = js.getString("userName");
			name = js.getString("name");
			email = js.getString("email");
			if (js.has("phoneNumber"))
				phonenumber = js.getString("phoneNumber");

			return service.editUserDetails(username, name, email, phonenumber, modifiedBy);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Insecured
	@Path("forgotpassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass forgotPassword(Map<String, String> data, @Context HttpHeaders httpHeaders) throws Exception {
		try {
			String origin = httpHeaders.getHeaderString("origin");
			String email = data.get("email");
			return service.forgotPassword(email, origin);
		} catch (Exception e) {
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@CreatePassword
	@Path("createpassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass createPassword(Map<String, String> data, @Context ContainerRequestContext context)
			throws Exception {
		try {
			JWT jwt = (JWT) context.getProperty("jwt");
			int userId = service.getUseridByName(jwt.getSub());
			String jti = jwt.getJti();

			String newPassword = data.get("password");
			return service.createPassword(newPassword, userId, jti);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@CreatePassword
	@Path("checktokenexpiry")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass checkTokenExpiry(@Context ContainerRequestContext context) throws Exception {
		try {
			JWT jwt = (JWT) context.getProperty("jwt");
			int userId = service.getUseridByName(jwt.getSub());
			String jti = jwt.getJti();

			return service.checkTokenExpiry(userId, jti);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Insecured
	@Path("resendpasswordlink")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass resendPasswordLink(Map<String, String> data, @Context HttpHeaders httpHeaders)
			throws Exception {
		try {
			String origin = httpHeaders.getHeaderString("origin");
			String token = data.get("token");
			System.out.println(token);
			return service.resendPasswordLink(token, origin);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpageusers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageUsers(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageUsers(storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpagesearchusers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageSearchUsers(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			String searchText = json.getString("searchText");
			return service.getPageSearchUsers(storeid, startIndex, load, searchText);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getsearchusercount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSearchUserCount(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");

			int count = service.getSearchUserCount(searchText, storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}
}
