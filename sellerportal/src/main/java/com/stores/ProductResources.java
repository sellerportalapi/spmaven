package com.stores;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.UserClaims;

@Path("")
public class ProductResources {

	private ProductServices service;

	public ProductResources() {
		service = new ProductServices();
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getallproducts")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllProducts(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getAllProducts(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpageproducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageProducts(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageProducts(storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpageproductsdownload")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageProductsDownload(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageProductsDownload(storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getselectedproductsdownload")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSelectedProductsDownload(HashMap<String, Object> selectedProducts,
			@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getSelectedProductsDownload(selectedProducts, storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getproductcount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getProductCount(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			int count = service.getProductCount(storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getsearchproducts")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSearchProducts(@QueryParam("searchText") String searchText,
			@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getSearchProducts(searchText, storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpagesearchproducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageSearchProducts(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageSearchProducts(searchText, storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpagesearchproductsdownload")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageSearchProductsDownload(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageSearchProducts(searchText, storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getsearchproductcount")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSearchProductCount(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");

			int count = service.getSearchProductCount(searchText, storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("addsingleproduct")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass addSingleProduct(String data, @Context ContainerRequestContext context) {

		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();
			JSONObject jsonObject = new JSONObject(data);

			String msg = service.isValidData(jsonObject).toString();
			if (msg.equalsIgnoreCase("")) {
				return service.addSingleProduct(jsonObject, storeid);
			} else {
				return new ResponseClass(500, "error", msg, "not added");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not added");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("addbulkproducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass addBulkProducts(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject jsonObject = new JSONObject(data);
			String path = jsonObject.getString("path");
			return service.addBulkProducts(path, storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not added");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("editbulkproducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass editBulkProducts(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject jsonObject = new JSONObject(data);
			String path = jsonObject.getString("path");
			return service.editBulkProducts(path, storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not added");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("deleteproduct")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass deleteProduct(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject jsonObject = new JSONObject(data);
			String sku = jsonObject.getString("sku");
			return service.deleteProduct(sku, storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not deleted");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("filterproduct")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getFilterProducts(String data, @Context ContainerRequestContext context) {
		String productName = null, brand = null, manufacturer = null;
		JSONObject json = new JSONObject();
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			json = new JSONObject(data);
			if (json.has("product_name")) {
				productName = json.getString("product_name");
			}
			if (json.has("brand")) {
				brand = json.getString("brand");
			}
			if (json.has("manufacturer")) {
				manufacturer = json.getString("manufacturer");
			}
			return service.getFilterProduct(productName, brand, manufacturer, storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getadvancefilter")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAdvanceFilter(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject jsonObject = new JSONObject(data);
			return service.getAdvanceFilter(jsonObject, storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpageadvancefilter")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageAdvanceFilter(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			json.remove("startIndex");
			int load = json.getInt("load");
			json.remove("load");
			return service.getPageAdvanceFilter(json, storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpageadvancefilterdownload")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageAdvanceFilterDownload(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			json.remove("startIndex");
			int load = json.getInt("load");
			json.remove("load");
			return service.getPageAdvanceFilterDownload(json, storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getadvancefiltercount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAdvanceFilterCount(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int count = service.getAdvanceFilterCount(json, storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getallbrands")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllBrandAndManufacturer(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getAllBrandAndManufacturer(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getallfields")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllFieldDetails() {
		try {
			return service.getAllFieldDetails();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("setselectedfields")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass setSelectedFields(Map<String, Object> selectedFields,
			@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int userid = userClaims.getUserId();

			service.setSelectedFields(selectedFields, userid);
			return new ResponseClass(201, "success", "SELECTED Fields are noted");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getsortedfields")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSortedFields(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int userid = userClaims.getUserId();

			return service.getSortedFields(userid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getproductinventory")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getProductInventory(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getProductInventory(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpageproductinventory")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getProductInventory(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageProductInventory(storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getproductinventorycount")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getProductInventoryCount(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			int count = service.getProductInventoryCount(storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getmanageprice")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getManagePrice(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getManagePrice(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpagemanageprice")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageManagePrice(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageManagePrice(storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getsearchmanageprice")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSearchManagePrice(@QueryParam("searchText") String searchText,
			@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getSearchManagePrice(searchText, storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getpagesearchmanageprice")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPageSearchManagePrice(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");
			int startIndex = json.getInt("startIndex");
			int load = json.getInt("load");
			return service.getPageSearchManagePrice(searchText, storeid, startIndex, load);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@POST
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getsearchmanagepricecount")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getSearchManagePriceCount(String data, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			JSONObject json = new JSONObject(data);
			String searchText = json.getString("searchText");

			int count = service.getSearchManagePriceCount(searchText, storeid);
			Map<String, Integer> map = new HashMap<>();
			map.put("count", count);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "Something wrong");
		}
	}

	@PUT
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("updatemanageprice")
	@Produces(MediaType.APPLICATION_JSON)
	public Object updateManagePrice(List<HashMap<String, Object>> mapList, @Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.updateManagePrice(storeid, mapList);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not added");
		}
	}
}
