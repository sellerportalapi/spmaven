package com.stores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.general.enums.UserType;

public class RoleServices extends StoreServices {

	@ApiMethod(apiname = "getmanagemeta", method = "GET")
	public String getMetaDetails() throws Exception {
		String s;
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.features);
			rs = ps.executeQuery();
			JSONArray jarray = new JSONArray();
			while (rs.next()) {
				JSONObject feature = new JSONObject();
				feature.put("feature", rs.getString("feature"));
				feature.put("none", rs.getInt("none"));
				feature.put("view", rs.getInt("view"));
				feature.put("view_edit", rs.getInt("view_edit"));
				feature.put("selected", 1);
				if (rs.getInt(3) > 0) {
					PreparedStatement ps1;
					ResultSet rs1;

					ps1 = connection.prepareStatement(
							"SELECT  * FROM " + Table.subfeatures + " WHERE id=" + rs.getInt("sub_feature_id"));
					rs1 = ps1.executeQuery();
					JSONArray ja = new JSONArray();

					while (rs1.next()) {
						JSONObject subfeature = new JSONObject();
						subfeature.put("feature", rs1.getString("feature"));
						subfeature.put("none", rs1.getInt("none"));
						subfeature.put("view", rs1.getInt("view"));
						subfeature.put("view_edit", rs1.getInt("view_edit"));
						subfeature.put("selected", 1);
						ja.put(subfeature);

					}
					ps1.close();
					rs1.close();
					feature.put("sub_features", ja);
				}

				jarray.put(feature);
			}
			ps.close();
			rs.close();
			s = jarray.toString();
			return s;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@ApiMethod(apiname = "getroleslist", method = "GET")
	public String getRoles(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.roles + " WHERE storeId=" + storeid);
			rs = ps.executeQuery();
			JSONArray jarray = new JSONArray();

			while (rs.next()) {
				JSONArray ja1 = new JSONArray();
				JSONObject jo = new JSONObject();
				jo.put("id", rs.getInt("roles_id"));
				jo.put("rolename", rs.getString("name"));
				jo.put("description", rs.getString("description"));

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement(
						"SELECT * FROM " + Table.selectedvalues + " WHERE roles_id=" + rs.getInt("roles_id"));
				rs1 = ps1.executeQuery();

				while (rs1.next()) {
					JSONObject jo1 = new JSONObject();
					jo1.put("feature", rs1.getString("feature"));
					jo1.put("none", rs1.getInt("none"));
					jo1.put("view", rs1.getInt("view"));
					jo1.put("view_edit", rs1.getInt("view_edit"));
					jo1.put("selected", rs1.getInt("selected"));

					PreparedStatement ps2;
					ResultSet rs2;
					ps2 = connection.prepareStatement("SELECT values_id FROM " + Table.selectedsubvalues
							+ " WHERE values_id=" + rs1.getInt("values_id"));
					rs2 = ps2.executeQuery();
					if (rs2.next()) {
						PreparedStatement ps3;
						ResultSet rs3;
						ps3 = connection.prepareStatement("SELECT * FROM " + Table.selectedsubvalues
								+ " WHERE values_id=" + rs1.getInt("values_id"));
						rs3 = ps3.executeQuery();
						JSONArray ja = new JSONArray();

						while (rs3.next()) {
							JSONObject jo2 = new JSONObject();
							jo2.put("feature", rs3.getString("feature"));
							jo2.put("none", rs3.getInt("none"));
							jo2.put("view", rs3.getInt("view"));
							jo2.put("view_edit", rs3.getInt("view_edit"));
							jo2.put("selected", rs3.getInt("selected"));
							ja.put(jo2);
						}
						ps3.close();
						rs3.close();
						jo1.put("sub_features", ja);
					}
					ps2.close();
					rs2.close();
					ja1.put(jo1);
				}
				ps1.close();
				rs1.close();
				jo.put("features", ja1);
				jarray.put(jo);
			}
			ps.close();
			rs.close();

			return jarray.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "createrole", method = "POST")
	public void createRole(JSONObject json, int storeid) throws Exception {
		try (Connection connection = getDBConnection()) {
			String _rolename = json.getString("rolename");
			String description = json.getString("description");
			PreparedStatement ps;
			ps = connection
					.prepareStatement("INSERT INTO " + Table.roles + " (name, storeId, description) values(?,?,?)");

			ps.setString(1, _rolename);
			ps.setInt(2, storeid);
			ps.setString(3, description);
			ps.execute();
			ps.close();
			JSONArray ja = (JSONArray) json.get("features");

			for (int i = 0; i < ja.length(); i++) {
				JSONObject j = ja.getJSONObject(i);

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement("SELECT roles_id FROM " + Table.roles + " WHERE storeId=" + storeid
						+ " and name=" + "'" + _rolename + "'");
				rs1 = ps1.executeQuery();

				PreparedStatement ps2;
				ps2 = connection.prepareStatement("INSERT into " + Table.selectedvalues
						+ " (roles_id,feature,selected,none,view,view_edit) values(?,?,?,?,?,?)");
				if (rs1.next()) {
					int _rolesid = rs1.getInt("roles_id");
					String _feature = j.getString("feature");
					int _selected = j.getInt("selected");
					ps2.setInt(1, rs1.getInt("roles_id"));
					ps2.setString(2, _feature);
					ps2.setInt(3, _selected);

					PreparedStatement ps3;
					ResultSet rs3;
					ps3 = connection.prepareStatement(
							"SELECT * FROM " + Table.features + " WHERE feature=" + "'" + _feature + "'");
					rs3 = ps3.executeQuery();
					if (rs3.next()) {
						ps2.setInt(4, rs3.getInt("none"));
						ps2.setInt(5, rs3.getInt("view"));
						ps2.setInt(6, rs3.getInt("view_edit"));
						ps2.execute();
					}
					ps3.close();
					rs3.close();
					ps2.close();

					if (j.has("sub_features")) {

						JSONArray js = j.getJSONArray("sub_features");
						PreparedStatement ps4;
						ResultSet rs4;
						ps4 = connection.prepareStatement("SELECT values_id from " + Table.selectedvalues
								+ " WHERE feature=" + "'" + _feature + "' and roles_id=" + _rolesid);
						rs4 = ps4.executeQuery();
						if (rs4.next()) {
							PreparedStatement ps5;
							ps5 = connection.prepareStatement("INSERT INTO " + Table.selectedsubvalues
									+ "(values_id,feature,selected,none,view,view_edit) values(?,?,?,?,?,?)");
							for (int k = 0; k < js.length(); k++) {
								JSONObject sub = js.getJSONObject(k);
								ps5.setInt(1, rs4.getInt("values_id"));
								ps5.setString(2, sub.getString("feature"));
								ps5.setInt(3, sub.getInt("selected"));

								PreparedStatement ps6;
								ResultSet rs6;
								ps6 = connection.prepareStatement("SELECT * FROM " + Table.subfeatures
										+ " WHERE feature=" + "'" + sub.getString("feature") + "'");
								rs6 = ps6.executeQuery();
								if (rs6.next()) {
									ps5.setInt(4, rs6.getInt("none"));
									ps5.setInt(5, rs6.getInt("view"));
									ps5.setInt(6, rs6.getInt("view_edit"));
								}
								ps5.execute();
								ps6.close();
								rs6.close();
							}
							ps5.close();
						}
						ps4.close();
						rs4.close();
					}
				}
				ps1.close();
				rs1.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "updaterole", method = "POST")
	public void updateRole(JSONObject json) throws Exception {
		try (Connection connection = getDBConnection()) {
			String _rolename = json.getString("rolename");
			String description = json.getString("description");
			int _valueid = 0;
			int _roleid = json.getInt("id");

			PreparedStatement ps;
			ps = connection.prepareStatement("UPDATE " + Table.roles + " SET name=?, description=? WHERE roles_id=?");
			ps.setString(1, _rolename);
			ps.setString(2, description);
			ps.setInt(3, _roleid);
			ps.executeUpdate();
			ps.close();

			JSONArray ja = (JSONArray) json.get("features");
			for (int i = 0; i < ja.length(); i++) {
				JSONObject j = ja.getJSONObject(i);
				String _feature = j.getString("feature");
				int _selected = j.getInt("selected");

				PreparedStatement ps1;
				ps1 = connection.prepareStatement("UPDATE " + Table.selectedvalues + " SET selected=? WHERE roles_id="
						+ _roleid + " and feature=" + "'" + _feature + "'");
				ps1.setInt(1, _selected);
				ps1.executeUpdate();
				ps1.close();

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection.prepareStatement("SELECT values_id FROM " + Table.selectedvalues + " WHERE roles_id="
						+ _roleid + " and feature=" + "'" + _feature + "'");
				rs2 = ps2.executeQuery();

				if (rs2.next()) {
					_valueid = rs2.getInt("values_id");
				}
				ps2.close();
				rs2.close();

				if (j.has("sub_features")) {
					JSONArray js = j.getJSONArray("sub_features");
					for (int k = 0; k < js.length(); k++) {
						JSONObject sub = js.getJSONObject(k);

						String _subfeature = sub.getString("feature");
						int _subselected = sub.getInt("selected");

						PreparedStatement ps3;
						ps3 = connection.prepareStatement(
								"UPDATE " + Table.selectedsubvalues + " SET selected=? WHERE values_id=" + _valueid
										+ " and feature=" + "'" + _subfeature + "'");
						ps3.setInt(1, _subselected);
						ps3.executeUpdate();
						ps3.close();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "storeroles", method = "POST")
	public void storeRoles(JSONObject json, String username) throws Exception {
		int _sellerId;
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT sellerId FROM " + Table.users + " WHERE userName=" + "'"
					+ json.getString("sellerName") + "'");

			rs = ps.executeQuery();
			if (rs.next()) {
				_sellerId = rs.getInt("sellerId");
				PreparedStatement ps1;

				ps1 = connection.prepareStatement("DELETE FROM assignednames WHERE sellerId= ? ");
				ps1.setInt(1, _sellerId);
				ps1.execute();
				JSONArray ja = json.getJSONArray("roles");
				for (int i = 0; i < ja.length(); i++) {
					JSONObject each = ja.getJSONObject(i);
					int roleid = each.getInt("roleid");

					ps1.close();

					PreparedStatement ps2;
					ps2 = connection
							.prepareStatement("INSERT INTO " + Table.assignednames + "(sellerId,roles_id) values(?,?)");
					ps2.setInt(1, _sellerId);
					ps2.setInt(2, roleid);
					ps2.execute();
					ps2.close();
				}
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@ApiMethod(apiname = "getassignedroles", method = "POST")
	public String getAssignedRoles(JSONObject json, String username) throws Exception {
		String name;
		try (Connection connection = getDBConnectionPool()) {
			name = json.getString("sellerName");
			JSONArray resarray = new JSONArray();

			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT sellerId FROM " + Table.users + " WHERE userName=" + "'" + name + "'");
			rs = ps.executeQuery();
			if (rs.next()) {
				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement(
						"SELECT * FROM " + Table.assignednames + " where sellerId=" + rs.getInt("sellerId"));
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					JSONObject res = new JSONObject();
					PreparedStatement ps2;
					ResultSet rs2;
					ps2 = connection.prepareStatement(
							"SELECT * FROM " + Table.roles + " where roles_id=" + rs1.getInt("roles_id"));
					rs2 = ps2.executeQuery();
					if (rs2.next()) {
						res.put("roleid", rs2.getInt("roles_id"));
						res.put("rolename", rs2.getString("name"));
					}
					ps2.close();
					rs2.close();
					resarray.put(res);
				}
				ps1.close();
				rs1.close();
			}
			ps.close();
			rs.close();
			return resarray.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpermissions", method = "GET")
	public String getPermissions(String username) throws Exception {
		int values_id = 0;
		String feature = "";
		int sellerId;
		int[] permission = new int[100];
		try (Connection connection = getDBConnectionPool()) {

			ArrayList<String> featurename = new ArrayList<>();
			featurename = getFeatureName();
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT sellerId, userType FROM " + Table.users + " WHERE userName=" + "'" + username + "'");
			rs = ps.executeQuery();
			if (rs.next()) {

				if (rs.getString("userType").equals(UserType.SUPERUSER.toString())) {
					JSONObject allFeatures = new JSONObject();
					for (int i = 0; i < featurename.size(); i++) {
						allFeatures.put(featurename.get(i).replaceAll(" ", ""), 3);
					}
					return allFeatures.toString();
				}

				sellerId = rs.getInt("sellerId");

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection
						.prepareStatement("SELECT * FROM " + Table.assignednames + " WHERE sellerId=" + sellerId);
				rs1 = ps1.executeQuery();

				while (rs1.next()) {

					for (int k = 0; k < featurename.size(); k++) {
						PreparedStatement ps2;
						ResultSet rs2;
						ps2 = connection.prepareStatement("SELECT * FROM " + Table.selectedvalues + " WHERE roles_id="
								+ rs1.getInt("roles_id") + " and feature=" + "'" + featurename.get(k) + "'");
						rs2 = ps2.executeQuery();

						feature = featurename.get(k);
						if (rs2.next()) {
							values_id = rs2.getInt("values_id");
							if (permission[k] == 0) {
								permission[k] = rs2.getInt("selected");
							}
							if (permission[k] < rs2.getInt("selected")) {
								permission[k] = rs2.getInt("selected");
							}
						} else {

							PreparedStatement ps3;
							ResultSet rs3;
							ps3 = connection.prepareStatement("SELECT * FROM " + Table.selectedsubvalues
									+ " WHERE values_id=" + values_id + " AND feature=" + "'" + feature + "'");
							rs3 = ps3.executeQuery();
							if (rs3.next()) {
								if (permission[k] == 0) {
									permission[k] = rs3.getInt("selected");
								}
								if (permission[k] < rs3.getInt("selected")) {
									permission[k] = rs3.getInt("selected");
								}
							}
							ps3.close();
							rs3.close();
						}
						ps2.close();
						rs2.close();
					}
				}
				ps1.close();
				rs1.close();
			}
			ps.close();
			rs.close();

			JSONObject jj = new JSONObject();
			for (int k = 0; k < featurename.size(); k++) {
				jj.put(featurename.get(k).replaceAll(" ", ""), permission[k]);
			}
			return jj.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ArrayList<String> getFeatureName() {
		ArrayList<String> names = new ArrayList<>();
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.features);
			rs = ps.executeQuery();
			while (rs.next()) {
				names.add(rs.getString("feature"));
				if (rs.getInt("sub_feature_id") > 0) {
					PreparedStatement ps1;
					ResultSet rs1;
					ps1 = connection.prepareStatement(
							"SELECT * FROM " + Table.subfeatures + " WHERE id=" + rs.getInt("sub_feature_id"));
					rs1 = ps1.executeQuery();
					while (rs1.next()) {
						names.add(rs1.getString("feature"));
					}
					ps1.close();
					rs1.close();
				}
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return names;
	}

	public Map<String, Object> getPermissionMap(int userid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			Map<String, Integer> maxFeature = new HashMap<>();
			Map<String, String> featureMapper = new HashMap<>();

			ps = connection.prepareStatement("SELECT id, feature FROM " + Table.features);
			rs = ps.executeQuery();
			while (rs.next()) {
				maxFeature.put(String.valueOf(rs.getInt("id")), 0);
				featureMapper.put(rs.getString("feature"), String.valueOf(rs.getInt("id")));
			}
			ps.close();
			rs.close();

			Map<String, Integer> maxSubfeature = new HashMap<>();
			Map<String, String> subfeatureMapper = new HashMap<>();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT subfeatureId, feature FROM " + Table.subfeatures);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				maxSubfeature.put(String.valueOf(rs1.getInt("subfeatureId")), 0);
				subfeatureMapper.put(rs1.getString("feature"), String.valueOf(rs1.getInt("subfeatureId")));
			}
			ps1.close();
			rs1.close();

			PreparedStatement ps2;
			ResultSet rs2;
			ps2 = connection.prepareStatement("SELECT roles_id FROM " + Table.assignednames + " WHERE sellerId = ?");
			ps2.setInt(1, userid);
			rs2 = ps2.executeQuery();

			while (rs2.next()) {
				int roles_id = rs2.getInt("roles_id");

				PreparedStatement ps3;
				ResultSet rs3;
				ps3 = connection.prepareStatement(
						"SELECT values_id, feature, selected FROM " + Table.selectedvalues + " WHERE roles_id = ?");
				ps3.setInt(1, roles_id);
				rs3 = ps3.executeQuery();

				while (rs3.next()) {
					String featureId = featureMapper.get(rs3.getString("feature"));

					if (maxFeature.get(featureId) < rs3.getInt("selected"))
						maxFeature.put(featureId, rs3.getInt("selected"));

					PreparedStatement ps4;
					ResultSet rs4;
					ps4 = connection.prepareStatement(
							"SELECT feature, selected FROM " + Table.selectedsubvalues + " WHERE values_id = ?");
					ps4.setInt(1, rs3.getInt("values_id"));
					rs4 = ps4.executeQuery();

					while (rs4.next()) {
						String subfeatureId = subfeatureMapper.get(rs4.getString("feature"));

						if (maxSubfeature.get(subfeatureId) < rs4.getInt("selected"))
							maxSubfeature.put(subfeatureId, rs4.getInt("selected"));
					}
					ps4.close();
					rs4.close();
				}
				ps3.close();
				rs3.close();
			}
			ps2.close();
			rs2.close();

			Map<String, Object> map = new HashMap<>();
			map.put("feature", maxFeature);
			map.put("subfeature", maxSubfeature);
			return map;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean checkStdUserPermission(Map<String, Object> permission, String methodName, String className)
			throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT apiId FROM " + Table.apilist + " WHERE methodName = ?");
			// + " WHERE methodName = ? AND className = ?");
			ps.setString(1, methodName);
			// ps.setString(2, className);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				return true;
				// throw new Exception("api is not added in the 'apilist' table.");
			}

			int apiId = rs.getInt("apiId");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT * FROM " + Table.apicontrol + " WHERE apiId = ?");
			ps1.setInt(1, apiId);
			rs1 = ps1.executeQuery();
			if (!rs1.next()) {
				ps1.close();
				rs1.close();
				return true;
			}
			if (rs1.getObject("subfeatureId") == null) {
				int reqFeatureVal = rs1.getInt("reqFeatureVal");
				String featureId = String.valueOf(rs1.getInt("featureId"));
				Map<String, Integer> feature = (Map<String, Integer>) permission.get("feature");
				int actualFeatureval = feature.get(featureId);
				ps1.close();
				rs1.close();

				if (actualFeatureval >= reqFeatureVal)
					return true;
				else
					return false;
			} else {
				int reqSubfeatureVal = rs1.getInt("reqSubfeatureVal");
				String subfeatureId = String.valueOf(rs1.getInt("subfeatureId"));
				Map<String, Integer> subfeature = (Map<String, Integer>) permission.get("subfeature");
				int actualSubfeatureval = subfeature.get(subfeatureId);
				ps1.close();
				rs1.close();

				if (actualSubfeatureval >= reqSubfeatureVal)
					return true;
				else
					return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
