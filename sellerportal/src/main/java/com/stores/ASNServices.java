package com.stores;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.integration.IntegrationClient;
import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.SellerCodeConversion;
import com.vossa.api.v1.seller.pojos.ASN;
import com.vossa.api.v1.seller.pojos.ASNCaseLevel;
import com.vossa.api.v1.seller.pojos.ASNPalletLevel;
import com.vossa.api.v1.seller.pojos.ASNSkuLevel;
import com.vossa.api.v1.seller.pojos.User;

public class ASNServices extends ProductServices {

	public List<Map<String, Object>> getSkuWithName(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT product_id, sku FROM " + Table.productmapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			List<Map<String, Object>> productList = new ArrayList<>();
			while (rs.next()) {
				Map<String, Object> productCompl = getProduct(rs.getInt("product_id"));

				Map<String, Object> product = new HashMap<>();
				product.put("sku", productCompl.get("sku"));
				product.put("product_name", productCompl.get("product_name"));
				product.put("product_length", productCompl.get("product_length"));
				product.put("product_height", productCompl.get("product_height"));
				product.put("product_width", productCompl.get("product_width"));
				product.put("dimensions_UOM", productCompl.get("dimensions_UOM"));
				product.put("weight", productCompl.get("weight"));
				product.put("weight_UOM", productCompl.get("weight_UOM"));

				productList.add(product);
			}

			ps.close();
			rs.close();

			return productList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "generateasnnumber", method = "GET")
	public Map<String, String> generateASNNumber() throws Exception {
		try {
			Map<String, String> map = new HashMap<>();
			String asnNumber;
			while (true) {
				asnNumber = randomAlphanumeric(10);
				asnNumber = "ASN000" + asnNumber;
				if (checkASNNumber(asnNumber))
					break;
			}
			map.put("asnNumber", asnNumber);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "checkasnnumber", method = "POST")
	public boolean checkASNNumber(String asnNumber) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.asn + " WHERE asnNumber = ?");
			ps.setString(1, asnNumber);
			rs = ps.executeQuery();
			rs.next();
			int cnt = rs.getInt("cnt");
			rs.close();
			if (cnt == 0)
				return true;
			else
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "createasn", method = "POST")
	public ResponseClass createASN(ASN asn, int userid, int storeid, String createdBy) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);
			User user = getUserDetails(getUsernameById(userid));

			PreparedStatement ps;
			ps = connection.prepareStatement("INSERT INTO " + Table.asn
					+ " (asnNumber, sellerId, sellerName, sellerEmail, sellerPhone, storeId, asnType, shipmentType,"
					+ " deliveryFacility, referenceNumber, bolNumber, carrier, trackingNumber, shipmentDate, estimatedArrival,"
					+ " shipmentFrom, cogi, additionalNotes, asnStatus, createdBy, modifiedBy)"
					+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			int i = 0;
			ps.setString(++i, asn.getAsnNumber());
			ps.setInt(++i, userid);
			ps.setString(++i, user.getName());
			ps.setString(++i, user.getEmail());
			ps.setString(++i, user.getPhoneNumber());
			ps.setInt(++i, storeid);
			ps.setString(++i, asn.getAsnType());
			ps.setString(++i, asn.getShipmentType());
			ps.setString(++i, asn.getDeliveryFacility());
			ps.setString(++i, asn.getReferenceNumber());
			ps.setString(++i, asn.getBolNumber());
			ps.setString(++i, asn.getCarrier());
			ps.setString(++i, asn.getTrackingNumber());
			if (asn.getShipmentDate() != null)
				ps.setTimestamp(++i, new Timestamp(asn.getShipmentDate()));
			else
				ps.setTimestamp(++i, null);
			if (asn.getEstimatedArrival() != null)
				ps.setTimestamp(++i, new Timestamp(asn.getEstimatedArrival()));
			else
				ps.setTimestamp(++i, null);
			ps.setString(++i, asn.getShipmentFrom());
			ps.setObject(++i, asn.getCogi());
			ps.setString(++i, asn.getAdditionalNotes());

			String asnStatus;
			if (asn.getCarrier() == null || asn.getTrackingNumber() == null || asn.getShipmentDate() == null
					|| asn.getEstimatedArrival() == null)
				asnStatus = "Incomplete";
			else
				asnStatus = "Submitted";

			ps.setString(++i, asnStatus);
			ps.setString(++i, createdBy);
			ps.setString(++i, createdBy);
			ps.execute();
			ps.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT asnId FROM " + Table.asn + " WHERE asnNumber = ?");
			ps1.setString(1, asn.getAsnNumber());
			rs1 = ps1.executeQuery();
			rs1.next();
			int asnId = rs1.getInt("asnId");
			ps1.close();
			rs1.close();

			try {
				if (asn.getAsnType().equals("Pallet Level"))
					insertPalletList(asn.getPallets(), asn.getCases(), asnId, storeid, connection);
				else if (asn.getAsnType().equals("Case Level"))
					insertCaseList(asn.getCases(), asnId, storeid, connection);
				else if (asn.getAsnType().equals("SKU Level"))
					insertSkuList(asn.getSkus(), asnId, storeid, connection);
				else
					throw new Exception("Invalid ASN type");
			} catch (Exception e) {
				connection.rollback();
				throw e;
			}

			String sellerCode = SellerCodeConversion.convert(storeid);
			asn.setSellerCode(sellerCode);

			long asnDate = System.currentTimeMillis();
			asn.setAsnDate(asnDate);

			if (asnStatus.equals("Submitted")) {
				IntegrationClient client = new IntegrationClient();
				client.sendASN(asn);
			}

			System.out.println("New ASN successfully created");
			connection.commit();

			return new ResponseClass(201, "success", "New ASN successfully created");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void insertPalletList(List<ASNPalletLevel> palletObjList, List<ASNCaseLevel> caseObjList, int asnId,
			int storeId, Connection connection) throws Exception {
		try {
			Map<String, ASNCaseLevel> caseObjMap = new HashMap<>();
			for (ASNCaseLevel caseObj : caseObjList) {
				if (caseObjMap.containsKey(caseObj.getCaseLpn()))
					throw new Exception("Duplicate CaseLpn '" + caseObj.getCaseLpn() + "' value in the CaseList.");
				else
					caseObjMap.put(caseObj.getCaseLpn(), caseObj);
			}

			List<String> allCaseLpnLabels = new ArrayList<>();
			for (ASNPalletLevel palletObj : palletObjList) {
				for (Map<String, Object> map : palletObj.getCaseLpnLabels()) {
					String caseLpn = (String) map.get("caseLpn");
					if (!caseObjMap.containsKey(caseLpn))
						throw new Exception("Given caseLpnLabel '" + caseLpn + "' is not exists in the caseList.");

					if (allCaseLpnLabels.contains(caseLpn))
						throw new Exception("Given caseLpnLabel '" + caseLpn + "' in the palletLpn '"
								+ palletObj.getPalletLpn() + "' already exists in another Pallet.");
					else
						allCaseLpnLabels.add(caseLpn);
				}
			}

			for (ASNPalletLevel palletObj : palletObjList) {
				PreparedStatement ps;
				ps = connection.prepareStatement("INSERT INTO " + Table.asnpalletlevel
						+ " (asnId, palletLpn, palletLength, palletWidth, palletHeight, palletDimensionUOM,"
						+ " palletWeight, palletWeightUOM)" + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
				ps.setInt(1, asnId);
				ps.setString(2, palletObj.getPalletLpn());
				ps.setObject(3, palletObj.getPalletLength());
				ps.setObject(4, palletObj.getPalletWidth());
				ps.setObject(5, palletObj.getPalletHeight());
				ps.setString(6, palletObj.getPalletDimensionUOM());
				ps.setObject(7, palletObj.getPalletWeight());
				ps.setString(8, palletObj.getPalletWeightUOM());
				ps.execute();
				ps.close();

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement(
						"SELECT palletId FROM " + Table.asnpalletlevel + " WHERE asnId = ? AND palletLpn = ?");
				ps1.setInt(1, asnId);
				ps1.setString(2, palletObj.getPalletLpn());
				rs1 = ps1.executeQuery();
				rs1.next();
				int palletId = rs1.getInt("palletId");
				ps1.close();
				rs1.close();

				for (Map<String, Object> map : palletObj.getCaseLpnLabels()) {
					String caseLpn = (String) map.get("caseLpn");
					ASNCaseLevel caseObj = caseObjMap.get(caseLpn);
					caseObj.setPalletLpn(palletObj.getPalletLpn());

					PreparedStatement ps2;

					ps2 = connection.prepareStatement("INSERT INTO " + Table.asncaselevel
							+ " (asnId, palletId, caseLpn, caseLength, caseWidth, caseHeight, caseDimensionUOM,"
							+ " caseWeight, caseWeightUOM, specialTreatmentNeeded, isMaterialHazardous)"
							+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					ps2.setInt(1, asnId);
					ps2.setInt(2, palletId);
					ps2.setString(3, caseObj.getCaseLpn());
					ps2.setObject(4, caseObj.getCaseLength());
					ps2.setObject(5, caseObj.getCaseWeight());
					ps2.setObject(6, caseObj.getCaseHeight());
					ps2.setString(7, caseObj.getCaseDimensionUOM());
					ps2.setObject(8, caseObj.getCaseWeight());
					ps2.setString(9, caseObj.getCaseWeightUOM());
					ps2.setString(10, caseObj.getSpecialTreatmentNeeded());
					ps2.setString(11, caseObj.getIsMaterialHazardous());
					ps2.execute();
					ps2.close();

					PreparedStatement ps3;
					ResultSet rs3;

					ps3 = connection.prepareStatement("SELECT caseId FROM " + Table.asncaselevel
							+ " WHERE asnId = ? AND palletId = ? AND caseLpn = ?");
					ps3.setInt(1, asnId);
					ps3.setInt(2, palletId);
					ps3.setString(3, caseLpn);
					rs3 = ps3.executeQuery();
					rs3.next();
					int caseId = rs3.getInt("caseId");

					ps3.close();
					rs3.close();

					PreparedStatement ps4;
					ResultSet rs4;

					ps4 = connection.prepareStatement(
							"SELECT product_id FROM " + Table.productmapper + " WHERE sku = ? AND storeId = ?");
					ps4.setString(1, caseObj.getSku());
					ps4.setInt(2, storeId);
					rs4 = ps4.executeQuery();
					if (!rs4.next())
						throw new Exception("Given SKU not exists.");
					int product_id = rs4.getInt("product_id");

					ps4.close();
					rs4.close();

					PreparedStatement ps5;
					Map<String, Object> product = getProduct(product_id);

					ps5 = connection.prepareStatement("INSERT INTO " + Table.asnskulevel
							+ " (asnId, palletId, caseId, product_id, sku, product_name, quantity)"
							+ " VALUES (?, ?, ?, ?, ?, ?, ?)");
					ps5.setInt(1, asnId);
					ps5.setInt(2, palletId);
					ps5.setInt(3, caseId);
					ps5.setInt(4, product_id);
					ps5.setString(5, (String) product.get("sku"));
					ps5.setString(6, (String) product.get("product_name"));
					ps5.setInt(7, caseObj.getQuantity());
					ps5.execute();
					ps5.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void insertCaseList(List<ASNCaseLevel> caseObjList, int asnId, int storeId, Connection connection)
			throws Exception {
		try {
			List<String> allCaseLpn = new ArrayList<>();
			for (ASNCaseLevel caseObj : caseObjList) {
				if (allCaseLpn.contains(caseObj.getCaseLpn()))
					throw new Exception("Duplicate CaseLpn '" + caseObj.getCaseLpn() + "' value in the CaseList.");
				else
					allCaseLpn.add(caseObj.getCaseLpn());
			}

			for (ASNCaseLevel caseObj : caseObjList) {
				PreparedStatement ps;
				ps = connection.prepareStatement("INSERT INTO " + Table.asncaselevel
						+ " (asnId, caseLpn, caseLength, caseWidth, caseHeight, caseDimensionUOM,"
						+ " caseWeight, caseWeightUOM, specialTreatmentNeeded, isMaterialHazardous)"
						+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				ps.setInt(1, asnId);
				ps.setString(2, caseObj.getCaseLpn());
				ps.setObject(3, caseObj.getCaseLength());
				ps.setObject(4, caseObj.getCaseWeight());
				ps.setObject(5, caseObj.getCaseHeight());
				ps.setString(6, caseObj.getCaseDimensionUOM());
				ps.setObject(7, caseObj.getCaseWeight());
				ps.setString(8, caseObj.getCaseWeightUOM());
				ps.setString(9, caseObj.getSpecialTreatmentNeeded());
				ps.setString(10, caseObj.getIsMaterialHazardous());
				ps.execute();
				ps.close();

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection.prepareStatement(
						"SELECT caseId FROM " + Table.asncaselevel + " WHERE asnId = ? AND caseLpn = ?");
				ps1.setInt(1, asnId);
				ps1.setString(2, caseObj.getCaseLpn());
				rs1 = ps1.executeQuery();
				rs1.next();
				int caseId = rs1.getInt("caseId");

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection.prepareStatement(
						"SELECT product_id FROM " + Table.productmapper + " WHERE sku = ? AND storeId = ?");
				ps2.setString(1, caseObj.getSku());
				ps2.setInt(2, storeId);
				rs2 = ps2.executeQuery();
				if (!rs2.next())
					throw new Exception("Given SKU not exists.");
				int product_id = rs2.getInt("product_id");

				ps2.close();
				rs2.close();

				Map<String, Object> product = getProduct(product_id);

				PreparedStatement ps3;
				ps3 = connection.prepareStatement("INSERT INTO " + Table.asnskulevel
						+ " (asnId, caseId, product_id, sku, product_name, quantity)" + " VALUES (?, ?, ?, ?, ?, ?)");
				ps3.setInt(1, asnId);
				ps3.setInt(2, caseId);
				ps3.setInt(3, product_id);
				ps3.setString(4, (String) product.get("sku"));
				ps3.setString(5, (String) product.get("product_name"));
				ps3.setInt(6, caseObj.getQuantity());
				ps3.execute();
				ps3.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void insertSkuList(List<ASNSkuLevel> skuObjList, int asnId, int storeId, Connection connection)
			throws Exception {
		try {
			List<String> skuList = new ArrayList<>();
			for (ASNSkuLevel skuObj : skuObjList) {
				if (skuList.contains(skuObj.getSku()))
					throw new Exception("Duplicate sku '" + skuObj.getSku() + "' value in the SkuList.");
				else
					skuList.add(skuObj.getSku());
			}

			for (ASNSkuLevel skuObj : skuObjList) {
				PreparedStatement ps = connection.prepareStatement("INSERT INTO " + Table.asnskulevel
						+ " (asnId, product_id, sku, product_name, quantity, product_length, product_width,"
						+ " product_height, dimensions_UOM, weight, weight_UOM, status)"
						+ " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

				int product_id = getProductidBySku(skuObj.getSku(), storeId);
				Map<String, Object> product = getProduct(product_id);
				ps.setInt(1, asnId);
				ps.setInt(2, product_id);
				ps.setString(3, skuObj.getSku());
				ps.setString(4, (String) product.get("product_name"));
				ps.setInt(5, skuObj.getQuantity());
				ps.setObject(6, product.get("product_length"));
				ps.setObject(7, product.get("product_width"));
				ps.setObject(8, product.get("product_height"));
				ps.setString(9, (String) product.get("dimensions_UOM"));
				ps.setObject(10, product.get("weight"));
				ps.setString(11, (String) product.get("weight_UOM"));
				ps.setString(12, skuObj.getStatus());
				ps.execute();

				ps.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "editasnshipment", method = "PUT")
	public ResponseClass editASNShipment(ASN asn, int storeid, String modifiedBy) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT asnId, asnStatus FROM " + Table.asn + " WHERE asnNumber = ? AND storeId = ?");
			ps.setString(1, asn.getAsnNumber());
			ps.setInt(2, storeid);
			rs = ps.executeQuery();

			if (!rs.next())
				throw new Exception("Invalid ASN Number");

			if (!rs.getString("asnStatus").equals("Incomplete"))
				throw new Exception("ASN is already submitted can't edit.");

			int asnId = rs.getInt("asnId");

			ps.close();
			rs.close();

			String asnStatus;
			if (asn.getCarrier() == null || asn.getTrackingNumber() == null || asn.getShipmentDate() == null
					|| asn.getEstimatedArrival() == null)
				asnStatus = "Incomplete";
			else
				asnStatus = "Submitted";

			PreparedStatement ps1;
			ps1 = connection.prepareStatement(
					"UPDATE " + Table.asn + " SET asnStatus = ?, carrier = ?, trackingNumber = ?, shipmentDate = ?,"
							+ " estimatedArrival = ?, shipmentFrom = ?, modifiedBy = ?" + " WHERE asnId = ?");
			ps1.setString(1, asnStatus);
			ps1.setString(2, asn.getCarrier());
			ps1.setString(3, asn.getTrackingNumber());
			if (asn.getShipmentDate() != null)
				ps1.setTimestamp(4, new Timestamp(asn.getShipmentDate()));
			else
				ps1.setTimestamp(4, null);
			if (asn.getEstimatedArrival() != null)
				ps1.setTimestamp(5, new Timestamp(asn.getEstimatedArrival()));
			else
				ps1.setTimestamp(5, null);
			ps1.setString(6, asn.getShipmentFrom());
			ps1.setString(7, modifiedBy);
			ps1.setInt(8, asnId);
			ps1.executeUpdate();
			ps1.close();

			if (asnStatus.equals("Submitted")) {
				asn = getASN(storeid, asn);
				IntegrationClient client = new IntegrationClient();
				client.sendASN(asn);
			}

			connection.commit();
			return new ResponseClass(200, "success", "ASN Shipment details successfully updated");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ASN getASN(int storeid, ASN asn) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			String sellerCode = SellerCodeConversion.convert(storeid);

			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.asn + " WHERE storeId = ? AND asnNumber = ?");
			ps.setInt(1, storeid);
			ps.setString(2, asn.getAsnNumber());
			rs = ps.executeQuery();
			if (rs.next()) {
				int asnId = rs.getInt("asnId");

				asn.setAsnId(asnId);
				asn.setAsnNumber(rs.getString("asnNumber"));
				asn.setSellerId(rs.getInt("sellerId"));
				asn.setSellerCode(sellerCode);
				asn.setSellerName(rs.getString("sellerName"));
				asn.setSellerEmail(rs.getString("sellerEmail"));
				asn.setSellerPhone(rs.getString("sellerPhone"));
				asn.setAsnStatus(rs.getString("asnStatus"));
				asn.setAsnType(rs.getString("asnType"));
				asn.setShipmentType(rs.getString("shipmentType"));
				asn.setDeliveryFacility(rs.getString("deliveryFacility"));
				asn.setReferenceNumber(rs.getString("referenceNumber"));
				asn.setBolNumber(rs.getString("bolNumber"));
				// asn.setCarrier(rs.getString("carrier"));
				// asn.setTrackingNumber(rs.getString("trackingNumber"));
				//
				// if (rs.getTimestamp("shipmentDate") != null)
				// asn.setShipmentDate(rs.getTimestamp("shipmentDate").getTime());
				// if (rs.getTimestamp("estimatedArrival") != null)
				// asn.setEstimatedArrival(rs.getTimestamp("estimatedArrival").getTime());

				// asn.setShipmentFrom(rs.getString("shipmentFrom"));
				if (rs.getObject("cogi") != null)
					asn.setCogi(rs.getFloat("cogi"));
				asn.setAdditionalNotes(rs.getString("additionalNotes"));
				if (rs.getTimestamp("asnDate") != null)
					asn.setAsnDate(rs.getTimestamp("asnDate").getTime());

				if (rs.getString("asnType").equals("Pallet Level")) {
					ASN asnTemp = getPalletList(asnId, storeid, connection);
					asn.setPallets(asnTemp.getPallets());
					asn.setCases(asnTemp.getCases());
				} else if (asn.getAsnType().equals("Case Level")) {
					List<ASNCaseLevel> caseObjList = getCaseList(asnId, storeid, connection);
					asn.setCases(caseObjList);
				} else if (asn.getAsnType().equals("SKU Level")) {
					List<ASNSkuLevel> skuObjList = getSkuList(asnId, storeid, connection);
					asn.setSkus(skuObjList);
				} else
					throw new Exception("Invalid ASN type");
				ps.close();
				rs.close();
				return asn;

			} else {
				ps.close();
				rs.close();
				throw new Exception("Invalid ASN Number");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getallasn", method = "GET")
	public List<ASN> getAllASN(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.asn + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			List<ASN> asnList = new ArrayList<ASN>();
			while (rs.next()) {
				int asnId = rs.getInt("asnId");

				ASN asn = new ASN();
				asn.setAsnId(asnId);
				asn.setAsnNumber(rs.getString("asnNumber"));
				asn.setSellerId(rs.getInt("sellerId"));
				asn.setSellerName(rs.getString("sellerName"));
				asn.setSellerEmail(rs.getString("sellerEmail"));
				asn.setSellerPhone(rs.getString("sellerPhone"));
				asn.setAsnStatus(rs.getString("asnStatus"));
				asn.setAsnType(rs.getString("asnType"));
				asn.setShipmentType(rs.getString("shipmentType"));
				asn.setDeliveryFacility(rs.getString("deliveryFacility"));
				asn.setReferenceNumber(rs.getString("referenceNumber"));
				asn.setBolNumber(rs.getString("bolNumber"));
				asn.setCarrier(rs.getString("carrier"));
				asn.setTrackingNumber(rs.getString("trackingNumber"));

				if (rs.getTimestamp("shipmentDate") != null)
					asn.setShipmentDate(rs.getTimestamp("shipmentDate").getTime());
				if (rs.getTimestamp("estimatedArrival") != null)
					asn.setEstimatedArrival(rs.getTimestamp("estimatedArrival").getTime());

				asn.setShipmentFrom(rs.getString("shipmentFrom"));
				if (rs.getObject("cogi") != null)
					asn.setCogi(rs.getFloat("cogi"));
				asn.setAdditionalNotes(rs.getString("additionalNotes"));
				if (rs.getTimestamp("asnDate") != null)
					asn.setAsnDate(rs.getTimestamp("asnDate").getTime());

				if (rs.getString("asnType").equals("Pallet Level")) {
					ASN asnTemp = getPalletList(asnId, storeid, connection);
					asn.setPallets(asnTemp.getPallets());
					asn.setCases(asnTemp.getCases());
				} else if (asn.getAsnType().equals("Case Level")) {
					List<ASNCaseLevel> caseObjList = getCaseList(asnId, storeid, connection);
					asn.setCases(caseObjList);
				} else if (asn.getAsnType().equals("SKU Level")) {
					List<ASNSkuLevel> skuObjList = getSkuList(asnId, storeid, connection);
					asn.setSkus(skuObjList);
				} else
					throw new Exception("Invalid ASN type");

				asnList.add(asn);
			}
			ps.close();
			rs.close();

			Collections.sort(asnList, new SortByTimeAsnDate());
			int i = 1;
			for (ASN asn : asnList)
				asn.setList_id(i++);
			return asnList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<ASN> getSearchASN(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.asn
					+ " WHERE storeId = ? AND (asnNumber LIKE ? OR trackingNumber LIKE ?)");
			ps.setInt(1, storeid);
			ps.setString(2, "%" + searchText + "%");
			ps.setString(3, "%" + searchText + "%");
			rs = ps.executeQuery();

			List<ASN> asnList = new ArrayList<ASN>();
			while (rs.next()) {
				int asnId = rs.getInt("asnId");

				ASN asn = new ASN();
				asn.setAsnId(asnId);
				asn.setAsnNumber(rs.getString("asnNumber"));
				asn.setSellerId(rs.getInt("sellerId"));
				asn.setSellerName(rs.getString("sellerName"));
				asn.setSellerEmail(rs.getString("sellerEmail"));
				asn.setSellerPhone(rs.getString("sellerPhone"));
				asn.setAsnStatus(rs.getString("asnStatus"));
				asn.setAsnType(rs.getString("asnType"));
				asn.setShipmentType(rs.getString("shipmentType"));
				asn.setDeliveryFacility(rs.getString("deliveryFacility"));
				asn.setReferenceNumber(rs.getString("referenceNumber"));
				asn.setBolNumber(rs.getString("bolNumber"));
				asn.setCarrier(rs.getString("carrier"));
				asn.setTrackingNumber(rs.getString("trackingNumber"));

				if (rs.getTimestamp("shipmentDate") != null)
					asn.setShipmentDate(rs.getTimestamp("shipmentDate").getTime());
				if (rs.getTimestamp("estimatedArrival") != null)
					asn.setEstimatedArrival(rs.getTimestamp("estimatedArrival").getTime());

				asn.setShipmentFrom(rs.getString("shipmentFrom"));
				if (rs.getObject("cogi") != null)
					asn.setCogi(rs.getFloat("cogi"));
				asn.setAdditionalNotes(rs.getString("additionalNotes"));
				if (rs.getTimestamp("asnDate") != null)
					asn.setAsnDate(rs.getTimestamp("asnDate").getTime());

				if (rs.getString("asnType").equals("Pallet Level")) {
					ASN asnTemp = getPalletList(asnId, storeid, connection);
					asn.setPallets(asnTemp.getPallets());
					asn.setCases(asnTemp.getCases());
				} else if (asn.getAsnType().equals("Case Level")) {
					List<ASNCaseLevel> caseObjList = getCaseList(asnId, storeid, connection);
					asn.setCases(caseObjList);
				} else if (asn.getAsnType().equals("SKU Level")) {
					List<ASNSkuLevel> skuObjList = getSkuList(asnId, storeid, connection);
					asn.setSkus(skuObjList);
				} else
					throw new Exception("Invalid ASN type");

				asnList.add(asn);
			}

			ps.close();
			rs.close();

			Collections.sort(asnList, new SortByTimeAsnDate());
			int i = 1;
			for (ASN asn : asnList)
				asn.setList_id(i++);
			return asnList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private ASN getPalletList(int asnId, int storeid, Connection connection) throws Exception {
		try {
			PreparedStatement ps = connection
					.prepareStatement("SELECT * FROM " + Table.asnpalletlevel + " WHERE asnId = ?");
			ps.setInt(1, asnId);
			ResultSet rs = ps.executeQuery();

			List<ASNPalletLevel> palletObjList = new ArrayList<>();
			List<ASNCaseLevel> caseObjList = new ArrayList<>();
			while (rs.next()) {
				int palletId = rs.getInt("palletId");
				String palletLpn = rs.getString("palletLpn");

				ASNPalletLevel palletObj = new ASNPalletLevel();
				palletObj.setPalletLpn(palletLpn);
				if (rs.getObject("palletLength") != null)
					palletObj.setPalletLength(rs.getFloat("palletLength"));
				if (rs.getObject("palletWidth") != null)
					palletObj.setPalletWidth(rs.getFloat("palletWidth"));
				if (rs.getObject("palletHeight") != null)
					palletObj.setPalletHeight(rs.getFloat("palletHeight"));
				palletObj.setPalletDimensionUOM(rs.getString("palletDimensionUOM"));
				if (rs.getObject("palletWeight") != null)
					palletObj.setPalletWeight(rs.getFloat("palletWeight"));
				palletObj.setPalletWeightUOM(rs.getString("palletWeightUOM"));

				PreparedStatement ps1 = connection
						.prepareStatement("SELECT * FROM " + Table.asncaselevel + " WHERE asnId = ? AND palletId = ?");
				ps1.setInt(1, asnId);
				ps1.setInt(2, palletId);
				ResultSet rs1 = ps1.executeQuery();

				List<Map<String, Object>> caseLpnLabels = new ArrayList<>();
				while (rs1.next()) {
					int caseId = rs1.getInt("caseId");
					Map<String, Object> caseLpnLabel = new HashMap<>();
					caseLpnLabel.put("caseLpn", rs1.getString("caseLpn"));
					caseLpnLabels.add(caseLpnLabel);

					ASNCaseLevel caseObj = new ASNCaseLevel();
					caseObj.setPalletLpn(palletLpn);
					caseObj.setCaseLpn(rs1.getString("caseLpn"));
					if (rs1.getObject("caseLength") != null)
						caseObj.setCaseLength(rs1.getFloat("caseLength"));
					if (rs1.getObject("caseWidth") != null)
						caseObj.setCaseWidth(rs1.getFloat("caseWidth"));
					if (rs1.getObject("caseHeight") != null)
						caseObj.setCaseHeight(rs1.getFloat("caseHeight"));
					caseObj.setCaseDimensionUOM(rs1.getString("caseDimensionUOM"));
					if (rs1.getObject("caseWeight") != null)
						caseObj.setCaseWeight(rs1.getFloat("caseWeight"));
					caseObj.setCaseWeightUOM(rs1.getString("caseWeightUOM"));
					caseObj.setSpecialTreatmentNeeded(rs1.getString("specialTreatmentNeeded"));
					caseObj.setIsMaterialHazardous(rs1.getString("isMaterialHazardous"));

					PreparedStatement ps2 = connection
							.prepareStatement("SELECT * FROM " + Table.asnskulevel + " WHERE asnId = ? AND caseId = ?");
					ps2.setInt(1, asnId);
					ps2.setInt(2, caseId);
					ResultSet rs2 = ps2.executeQuery();
					if (rs2.next()) {
						caseObj.setSku(rs2.getString("sku"));
						caseObj.setProduct_name(rs2.getString("product_name"));
						caseObj.setQuantity(rs2.getInt("quantity"));
					} else
						throw new Exception("SKU or caseLpn doesn't exist in the ASNSkuLevel.");
					ps2.close();
					rs2.close();
					caseObjList.add(caseObj);
				}

				ps1.close();
				rs1.close();

				palletObj.setCaseLpnLabels(caseLpnLabels);
				palletObjList.add(palletObj);
			}

			ps.close();
			rs.close();
			ASN asn = new ASN();
			asn.setPallets(palletObjList);
			asn.setCases(caseObjList);
			return asn;
		} catch (Exception e) {
			throw e;
		}
	}

	private List<ASNCaseLevel> getCaseList(int asnId, int storeId, Connection connection) throws Exception {
		try {
			PreparedStatement ps = connection
					.prepareStatement("SELECT * FROM " + Table.asncaselevel + " WHERE asnId = ?");
			ps.setInt(1, asnId);
			ResultSet rs = ps.executeQuery();

			List<ASNCaseLevel> caseObjList = new ArrayList<>();
			while (rs.next()) {
				int caseId = rs.getInt("caseId");

				ASNCaseLevel caseObj = new ASNCaseLevel();
				caseObj.setCaseLpn(rs.getString("caseLpn"));
				if (rs.getObject("caseLength") != null)
					caseObj.setCaseLength(rs.getFloat("caseLength"));
				if (rs.getObject("caseWidth") != null)
					caseObj.setCaseWidth(rs.getFloat("caseWidth"));
				if (rs.getObject("caseHeight") != null)
					caseObj.setCaseHeight(rs.getFloat("caseHeight"));
				caseObj.setCaseDimensionUOM(rs.getString("caseDimensionUOM"));
				if (rs.getObject("caseWeight") != null)
					caseObj.setCaseWeight(rs.getFloat("caseWeight"));
				caseObj.setCaseWeightUOM(rs.getString("caseWeightUOM"));
				caseObj.setSpecialTreatmentNeeded(rs.getString("specialTreatmentNeeded"));
				caseObj.setIsMaterialHazardous(rs.getString("isMaterialHazardous"));

				PreparedStatement ps1 = connection
						.prepareStatement("SELECT * FROM " + Table.asnskulevel + " WHERE asnId = ? AND caseId = ?");
				ps1.setInt(1, asnId);
				ps1.setInt(2, caseId);
				ResultSet rs1 = ps1.executeQuery();
				if (rs1.next()) {
					caseObj.setSku(rs1.getString("sku"));
					caseObj.setProduct_name(rs1.getString("product_name"));
					caseObj.setQuantity(rs1.getInt("quantity"));
				} else
					throw new Exception("SKU or caseLpn doesn't exist in the ASNSkuLevel.");

				ps1.close();
				rs1.close();
				caseObjList.add(caseObj);
			}
			ps.close();
			rs.close();
			return caseObjList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private List<ASNSkuLevel> getSkuList(int asnId, int storeid, Connection connection) throws Exception {
		try {
			PreparedStatement ps = connection
					.prepareStatement("SELECT * FROM " + Table.asnskulevel + " WHERE asnId = ?");
			ps.setInt(1, asnId);
			ResultSet rs = ps.executeQuery();

			List<ASNSkuLevel> skuObjList = new ArrayList<>();
			while (rs.next()) {
				ASNSkuLevel skuObj = new ASNSkuLevel();
				skuObj.setAsnId(asnId);
				skuObj.setProduct_id(rs.getInt("product_id"));
				skuObj.setSku(rs.getString("sku"));
				skuObj.setProduct_name(rs.getString("product_name"));
				skuObj.setQuantity(rs.getInt("quantity"));
				skuObj.setProduct_length((Float) rs.getObject("product_length"));
				skuObj.setProduct_width((Float) rs.getObject("product_width"));
				skuObj.setProduct_height((Float) rs.getObject("product_height"));
				skuObj.setDimensions_UOM(rs.getString("dimensions_UOM"));
				skuObj.setWeight((Float) rs.getObject("weight"));
				skuObj.setWeight_UOM(rs.getString("weight_UOM"));

				skuObjList.add(skuObj);
			}
			ps.close();
			rs.close();
			return skuObjList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpageasn", method = "POST")
	public List<ASN> getPageASN(int storeid, int startIndex, int load) throws Exception {
		try {
			List<ASN> allASNList = getAllASN(storeid);
			List<ASN> asnList = new ArrayList<>();

			for (ASN asn : allASNList) {
				int list_id = (int) asn.getList_id();
				if (list_id >= startIndex && list_id < startIndex + load)
					asnList.add(asn);
			}
			return asnList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpagesearchasn", method = "POST")
	public List<ASN> getPageSearchASN(String searchText, int storeid, int startIndex, int load) throws Exception {
		try {
			List<ASN> allASNList = getSearchASN(searchText, storeid);
			List<ASN> asnList = new ArrayList<>();

			for (ASN asn : allASNList) {
				int list_id = (int) asn.getList_id();
				if (list_id >= startIndex && list_id < startIndex + load)
					asnList.add(asn);
			}
			return asnList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getasncount", method = "GET")
	public int getASNCount(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.asn + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			rs.next();
			int count = rs.getInt("cnt");

			ps.close();
			rs.close();
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getsearchasncount", method = "GET")
	public int getSearchASNCount(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.asn
					+ " WHERE storeId = ? AND (asnNumber LIKE ? OR trackingNumber LIKE ?)");
			ps.setInt(1, storeid);
			ps.setString(2, "%" + searchText + "%");
			ps.setString(3, "%" + searchText + "%");
			rs = ps.executeQuery();
			rs.next();
			int count = rs.getInt("cnt");

			ps.close();
			rs.close();
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "deleteasn", method = "POST")
	public ResponseClass deleteASN(String asnNumber) throws Exception {
		try (Connection connection = getDBConnection()) {
			connection.setAutoCommit(false);
			int asnId = getASNid(asnNumber);
			PreparedStatement ps;
			ps = connection.prepareStatement("DELETE FROM " + Table.asnskulevel + " WHERE asnId = ?");
			ps.setInt(1, asnId);
			ps.execute();
			ps.close();

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("DELETE FROM " + Table.asncaselevel + " WHERE asnId = ?");
			ps1.setInt(1, asnId);
			ps1.execute();
			ps1.close();

			PreparedStatement ps2;
			ps2 = connection.prepareStatement("DELETE FROM " + Table.asnpalletlevel + " WHERE asnId = ?");
			ps2.setInt(1, asnId);
			ps2.execute();
			ps2.close();

			PreparedStatement ps3;
			ps3 = connection.prepareStatement("DELETE FROM " + Table.asn + " WHERE asnId = ?");
			ps3.setInt(1, asnId);
			ps3.execute();
			ps3.close();
			connection.commit();

			return new ResponseClass(200, "success", "Successfully deleted the selected ASN");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String getASNNumber(int asnId) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnectionPool()) {
			ps = connection.prepareStatement("SELECT asnNumber FROM " + Table.asn + " WHERE asnId = ?");
			ps.setInt(1, asnId);
			rs = ps.executeQuery();

			String asnNumber = null;
			if (rs.next()) {
				asnNumber = rs.getString("asnNumber");
			}

			ps.close();
			rs.close();
			return asnNumber;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int getASNid(String asnNumber) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnectionPool()) {
			ps = connection.prepareStatement("SELECT asnId FROM " + Table.asn + " WHERE asnNumber = ?");
			ps.setString(1, asnNumber);
			rs = ps.executeQuery();

			int asnId = 0;
			if (rs.next()) {
				asnId = rs.getInt("asnId");
			}
			ps.close();
			rs.close();
			return asnId;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String randomAlphanumeric(int length) {
		// final String alphanumericText =
		// "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		final String alphanumericText = "0123456789";
		SecureRandom random = new SecureRandom();

		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(alphanumericText.charAt(random.nextInt(alphanumericText.length())));
		return sb.toString();
	}
}

class SortByTimeAsnDate implements Comparator<ASN> {

	@Override
	public int compare(ASN o1, ASN o2) {
		long t1 = o1.getAsnDate();
		long t2 = o2.getAsnDate();

		if (t2 > t1)
			return 1;
		else if (t2 == t1)
			return 0;
		else
			return -1;
	}
}
