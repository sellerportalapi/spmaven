package com.stores;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.vossa.api.general.annotations.filterannotations.Secured;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.token.UserClaims;

@Path("")
public class DashboardResources {

	private DashboardServices service;

	public DashboardResources() {
		service = new DashboardServices();
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("getdashboarddetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getDashboardDetails(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getDashboardDetails(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@GET
	@Secured({ UserType.SUPERUSER, UserType.STANDARDUSER })
	@Path("gethomedashboarddetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Object getHomeDashboardDetails(@Context ContainerRequestContext context) {
		try {
			UserClaims userClaims = (UserClaims) context.getProperty("user");
			int storeid = userClaims.getStoreId();

			return service.getHomeDashboardDetails(storeid);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
}
