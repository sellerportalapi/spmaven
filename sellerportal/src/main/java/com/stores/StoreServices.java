package com.stores;

import java.io.IOException;
import java.net.URL;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.general.enums.PswdJwtType;
import com.vossa.api.general.enums.UserType;
import com.vossa.api.general.policies.PasswordPolicy;
import com.vossa.api.others.DBConnection;
import com.vossa.api.others.ResponseClass;
import com.vossa.api.others.SellerCodeConversion;
import com.vossa.api.others.encryption.AES;
import com.vossa.api.others.filehandlers.ConfigFileHandler;
import com.vossa.api.others.mailservices.SellerMailService;
import com.vossa.api.others.token.JWT;
import com.vossa.api.others.token.JWTHandler;
import com.vossa.api.others.token.UserClaims;
import com.vossa.api.server.DataSourceFactory;
import com.vossa.api.v1.common.pojos.Address;
import com.vossa.api.v1.seller.pojos.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;

public class StoreServices {

	public StoreServices() {
	}

	protected Connection getDBConnection() throws Exception {
		DBConnection dbConnection = new DBConnection();
		Connection connection = dbConnection.getConnection();
		dbConnection = null;
		return connection;
	}

	protected Connection getDBConnectionPool() throws Exception {
		return DataSourceFactory.getInstance().getConnection();
	}

	public Map<String, Object> getDBDetails() {

		try {
			Map<String, Object> map = new HashMap<>();

			try (Connection connection = getDBConnection()) {
				map.put("normalconnurl", connection.getMetaData().getURL());
				map.put("normalconnuser", connection.getMetaData().getUserName());
			}
			try (Connection connection = getDBConnectionPool()) {
				map.put("poolconnurl", connection.getMetaData().getURL());
				map.put("poolconnuser", connection.getMetaData().getUserName());
			}
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@ApiMethod(apiname = "storelogin", method = "POST")
	public ResponseClass storeLogin(String username, String password, String origin) {
		try (Connection connection = getDBConnection()) {
			long curr_time = System.currentTimeMillis();
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.users + " WHERE userName = ?");
			ps.setString(1, username);
			rs = ps.executeQuery();

			if (!rs.next()) {
				ps.close();
				rs.close();
				return new ResponseClass(401, "fail", "Invalid Username/Password",
						"Please check the Username/Password");
			}

			String plainPassword = AES.decrypt(rs.getString("password"));
			int userid = rs.getInt("sellerId");
			String userType = rs.getString("userType");
			String name = rs.getString("name");
			String sellerStatus = rs.getString("sellerStatus");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT * FROM " + Table.stores + " T1 JOIN " + Table.storeusermapper
					+ " T3 ON T1.storeId = T3.storeId JOIN " + Table.users + " T2 ON T2.sellerId = T3.sellerId"
					+ " WHERE T3.sellerId = ?");
			ps1.setInt(1, userid);
			rs1 = ps1.executeQuery();
			rs1.next();

			int storeid = rs1.getInt("storeId");
			String storename = rs1.getString("storeName");
			String storeStatus = rs1.getString("storeStatus");
			ps1.close();
			rs1.close();

			if (storeStatus.equals("INACTIVE"))
				return new ResponseClass(403, "fail", "Your store is currently inactive",
						"Please contact your VERT admin");

			if (sellerStatus.equals("INACTIVE"))
				return new ResponseClass(403, "fail", "Your account is currently inactive",
						"Please contact your store admin or VERT admin");

			if (isAccountLocked(userid, curr_time)) {
				return new ResponseClass(403, "fail", "Your account is currently locked for some time",
						"Please wait for some time, your account will be automatically unlocked.");
			}

			if (plainPassword.equals(password)) {
				loginSucceed(userid, curr_time);
			} else {
				loginFailed(userid, curr_time);
				return new ResponseClass(401, "fail", "Invalid Username/Password",
						"Please check the Username/Password");
			}

			UserClaims userClaims = new UserClaims(userid, username, userType, storeid, storename);

			if (userType.equals(UserType.STANDARDUSER.toString())) {
				Map<String, Object> permission = new RoleServices().getPermissionMap(userid);
				userClaims.setPermission(permission);
			}

			JWT jwt = getJWTCreator(userClaims, origin);
			String token = JWTHandler.buildJWT(jwt);

			Timestamp issueTime = new Timestamp(jwt.getIat());
			setTokenIssuedAt(issueTime, username);

			return new ResponseClass(200, "success", "Login successful", token, userType, storename, name, username);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}

	@ApiMethod(apiname = "createuser", method = "POST")
	public ResponseClass createUser(User user, int storeid, String createdBy) {
		try (Connection connection = getDBConnection()) {
			boolean validUniqueData;
			validUniqueData = checkFieldUser("userName", user.getUserName());
			if (validUniqueData == false) {
				throw new Exception("UserName already exists");
			}

			validUniqueData = checkFieldUser("email", user.getEmail());
			if (validUniqueData == false) {
				throw new Exception("User Email already exists");
			}

			validUniqueData = checkFieldUser("phoneNumber", user.getPhoneNumber());
			if (validUniqueData == false) {
				throw new Exception("User PhoneNumber already exists");
			}

			String cipherPassword = AES.encrypt(randomAlphanumeric(12));

			PreparedStatement ps;
			ps = connection.prepareStatement(
					"INSERT INTO " + Table.users + "(userName, password, name, email, phoneNumber, userType,"
							+ " isCreatedByVert, isFirstUser, createdBy, modifiedBy)"
							+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			int i = 0;
			ps.setString(++i, user.getUserName());
			ps.setString(++i, cipherPassword);
			ps.setString(++i, user.getName());
			ps.setString(++i, user.getEmail());

			if (user.getPhoneNumber() != null && user.getPhoneNumber().isEmpty())
				ps.setString(++i, null);
			else
				ps.setString(++i, user.getPhoneNumber());

			ps.setString(++i, user.getUserType());
			ps.setBoolean(++i, false); // isCreatedByVert is false
			ps.setBoolean(++i, false);
			ps.setString(++i, createdBy);
			ps.setString(++i, createdBy);
			ps.execute();
			ps.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT sellerId FROM " + Table.users + " WHERE  userName = ?");
			ps1.setString(1, user.getUserName());
			rs1 = ps1.executeQuery();
			rs1.next();
			int sellerid = rs1.getInt("sellerId");
			ps1.close();
			rs1.close();

			PreparedStatement ps2;
			ps2 = connection.prepareStatement("INSERT INTO " + Table.storeusermapper + " VALUES (?, ?)");
			ps2.setInt(1, sellerid);
			ps2.setInt(2, storeid);
			ps2.execute();
			ps2.close();

			PreparedStatement ps3;
			ps3 = connection.prepareStatement("INSERT INTO " + Table.userrecentpassword + " VALUES (?, ?, ?, ?)");
			ps3.setInt(1, sellerid);
			ps3.setString(2, cipherPassword);
			ps3.setString(3, cipherPassword);
			ps3.setString(4, cipherPassword);
			ps3.execute();
			ps3.close();

			PreparedStatement ps4;
			ps4 = connection.prepareStatement("INSERT INTO " + Table.userlogindetails + " VALUES (?, ?, ?, ?, ?, ?)");
			ps4.setInt(1, sellerid);
			ps4.setInt(2, 0);
			ps4.setTimestamp(3, null);
			ps4.setTimestamp(4, null);
			ps4.setTimestamp(5, null);
			ps4.setTimestamp(6, null);
			ps4.execute();
			ps4.close();
			System.out.println("New USER Created");

			return new ResponseClass(201, "success", "New USER successfully created or registered");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "USER not successfully created or registered");
		}
	}

	@ApiMethod(apiname = "updateuser", method = "PUT")
	public ResponseClass updateUser(User user, String modifiedBy) {
		PreparedStatement ps;
		try (Connection connection = getDBConnection()) {
			int userid = getUseridByName(user.getUserName());

			boolean validUniqueData;
			validUniqueData = checkFieldUserOnUpdate(userid, "email", user.getEmail());
			if (validUniqueData == false) {
				throw new Exception("User Email already exists");
			}

			validUniqueData = checkFieldUserOnUpdate(userid, "phoneNumber", user.getPhoneNumber());
			if (validUniqueData == false) {
				throw new Exception("User PhoneNumber already exists");
			}

			ps = connection.prepareStatement("UPDATE " + Table.users
					+ " SET name = ?, email = ?, phoneNumber = ?, modifiedBy = ?" + " WHERE sellerId = ?");
			ps.setString(1, user.getName());
			ps.setString(2, user.getEmail());

			if (user.getPhoneNumber() != null && user.getPhoneNumber().isEmpty())
				ps.setString(3, null);
			else
				ps.setString(3, user.getPhoneNumber());

			ps.setString(4, modifiedBy);
			ps.setInt(5, userid);

			ps.execute();
			ps.close();
			System.out.println("user details updated");

			return new ResponseClass(201, "success", "Your details has been updated");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	@ApiMethod(apiname = "getallstoreusers", method = "GET")
	public List<Map<String, Object>> getAllStoreUsers(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet sellerRS;
			ps = connection.prepareStatement("SELECT sellerId FROM " + Table.storeusermapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			sellerRS = ps.executeQuery();

			List<Integer> allIds = new ArrayList<>();
			while (sellerRS.next())
				allIds.add(sellerRS.getInt("sellerId"));
			ps.close();
			sellerRS.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT * FROM " + Table.users);
			rs1 = ps1.executeQuery();

			List<Map<String, Object>> allUsers = new ArrayList<>();
			while (rs1.next()) {
				int id = rs1.getInt("sellerId");
				if (!allIds.contains(id))
					continue;

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection
						.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.assignednames + " WHERE sellerId = ?");
				ps2.setInt(1, id);
				rs2 = ps2.executeQuery();
				rs2.next();
				int rolesCount = rs2.getInt("cnt");

				Map<String, Object> userDetails = new HashMap<>();
				userDetails.put("name", rs1.getString("name"));
				userDetails.put("userName", rs1.getString("userName"));
				userDetails.put("userType", rs1.getString("userType"));
				userDetails.put("email", rs1.getString("email"));

				String phoneNumber = rs1.getString("phoneNumber");
				if (phoneNumber != null) {
					phoneNumber = phoneNumber.trim();
					if (!phoneNumber.isEmpty())
						userDetails.put("phoneNumber", phoneNumber);
					else
						userDetails.put("phoneNumber", null);
				} else
					userDetails.put("phoneNumber", null);

				userDetails.put("sellerStatus", rs1.getString("sellerStatus"));
				userDetails.put("assignedRolesCount", rolesCount);
				userDetails.put("createdOn", rs1.getTimestamp("createdOn").getTime());
				allUsers.add(userDetails);
			}

			Collections.sort(allUsers, new SortByCreatedOn("createdOn"));
			int i = 1;
			for (Map<String, Object> product : allUsers)
				product.put("list_id", i++);

			return allUsers;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "checkfielduser", method = "POST")
	public boolean checkFieldUser(String key, String value) throws Exception {
		int count = 0;
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT COUNT(*) as cnt FROM " + Table.users + " WHERE " + key + " = ?");
			ps.setString(1, value);
			rs = ps.executeQuery();
			rs.next();
			count += rs.getInt("cnt");
			ps.close();
			rs.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return (count == 0) ? true : false;
	}

	public boolean checkFieldUserOnUpdate(int userid, String key, String value) throws Exception {
		int count = 0;
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT COUNT(*) as cnt FROM " + Table.users + " WHERE sellerId <> ? AND " + key + " = ?");
			ps.setInt(1, userid);
			ps.setString(2, value);
			rs = ps.executeQuery();
			rs.next();
			count += rs.getInt("cnt");

			rs.close();
			ps.close();
			return (count == 0) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "resetpassword", method = "PUT")
	public ResponseClass resetPassword(String username, String currentPassword, String newPassword) {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT sellerId, password FROM " + Table.users + " WHERE userName = ?");
			ps.setString(1, username);
			rs = ps.executeQuery();
			rs.next();

			int userid = rs.getInt("sellerId");
			String plainPassword = AES.decrypt(rs.getString("password"));
			ps.close();
			rs.close();

			if (!plainPassword.equals(currentPassword))
				return new ResponseClass(403, "fail", "Wrong password",
						"Password entered doesn't match with existing password");

			PasswordPolicy.validatePassword(newPassword);
			checkRecentPassword(newPassword, userid);

			String cipherPassword = AES.encrypt(newPassword);

			PreparedStatement ps1;
			ps1 = connection.prepareStatement(
					"UPDATE " + Table.users + " SET password = ?, modifiedBy = ?" + " WHERE userName = ?");
			ps1.setString(1, cipherPassword);
			ps1.setString(2, username);
			ps1.setString(3, username);
			ps1.execute();
			ps1.close();
			ps1.close();

			updateRecentPassword(newPassword, userid);
			System.out.println("Password reset");

			return new ResponseClass(201, "success", "Password has been successfully reset");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	@ApiMethod(apiname = "updateuserstatus", method = "PUT")
	public ResponseClass updateUserStatus(int userid, String sellerStatus, String modifiedBy, String origin) {
		try (Connection connection = getDBConnection()) {
			if (!sellerStatus.equals("ACTIVE") && !sellerStatus.equals("INACTIVE"))
				return new ResponseClass(400, "fail", "Invalid JSON input", "Invalid sellerStatus");
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT storeId FROM " + Table.storeusermapper + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();
			int storeid = rs.getInt("storeId");
			ps.close();
			rs.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT COUNT(*) AS activeUsersCount, T3.allowedUsersCount FROM "
					+ Table.users + " T1 JOIN " + Table.storeusermapper + " T2 ON T1.sellerId = T2.sellerId JOIN "
					+ Table.stores + " T3 ON T2.storeId = T3.storeId"
					+ " WHERE T2.storeId = ? AND T1.sellerStatus = 'ACTIVE'");
			ps1.setInt(1, storeid);
			rs1 = ps1.executeQuery();
			rs1.next();
			int activeUsersCount = rs1.getInt("activeUsersCount");
			int allowedUsersCount = rs1.getInt("allowedUsersCount");
			ps1.close();
			rs1.close();

			PreparedStatement ps2;
			ResultSet rs2;
			ps2 = connection.prepareStatement("SELECT * FROM " + Table.users + " WHERE sellerId = ?");
			ps2.setInt(1, userid);
			rs2 = ps2.executeQuery();
			rs2.next();
			String userType = rs2.getString("userType");
			ps2.close();
			rs2.close();

			if (sellerStatus.equals("ACTIVE") && userType.equals(UserType.STANDARDUSER.toString())) {
				PreparedStatement ps3;
				ResultSet rs3;
				ps3 = connection.prepareStatement(
						"SELECT COUNT(*) AS assignedRolesCount FROM " + Table.assignednames + " WHERE sellerId = ?");
				ps3.setInt(1, userid);
				rs3 = ps3.executeQuery();
				rs3.next();
				int assignedRolesCount = rs3.getInt("assignedRolesCount");
				ps3.close();
				rs3.close();

				if (assignedRolesCount == 0) {
					String errmsg = "No roles asigned for this STANDARDUSER";
					return new ResponseClass(409, "fail", errmsg, "Invalid user activation");
				}
			}

			if (sellerStatus.equals("ACTIVE")) {
				if (activeUsersCount + 1 > allowedUsersCount) {
					String errmsg = "License limit exceeded.";
					return new ResponseClass(409, "fail", errmsg, "Invalid user activation");
				}

				checkUserActivation(userid, origin);
			} else if (sellerStatus.equals("INACTIVE")) {
				if (activeUsersCount - 1 < 1) {
					String errmsg = "Atleast one user should be active in a store.";
					return new ResponseClass(409, "fail", errmsg, "Invalid user deactivation");
				}
			}

			PreparedStatement ps4;
			ps4 = connection.prepareStatement(
					"UPDATE " + Table.users + " SET sellerStatus = ?, modifiedBy = ?" + " WHERE sellerId = ?");
			ps4.setString(1, sellerStatus);
			ps4.setString(2, modifiedBy);
			ps4.setInt(3, userid);
			ps4.executeUpdate();
			ps4.close();

			return new ResponseClass(201, "success", "USER_STATUS has been updated successfully");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	@ApiMethod(apiname = "getuserdetails", method = "GET")
	public User getUserDetails(String username) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.users + " WHERE userName = ? ");
			ps.setString(1, username);
			rs = ps.executeQuery();

			if (rs.next()) {
				User user = new User();
				user.setSellerId(rs.getInt("sellerId"));
				user.setUserName(rs.getString("userName"));
				user.setName(rs.getString("name"));
				user.setEmail(rs.getString("email"));

				String phoneNumber = rs.getString("phoneNumber");
				if (phoneNumber != null) {
					phoneNumber = phoneNumber.trim();
					if (!phoneNumber.isEmpty())
						user.setPhoneNumber(phoneNumber);
				}

				user.setUserType(rs.getString("userType"));
				user.setSellerStatus(rs.getString("sellerStatus"));
				user.setFirstlogin(rs.getBoolean("firstlogin"));
				user.setIsFirstUser(rs.getBoolean("isFirstUser"));
				user.setIsActivated(rs.getBoolean("isActivated"));
				user.setIsCreatedByVert(rs.getBoolean("isCreatedByVert"));

				int sellerId = rs.getInt("sellerId");
				ps.close();
				rs.close();

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection
						.prepareStatement("SELECT storeId FROM " + Table.storeusermapper + " WHERE sellerId = ? ");
				ps1.setInt(1, sellerId);
				rs1 = ps1.executeQuery();
				rs1.next();

				int storeId = rs1.getInt("storeId");
				user.setStoreId(storeId);
				ps1.close();
				rs1.close();

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection.prepareStatement("SELECT storeName FROM " + Table.stores + " WHERE storeId = ? ");
				ps2.setInt(1, storeId);
				rs2 = ps2.executeQuery();
				rs2.next();

				user.setStoreName(rs2.getString("storeName"));
				user.setSellerCode(SellerCodeConversion.convert(storeId));
				ps2.close();
				rs2.close();
				return user;
			} else
				throw new Exception("Invalid username or userid");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "edituserdetails", method = "POST")
	public ResponseClass editUserDetails(String username, String name, String email, String phonenumber,
			String modifiedBy) {
		try (Connection connection = getDBConnection()) {
			int userid = getUseridByName(username);

			boolean validUniqueData;
			validUniqueData = checkFieldUserOnUpdate(userid, "email", email);
			if (validUniqueData == false) {
				throw new Exception("User Email already exists");
			}

			validUniqueData = checkFieldUserOnUpdate(userid, "phoneNumber", phonenumber);
			if (validUniqueData == false) {
				throw new Exception("User PhoneNumber already exists");
			}

			PreparedStatement ps;
			ps = connection.prepareStatement("UPDATE " + Table.users
					+ " SET name = ?, email = ?, phoneNumber = ?, modifiedBy = ?" + " WHERE userName = ?");
			ps.setString(1, name);
			ps.setString(2, email);

			if (phonenumber != null && phonenumber.isEmpty())
				ps.setString(3, null);
			else
				ps.setString(3, phonenumber);

			ps.setString(4, modifiedBy);
			ps.setString(5, username);

			ps.execute();
			ps.close();
			return new ResponseClass(201, "success", "Record has been updated successfully");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "not updated");
		}
	}

	@ApiMethod(apiname = "forgotpassword", method = "POST")
	public ResponseClass forgotPassword(String email, String origin) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnection()) {
			ps = connection.prepareStatement("SELECT sellerId, userName FROM " + Table.users + " WHERE email = ?");
			ps.setString(1, email);
			rs = ps.executeQuery();
			if (!rs.next())
				return new ResponseClass(200, "success", "process initiated--negative");

			int userid = rs.getInt("sellerId");
			String userName = rs.getString("userName");

			User user = new User();
			user.setSellerId(userid);
			user.setUserName(userName);
			user.setEmail(email);

			createNewPasswordLink(user, origin, PswdJwtType.JWT_FP);
			ps.close();
			rs.close();

			return new ResponseClass(200, "success", "process initiated");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "createpassword", method = "POST")
	public ResponseClass createPassword(String newPassword, int userid, String jti) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT * FROM " + Table.userpasswordjti + " WHERE sellerId = ? and jti = ?");
			ps.setInt(1, userid);
			ps.setString(2, jti);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				return new ResponseClass(200, "failure", "Invalid Link");
			}
			PasswordPolicy.validatePassword(newPassword);
			checkRecentPassword(newPassword, userid);

			PreparedStatement ps1;
			ps1 = connection
					.prepareStatement("DELETE FROM " + Table.userpasswordjti + " WHERE sellerId = ? and jti = ?");
			ps1.setInt(1, userid);
			ps1.setString(2, jti);
			ps1.execute();
			ps1.close();

			String cipherPassword = AES.encrypt(newPassword);

			PreparedStatement ps2;
			ps2 = connection.prepareStatement("UPDATE " + Table.users + " SET password = ?" + " WHERE sellerId = ?");
			ps2.setString(1, cipherPassword);
			ps2.setInt(2, userid);
			ps2.executeUpdate();
			ps2.close();

			updateRecentPassword(newPassword, userid);
			return new ResponseClass(200, "success", "password updated");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "checktokenexpiry", method = "GET")
	public ResponseClass checkTokenExpiry(int userid, String jti) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnectionPool()) {
			ps = connection
					.prepareStatement("SELECT * FROM " + Table.userpasswordjti + " WHERE sellerId = ? and jti = ?");
			ps.setInt(1, userid);
			ps.setString(2, jti);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				return new ResponseClass(200, "failure", "Invalid Link");
			}
			ps.close();
			rs.close();
			return new ResponseClass(200, "success", "valid token");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "resendpasswordlink", method = "GET")
	public ResponseClass resendPasswordLink(String token, String origin) throws Exception {
		try (Connection connection = getDBConnection()) {

			String username = null;
			String jti = null;
			Integer userid = null;

			try {
				JWTHandler.parseJWTPassword(token);
			} catch (ExpiredJwtException e) {
				Claims claims = e.getClaims();
				jti = claims.getId();
				username = claims.getSubject();
				userid = getUseridByName(username);
			} catch (Exception e) {
				new ResponseClass(401, "fail", "Invalid Token");
			}

			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT * FROM " + Table.userpasswordjti + " WHERE sellerId = ? and jti = ?");
			ps.setInt(1, userid);
			ps.setString(2, jti);
			rs = ps.executeQuery();
			if (!rs.next()) {
				ps.close();
				rs.close();
				return new ResponseClass(200, "failure", "Invalid Link");
			}

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT email FROM " + Table.users + " WHERE sellerId = ?");
			ps1.setInt(1, userid);
			rs1 = ps1.executeQuery();
			rs1.next();
			String email = rs1.getString("email");
			ps1.close();
			rs1.close();

			User user = new User();
			user.setSellerId(userid);
			user.setUserName(username);
			user.setEmail(email);

			createNewPasswordLink(user, origin, PswdJwtType.JWT_CP);

			return new ResponseClass(200, "success", "Valid token. Mail sent.");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int findStoreidByUserid(int userid) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnectionPool()) {
			ps = connection.prepareStatement("SELECT storeId FROM " + Table.storeusermapper + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();

			if (rs.next()) {
				int storeid = rs.getInt("storeId");
				ps.close();
				rs.close();
				return storeid;
			} else {
				ps.close();
				rs.close();
				throw new Exception("userId not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int getStoreidByName(String storename) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnectionPool()) {
			ps = connection.prepareStatement("SELECT storeId FROM " + Table.stores + " WHERE storeName = ?");
			ps.setString(1, storename);
			rs = ps.executeQuery();

			if (rs.next()) {
				int storeid = rs.getInt("storeId");
				ps.close();
				rs.close();
				return storeid;
			} else {
				ps.close();
				rs.close();
				throw new Exception("StoreName not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String getStoreNameById(int storeid) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnectionPool()) {
			ps = connection.prepareStatement("SELECT storeName FROM " + Table.stores + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			if (rs.next()) {
				String storename = rs.getString("storeName");
				ps.close();
				rs.close();
				return storename;
			} else {
				ps.close();
				rs.close();
				throw new Exception("StoreId not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int getUseridByName(String username) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT sellerId FROM " + Table.users + " WHERE  userName = ?");
			ps.setString(1, username);
			rs = ps.executeQuery();

			if (rs.next()) {
				int userid = rs.getInt("sellerId");
				ps.close();
				rs.close();
				return userid;
			} else {
				ps.close();
				rs.close();
				throw new Exception("UserName not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String getUsernameById(int userid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT userName FROM " + Table.users + " WHERE  sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();

			if (rs.next()) {
				String username = rs.getString("userName");
				ps.close();
				rs.close();
				return username;
			} else {
				ps.close();
				rs.close();
				throw new Exception("UserId not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Address getAddressDetails(String zipcode) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			Address addr = new Address();
			PreparedStatement ps;
			ResultSet rs;
			ps = connection
					.prepareStatement("SELECT cityName, stateCode FROM " + Table.cities + " WHERE zipCode LIKE ?");
			ps.setString(1, zipcode);
			rs = ps.executeQuery();
			if (!rs.next()) {
				addr.setValidZip(false);
				ps.close();
				rs.close();
				return addr;
			}

			addr.setZipCode(zipcode);
			addr.setCityName(rs.getString("cityName"));
			addr.setStateCode(rs.getString("stateCode"));

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection
					.prepareStatement("SELECT stateName, countryCode FROM " + Table.states + " WHERE stateCode = ?");
			ps1.setString(1, addr.getStateCode());
			rs1 = ps1.executeQuery();

			if (!rs1.next()) {
				ps1.close();
				rs1.close();
				return null;
			}
			addr.setStateName(rs1.getString("stateName"));
			addr.setCountryCode(rs1.getString("countryCode"));
			ps1.close();
			rs1.close();

			PreparedStatement ps2;
			ResultSet rs2;
			ps2 = connection.prepareStatement("SELECT countryName FROM " + Table.countries + " WHERE countryCode = ?");
			ps2.setString(1, addr.getCountryCode());
			rs2 = ps2.executeQuery();

			if (!rs2.next()) {
				ps2.close();
				rs2.close();
				return null;
			}

			addr.setCountryName(rs2.getString("countryName"));
			ps2.close();
			rs2.close();
			addr.setValidZip(true);

			return addr;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	// Token function calls
	public void setTokenIssuedAt(Timestamp time, String _username) throws Exception {
		PreparedStatement ps;
		try (Connection connection = getDBConnection()) {
			ps = connection
					.prepareStatement("UPDATE " + Table.users + " SET tokenIssuedAt = ? " + " WHERE userName = ?");
			ps.setTimestamp(1, time);
			ps.setString(2, _username);

			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public boolean checkTokenIssuedAt(Timestamp time, String _username) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnectionPool()) {
			ps = connection.prepareStatement("SELECT tokenIssuedAt FROM " + Table.users + " WHERE  userName = ?");
			ps.setString(1, _username);
			rs = ps.executeQuery();

			if (!rs.next()) {
				ps.close();
				rs.close();
				return false;
			}
			if (rs.getTimestamp("tokenIssuedAt").equals(time)) {
				ps.close();
				rs.close();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return false;
	}

	protected JWT getJWTCreator(UserClaims userClaims, String origin) throws IOException {
		ConfigFileHandler config = new ConfigFileHandler();
		String issConfig = config.get("iss");
		String audConfig = config.get("aud");

		URL issurl = new URL(issConfig);
		URL audurl;

		if (origin == null)
			audurl = new URL(audConfig);
		else
			audurl = new URL(origin);

		String jti = UUID.randomUUID().toString();
		String iss = issurl.getAuthority();
		String aud = audurl.getAuthority();
		String sub = userClaims.getUserName();

		JWT jwt = new JWT(jti, iss, aud, sub, userClaims);
		return jwt;
	}

	public void checkUserActivation(int userid, String origin) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.users + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();

			if (rs.getBoolean("isActivated") == false) {
				PreparedStatement ps1;
				ps1 = connection
						.prepareStatement("UPDATE " + Table.users + " SET isActivated = ?" + " WHERE sellerId = ?");
				ps1.setBoolean(1, true);
				ps1.setInt(2, userid);
				ps1.executeUpdate();
				ps1.close();

				String username = rs.getString("userName");
				String email = rs.getString("email");

				User user = new User();
				user.setSellerId(userid);
				user.setUserName(username);
				user.setEmail(email);

				createNewPasswordLink(user, origin, PswdJwtType.JWT_CP);
			}
			ps.close();
			rs.close();
		} catch (Exception e) {
			throw e;
		}
	}

	public List<Map<String, Object>> getSearchUsers(int storeid, String searchText) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet sellerRS;
			ps = connection.prepareStatement("SELECT sellerId FROM " + Table.storeusermapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			sellerRS = ps.executeQuery();

			List<Integer> allIds = new ArrayList<>();
			while (sellerRS.next())
				allIds.add(sellerRS.getInt("sellerId"));
			ps.close();
			sellerRS.close();

			PreparedStatement ps1;
			ResultSet rs1;

			ps1 = connection.prepareStatement("SELECT * FROM " + Table.users + " WHERE userName LIKE ?"
					+ " OR name LIKE ? OR email LIKE ? OR phoneNumber LIKE ?");
			ps1.setString(1, "%" + searchText + "%");
			ps1.setString(2, "%" + searchText + "%");
			ps1.setString(3, "%" + searchText + "%");
			ps1.setString(4, "%" + searchText + "%");
			rs1 = ps1.executeQuery();

			List<Map<String, Object>> allUsers = new ArrayList<>();
			while (rs1.next()) {
				int id = rs1.getInt("sellerId");
				if (!allIds.contains(id))
					continue;

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection
						.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.assignednames + " WHERE sellerId = ?");
				ps2.setInt(1, id);
				rs2 = ps2.executeQuery();
				rs2.next();
				int rolesCount = rs2.getInt("cnt");
				ps2.close();
				rs2.close();

				Map<String, Object> userDetails = new HashMap<>();
				userDetails.put("name", rs1.getString("name"));
				userDetails.put("userName", rs1.getString("userName"));
				userDetails.put("userType", rs1.getString("userType"));
				userDetails.put("email", rs1.getString("email"));

				String phoneNumber = rs1.getString("phoneNumber");
				if (phoneNumber != null) {
					phoneNumber = phoneNumber.trim();
					if (!phoneNumber.isEmpty())
						userDetails.put("phoneNumber", phoneNumber);
					else
						userDetails.put("phoneNumber", null);
				} else
					userDetails.put("phoneNumber", null);

				userDetails.put("sellerStatus", rs1.getString("sellerStatus"));
				userDetails.put("assignedRolesCount", rolesCount);
				userDetails.put("createdOn", rs1.getTimestamp("createdOn").getTime());
				allUsers.add(userDetails);
			}

			Collections.sort(allUsers, new SortByCreatedOn("createdOn"));
			int i = 1;
			for (Map<String, Object> product : allUsers)
				product.put("list_id", i++);

			return allUsers;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpageusers", method = "POST")
	public List<Map<String, Object>> getPageUsers(int storeid, int startIndex, int load) throws Exception {
		try {
			List<Map<String, Object>> allUserList = getAllStoreUsers(storeid);
			List<Map<String, Object>> userList = new ArrayList<>();

			for (Map<String, Object> users : allUserList) {
				int list_id = (int) users.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					userList.add(users);
			}
			return userList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpagesearchusers", method = "POST")
	public List<Map<String, Object>> getPageSearchUsers(int storeid, int startIndex, int load, String searchText)
			throws Exception {
		try {
			List<Map<String, Object>> allUserList = getSearchUsers(storeid, searchText);
			List<Map<String, Object>> userList = new ArrayList<>();

			for (Map<String, Object> users : allUserList) {
				int list_id = (int) users.get("list_id");
				if (list_id >= startIndex && list_id < startIndex + load)
					userList.add(users);
			}
			return userList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getsearchuserscount", method = "GET")
	public int getSearchUserCount(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet sellerRS;
			List<Integer> allIds = new ArrayList<>();

			ps = connection.prepareStatement("SELECT sellerId FROM " + Table.storeusermapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			sellerRS = ps.executeQuery();
			while (sellerRS.next())
				allIds.add(sellerRS.getInt("sellerId"));
			ps.close();
			sellerRS.close();

			PreparedStatement ps1;
			ResultSet rs1;
			ps1 = connection.prepareStatement("SELECT * FROM " + Table.users + " WHERE userName LIKE ? OR "
					+ "name LIKE ? OR email LIKE ? OR phoneNumber LIKE ? ");
			ps1.setString(1, "%" + searchText + "%");
			ps1.setString(2, "%" + searchText + "%");
			ps1.setString(3, "%" + searchText + "%");
			ps1.setString(4, "%" + searchText + "%");
			rs1 = ps1.executeQuery();

			int count = 0;
			while (rs1.next()) {
				int id = rs1.getInt("sellerId");
				if (allIds.contains(id))
					count++;
			}
			ps1.close();
			rs1.close();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String randomAlphanumeric(int length) {
		final String alphanumericText = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom random = new SecureRandom();

		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(alphanumericText.charAt(random.nextInt(alphanumericText.length())));
		return sb.toString();
	}

	public void createNewPasswordLink(User user, String origin, PswdJwtType jwtType) throws Exception {
		if (!jwtType.equals(PswdJwtType.JWT_CP) && !jwtType.equals(PswdJwtType.JWT_FP))
			throw new Exception("Invalid JWT_TYPE");

		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;

			String jti = UUID.randomUUID().toString();

			ps = connection.prepareStatement("DELETE FROM " + Table.userpasswordjti + " WHERE sellerId = ?");
			ps.setInt(1, user.getSellerId());
			ps.execute();
			ps.close();

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("INSERT INTO " + Table.userpasswordjti + " VALUES (?, ?)");
			ps1.setInt(1, user.getSellerId());
			ps1.setString(2, jti);
			ps1.execute();
			ps1.close();

			JWT jwt = new JWT();
			jwt.setJti(jti);
			jwt.setSub(user.getUserName());
			String token = JWTHandler.buildJWTPassword(jwt, PswdJwtType.JWT_CP);

			if (jwtType.equals(PswdJwtType.JWT_CP)) {
				String passwordUrl = origin + "/createpassword?cp_token=" + token + "&vert=false";
				String receiverEmail = user.getEmail();
				String mailSubject = "Account has been Activated";

				SellerMailService mailService = new SellerMailService();
				mailService.sendmailActivation(receiverEmail, mailSubject, user.getUserName(), passwordUrl);

			} else if (jwtType.equals(PswdJwtType.JWT_FP)) {
				String passwordResetUrl = origin + "/resetpassword?rp_token=" + token + "&vert=false";
				String receiverEmail = user.getEmail();
				String mailSubject = "Reset Password";

				SellerMailService mailService = new SellerMailService();
				mailService.sendmailForgotPassword(receiverEmail, mailSubject, passwordResetUrl);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void checkRecentPassword(String plainPassword, int userid) throws Exception {
		PreparedStatement ps;
		ResultSet rs;
		try (Connection connection = getDBConnectionPool()) {
			ps = connection.prepareStatement(
					"SELECT password1, password2, password3 FROM " + Table.userrecentpassword + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();
			String password1 = AES.decrypt(rs.getString("password1"));
			String password2 = AES.decrypt(rs.getString("password2"));
			String password3 = AES.decrypt(rs.getString("password3"));
			ps.close();
			rs.close();

			if (plainPassword.equals(password1) || plainPassword.equals(password2) || plainPassword.equals(password3))
				throw new Exception("New password cannot be same as last 3 passwords");

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void updateRecentPassword(String plainPassword, int userid) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement(
					"SELECT password1, password2, password3 FROM " + Table.userrecentpassword + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();

			String password1 = rs.getString("password1");
			String password2 = rs.getString("password2");
			String password3 = rs.getString("password3");
			ps.close();
			rs.close();

			password3 = password2;
			password2 = password1;
			password1 = AES.encrypt(plainPassword);

			PreparedStatement ps1;
			ps1 = connection.prepareStatement("UPDATE " + Table.userrecentpassword
					+ " SET password1 = ?, password2 = ?, password3 = ?  WHERE sellerId = ?");
			ps1.setString(1, password1);
			ps1.setString(2, password2);
			ps1.setString(3, password3);
			ps1.setInt(4, userid);
			ps1.executeUpdate();
			ps1.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void loginFailed(int userid, long curr_time) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.userlogindetails + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();

			int failedLoginCount = rs.getInt("failedLoginCount");
			if (failedLoginCount < 3) {
				long lastLoginFailedAt = 0;
				if (rs.getTimestamp("lastLoginFailedAt") != null)
					lastLoginFailedAt = rs.getTimestamp("lastLoginFailedAt").getTime();

				if (failedLoginCount != 0 && curr_time - lastLoginFailedAt > PasswordPolicy.LOCKOUT_RESET_TIME) {
					failedLoginCount = 0;
				}
				failedLoginCount++;

				PreparedStatement ps1;
				if (failedLoginCount == 3) {
					ps1 = connection.prepareStatement("UPDATE " + Table.userlogindetails
							+ " SET failedLoginCount = ?, lastLoginAttemptAt = ?, lastLoginFailedAt = ?, accountLockedAt = ?"
							+ " WHERE sellerId = ?");
					ps1.setInt(1, failedLoginCount);
					ps1.setTimestamp(2, new Timestamp(curr_time));
					ps1.setTimestamp(3, new Timestamp(curr_time));
					ps1.setTimestamp(4, new Timestamp(curr_time));
					ps1.setInt(5, userid);
					ps1.executeUpdate();
					ps.close();
				} else {
					ps1 = connection.prepareStatement("UPDATE " + Table.userlogindetails
							+ " SET failedLoginCount = ?, lastLoginAttemptAt = ?, lastLoginFailedAt = ?"
							+ " WHERE sellerId = ?");
					ps1.setInt(1, failedLoginCount);
					ps1.setTimestamp(2, new Timestamp(curr_time));
					ps1.setTimestamp(3, new Timestamp(curr_time));
					ps1.setInt(4, userid);
					ps1.executeUpdate();
					ps1.close();
				}

			} else
				throw new Exception("Server error. failedLoginCount is already 3");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void loginSucceed(int userid, long curr_time) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ps = connection.prepareStatement("UPDATE " + Table.userlogindetails
					+ " SET failedLoginCount = ?, lastLoginAttemptAt = ?, lastLoginAt = ?" + " WHERE sellerId = ?");
			ps.setInt(1, 0);
			ps.setTimestamp(2, new Timestamp(curr_time));
			ps.setTimestamp(3, new Timestamp(curr_time));
			ps.setInt(4, userid);
			ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public boolean isAccountLocked(int userid, long curr_time) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.userlogindetails + " WHERE sellerId = ?");
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			rs.next();

			int failedLoginCount = rs.getInt("failedLoginCount");
			if (failedLoginCount == 3) {
				long accountLockedAt = 0;
				if (rs.getTimestamp("accountLockedAt") != null) {
					accountLockedAt = rs.getTimestamp("accountLockedAt").getTime();
					ps.close();
					rs.close();
				}
				if (curr_time - accountLockedAt > PasswordPolicy.ACCOUNT_LOCKOUT_TIME) {
					PreparedStatement ps1;
					ps1 = connection.prepareStatement(
							"UPDATE " + Table.userlogindetails + " SET failedLoginCount = ?" + " WHERE sellerId = ?");
					ps1.setInt(1, 0);
					ps1.setInt(2, userid);
					ps1.executeUpdate();
					ps1.close();

					return false; // Account lock time is over, so reset the failedLoginCount.
				} else {
					return true; // Account is still locked.
				}
			} else
				return false; // failedLoginCount<3, So Account will not be locked.
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}

// Will sort (comparing the time object and will sort based on max time first)
class SortByCreatedOn implements Comparator<Map<String, Object>> {

	private String fieldname;

	public SortByCreatedOn(String name) {
		fieldname = name;
	}

	@Override
	public int compare(Map<String, Object> o1, Map<String, Object> o2) {
		long t1 = (long) o1.get(fieldname);
		long t2 = (long) o2.get(fieldname);

		if (t2 > t1)
			return 1;
		else if (t2 == t1)
			return 0;
		else
			return -1;
	}
}