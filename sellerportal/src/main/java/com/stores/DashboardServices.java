package com.stores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vossa.api.general.constants.Table;

public class DashboardServices extends ProductServices {

	public Map<String, Integer> getDashboardDetails(int storeid) throws Exception {
		try (Connection connection = getDBConnection()) {
			PreparedStatement ps = connection
					.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.productmapper + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			ResultSet rs = ps.executeQuery();
			rs.next();

			Map<String, Integer> map = new HashMap<>();
			map.put("totalProducts", rs.getInt("cnt"));

			// ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " +
			// ordersListTable + " WHERE storeName = ?");
			// ps.setInt(1, storeid);
			// rs = ps.executeQuery();
			// rs.next();
			map.put("totalOrders", 0);

			// ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " +
			// ordersListTable + " WHERE storeName = ? AND orderCapturedAt > ?");
			// ps.setInt(1, storeid);
			// ps.setTimestamp(2, new Timestamp(System.currentTimeMillis() - 86400000));
			// rs = ps.executeQuery();
			// rs.next();
			map.put("ordersLastDay", 0);

			// ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " +
			// customersTable + " WHERE storeName = ?");
			// ps.setInt(1, storeid);
			// rs = ps.executeQuery();
			// rs.next();
			map.put("totalCustomers", 0);

			ps.close();
			rs.close();

			return map;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public Map<String, Object> getHomeDashboardDetails(int storeid) throws Exception {
		try {
			Map<String, Object> details = new HashMap<>();
			details.put("best_sellers", getBestSellers(storeid));
			details.put("last_orders", getLastOrders(storeid));
			details.put("recent_orders", getRecentOrders(storeid));
			details.put("recent_returns", getRecentReturns(storeid));
			details.put("sales_overview", getSalesOverview(storeid));

			return details;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Map<String, Object>> getBestSellers(int storeid) throws Exception {

		try (Connection connection = getDBConnectionPool()) {
			int rowLimit = 15;
			int field_id = getFieldidByName("product_name");

			PreparedStatement ps = connection
					.prepareStatement("SELECT T2.value AS product_name, SUM(T1.quantity) AS total_quantity,"
							+ " SUM(T1.quantity * T1.price) AS total_price FROM " + Table.orderlist + " T1 JOIN "
							+ Table.productvaluemaster + " T2 ON T1.product_id = T2.product_id"
							+ " WHERE T1.storeId = ? AND T2.field_id = ?"
							+ " GROUP BY T1.product_id ORDER BY total_quantity DESC LIMIT ?");

			ps.setInt(1, storeid);
			ps.setInt(2, field_id);
			ps.setInt(3, rowLimit);
			ResultSet rs = ps.executeQuery();

			int list_id = 1;
			List<Map<String, Object>> detailsList = new ArrayList<>();
			while (rs.next()) {
				Map<String, Object> details = new HashMap<>();
				details.put("list_id", list_id);
				details.put("product_name", rs.getString("product_name"));
				details.put("total_quantity", rs.getInt("total_quantity"));
				details.put("total_price", rs.getInt("total_price"));
				detailsList.add(details);

				list_id++;
			}
			ps.close();
			rs.close();

			return detailsList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Map<String, Object>> getLastOrders(int storeid) throws Exception {

		try (Connection connection = getDBConnectionPool()) {
			int rowLimit = 15;

			PreparedStatement ps = connection.prepareStatement(
					"SELECT T1.orderId, T3.customerName, SUM(T2.quantity) AS itemCount, T1.orderTotal FROM "
							+ Table.orders + " T1 JOIN " + Table.orderlist + " T2 ON T1.orderId = T2.orderId JOIN "
							+ Table.customers + " T3 ON T1.orderId = T3.orderId"
							+ " WHERE T1.storeId = ? GROUP BY T1.createdOn DESC, T2.orderId LIMIT ?");
			ps.setInt(1, storeid);
			ps.setInt(2, rowLimit);
			ResultSet rs = ps.executeQuery();

			int list_id = 1;
			List<Map<String, Object>> lastOrdersList = new ArrayList<>();
			while (rs.next()) {
				Map<String, Object> order = new HashMap<String, Object>();
				order.put("list_id", list_id);
				order.put("orderId", rs.getString("orderId"));
				order.put("customerName", rs.getString("customerName"));
				order.put("itemCount", rs.getInt("itemCount"));
				order.put("orderTotal", rs.getInt("orderTotal"));
				lastOrdersList.add(order);

				list_id++;

			}
			ps.close();
			rs.close();
			return lastOrdersList;
		}
	}

	public Map<String, Object> getRecentOrders(int storeid) throws Exception {

		Calendar lastMonth = Calendar.getInstance();
		lastMonth.add(Calendar.MONTH, -1); // -1 means previous month
		long lastmonth = lastMonth.getTimeInMillis();

		try (Connection connection = getDBConnectionPool()) {
			Map<String, Object> orderDetails = new HashMap<>();
			orderDetails.put("recentordercount", 0);
			orderDetails.put("Processing", 0);
			orderDetails.put("Partially Shipped", 0);
			orderDetails.put("Shipped", 0);
			orderDetails.put("Delivered", 0);
			orderDetails.put("Cancelled", 0);

			PreparedStatement ps = connection.prepareStatement(
					"SELECT COUNT(*) AS ordercount FROM " + Table.orders + " WHERE storeId = ? AND createdOn > ?");

			ps.setInt(1, storeid);
			ps.setTimestamp(2, new Timestamp(lastmonth));
			ResultSet rs = ps.executeQuery();
			rs.next();

			int ordercount = rs.getInt("ordercount");
			orderDetails.put("recentordercount", ordercount);

			ps.close();
			rs.close();

			PreparedStatement ps1 = connection.prepareStatement("SELECT orderStatus, COUNT(*) AS count FROM "
					+ Table.orders + " WHERE storeId = ? AND createdOn > ? GROUP BY orderStatus");

			ps1.setInt(1, storeid);
			ps1.setTimestamp(2, new Timestamp(lastmonth));
			ResultSet rs1 = ps1.executeQuery();
			while (rs1.next()) {
				orderDetails.put(rs1.getString("orderStatus"), rs1.getInt("count"));

			}
			ps1.close();
			rs1.close();

			return orderDetails;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Map<String, Object> getRecentReturns(int storeid) throws Exception {
		Calendar lastMonth = Calendar.getInstance();
		lastMonth.add(Calendar.MONTH, -1); // -1 means previous month
		long lastmonth = lastMonth.getTimeInMillis();

		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) AS returncount FROM "
					+ Table.orderreturn + " WHERE storeId = ? AND returnRequestedOn > ? ");

			ps.setInt(1, storeid);
			ps.setTimestamp(2, new Timestamp(lastmonth));
			ResultSet rs = ps.executeQuery();
			rs.next();
			Map<String, Object> orderReturn = new HashMap<>();
			orderReturn.put("recentreturncount", rs.getInt("returncount"));

			ps.close();
			rs.close();

			return orderReturn;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Map<String, Object> getSalesOverview(int storeid) throws Exception {
		PreparedStatement ps;
		ResultSet rs;

		try (Connection connection = getDBConnectionPool()) {

			ps = connection.prepareStatement("SELECT COUNT(*) AS totalordercount, SUM(orderTotal) as totalsales, "
					+ "ROUND(AVG(orderTotal),2) as averagesales FROM " + Table.orders + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			rs.next();

			Map<String, Object> salesOverview = new HashMap<>();
			int totalordercount = rs.getInt("totalordercount");
			float totalsales = rs.getFloat("totalsales");
			float averagesales = rs.getFloat("averagesales");

			salesOverview.put("totalordercount", totalordercount);
			salesOverview.put("totalsales", totalsales);
			salesOverview.put("averagesales", averagesales);

			ps.close();
			rs.close();

			PreparedStatement ps1 = connection.prepareStatement(
					"SELECT SUM(quantity) AS totalsalesquantity FROM " + Table.orderlist + " WHERE storeId = ?");

			ps1.setInt(1, storeid);
			ResultSet rs1 = ps1.executeQuery();
			rs1.next();

			salesOverview.put("totalsalesquantity", rs1.getInt("totalsalesquantity"));

			ps1.close();
			rs1.close();

			return salesOverview;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
