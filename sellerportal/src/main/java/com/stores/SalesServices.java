package com.stores;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SalesServices extends StoreServices {

	public List<Map<String, Object>> getSalesLastOrder(int storeid) throws Exception {
//		PreparedStatement ps;
//		ResultSet rs;
		try (Connection connection = getDBConnection()) {
//			ps = connection.prepareStatement( "SELECT * FROM " + ordersListTable +
//					" WHERE storeName = ? " +
//					" ORDER BY orderCapturedAt DESC LIMIT 3");
//			ps.setString(1, _storename);
//			rs = ps.executeQuery();

			List<Map<String, Object>> orders = new ArrayList<Map<String, Object>>();
			Map<String, Object> map;
			int i = 1;
			while (i <= 3) {
				map = new HashMap<>();
				map.put("orderID", i);
				map.put("customerName", "user" + i);
				map.put("count", i * 10);
				orders.add(map);
				i++;
			}

			return orders;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
