package com.stores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.vossa.api.general.annotations.ApiMethod;
import com.vossa.api.general.constants.Table;
import com.vossa.api.v1.seller.pojos.Order;
import com.vossa.api.v1.seller.pojos.OrderCustomer;
import com.vossa.api.v1.seller.pojos.OrderList;
import com.vossa.api.v1.seller.pojos.OrderReturn;

public class OrderServices extends ProductServices {

	@ApiMethod(apiname = "getallorders", method = "GET")
	public List<Order> getAllOrders(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {

			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT * FROM " + Table.orders + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			List<Order> orderList = new ArrayList<Order>();
			while (rs.next()) {
				Order order = new Order();
				String orderId = rs.getString("orderId");
				order.setOrderId(orderId);
				order.setStoreId(storeid);
				order.setOrderStatus(rs.getString("orderStatus"));
				order.setSource(rs.getString("source"));
				order.setSourceCode(rs.getString("sourceCode"));
				order.setOrderSubtotal(rs.getFloat("orderSubtotal"));
				order.setOrderTax(rs.getFloat("orderTax"));
				order.setOrderShippingCharges(rs.getFloat("orderShippingCharges"));
				order.setOrderTotal(rs.getFloat("orderTotal"));
				order.setCreatedOn(rs.getTimestamp("createdOn").getTime());
				order.setLastStatusUpdate(rs.getTimestamp("lastStatusUpdate").getTime());

				PreparedStatement ps1;
				ResultSet rs1;
				ps1 = connection
						.prepareStatement("SELECT * FROM " + Table.customers + " WHERE orderId = ? AND storeId = ?");
				ps1.setString(1, orderId);
				ps1.setInt(2, storeid);
				rs1 = ps1.executeQuery();
				rs1.next();

				OrderCustomer customer = new OrderCustomer();
				customer.setCustomerId(rs1.getInt("customerId"));
				customer.setCustomerName(rs1.getString("customerName"));
				customer.setEmail(rs1.getString("email"));
				customer.setPhoneNumber(rs1.getString("phoneNumber"));
				customer.setAddress1(rs1.getString("address1"));
				customer.setAddress2(rs1.getString("address2"));
				customer.setAddress3(rs1.getString("address3"));
				customer.setCity(rs1.getString("city"));
				customer.setState(rs1.getString("state"));
				customer.setCountry(rs1.getString("country"));
				customer.setZipCode(rs1.getString("zipCode"));
				order.setCustomer(customer);

				PreparedStatement ps2;
				ResultSet rs2;
				ps2 = connection
						.prepareStatement("SELECT * FROM " + Table.orderlist + " WHERE orderId = ? AND storeId = ?");
				ps2.setString(1, orderId);
				ps2.setInt(2, storeid);
				rs2 = ps2.executeQuery();

				List<OrderList> orderItemList = new ArrayList<OrderList>();
				while (rs2.next()) {

					int product_id = rs2.getInt("product_id");
					Map<String, Object> product = getProduct(product_id);

					OrderList item = new OrderList();
					item.setSku((String) product.get("sku"));
					item.setImage((String) product.get("main_image"));
					item.setPrice(rs2.getFloat("price"));
					item.setProduct_name((String) product.get("product_name"));
					item.setQuantity(rs2.getInt("quantity"));
					orderItemList.add(item);
				}
				ps2.close();
				rs2.close();
				order.setOrderList(orderItemList);
				orderList.add(order);
				ps1.close();
				rs1.close();
			}
			ps.close();
			rs.close();
			Collections.sort(orderList, new SortByTimeOrders());
			int i = 1;
			for (Order order : orderList)
				order.setList_id(i++);

			return orderList;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpageorders", method = "POST")
	public List<Order> getPageOrders(int storeid, int startIndex, int load) throws Exception {
		try {
			List<Order> allOrderList = getAllOrders(storeid);
			List<Order> orderList = new ArrayList<>();

			for (Order order : allOrderList) {
				int list_id = (int) order.getList_id();
				if (list_id >= startIndex && list_id < startIndex + load)
					orderList.add(order);
			}
			return orderList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getordercount", method = "GET")
	public int getOrderCount(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.orders + " WHERE storeID = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			rs.next();
			int count = rs.getInt("cnt");

			ps.close();
			rs.close();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getallreturns", method = "GET")
	public List<OrderReturn> getAllReturns(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;
			ps = connection.prepareStatement("SELECT * FROM " + Table.orderreturn + " WHERE storeId = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();

			List<OrderReturn> returnList = new ArrayList<>();
			while (rs.next()) {
				OrderReturn orderReturn = new OrderReturn();
				String returnId = rs.getString("orderReturnId");
				String orderId = rs.getString("orderId");
				orderReturn.setOrderReturnId(returnId);
				orderReturn.setOrderId(orderId);
				orderReturn.setStoreId(storeid);

				PreparedStatement ps1;
				ResultSet rs1;

				ps1 = connection
						.prepareStatement("SELECT * FROM " + Table.customers + " WHERE orderId = ? AND storeId = ?");
				ps1.setString(1, orderId);
				ps1.setInt(2, storeid);
				rs1 = ps1.executeQuery();
				rs1.next();
				orderReturn.setCustomerName(rs1.getString("customerName"));

				ps1.close();
				rs1.close();

				int product_id = rs.getInt("product_id");
				Map<String, Object> product = getProduct(product_id);
				orderReturn.setProductId(product_id);
				orderReturn.setSku((String) product.get("sku"));
				orderReturn.setProduct_name((String) product.get("product_name"));
				orderReturn.setProduct_category((String) product.get("product_category"));
				orderReturn.setSub_category((String) product.get("sub_category"));

				PreparedStatement ps2;
				ResultSet rs2;

				ps2 = connection.prepareStatement(
						"SELECT * FROM " + Table.orderlist + " WHERE orderId = ? AND storeId = ? AND product_id = ?");
				ps2.setString(1, orderId);
				ps2.setInt(2, storeid);
				ps2.setInt(3, product_id);
				rs2 = ps2.executeQuery();
				rs2.next();
				orderReturn.setOrderedQuantity(rs2.getInt("quantity"));
				ps2.close();
				rs2.close();
				
				orderReturn.setReturnedQuantity(rs.getInt("returnedQuantity"));
				orderReturn.setReturnStatus(rs.getString("returnStatus"));
				orderReturn.setRefundAmount(rs.getFloat("refundAmount"));

				PreparedStatement ps3;
				ResultSet rs3;

				ps3 = connection
						.prepareStatement("SELECT * FROM " + Table.orders + " WHERE orderId = ? AND storeId = ?");
				ps3.setString(1, orderId);
				ps3.setInt(2, storeid);
				rs3 = ps3.executeQuery();
				rs3.next();
				orderReturn.setPurchasedOn(rs3.getTimestamp("createdOn").getTime());
				ps3.close();
				rs3.close();
				
				orderReturn.setReturnRequestedOn(rs.getTimestamp("returnRequestedOn").getTime());
				orderReturn.setReasonForReturn(rs.getString("reasonForReturn"));
				orderReturn.setAdditionalComments(rs.getString("additionalComments"));

				returnList.add(orderReturn);

				

			}

			ps.close();
			rs.close();

			Collections.sort(returnList, new SortByTimeReturns());
			int i = 1;
			for (OrderReturn orderReturn : returnList)
				orderReturn.setList_id(i++);

			return returnList;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpagereturns", method = "POST")
	public List<OrderReturn> getPageReturns(int storeid, int startIndex, int load) throws Exception {
		try {
			List<OrderReturn> allReturnList = getAllReturns(storeid);
			List<OrderReturn> returnList = new ArrayList<>();

			for (OrderReturn orderReturn : allReturnList) {
				int list_id = (int) orderReturn.getList_id();
				if (list_id >= startIndex && list_id < startIndex + load)
					returnList.add(orderReturn);
			}
			return returnList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getreturncount", method = "GET")
	public int getReturnCount(int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.orderreturn + " WHERE storeID = ?");
			ps.setInt(1, storeid);
			rs = ps.executeQuery();
			rs.next();
			int count = rs.getInt("cnt");

			ps.close();
			rs.close();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpagesearchorders", method = "POST")
	public Object getPageSearchOrders(int storeid, String searchText, int startIndex, int load) throws Exception {
		try {
			List<Order> allOrderList = getSearchOrders(searchText, storeid);
			List<Order> orderList = new ArrayList<>();

			for (Order order : allOrderList) {
				int list_id = (int) order.getList_id();
				if (list_id >= startIndex && list_id < startIndex + load)
					orderList.add(order);
			}
			return orderList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getsearchordercount", method = "POST")
	public int getSearchOrderCount(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT COUNT(*) AS cnt FROM " + Table.orders + " T1 JOIN "
					+ Table.customers + " T2 ON T1.orderId = T2.orderId"
					+ " WHERE T1.storeId = ? AND (T1.orderId LIKE ? OR T1.source LIKE ? OR T2.customerName LIKE ?)");
			ps.setInt(1, storeid);
			ps.setString(2, "%" + searchText + "%");
			ps.setString(3, "%" + searchText + "%");
			ps.setString(4, "%" + searchText + "%");
			rs = ps.executeQuery();
			rs.next();
			int count = rs.getInt("cnt");

			ps.close();
			rs.close();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Order> getSearchOrders(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {

			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement("SELECT * FROM " + Table.orders + " T1 JOIN " + Table.customers
					+ " T2 ON T1.orderId = T2.orderId"
					+ " WHERE T1.storeId = ? AND (T1.orderId LIKE ? OR T1.source LIKE ? OR T2.customerName LIKE ?)");

			ps.setInt(1, storeid);
			ps.setString(2, "%" + searchText + "%");
			ps.setString(3, "%" + searchText + "%");
			ps.setString(4, "%" + searchText + "%");
			rs = ps.executeQuery();

			List<Order> orderList = new ArrayList<Order>();
			while (rs.next()) {
				Order order = new Order();
				String orderId = rs.getString("orderId");
				order.setOrderId(orderId);
				order.setStoreId(storeid);
				order.setOrderStatus(rs.getString("orderStatus"));
				order.setSource(rs.getString("source"));
				order.setSourceCode(rs.getString("sourceCode"));
				order.setOrderSubtotal(rs.getFloat("orderSubtotal"));
				order.setOrderTax(rs.getFloat("orderTax"));
				order.setOrderShippingCharges(rs.getFloat("orderShippingCharges"));
				order.setOrderTotal(rs.getFloat("orderTotal"));
				order.setCreatedOn(rs.getTimestamp("createdOn").getTime());
				order.setLastStatusUpdate(rs.getTimestamp("lastStatusUpdate").getTime());

				PreparedStatement ps1;
				ResultSet rs1;

				ps1 = connection
						.prepareStatement("SELECT * FROM " + Table.customers + " WHERE orderId = ? AND storeId = ?");
				ps1.setString(1, orderId);
				ps1.setInt(2, storeid);
				rs1 = ps1.executeQuery();
				rs1.next();

				OrderCustomer customer = new OrderCustomer();
				customer.setCustomerId(rs1.getInt("customerId"));
				customer.setCustomerName(rs1.getString("customerName"));
				customer.setEmail(rs1.getString("email"));
				customer.setPhoneNumber(rs1.getString("phoneNumber"));
				customer.setAddress1(rs1.getString("address1"));
				customer.setAddress2(rs1.getString("address2"));
				customer.setAddress3(rs1.getString("address3"));
				customer.setCity(rs1.getString("city"));
				customer.setState(rs1.getString("state"));
				customer.setCountry(rs1.getString("country"));
				customer.setZipCode(rs1.getString("zipCode"));
				order.setCustomer(customer);

				PreparedStatement ps2;
				ResultSet rs2;

				ps2 = connection
						.prepareStatement("SELECT * FROM " + Table.orderlist + " WHERE orderId = ? AND storeId = ?");
				ps2.setString(1, orderId);
				ps2.setInt(2, storeid);
				rs2 = ps2.executeQuery();

				List<OrderList> orderItemList = new ArrayList<OrderList>();
				while (rs2.next()) {
					int product_id = rs2.getInt("product_id");
					Map<String, Object> product = getProduct(product_id);

					OrderList item = new OrderList();
					item.setSku((String) product.get("sku"));
					item.setImage((String) product.get("main_image"));
					item.setPrice(rs2.getFloat("price"));
					item.setProduct_name((String) product.get("product_name"));
					item.setQuantity(rs2.getInt("quantity"));
					orderItemList.add(item);
				}

				ps2.close();
				rs2.close();

				ps1.close();
				rs1.close();

				order.setOrderList(orderItemList);
				orderList.add(order);
			}

			ps.close();
			rs.close();

			Collections.sort(orderList, new SortByTimeOrders());
			int i = 1;
			for (Order order : orderList)
				order.setList_id(i++);

			return orderList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getpagesearchreturns", method = "POST")
	public List<OrderReturn> getPageSearchReturns(int storeid, String searchText, int startIndex, int load)
			throws Exception {
		try {
			List<OrderReturn> allReturnList = getSearchReturns(searchText, storeid);
			List<OrderReturn> returnList = new ArrayList<>();

			for (OrderReturn orderReturn : allReturnList) {
				int list_id = (int) orderReturn.getList_id();
				if (list_id >= startIndex && list_id < startIndex + load)
					returnList.add(orderReturn);
			}
			return returnList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ApiMethod(apiname = "getsearchreturncount", method = "POST")
	public int getSearchReturnCount(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {
			PreparedStatement ps;
			ResultSet rs;

			ps = connection.prepareStatement(
					"SELECT COUNT(*) AS cnt FROM " + Table.orderreturn + " T1 JOIN " + Table.productvaluemaster
							+ " T2 ON T1.product_id = T2.product_id" + " WHERE T1.storeId = ? AND T2.field_id = ? AND"
							+ " (T1.orderId LIKE ? OR T1.orderReturnId LIKE ? OR T2.value LIKE ?)");
			ps.setInt(1, storeid);
			ps.setInt(2, getFieldidByName("product_name"));
			ps.setString(3, "%" + searchText + "%");
			ps.setString(4, "%" + searchText + "%");
			ps.setString(5, "%" + searchText + "%");
			rs = ps.executeQuery();
			rs.next();
			int count = rs.getInt("cnt");

			ps.close();
			rs.close();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<OrderReturn> getSearchReturns(String searchText, int storeid) throws Exception {
		try (Connection connection = getDBConnectionPool()) {

			PreparedStatement ps;
			ResultSet rs;

			ps = connection
					.prepareStatement("SELECT * FROM " + Table.orderreturn + " T1 JOIN " + Table.productvaluemaster
							+ " T2 ON T1.product_id = T2.product_id" + " WHERE T1.storeId = ? AND T2.field_id = ? AND"
							+ " (T1.orderId LIKE ? OR T1.orderReturnId LIKE ? OR T2.value LIKE ?)");
			ps.setInt(1, storeid);
			ps.setInt(2, getFieldidByName("product_name"));
			ps.setString(3, "%" + searchText + "%");
			ps.setString(4, "%" + searchText + "%");
			ps.setString(5, "%" + searchText + "%");
			rs = ps.executeQuery();

			List<OrderReturn> returnList = new ArrayList<>();
			while (rs.next()) {
				String returnId = rs.getString("orderReturnId");
				String orderId = rs.getString("orderId");
				int product_id = rs.getInt("product_id");
				Map<String, Object> product = getProduct(product_id);

				OrderReturn orderReturn = new OrderReturn();
				orderReturn.setOrderReturnId(returnId);
				orderReturn.setOrderId(orderId);
				orderReturn.setStoreId(storeid);

				PreparedStatement ps1;
				ResultSet rs1;

				ps1 = connection
						.prepareStatement("SELECT * FROM " + Table.customers + " WHERE orderId = ? AND storeId = ?");
				ps1.setString(1, orderId);
				ps1.setInt(2, storeid);
				rs1 = ps1.executeQuery();
				rs1.next();
				orderReturn.setCustomerName(rs1.getString("customerName"));

				orderReturn.setProductId(product_id);
				orderReturn.setSku((String) product.get("sku"));
				orderReturn.setProduct_name((String) product.get("product_name"));
				orderReturn.setProduct_category((String) product.get("product_category"));
				orderReturn.setSub_category((String) product.get("sub_category"));

				ps1.close();
				rs1.close();

				PreparedStatement ps2;
				ResultSet rs2;

				ps2 = connection.prepareStatement(
						"SELECT * FROM " + Table.orderlist + " WHERE orderId = ? AND storeId = ? AND product_id = ?");
				ps2.setString(1, orderId);
				ps2.setInt(2, storeid);
				ps2.setInt(3, product_id);
				rs2 = ps2.executeQuery();
				rs2.next();
				orderReturn.setOrderedQuantity(rs2.getInt("quantity"));

				orderReturn.setReturnedQuantity(rs.getInt("returnedQuantity"));
				orderReturn.setReturnStatus(rs.getString("returnStatus"));
				orderReturn.setRefundAmount(rs.getFloat("refundAmount"));

				ps2.close();
				rs2.close();

				PreparedStatement ps3;
				ResultSet rs3;

				ps3 = connection
						.prepareStatement("SELECT * FROM " + Table.orders + " WHERE orderId = ? AND storeId = ?");
				ps3.setString(1, orderId);
				ps3.setInt(2, storeid);
				rs3 = ps3.executeQuery();
				rs3.next();

				orderReturn.setPurchasedOn(rs3.getTimestamp("createdOn").getTime());
				orderReturn.setReturnRequestedOn(rs.getTimestamp("returnRequestedOn").getTime());
				orderReturn.setReasonForReturn(rs.getString("reasonForReturn"));
				orderReturn.setAdditionalComments(rs.getString("additionalComments"));

				returnList.add(orderReturn);
			}

			ps.close();
			rs.close();

			Collections.sort(returnList, new SortByTimeReturns());
			int i = 1;
			for (OrderReturn orderReturn : returnList)
				orderReturn.setList_id(i++);

			return returnList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}

// Will sort (comparing the time object and will sort based on max time first)
class SortByTimeOrders implements Comparator<Order> {

	@Override
	public int compare(Order o1, Order o2) {
		long t1 = o1.getLastStatusUpdate();
		long t2 = o2.getLastStatusUpdate();

		if (t2 > t1)
			return 1;
		else if (t2 == t1)
			return 0;
		else
			return -1;
	}
}

// Will sort (comparing the time object and will sort based on max time first)
class SortByTimeReturns implements Comparator<OrderReturn> {

	@Override
	public int compare(OrderReturn o1, OrderReturn o2) {
		long t1 = o1.getReturnRequestedOn();
		long t2 = o2.getReturnRequestedOn();

		if (t2 > t1)
			return 1;
		else if (t2 == t1)
			return 0;
		else
			return -1;
	}
}
